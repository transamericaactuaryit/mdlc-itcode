﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace MetaConfigConverter
{
    public partial class Form1 : Form
    {
        string _sourceDirectory = "";
        string _destinationDirectory = "";
        StringBuilder _sbSuccess = new StringBuilder();
        StringBuilder _sbFail = new StringBuilder();
        private int _jobInstanceID = 99;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            textBox1.Text = "";

                if ((!String.IsNullOrEmpty(_sourceDirectory)) &&
                    (!String.IsNullOrEmpty(_destinationDirectory) &&
                    (_sourceDirectory != _destinationDirectory))
                    )
            {
                ParseConfigFiles();
                MessageBox.Show("Processing Completed.");
            }
            else
            {
                MessageBox.Show("Source and Destination directories must be selected.");
            }
            textBox1.Text = _sbFail.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                _sourceDirectory = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog2.ShowDialog();
            if (result == DialogResult.OK)
            {
                _destinationDirectory = folderBrowserDialog2.SelectedPath;
            }


        }

        private void ParseConfigFiles()
        {

                string[] fileEntries = Directory.GetFiles(_sourceDirectory, "*Pre.xml");
            foreach (string fileName in fileEntries)
            {
                if (!PostFileExists(fileName))
                {
                    _sbFail.Append(fileName + "\tNo corresponding 'post' file.");
                }
                else
                {
                    // XElement xelement = XElement.Load(fileName);
                    try
                    {
                        XDocument thisDoc = XDocument.Load(fileName);
                        string postTriggerFilePath;
                        bool postTriggerFileFound = FindPostTriggerFilePath(thisDoc, out postTriggerFilePath);

                        if (postTriggerFileFound)
                        {
                            string modelName = FindModelName(thisDoc);
                            string jobHistoryConnection = FindHistoryConnection(thisDoc);
                            AlterMGAlfaTrigger(thisDoc, postTriggerFilePath, modelName, jobHistoryConnection);

                            thisDoc.Root.Add(AppendTimedFileCheckerToEnd(thisDoc, postTriggerFilePath, modelName, jobHistoryConnection, "600"));
                            AppendPostFileItems(thisDoc, fileName.Replace("Pre.xml", "Post.xml"));
                            UpdateDriveMappings(thisDoc);
                            thisDoc.Save(_destinationDirectory + @"\" + Path.GetFileName(fileName).Replace("Pre.xml", ".xml"));
                        }
                        else
                        {
                            _sbFail.Append(fileName + "\tCould not find post processing file path.");
                        }

                        //xelement.Save(_destinationDirectory + fileName.Replace("Pre.xml", ".xml");
                    }
                    catch (Exception eX)
                    {
                        _sbFail.Append(fileName + "\t" + eX.Message + "\r\n");
                    }

                }
            }

        }

        private void UpdateDriveMappings(XDocument thisDoc)
        {
            IEnumerable<XElement> configs;

            configs = from configSettings in thisDoc.Elements()
                          select configSettings;

            //Top Level
            foreach (var thisElement in configs)
            {
                //Process level
                foreach (var processElement in thisElement.Elements())
                {
                    //Automation Job type level
                    foreach (var subElement in processElement.Elements())
                    {
                        ReplaceMappings(subElement);
                    }

                    //XAttribute processNameAttribute = new XAttribute("ProcessWorkflow", "PostModelRun");
                    //subElement.Add(processNameAttribute);
                }
               // thisDoc.Root.Add(thisElement);
            }
        }

        private void ReplaceMappings(XElement subElement)
        {
            string currentMapping = txtCurrentDriveMapping.Text;

            List<XElement> elementsToRemove = new List<XElement>();

            foreach (var keyValue in subElement.Elements())
            {
                if (keyValue.Attribute("value") != null)
                {
                    string keyValueUpper = keyValue.Attribute("value").Value.ToString().ToUpper();
                    string keyValueStr = keyValue.Attribute("value").Value.ToString();

                    if (keyValueUpper.Contains(currentMapping.ToUpper()))
                    {
                        elementsToRemove.Add(keyValue);

                        if (!string.IsNullOrEmpty(txtDevDriveMapping.Text))
                        {
                            AddEnvironmentMapping(subElement, "DEV", keyValue.Attribute("key").Value.ToString(), keyValueStr, txtCurrentDriveMapping.Text, (txtDevDriveMapping.Text));
                        }

                        if (!string.IsNullOrEmpty(txtTestDriveMapping.Text))
                        {
                            AddEnvironmentMapping(subElement, "TEST", keyValue.Attribute("key").Value.ToString(), keyValueStr, txtCurrentDriveMapping.Text, (txtTestDriveMapping.Text));
                        }

                        if (!string.IsNullOrEmpty(txtIntegrationDriveMapping.Text))
                        {
                            AddEnvironmentMapping(subElement, "INT", keyValue.Attribute("key").Value.ToString(), keyValueStr, txtCurrentDriveMapping.Text, (txtIntegrationDriveMapping.Text));
                        }


                        if (!string.IsNullOrEmpty(txtProductionDriveMapping.Text))
                        {
                            AddEnvironmentMapping(subElement, "PROD", keyValue.Attribute("key").Value.ToString(), keyValueStr, txtCurrentDriveMapping.Text, (txtProductionDriveMapping.Text));
                        }
                        //XElement newElement = new XElement("add");
                        //XAttribute newKey = new XAttribute("key", environment + "_" + keyValue.Attribute("key").Value.ToString());
                        //string newString = ReplaceEx(configValue, currentMapping, newMapping);
                        //XAttribute newValue = new XAttribute("value", newString);
                        //newElement.Add(newKey);
                        //newElement.Add(newValue);
                        //subElement.Add(newElement);
                        //TODO: remov old
                    }
                }
            }

            foreach(var thisElement in elementsToRemove)
            {
                thisElement.Remove();
            }
        }

        private void AddEnvironmentMapping(XElement subElement, string environment, string keyName, string keyValue, string oldValue, string newValue)
        {
            XElement newElement = new XElement("add");
            XAttribute newKey = new XAttribute("key", environment + "_" + keyName);
            string newString = ReplaceEx(keyValue, oldValue, newValue);
            XAttribute newValueAttrib = new XAttribute("value", newString);
            newElement.Add(newKey);
            newElement.Add(newValueAttrib);
            subElement.Add(newElement);
        }

        private XDocument AppendPostFileItems(XDocument thisDoc, string postTriggerFilePath)
        {
            XElement xelementPost = XElement.Load(postTriggerFilePath);
            IEnumerable<XElement> postConfigs;

            postConfigs = from configSettings in xelementPost.Elements()
                          select configSettings;

            //Alfa level
            foreach (var thisElement in postConfigs)
            {
                if(!thisElement.Name.ToString().ToLower().Contains("alfa"))
                {
                    //foreach (var processElement in thisElement.Elements())
                    //{
                        foreach (var subElement in thisElement.Elements())
                        {
                            XAttribute processNameAttribute = new XAttribute("ProcessWorkflow", "PostModelRun");
                            subElement.Add(processNameAttribute);
                        }
                    //}

                    thisDoc.Root.Add(thisElement);
                }
                else { MessageBox.Show("Name is: " + thisElement.Name.ToString()); }

            }
            return thisDoc;
        }

        private bool FindPostTriggerFilePath(XDocument thisDoc, out string postFilePath)
        {

            //IEnumerable<XElement> list =
            //    thisDoc.Elements("SetAlfaProcessTrigger");
            var root = thisDoc.Root;
            IEnumerable<XElement> list = root.XPathSelectElements("/*/*/SetAlfaProcessTrigger");
            var thisElement = list.First();
            postFilePath = "";

            if (thisElement != null)
            {
                var keys = from el in thisElement.Elements()
                           where (string)el.Attribute("key") == "PostProcessingTriggerFile"
                           select el;
                if(keys.Count() > 0)
                {
                    postFilePath = keys.First().Attribute("value").ToString().Replace("value=", "").Replace("\"", "");
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            else
            {
                return false;
            }
        }

        private string FindHistoryConnection(XDocument thisDoc)
        {
            var root = thisDoc.Root;
            IEnumerable<XElement> list = root.XPathSelectElements("/*/*/SetAlfaProcessTrigger");
            var thisElement = list.First();
            string tempConnectionName = "";

            if (thisElement != null)
            {
                var keys = from el in thisElement.Elements()
                           where (string)el.Attribute("key") == "JobHistoryConnection"
                           select el;
                if (keys.Count() > 0)
                {
                    tempConnectionName = keys.First().Attribute("value").ToString().Replace("value=", "").Replace("\"", "");
                    tempConnectionName = Path.GetDirectoryName(tempConnectionName);
                    tempConnectionName += @"\PostProcessingTriggerCheck.txt";
                }
            }

            return tempConnectionName;
        }

        //This changes Mgalfaexternalprocess to SetAlfaProcessTrigger
        private string AlterMGAlfaTrigger(XDocument thisDoc, string postProcessingFileName, string modelName, string jobHistoryConnection)
        {
            var root = thisDoc.Root;
            IEnumerable<XElement> list = root.XPathSelectElements("/*/*/Mgalfaexternalprocess");
            string postFileTrigger = "";
            bool postTrigFound = false;

            jobHistoryConnection = jobHistoryConnection.Replace(".txt", "_ScenarioImport.txt");

            foreach (var thisElement in list)
            {
                thisElement.Name = "SetAlfaProcessTrigger";


                var keys = from el in thisElement.Elements()
                           where (string)el.Attribute("key") == "PostProcessingTriggerFile"
                           select el;

                if (keys.Count() > 0)
                {
                    string postFilePath = keys.First().Attribute("value").ToString().Replace("value=", "").Replace("\"", "");
                    postFilePath = Path.GetDirectoryName(postFilePath);
                    postFileTrigger = postFilePath + @"\" + modelName + "_scenario.txt";
                    keys.First().Attribute("value").Value = postFileTrigger;
                    postTrigFound = true;
                }
                else
                {
                    postTrigFound = false;
                }

                //Timed File check is wrapped in a <process>, so we go to the parent
                thisElement.Parent.AddAfterSelf(AppendTimedFileCheckerToEnd(thisDoc, postFileTrigger, modelName, jobHistoryConnection, "20"));
            }
                
            return postFileTrigger;
        }
        private string FindModelName(XDocument thisDoc)
        {
            var root = thisDoc.Root;
            IEnumerable<XElement> list = root.XPathSelectElements("/*/*/SetAlfaProcessTrigger");
            var thisElement = list.First();
            string tempModelName = "";
            string modelName = "";

            if (thisElement != null)
            {
                var keys = from el in thisElement.Elements()
                           where (string)el.Attribute("key") == "ModelAinLocation"
                           select el;
                if (keys.Count() > 0)
                {
                    tempModelName = keys.First().Attribute("value").ToString().Replace("value=", "").Replace("\"", "");
                    tempModelName = Path.GetDirectoryName(tempModelName);
                    var stringArray = tempModelName.Split('\\');
                    modelName = stringArray.Last();
                }
            }

            return modelName;
        }

        //private XDocument AppendTimedFileCheckerToEnd(string fileName)
        //{
        //    XDocument doc = XDocument.Load(fileName);
        //    XElement processElement = new XElement("Process");
        //    XAttribute processNameAttribute = new XAttribute("name", "Wait for Alfa post processing file");
        //    XAttribute processNotesAttribute = new XAttribute("notes", "Waits for the trigger file produced at the end of Alfa run.");
        //    processElement.Add(processNameAttribute);
        //    processElement.Add(processNotesAttribute);

        //    XElement timeCheckElement = new XElement("TimedFileChecker");
        //    processElement.Add(timeCheckElement);
        //    doc.Root.Add(processElement);
        //    //doc.Root.Add(new XElement("TimedFileChecker"));
        //    return doc;
        //}

        private XElement AppendTimedFileCheckerToEnd(XDocument doc, 
            string postProcessTriggerFile, string modelName, string jobHistoryConnection, string maximumFileCheckAttempts)
        {
            _jobInstanceID++;
            XElement processElement = new XElement("Process");
            XAttribute processNameAttribute = new XAttribute("name", "Wait for Alfa post processing file");
            XAttribute processNotesAttribute = new XAttribute("notes", "Waits for the trigger file produced at the end of Alfa run.");
            processElement.Add(processNameAttribute);
            processElement.Add(processNotesAttribute);

            XElement timeCheckElement = new XElement("TimedFileCheckProcess");

            XElement dashboardIDElement = new XElement("add");
            XAttribute dashboardIDKey = new XAttribute("key", "DashboardID");
            XAttribute dashboardIDValue = new XAttribute("value", modelName);
            dashboardIDElement.Add(dashboardIDKey);
            dashboardIDElement.Add(dashboardIDValue);
            timeCheckElement.Add(dashboardIDElement);

            XElement dashboardProcessElement = new XElement("add");
            XAttribute dashboardProcessKey = new XAttribute("key", "DashboardProcessName");
            XAttribute dashboardProcessValue = new XAttribute("value", "TimedFileCheckProcess");
            dashboardProcessElement.Add(dashboardProcessKey);
            dashboardProcessElement.Add(dashboardProcessValue);
            timeCheckElement.Add(dashboardProcessElement);

            XElement dashboardConnectionElement = new XElement("add");
            XAttribute dashboardConnectionKey = new XAttribute("key", "DashboardConnection");
            XAttribute dashboardConnectionValue = new XAttribute("value", "null");
            dashboardConnectionElement.Add(dashboardConnectionKey);
            dashboardConnectionElement.Add(dashboardConnectionValue);
            timeCheckElement.Add(dashboardConnectionElement);

            XElement jobHistoryConnectionElement = new XElement("add");
            XAttribute jobHistoryConnectionKey = new XAttribute("key", "JobHistoryConnection");
            XAttribute jobHistoryConnectionValue = new XAttribute("value",jobHistoryConnection);
            jobHistoryConnectionElement.Add(jobHistoryConnectionKey);
            jobHistoryConnectionElement.Add(jobHistoryConnectionValue);
            timeCheckElement.Add(jobHistoryConnectionElement);

            XElement jobInstanceIDElement = new XElement("add");
            XAttribute jobInstanceIDKey = new XAttribute("key", "JobInstanceID");
            XAttribute jobInstanceIDValue = new XAttribute("value", _jobInstanceID.ToString());
            jobInstanceIDElement.Add(jobInstanceIDKey);
            jobInstanceIDElement.Add(jobInstanceIDValue);
            timeCheckElement.Add(jobInstanceIDElement);

            XElement directoryElement = new XElement("add");
            XAttribute directoryKey = new XAttribute("key", "DirectoryName");
            XAttribute directoryValue = new XAttribute("value", Path.GetDirectoryName(postProcessTriggerFile));
            directoryElement.Add(directoryKey);
            directoryElement.Add(directoryValue);
            timeCheckElement.Add(directoryElement);

            XElement fileElement = new XElement("add");
            XAttribute fileKey = new XAttribute("key", "WildcardOrFileName");
            XAttribute fileValue = new XAttribute("value", Path.GetFileName(postProcessTriggerFile));
            fileElement.Add(fileKey);
            fileElement.Add(fileValue);
            timeCheckElement.Add(fileElement);

            XElement intervalElement = new XElement("add");
            XAttribute intervalKey = new XAttribute("key", "CheckIntervalInMinutes");
            XAttribute intervalValue = new XAttribute("value", "1");
            intervalElement.Add(intervalKey);
            intervalElement.Add(intervalValue);
            timeCheckElement.Add(intervalElement);

            XElement maxAttemptElement = new XElement("add");
            XAttribute maxAttemptKey = new XAttribute("key", "MaximumFileCheckAttempts");
            XAttribute maxAttemptValue = new XAttribute("value", maximumFileCheckAttempts);
            maxAttemptElement.Add(maxAttemptKey);
            maxAttemptElement.Add(maxAttemptValue);
            timeCheckElement.Add(maxAttemptElement);

            XElement expectNumElement = new XElement("add");
            XAttribute expectNumKey = new XAttribute("key", "ExpectedNumberOfFiles");
            XAttribute expectNumValue = new XAttribute("value", "1");
            expectNumElement.Add(expectNumKey);
            expectNumElement.Add(expectNumValue);
            timeCheckElement.Add(expectNumElement);

            processElement.Add(timeCheckElement);

            //doc.Root.Add(new XElement("TimedFileChecker"));
            return processElement;
        }

        //lastElement.Add(new XElement("TimedFileChecker"));


        private bool PostFileExists(string fileName)
        {
            return File.Exists(fileName.Replace("Pre.xml", "Post.xml"));
        }


        private string ReplaceEx(string original,
                    string pattern, string replacement)
        {
            int count, position0, position1;
            count = position0 = position1 = 0;
            string upperString = original.ToUpper();
            string upperPattern = pattern.ToUpper();
            int inc = (original.Length / pattern.Length) *
                      (replacement.Length - pattern.Length);
            char[] chars = new char[original.Length + Math.Max(0, inc)];
            while ((position1 = upperString.IndexOf(upperPattern,
                                              position0)) != -1)
            {
                for (int i = position0; i < position1; ++i)
                    chars[count++] = original[i];
                for (int i = 0; i < replacement.Length; ++i)
                    chars[count++] = replacement[i];
                position0 = position1 + pattern.Length;
            }
            if (position0 == 0) return original;
            for (int i = position0; i < original.Length; ++i)
                chars[count++] = original[i];
            return new string(chars, 0, count);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
