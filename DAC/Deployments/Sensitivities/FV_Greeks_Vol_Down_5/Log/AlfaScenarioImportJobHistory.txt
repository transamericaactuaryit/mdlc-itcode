Alfa Run Log:
Alfa log for scenarios: 1
Start MG-ALFA script processing: E:\MGALFA\AutomationCode\Sensitivities\FV_Greeks_Vol_Down_5\Executables\testscript.asc

Start date and time .... 03-05-2018 at 13:20:55
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


03-05-2018 at 13:20:55 --------------------------------------------------------------------------------

OpenModel "E:\MGALFA\Model\Executionfolder\Scenario\SCN_RN_1000.ain2"
ValDate 12/2017
Run 1

::AinRun::Message::Begin::

Return Message: 

 Run.001  Yes    Na  Ok--03-05-2018 at 13:23:40  Not defined 

::AinRun::Message::End::
Close


03-05-2018 at 13:23:40 --------------------------------------------------------------------------------

End date and time ...... 03-05-2018 at 13:23:40
End of script .......... 

Input param:JobHistoryConnection:E:\MGALFA\AutomationCode\Sensitivities\FV_Greeks_Vol_Down_5\Log\AlfaScenarioImportJobHistory.txt
Input param:JobInstanceID:2
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\MGALFA\AutomationCode\Sensitivities\FV_Greeks_Vol_Down_5\Executables\testscript.asc"
Input param:ModelAinLocation:E:\MGALFA\Model\Executionfolder\Scenario\SCN_RN_1000.ain2
Input param:RunScenarios:1
Input param:ProcessTimeToMonthOffsetForValDate:-3
Input param:ScriptRunFile:E:\MGALFA\AutomationCode\Sensitivities\FV_Greeks_Vol_Down_5\Executables\testscript.asc
Input param:AlfaLogFile:E:\MGALFA\AutomationCode\Sensitivities\FV_Greeks_Vol_Down_5\Executables\testscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Scenario
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorSearchString:error
Input param:ToAddress:TAFinVAFVModelers@transamerica.com,TAFinACTALFASUPPORT@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:Scenario File Import Run Complete
Input param:Body:Scenario File Import Complete (FV_Greeks_Vol_Down_5)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :1
ModelAinReName:E:\MGALFA\Model\Executionfolder\Scenario\SCN_RN_1000.ain2
FinishTime:03/05/2018 13:23:41.497
