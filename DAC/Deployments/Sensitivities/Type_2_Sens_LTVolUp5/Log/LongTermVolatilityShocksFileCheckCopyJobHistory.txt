Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\Type_2_Sens_LTVolUp5\Log\LongTermVolatilityShocksFileCheckCopyJobHistory.txt
Input param:JobInstanceID:4
Input param:UseDBJobHistory:false
Input param:DestinationDirectory:E:\MGALFA\Staging\FairValue\Sensitivity\ModelInputs
Input param:SourceDirectoryPattern:\\us.aegon.com\ait\Actuary\TCMActuary\Valtest\{YearHolderFourDigit}M{MonthHolderLeadingZero}\Riders\zAlfa\Projects\Model_Inputs
Input param:Q1MonthMap:1,2,3
Input param:Q2MonthMap:4,5,6
Input param:Q3MonthMap:7,8,9
Input param:Q4MonthMap:10,11,12
Input param:ProcessTimeToMonthOffset:-3
Input param:OverwriteDestinationFile:true
Input param:Expectednumberoffiles:1
Input param:Wildcard:Long-term Volatility shocks*.xlsx
Input param:WildcardExclusions:
Input param:ReplaceFromString:\s\d{1}Q\d{2}
Input param:ReplaceToString:
ResolvedDirectoryName:\\us.aegon.com\ait\Actuary\TCMActuary\Valtest\2017M12\Riders\zAlfa\Projects\Model_Inputs
FinishTime:03/05/2018 11:03:44.333
