Alfa Run Log:Start MG-ALFA script processing: E:\mgalfa\AutomationCode\Sensitivities\FP_1YROUT\Executables\AlfaAutomation\modelscript.asc

Start date and time .... 01-09-2018 at 10:25:18
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


01-09-2018 at 10:25:18 --------------------------------------------------------------------------------

OpenModel "E:\mgalfa\Model\Executionfolder\Sensitivities\FP_1YROUT\FP_1YROUT.ain2"
ValDate 09/2017
Run 133

::AinRun::Message::Begin::

Return Message: 

 Run.133  Yes    Na  Ok--01-09-2018 at 10:32:47  TCM VA URL 2013 TLIC - Single Scenario 

::AinRun::Message::End::
Close


01-09-2018 at 10:32:47 --------------------------------------------------------------------------------

End date and time ...... 01-09-2018 at 10:32:47
End of script .......... 

Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\FP_1YROUT\Log\AlfaTriggerFileSet.txt
Input param:JobInstanceID:2
Input param:ModelName:FP_1YROUT
Input param:AlfaTriggerFileLocation:E:\MGALFA\AlfaRunTriggerFiles
Input param:PostProcessingTriggerFile:E:\MGALFA\AlfaPostProcessingTriggerFiles\FP_1YROUT.txt
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\mgalfa\AutomationCode\Sensitivities\FP_1YROUT\Executables\AlfaAutomation\modelscript.asc"
Input param:ModelAinLocation:E:\mgalfa\Model\Executionfolder\Sensitivities\FP_1YROUT\Ifrs_Fair_Value.ain2
Input param:RunScenarios:133
Input param:ProcessTimeToMonthOffsetForValDate:-4
Input param:ScriptRunFile:E:\mgalfa\AutomationCode\Sensitivities\FP_1YROUT\Executables\AlfaAutomation\modelscript.asc
Input param:AlfaLogFile:E:\mgalfa\AutomationCode\Sensitivities\FP_1YROUT\Executables\AlfaAutomation\modelscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_1YROUT
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorIndicatorList:Wrn|Err|Not Processed
Input param:ErrorFileSaveLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_1YROUT\FP_1YROUT_Error.txt
Input param:ToAddress:TAFinVAFVModelers@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:FP_1YROUT Complete
Input param:Body:MG-Alfa automation complete (FP_1YROUT)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :133
ModelAinReName:E:\mgalfa\Model\Executionfolder\Sensitivities\FP_1YROUT\FP_1YROUT.ain2
FinishTime:01/09/2018 10:32:48.720
