Alfa Run Log:
Alfa log for scenarios: 29
Start MG-ALFA script processing: E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_LapseUp20\Executables\AlfaAutomation\modelscript.asc

Start date and time .... 03-06-2018 at 08:50:18
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


03-06-2018 at 08:50:18 --------------------------------------------------------------------------------

OpenModel "E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_LapseUp20\Type_III_UW_LapseUp20.ain2"
ValDate 12/2017
Run 29

::AinRun::Message::Begin::

Return Message: 

 Run.029  Yes    Na  Ok (Messages)--03-06-2018 at 12:30:33  TCM VA POST 10/01/11 TLIC 

::AinRun::Message::End::
Close


03-06-2018 at 12:30:33 --------------------------------------------------------------------------------

End date and time ...... 03-06-2018 at 12:30:34
End of script .......... 

Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\Type_III_UW_LapseUp20\Log\AlfaTriggerFileSet.txt
Input param:JobInstanceID:2
Input param:ModelName:Type_III_UW_LapseUp20
Input param:AlfaTriggerFileLocation:E:\MGALFA\AlfaRunTriggerFiles
Input param:PostProcessingTriggerFile:E:\MGALFA\AlfaPostProcessingTriggerFiles\Type_III_UW_LapseUp20.txt
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_LapseUp20\Executables\AlfaAutomation\modelscript.asc"
Input param:ModelAinLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_LapseUp20\Ifrs_Fair_Value.Ain2
Input param:RunScenarios:29
Input param:ProcessTimeToMonthOffsetForValDate:-3
Input param:ScriptRunFile:E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_LapseUp20\Executables\AlfaAutomation\modelscript.asc
Input param:AlfaLogFile:E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_LapseUp20\Executables\AlfaAutomation\modelscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_LapseUp20
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorIndicatorList:Wrn|Err|Not Processed
Input param:ErrorFileSaveLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_LapseUp20\Type_III_UW_LapseUp20_Error.txt
Input param:ToAddress:TAFinVAFVModelers@transamerica.com,TAFinACTALFASUPPORT@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:Type_III_UW_LapseUp20 Complete
Input param:Body:MG-Alfa automation complete (Type_III_UW_LapseUp20)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :29
ModelAinReName:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_LapseUp20\Type_III_UW_LapseUp20.ain2
FinishTime:03/06/2018 12:30:36.848
