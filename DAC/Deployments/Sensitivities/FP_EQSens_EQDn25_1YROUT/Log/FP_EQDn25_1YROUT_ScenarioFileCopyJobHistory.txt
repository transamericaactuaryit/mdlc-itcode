Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\FP_EQSens_EQDn25_1YROUT\Log\FP_EQDn25_1YROUT_ScenarioFileCopyJobHistory.txt
Input param:JobInstanceID:4
Input param:UseDBJobHistory:false
Input param:DestinationDirectory:E:\mgalfa\Staging\FairValue\Scenario
Input param:SourceDirectoryPattern:\\us.aegon.com\ait\Actuary\TCMActuary\Valtest\sharedactuarial\EquityRisk\{YearHolderFourDigit}\Q{QuarterHolder}{YearHolderTwoDigit}\RN\Stochastic\TCM\FP\1YROUT
Input param:Q1MonthMap:1,2,3
Input param:Q2MonthMap:4,5,6
Input param:Q3MonthMap:7,8,9
Input param:Q4MonthMap:10,11,12
Input param:ProcessTimeToMonthOffset:-1
Input param:OverwriteDestinationFile:true
Input param:ExpectedNumberOfFiles:1
Input param:Wildcard:TCMALFA1_FP_*_Base_1000BDG.agu
Input param:WildcardExclusions:
Input param:ReplaceFromString:FP_\d{6}_Base_1000BDG
Input param:ReplaceToString:RN_Base_1000
ResolvedDirectoryName:\\us.aegon.com\ait\Actuary\TCMActuary\Valtest\sharedactuarial\EquityRisk\2018\Q118\RN\Stochastic\TCM\FP\1YROUT
FinishTime:04/19/2018 14:38:27.028
