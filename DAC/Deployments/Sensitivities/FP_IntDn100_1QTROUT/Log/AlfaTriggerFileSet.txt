Alfa Run Log:
Alfa log for scenarios: 29
Start MG-ALFA script processing: E:\MGALFA\AutomationCode\Sensitivities\FP_IntDn100_1QTROUT\Executables\AlfaAutomation\modelscript.asc

Start date and time .... 02-09-2018 at 17:35:20
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


02-09-2018 at 17:35:20 --------------------------------------------------------------------------------

OpenModel "E:\MGALFA\Model\Executionfolder\Sensitivities\FP_IntDn100_1QTROUT\FP_IntDn100_1QTROUT.ain2"
ValDate 12/2017
Run 29

::AinRun::Message::Begin::

Return Message: 

 Run.029  Yes    Na  Ok (Messages)--02-10-2018 at 00:57:44  TCM VA POST 10/01/11 TLIC 

::AinRun::Message::End::
Close


02-10-2018 at 00:57:45 --------------------------------------------------------------------------------

End date and time ...... 02-10-2018 at 00:57:45
End of script .......... 

Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\FP_IntDn100_1QTROUT\Log\AlfaTriggerFileSet.txt
Input param:JobInstanceID:2
Input param:ModelName:FP_IntDn100_1QTROUT
Input param:AlfaTriggerFileLocation:E:\MGALFA\AlfaRunTriggerFiles
Input param:PostProcessingTriggerFile:E:\MGALFA\AlfaPostProcessingTriggerFiles\FP_IntDn100_1QTROUT.txt
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\MGALFA\AutomationCode\Sensitivities\FP_IntDn100_1QTROUT\Executables\AlfaAutomation\modelscript.asc"
Input param:ModelAinLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_IntDn100_1QTROUT\Ifrs_Fair_Value.Ain2
Input param:RunScenarios:29
Input param:ProcessTimeToMonthOffsetForValDate:-2
Input param:ScriptRunFile:E:\MGALFA\AutomationCode\Sensitivities\FP_IntDn100_1QTROUT\Executables\AlfaAutomation\modelscript.asc
Input param:AlfaLogFile:E:\MGALFA\AutomationCode\Sensitivities\FP_IntDn100_1QTROUT\Executables\AlfaAutomation\modelscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_IntDn100_1QTROUT
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorIndicatorList:Wrn|Err|Not Processed
Input param:ErrorFileSaveLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_IntDn100_1QTROUT\FP_1QTROUT_Error.txt
Input param:ToAddress:TAFinVAFVModelers@transamerica.com,TAFinACTALFASUPPORT@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:FP_IntDn100_1QTROUT Complete
Input param:Body:MG-Alfa automation complete (FP_IntDn100_1QTROUT)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :29
ModelAinReName:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_IntDn100_1QTROUT\FP_IntDn100_1QTROUT.ain2
FinishTime:02/10/2018 00:57:48.211
