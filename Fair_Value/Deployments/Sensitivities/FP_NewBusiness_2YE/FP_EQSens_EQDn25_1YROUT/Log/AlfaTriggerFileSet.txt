Alfa Run Log:
Alfa log for scenarios: 29
Start MG-ALFA script processing: E:\MGALFA\AutomationCode\Sensitivities\FP_EQSens_EQDn20_1YROUT\Executables\AlfaAutomation\modelscript.asc

Start date and time .... 01-30-2018 at 10:40:19
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


01-30-2018 at 10:40:19 --------------------------------------------------------------------------------

OpenModel "E:\MGALFA\Model\Executionfolder\Sensitivities\FP_EQSens_EQDn20_1YROUT\FP_EQSens_EQDn20_1YROUT.ain2"
ValDate 12/2017
Run 29

::AinRun::Message::Begin::

Return Message: 

 Run.029  Yes    Na  Ok (Messages)--01-30-2018 at 14:26:05  TCM VA POST 10/01/11 TLIC 

::AinRun::Message::End::
Close


01-30-2018 at 14:26:06 --------------------------------------------------------------------------------

End date and time ...... 01-30-2018 at 14:26:07
End of script .......... 

Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\FP_EQSens_EQDn20_1YROUT\Log\AlfaTriggerFileSet.txt
Input param:JobInstanceID:2
Input param:ModelName:FP_EQSens_EQDn20_1YROUT
Input param:AlfaTriggerFileLocation:E:\MGALFA\AlfaRunTriggerFiles
Input param:PostProcessingTriggerFile:E:\MGALFA\AlfaPostProcessingTriggerFiles\FP_EQSens_EQDn20_1YROUT.txt
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\MGALFA\AutomationCode\Sensitivities\FP_EQSens_EQDn20_1YROUT\Executables\AlfaAutomation\modelscript.asc"
Input param:ModelAinLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_EQSens_EQDn20_1YROUT\Ifrs_Fair_Value.Ain2
Input param:RunScenarios:29
Input param:ProcessTimeToMonthOffsetForValDate:-1
Input param:ScriptRunFile:E:\MGALFA\AutomationCode\Sensitivities\FP_EQSens_EQDn20_1YROUT\Executables\AlfaAutomation\modelscript.asc
Input param:AlfaLogFile:E:\MGALFA\AutomationCode\Sensitivities\FP_EQSens_EQDn20_1YROUT\Executables\AlfaAutomation\modelscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_EQSens_EQDn20_1YROUT
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorIndicatorList:Wrn|Err|Not Processed
Input param:ErrorFileSaveLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_EQSens_EQDn20_1YROUT\FP_1YROUT_Error.txt
Input param:ToAddress:TAFinVAFVModelers@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:FP_EQSens_EQDn20_1YROUT Complete
Input param:Body:MG-Alfa automation complete (FP_EQSens_EQDn20_1YROUT)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :29
ModelAinReName:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_EQSens_EQDn20_1YROUT\FP_EQSens_EQDn20_1YROUT.ain2
FinishTime:01/30/2018 14:26:09.950
