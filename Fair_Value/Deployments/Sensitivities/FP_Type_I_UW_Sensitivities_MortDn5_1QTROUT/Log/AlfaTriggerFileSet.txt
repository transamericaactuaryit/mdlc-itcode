Alfa Run Log:
Alfa log for scenarios: 2,102
Start MG-ALFA script processing: E:\MGALFA\AutomationCode\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\Executables\AlfaAutomation\modelscript.asc

Start date and time .... 07-26-2018 at 13:45:19
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


07-26-2018 at 13:45:19 --------------------------------------------------------------------------------

OpenModel "E:\MGALFA\Model\Executionfolder\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\FP_Type_I_UW_MortDn5_1QTROUT.ain2"
ValDate 06/2018
Run 2,102

::AinRun::Message::Begin::

Return Message: 

 Run.002  Yes    Na  Ok (Messages)--07-26-2018 at 14:08:09  TCM IRT VA Partners                   
 Run.102  Yes    Na  Ok (Messages)--07-26-2018 at 13:56:40  TCM IRT VA Partners - Single Scenario 

::AinRun::Message::End::
Close


07-26-2018 at 14:08:09 --------------------------------------------------------------------------------

End date and time ...... 07-26-2018 at 14:08:21
End of script .......... 

Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\Log\AlfaTriggerFileSet.txt
Input param:JobInstanceID:2
Input param:ModelName:FP_Type_I_UW_MortDn5_1QTROUT
Input param:AlfaTriggerFileLocation:E:\MGALFA\AlfaRunTriggerFiles
Input param:PostProcessingTriggerFile:E:\MGALFA\AlfaPostProcessingTriggerFiles\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT.txt
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\MGALFA\AutomationCode\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\Executables\AlfaAutomation\modelscript.asc"
Input param:ModelAinLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\Ifrs_Fair_Value.Ain2
Input param:RunScenarios:2,102
Input param:ProcessTimeToMonthOffsetForValDate:-1
Input param:ScriptRunFile:E:\MGALFA\AutomationCode\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\Executables\AlfaAutomation\modelscript.asc
Input param:AlfaLogFile:E:\MGALFA\AutomationCode\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\Executables\AlfaAutomation\modelscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorIndicatorList:Wrn|Err|Not Processed
Input param:ErrorFileSaveLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT_Error.txt
Input param:ToAddress:TAFinVAFVModelers@transamerica.com,TAFinACTALFASUPPORT@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT Complete
Input param:Body:MG-Alfa automation complete (FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :2,102
ModelAinReName:E:\MGALFA\Model\Executionfolder\Sensitivities\FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT\FP_Type_I_UW_MortDn5_1QTROUT.ain2
FinishTime:07/26/2018 14:08:22.737
