Alfa Run Log:
Alfa log for scenarios: 2,102
Start MG-ALFA script processing: E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_MortDn5\Executables\AlfaAutomation\modelscript.asc

Start date and time .... 07-25-2018 at 16:00:19
Workspace Id ........... C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\UI\MGALFA105
System version ......... MG-ALFA Version 10.5.2011[10.5.016]


07-25-2018 at 16:00:19 --------------------------------------------------------------------------------

OpenModel "E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_MortDn5\Type_III_UW_MortDn5.ain2"
ValDate 06/2018
Run 2,102

::AinRun::Message::Begin::

Return Message: 

 Run.002  Yes    Na  Ok (Messages)--07-25-2018 at 22:29:34  TCM IRT VA Partners                   
 Run.102  Yes    Na  Ok (Messages)--07-25-2018 at 22:19:08  TCM IRT VA Partners - Single Scenario 

::AinRun::Message::End::
Close


07-25-2018 at 22:29:34 --------------------------------------------------------------------------------

End date and time ...... 07-25-2018 at 22:29:47
End of script .......... 

Input param:JobHistoryConnection:E:\mgalfa\AutomationCode\Sensitivities\Type_III_UW_MortDn5\Log\AlfaTriggerFileSet.txt
Input param:JobInstanceID:2
Input param:ModelName:Type_III_UW_MortDn5
Input param:AlfaTriggerFileLocation:E:\MGALFA\AlfaRunTriggerFiles
Input param:PostProcessingTriggerFile:E:\MGALFA\AlfaPostProcessingTriggerFiles\Type_III_UW_MortDn5.txt
Input param:ProcessLocation:"C:\Program Files (x86)\Milliman\MG-ALFA 10.5.2011\NUX\MgAlfa.exe"
Input param:Arguments:"E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_MortDn5\Executables\AlfaAutomation\modelscript.asc"
Input param:ModelAinLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_MortDn5\Ifrs_Fair_Value.Ain2
Input param:RunScenarios:2,102
Input param:ProcessTimeToMonthOffsetForValDate:-1
Input param:ScriptRunFile:E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_MortDn5\Executables\AlfaAutomation\modelscript.asc
Input param:AlfaLogFile:E:\MGALFA\AutomationCode\Sensitivities\Type_III_UW_MortDn5\Executables\AlfaAutomation\modelscript.asc.Log
Input param:ErrorOutputSizeMinimumInBytes:3219456
Input param:ErrorOutputSizeLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_MortDn5
Input param:ErrorOutputSizeWildcard:*.Arx2
Input param:ErrorIndicatorList:Wrn|Err|Not Processed
Input param:ErrorFileSaveLocation:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_MortDn5\Type_III_UW_LapseDn20_Error.txt
Input param:ToAddress:TAFinVAFVModelers@transamerica.com,TAFinACTALFASUPPORT@transamerica.com
Input param:FromAddress:MDLC_DoNotReply@transamerica.com
Input param:Subject:Type_III_UW_MortDn5 Complete
Input param:Body:MG-Alfa automation complete (Type_III_UW_MortDn5)
Input param:SmtpServer:na-mail.aegon.com
Initial :Scenarios:
Scenarios: :2,102
ModelAinReName:E:\MGALFA\Model\Executionfolder\Sensitivities\Type_III_UW_MortDn5\Type_III_UW_MortDn5.ain2
FinishTime:07/25/2018 22:29:48.904
