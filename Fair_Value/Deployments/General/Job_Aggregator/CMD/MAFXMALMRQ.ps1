param([string]$Environment)
#$Environment = "TEST"

####################################################################
#  Date: 11/22/2017                                                #
#  Author: Rahul Rastogi				                           #
#  Decription: Powershell Script to check Post Run Files		   #
####################################################################


	$rootdirectory = "E:\MGALFA\AutomationCode\BondMMAttribution"
	$JobName = "File Watcher Job"
	$Logfile = $rootdirectory+"\Log\MAFXMALMRQ.txt"
	$InputFile = "E:\MGALFA\AlfaPostProcessingTriggerFiles\BondMM.txt"
	$TimeOutLimit = 86400 #  24 hours
	$ServerSupportAcct = "MDLC_DoNotReply@transamerica.com"
	$AutoEmailer =  "E:\MGALFA\AutomationCode\Common\autoemailer\AutoEmailer.exe"
	
if($Environment -eq "DEV")
{
	$ITSupport1 = "TAFinACTALFASUPPORT@transamerica.com"
    $ModelSupport = "TAFinVAFVModelers@transamerica.com"
}
if($Environment -eq "INT")
{
	$ITSupport1 = "TAFinACTALFASUPPORT@transamerica.com"
    $ModelSupport = "TAFinVAFVModelers@transamerica.com"
}
if($Environment -eq "TEST")
{
	$ITSupport1 = "TAFinACTALFASUPPORT@transamerica.com"
    $ModelSupport = "TAFinVAFVModelers@transamerica.com"
}
if($Environment -eq "PROD")
{
	$ITSupport1 = "TAFinACTALFASUPPORT@transamerica.com"
    $ModelSupport = "TAFinVAFVModelers@transamerica.com"
}
####################################################################
# Function                                                         #
####################################################################
	

	Function EmailModellers($AtStep, $StepPurpose, $StepResults) 
{

	$LogEntry =  $Jobname + " " + $AtStep + " - " + $StepResults + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru
	$EmailParms = "-u " + $ServerSupportAcct + " -r " + $ModelSupport + ", " + $ITSupport1 + " -s " + $LogEntry 
	$MailerResults = start-process $AutoEmailer $EmailParms
   
}

####################################################################
# Start of program                                                 #
####################################################################
	
	$Step = "Step 010"
	$StepPurpose = $JobName + " " + $Step + " - Begin checking for files" 
	$LogEntry = $StepPurpose + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $Step + " " + $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru

			$ActionCode = 0
			$RunTime = 0
			$LogActionCode = 0


			Do{
				$ValidPath1 = Test-Path $InputFile
				try
				{
					If ($ValidPath1 -eq $True) 
					   {
						$ActionCode = 1 
						$LogActionCode = $Environment+ "File Found - BondMMAttribution - Watching - EndedOK"
						#
						$LASTEXITCODE = 0
						exit $LASTEXITCODE
                       }
					Else 
                        {
						# FILE NOT FOUND
						Start-Sleep -s 600 
						$RunTime = $RunTime + 600
						$LogActionCode = $Environment + " Waiting for file for BondMMAttribution " + [DateTime]::Now.ToString("hh:mm:ss") 
						
					    }
					#TEST TO SEE IF JOB OUT OF TIME
					If ($TimeOutLimit -lt $RunTime)
					{
						$ActionCode = 2 
						$LogActionCode = $Environment +"OutOfTime" +  "MGALFA BondMMAttribution Model Post run file was not received " + [DateTime]::Now.ToString("hh:mm:ss") 
						EmailModellers $StepName $StepPurpose $LogActionCode 
						$LASTEXITCODE = -1
						exit $LASTEXITCODE

					}
				} 
				Catch
				{
					continue
				} 
			} 
			While ($ValidPath1 -eq $False -and $ActionCode -lt 1)
exit 0