param([string]$Environment)
#$Environment = "TEST"

####################################################################
#  Date: 11/22/2017                                                #
#  Author: Rahul Rastogi				                           #
#  Decription: Powershell Script to check Post Run Files		   #
####################################################################


	$rootdirectory = "E:\MGALFA\AutomationCode\RateAttribution"
	$JobName = "File Watcher Job"
	$Logfile = $rootdirectory+"\Powershell\Logs\MAFDMAL21Q.txt"
	$InputFile = "E:\MGALFA\AlfaPostProcessingTriggerFiles\Rate.txt"
	$TimeOutLimit = 28800 #  6 hours
	$ITSupport1 = "rahul.rastogi@transamerica.com"
	$ModelSupport = "TAFinVAFVModelers@transamerica.com"
	$ServerSupportAcct = "MDLC_DoNotReply@transamerica.com"
	$AutoEmailer =  "E:\MGALFA\AutomationCode\Common\autoemailer\AutoEmailer.exe"
	


####################################################################
# Function                                                         #
####################################################################
	
	Function TestLogActionCodeResults($AtStep, $StepPurpose, $StepResults) 
{

	$LogEntry = $Jobname + " " + $AtStep + " - " + $StepResults + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru
	$EmailParms = "-u " + $ServerSupportAcct + " -r " + $ITSupport1 + " -s " + $LogEntry 
	$MailerResults = start-process $AutoEmailer $EmailParms
   
}

	Function EmailModellers($AtStep, $StepPurpose, $StepResults) 
{

	$LogEntry =  $Jobname + " " + $AtStep + " - " + $StepResults + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru
	$EmailParms = "-u " + $ServerSupportAcct + " -r " + $ModelSupport + ", " + $ITSupport1 + " -s " + $LogEntry 
	$MailerResults = start-process $AutoEmailer $EmailParms
   
}

####################################################################
# Start of program                                                 #
####################################################################
	
	$Step = "Step 010"
	$StepPurpose = $JobName + " " + $Step + " - Begin checking for files" 
	$LogEntry = $StepPurpose + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $Step + " " + $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru

			$ActionCode = 0
			$RunTime = 0
			$LogActionCode = 0

			Do{
				$ValidPath1 = Test-Path $InputFile
				try
				{
					If ($ValidPath1 -eq $True) 
					   {
						$ActionCode = 1 
						$LogActionCode = "File Found - RateAttribution - Watching - EndedOK"
						TestLogActionCodeResults $StepName $StepPurpose $LogActionCode 
						$LASTEXITCODE = 0
						exit $LASTEXITCODE
                       }
					Else 
                        {
						# FILE NOT FOUND
						Start-Sleep -s 600 
						$RunTime = $RunTime + 600
						$LogActionCode = " Waiting for file for RateAttribution " + [DateTime]::Now.ToString("hh:mm:ss") 
						#TestLogActionCodeResults $StepName $StepPurpose $LogActionCode 
					    }
					#TEST TO SEE IF JOB OUT OF TIME
					If ($TimeOutLimit -lt $RunTime)
					{
						$ActionCode = 2 
						$LogActionCode = "OutOfTime" +  "MGALFA RateAttribution Model  Post run file was not received " + [DateTime]::Now.ToString("hh:mm:ss") 
						EmailModellers $StepName $StepPurpose $LogActionCode 
						$LASTEXITCODE = -1
						exit $LASTEXITCODE

					}
				} 
				Catch
				{
					continue
				} 
			} 
			While ($ValidPath1 -eq $False -and $ActionCode -lt 1)
exit 0