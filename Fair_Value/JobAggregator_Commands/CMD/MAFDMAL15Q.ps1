param([string]$Environment)
#$Environment = "TEST"

####################################################################
#  Date: 11/22/2017                                                #
#  Author: Rahul Rastogi				                           #
#  Decription: Powershell Script to check Post Run Files		   #
####################################################################

	$rootdirectory = "E:\MGALFA\AutomationCode\PassageOfTimeAttribution"
	$executionrootdirectory = "E:\mgalfa\model\Executionfolder\PassageOfTimeAttribution"
	$JobName = "File Watcher Job"
	$Logfile = $rootdirectory+"\Powershell\Logs\MAFDMAL15Q.txt"
	$InputFile = "E:\MGALFA\AlfaPostProcessingTriggerFiles\PoT.txt"
	$TimeOutLimit = 86400 #  6 hours
	#$RetryLimit = 6
	$ITSupport1 = "rahul.rastogi@transamerica.com"
	$ModelSupport = "TAFinVAFVModelers@transamerica.com"
	$ServerSupportAcct = "MDLC_DoNotReply@transamerica.com"
	$AutoEmailer =  "E:\MGALFA\AutomationCode\Common\autoemailer\AutoEmailer.exe"
	


####################################################################
# Function                                                         #
####################################################################
	
	Function TestLogActionCodeResults($AtStep, $StepPurpose, $StepResults) 
{

	$LogEntry =  $Jobname + " " + $AtStep + " - " + $StepResults + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru
	$EmailParms = "-u " + $ServerSupportAcct + " -r " + $ITSupport1 + " -s " + $LogEntry 
	$MailerResults = start-process $AutoEmailer $EmailParms
   
}

	Function EmailModellers($AtStep, $StepPurpose, $StepResults) 
{

	$LogEntry =  $Jobname + " " + $AtStep + " - " + $StepResults + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru
	$EmailParms = "-u " + $ServerSupportAcct + " -r " + $ModelSupport + ", " + $ITSupport1 + " -s " + $LogEntry 
	$MailerResults = start-process $AutoEmailer $EmailParms
   
}

####################################################################
# Start of program                                                 #
####################################################################
	
	$Step = "Step 010"
	$StepPurpose = $JobName + " " + $Step + " - Begin checking for files" 
	$LogEntry = $StepPurpose + [DateTime]::Now.ToString("hh:mm:ss") 
	$WriteString = $Step + " " + $LogEntry
	add-content -Path $Logfile -Value $LogEntry -passthru

			$ActionCode = 0
			$RunTime = 0
			$RetryCount = 0
			$RetryTime = 0

			Do{
				$ValidPath1 = Test-Path $InputFile
				#$ValidPath2 = Test-Path $executionrootdirectory\*.lock
				try
				{
					#If ($ValidPath1 -eq $True -and $ValidPath2 -eq $False) 
					If ($ValidPath1 -eq $True) 
					   { 
						$ActionCode = 1 
						$LogActionCode = "File Found - PassageOfTimeAttribution - Watching - EndedOK"
						TestLogActionCodeResults $StepName $StepPurpose $LogActionCode 
						$RetryCount = 0
						$LASTEXITCODE = 0
						exit $LASTEXITCODE
                       }
					#If ($ValidPath1 -eq $True -and $ValidPath2 -eq $True) 
						#{ 
							#if($RetryCount -lt $RetryLimit)
							#	{
									#$LogActionCode = "Lock file found Model still locked - Waiting for lock to release"
									##TestLogActionCodeResults $StepName $StepPurpose $LogActionCode 
									#$RetryCount = $RetryCount+1
									#Start-Sleep -s 300 
									#$RetryTime = $RetryTime + 300
								#}
							#else
								#{
									#$ActionCode = 2 
									#$LogActionCode = "OutOfTime" +  "Model is still locked 30 mins after Model completion. Take necessary action" + [DateTime]::Now.ToString("hh:mm:ss") 
									#EmailModellers $StepName $StepPurpose $LogActionCode 
									#$LASTEXITCODE = -1
									#exit $LASTEXITCODE
								#}
						#}
					Else 
                        {
						# FILE NOT FOUND
						Start-Sleep -s 600 
						$RunTime = $RunTime + 600
						$LogActionCode = " Waiting for file PassageOfTimeAttribution " + [DateTime]::Now.ToString("hh:mm:ss") 
						#TestLogActionCodeResults $StepName $StepPurpose $LogActionCode 
					    }
					#TEST TO SEE IF JOB OUT OF TIME
					If ($TimeOutLimit -lt $RunTime)
					{
						$ActionCode = 3 
						$LogActionCode = "OutOfTime" +  "MGALFA PassageOfTimeAttribution Model Post run file was not received " + [DateTime]::Now.ToString("hh:mm:ss") 
						EmailModellers $StepName $StepPurpose $LogActionCode 
						$LASTEXITCODE = -1
						exit $LASTEXITCODE
					}
				} 
				Catch
				{
					continue
				} 
			} 
			While ($ValidPath1 -eq $False -and $ActionCode -lt 1)
exit 0