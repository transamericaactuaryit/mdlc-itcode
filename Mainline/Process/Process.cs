﻿using EnablingSystems.ProcessInterfaces;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Xml.Linq;
using System.Xml.XPath;
using EnablingSystems.Utilities;
using System.Threading;
using System.Timers;
using System.Text.RegularExpressions;

namespace EnablingSystems.MDLC.Processes
{

    public abstract class ProcessBaseWithHistory : IProcess
    {
        #region Fields
        protected NameValueCollection _processHistory = new NameValueCollection();
        protected LogWriter _logWriter;
        protected IJobHistory _jobHistory;
        protected NameValueCollection _processParams;
        public event EventHandler Finish;

        public const string Q1Map = "1,2,3";
        public const string Q2Map = "4,5,6";
        public const string Q3Map = "7,8,9";
        public const string Q4Map = "10,11,12";
        #endregion

        #region Properties
        public string ProcessName { get; set; }
        public NameValueCollection ProcessParams
        {
            set
            {
                _processParams = value;
            }
            get
            {
                return _processParams;
            }
        }

        public IJobHistory JobHistory
        {
            set
            {
                _jobHistory = value;
            }
            get
            {
                return _jobHistory;
            }
        }

        protected string FinishTime { get; private set; } = string.Empty;
        #endregion
        protected ProcessBaseWithHistory(LogWriter logWriter)
        {
            _logWriter = logWriter;
        }

        protected ProcessBaseWithHistory()
        {
        }

        public abstract void Execute();

        public virtual void NotifyDone()
        {
            FinishTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff",
                                CultureInfo.InvariantCulture);
            if (Finish != null)
            {
                Finish.Invoke(this, EventArgs.Empty);
            }
            
        }

        public virtual void SaveHistory(int historyId)
        {
            if (_jobHistory != null)
            {
                foreach (string key in _processParams)
                {
                    _processHistory.Add(String.Format(CultureInfo.InvariantCulture, "Input param:{0}", key), _processParams[key]);
                }

                var myType = this.GetType();
                foreach (var prop in 
                    myType.GetProperties(System.Reflection.BindingFlags.Instance|
                    System.Reflection.BindingFlags.NonPublic 
                    | System.Reflection.BindingFlags.Public))
                {
                    if (ShouldSave(prop.Name))
                    {
                        var hasGetter = prop.GetGetMethod(true);
                        if (hasGetter != null)
                        {
                            var propVal = prop.GetValue(this, null);

                            if (propVal.GetType() == typeof(System.Collections.Specialized.NameValueCollection))
                            {
                                var thisCollection = (NameValueCollection)propVal;

                                foreach (var k in thisCollection.AllKeys)
                                {
                                    _processHistory.Add(k, thisCollection[k]);
                                }
                            }
                            else
                            {
                                if (propVal != null)
                                {
                                    _processHistory.Add(prop.Name,
                                        propVal.ToString());
                                }
                            }
                        }
                    }
                }

                _jobHistory.SaveHistory(historyId,_processHistory);
            }

        }

        private static bool ShouldSave(string name)
        {
            var nameStr = name.ToUpperInvariant();
            return ((nameStr != "   LOGWRITER") &&
                (nameStr != "PROCESSPARAMS") &&
                (nameStr != "JOBHISTORY"));            
        }
    }


    public class CopyFileProcess : ProcessBaseWithHistory
    {
        public override void Execute()
        {
            bool overwriteIfExists = bool.Parse(_processParams["OverwriteDestinationFile"] ?? "true");

            string sourceFile = _processParams["SourceFile"];
            string destinationFile = _processParams["DestinationFile"];

            System.IO.File.Copy(sourceFile, destinationFile, overwriteIfExists);
            //SaveHistory();
            NotifyDone();

        }
    }


    public class SVNUpdate : ProcessBaseWithHistory
    {
        private string _svnReturnMessage = "";

        public string SvnReturnMessage
        {
            get
            {
                return _svnReturnMessage;
            }
        }
        public override void Execute()
        {

            // if SVN location isn't present, just issuing the command "svn" might work
            string svnLocation = @_processParams["SvnLocation"] ?? "svn";
            string locationToCommit = @_processParams["LocationToUpdate"];
            //string workingDirectory = @_processParams["WorkingDirectory"] ?? "";
            string updateSVNRepo = @_processParams["SVNUpdateCommand"] ?? "";
            string svnNonErrorsToExtract = _processParams["SVNNonErrorsToExtract"] ?? "";

            //commit at the folder level, not beneath
            if (locationToCommit.EndsWith("\\\"", StringComparison.OrdinalIgnoreCase))
            {
                locationToCommit = locationToCommit.Substring(0, locationToCommit.Length - 2);
                locationToCommit += "\"";
            }

            //generally the will add everything
            if (!string.IsNullOrEmpty(updateSVNRepo))
            {
                string preCommitAddWithLocation = updateSVNRepo + " " + locationToCommit;
                _svnReturnMessage += ExternalProcessHelper.ExecuteProcessReturnMessage(false, preCommitAddWithLocation,
                    svnLocation, locationToCommit, svnNonErrorsToExtract);
            }

            NotifyDone();
        }
    }

    public class SVNTimerEventArgs : EventArgs
    {
        public Exception SVNException { get; set; }
    }
    public class SVNCheckout : ProcessBaseWithHistory, IDisposable
    {
        private bool disposed = false;
        private string _svnReturnMessage = "";
        string _svnInfoFile = "";
        string checkoutURL = "";
        private System.Timers.Timer m_SVNTimeout_Timer;

        public delegate void HandleExceptionEventDelegate(object sender, SVNTimerEventArgs e);
        public event HandleExceptionEventDelegate HandleExceptionEvent;
        public string SvnReturnMessage
        {
            get
            {
                return _svnReturnMessage;
            }
        }

        public override void Execute()
        {

            checkoutURL = @_processParams["CheckoutURL"] ?? "";
            string svnLocation = @_processParams["SvnLocation"] ?? "svn";
            string checkoutCommand = @_processParams["CheckoutCommand"] ?? "";
            string workingDirectory = @_processParams["WorkingDirectory"] ?? "";
            _svnInfoFile = @_processParams["SVNInfoFile"] ?? @"E:\MGALFA\ServiceAccountInfo\ServiceAccountInfo.xml";
            string svnUser = "";
            string svnPwd = "";
            string svnNonErrorsToExtract = _processParams["SVNNonErrorsToExtract"] ?? "";
            bool _decryptSVNPassword = bool.Parse(_processParams["DecryptSVNPassword"] ?? "true");
            bool _deleteWorkingDirectoryBeforeCheckout = bool.Parse(_processParams["DeleteWorkingDirectoryBeforeCheckout"] ?? "true");
            int processTimeout = int.Parse(_processParams["TimeoutInMiliseconds"] ?? "60000", CultureInfo.InvariantCulture);
            int _sleepTimeAfterDirectoryDelete = int.Parse(_processParams["SleepTimeAfterDirectoryDelete"] ?? "5000", CultureInfo.InvariantCulture);

            HandleExceptionEvent += new HandleExceptionEventDelegate(HandleExceptionEventHandler);


            string pwdUnEncrypted = "";

            VersionControlHelper.RetrieveSVNInfo(_svnInfoFile, ref svnUser, ref svnPwd);

            if (_decryptSVNPassword)
            {
                SecureString encryptedPwdSecure = DPAPI_Utilities.DecryptString(svnPwd);
                pwdUnEncrypted = DPAPI_Utilities.ToInsecureString(encryptedPwdSecure);
            }
            else
            {
                pwdUnEncrypted = svnPwd;
            }

            checkoutURL = ProcessHelper.WrapQuotes(checkoutURL);
            svnLocation = ProcessHelper.WrapQuotes(svnLocation);

            if (_deleteWorkingDirectoryBeforeCheckout)
            {
                if(Directory.Exists(workingDirectory))
                {
                    DeleteDirectory(workingDirectory);
                }
            }

            //let's breathe a little before we re-create the folder

            Thread.Sleep(_sleepTimeAfterDirectoryDelete);

            //of course it doesn't exist!
            if (!Directory.Exists(workingDirectory))
            {
                Directory.CreateDirectory(workingDirectory);
            }

            if (!string.IsNullOrEmpty(checkoutURL))
            {
                m_SVNTimeout_Timer = new System.Timers.Timer(processTimeout);
                m_SVNTimeout_Timer.Elapsed += new ElapsedEventHandler(Timeout_Timer_Tick);
                m_SVNTimeout_Timer.Enabled = true;
                m_SVNTimeout_Timer.Start();

                string preCommitAddWithLocation = checkoutCommand + " " + checkoutURL
                    + " \"" +  workingDirectory + "\""
                    + " --non-interactive --no-auth-cache "
                    + " " +"--username \"" + svnUser + "\""
                    + " " + "--password \"" + pwdUnEncrypted + "\""; ;
                _svnReturnMessage += ExternalProcessHelper.ExecuteProcessReturnMessage(false, preCommitAddWithLocation,
                    svnLocation, workingDirectory, svnNonErrorsToExtract);
                m_SVNTimeout_Timer.Enabled = false;
                NotifyDone();
            }
        }

        private void Timeout_Timer_Tick(object sender, ElapsedEventArgs e)
        {
            m_SVNTimeout_Timer.Enabled = false;
            
            var eX = new TimeoutException("SVNCheckout timed out. Configured password may be incorrect. "
                + "Check " + _svnInfoFile +". Checkout URL: " + checkoutURL );

            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = ".NET Runtime";
                eventLog.WriteEntry(eX.Message, EventLogEntryType.Error, 1000, 1);
            }

            SVNTimerEventArgs svnEventArgs = new SVNTimerEventArgs()
            {
                SVNException = eX
            };


            HandleExceptionEvent(this, svnEventArgs);
        }

        void HandleExceptionEventHandler(object sender, SVNTimerEventArgs eventArgs)
        {
            //throw exception;
            //the throw exception doesn't work, so exit since we 
            //wrote to the event log
            Environment.Exit(-1);
        }


        private void DeleteDirectory(string path)
        {
            var directory = new DirectoryInfo(path);
            DisableReadOnly(directory);
            directory.Delete(true);
        }

        private void DisableReadOnly(DirectoryInfo directory)
        {
            foreach (var file in directory.GetFiles())
            {
                if (file.IsReadOnly)
                    file.IsReadOnly = false;
            }
            foreach (var subdirectory in directory.GetDirectories())
            {
                DisableReadOnly(subdirectory);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                m_SVNTimeout_Timer.Dispose();
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }
    }



    //If a control file is present, SVN commit will not occur, and exception will be thrown.
    public class SVNCommitConditional : SVNCommit
    {
        private string _controlFileToPreventCommit;
        public override void Execute()
        {
            _controlFileToPreventCommit = @_processParams["ControlFileToPreventCommit"];

            if (!string.IsNullOrEmpty(_controlFileToPreventCommit))
            {
                string archiveFileName = string.Concat(
                     Path.GetDirectoryName(_controlFileToPreventCommit),
                     Path.GetFileNameWithoutExtension(_controlFileToPreventCommit),
                     DateTime.Now.ToString("yyyyMMddHHmmssfff", CultureInfo.InvariantCulture),
                     Path.GetExtension(_controlFileToPreventCommit)
                    );

                if (File.Exists(_controlFileToPreventCommit))
                {
                    File.Move(_controlFileToPreventCommit, archiveFileName);
                    
                    throw new FileNotFoundException(string.Format(CultureInfo.InvariantCulture, "Control file {0} was found. {1} was not committed to SVN."
                        + " SVN commit was not performed.", _controlFileToPreventCommit, locationToCommit));
                }
                else
                {
                    base.Execute();
                }

            }
            else
            {
                base.Execute();
            }
        }
    }

    public class SVNCommit : ProcessBaseWithHistory, IDisposable
    {
        private bool disposed = false;
        private string _svnReturnMessage = "";
        protected string locationToCommit;
        private System.Timers.Timer m_SVNTimeout_Timer;
        string commitCommand = "";
        string cleanupCommand = "";
        string svnLocation = "";
        private string workingDirectory = "";
        private int _numberRetries = 1;
        private int _maxNumberRetries;
        private int _sleepBetweenCleanupAndCommit;
        private string svnNonErrorsToExtract = "";
        private string _svnInfoFile = "";


       // public delegate void HandleExceptionEventDelegate(Exception exception);

        public string SvnReturnMessage
        {
            get
            {
                return _svnReturnMessage;
            }
        }

        public override void Execute()
        {
            // if SVN location isn't present, just issuing the command "svn" might work
            svnLocation = @_processParams["SvnLocation"] ?? "svn";
            locationToCommit = @_processParams["LocationToCommit"] ?? @_processParams["WorkingDirectory"];
            string commitMessage = @_processParams["CommitMessage"];
            string addNewFilesToSVN = @_processParams["AddNewFilesCommand"] ?? "";
            string removeDeletedFilesFromSVN = @_processParams["RemoveDeletedFilesCommand"] ?? "";
            string powershellLaunchCommandForDelete = @_processParams["PowershellLaunchCommandForDelete"] ?? "";
            workingDirectory = @_processParams["WorkingDirectory"] ?? "";
            _svnInfoFile = @_processParams["SVNInfoFile"] ?? @"E:\MGALFA\ServiceAccountInfo\ServiceAccountInfo.xml";
            string svnUser = "";
            string svnPwd = "";
            string q1MonthMap = _processParams["Q1MonthMap"] ?? Q1Map;
            string q2MonthMap = _processParams["Q2MonthMap"] ?? Q2Map;
            string q3MonthMap = _processParams["Q3MonthMap"] ?? Q3Map;
            string q4MonthMap = _processParams["Q4MonthMap"] ?? Q4Map;
            svnNonErrorsToExtract = _processParams["SVNNonErrorsToExtract"] ?? "";
            int processTimeToMonthOffset = int.Parse(_processParams["ProcessTimeToMonthOffset"]?? "-1", CultureInfo.InvariantCulture);
            bool _decryptSVNPassword = bool.Parse(_processParams["DecryptSVNPassword"] ?? "true");
            int processTimeout = int.Parse(_processParams["TimeoutInMiliseconds"] ?? "60000", CultureInfo.InvariantCulture);
            _sleepBetweenCleanupAndCommit = int.Parse(_processParams["SleepBetweenCleanupAndCommit"] ?? "5000", CultureInfo.InvariantCulture);
            _maxNumberRetries = int.Parse(_processParams["MaxNumberRetries"] ?? "2", CultureInfo.InvariantCulture);

            string pwdUnEncrypted = "";

            VersionControlHelper.RetrieveSVNInfo(_svnInfoFile, ref svnUser, ref svnPwd);

            if (_decryptSVNPassword)
            {
                SecureString encryptedPwdSecure = DPAPI_Utilities.DecryptString(svnPwd);
                pwdUnEncrypted = DPAPI_Utilities.ToInsecureString(encryptedPwdSecure);
            }
            else
            {
                pwdUnEncrypted = svnPwd;
            }
            //commit at the folder level, not beneath
            if (locationToCommit.EndsWith("\\\"", StringComparison.OrdinalIgnoreCase))
            {
                locationToCommit = locationToCommit.Substring(0,locationToCommit.Length - 2);
                locationToCommit += "\"";
            }
                
            if (!String.IsNullOrEmpty(q1MonthMap))
            {
                DynamicFiscalNameResolver resolver = new DynamicFiscalNameResolver(
                commitMessage,
                q1MonthMap,
                q2MonthMap,
                q3MonthMap,
                q4MonthMap,
                processTimeToMonthOffset
                );
                commitMessage = resolver.ResolvedName.TrimEnd('\\');
            }

            cleanupCommand = "cleanup \"" + locationToCommit;
            _svnReturnMessage += "\r\nInitial cleanup\r\n" + ExternalProcessHelper.ExecuteProcessReturnMessage(false, cleanupCommand,
                svnLocation, workingDirectory, svnNonErrorsToExtract);

            Thread.Sleep(_sleepBetweenCleanupAndCommit);

            //generally the will add everything
            if (!string.IsNullOrEmpty(addNewFilesToSVN))
            {
                string preCommitAddWithLocation = addNewFilesToSVN + " " + locationToCommit;
                _svnReturnMessage += ExternalProcessHelper.ExecuteProcessReturnMessage(false, preCommitAddWithLocation, 
                    svnLocation, workingDirectory, svnNonErrorsToExtract);
            }

            if (!string.IsNullOrEmpty(removeDeletedFilesFromSVN))
            {
                string preCommitDeleteWithLocation = removeDeletedFilesFromSVN + " " + locationToCommit;
                _svnReturnMessage += ExternalProcessHelper.ExecuteProcessReturnMessage(false, preCommitDeleteWithLocation, 
                    powershellLaunchCommandForDelete, workingDirectory, svnNonErrorsToExtract);
            }

            //let's breathe a little and clean up any lock files

            Thread.Sleep(_sleepBetweenCleanupAndCommit);


            _svnReturnMessage += "\r\nCleanup before commit\r\n" + ExternalProcessHelper.ExecuteProcessReturnMessage(false, cleanupCommand,
                svnLocation, workingDirectory, svnNonErrorsToExtract);

            Thread.Sleep(_sleepBetweenCleanupAndCommit);


            commitCommand = "commit \"" + locationToCommit + "\" -m \"" 
                + commitMessage + "\""  
                + " --non-interactive --no-auth-cache " 
                + "--username \"" + svnUser + "\"" 
                + " --password \"" + pwdUnEncrypted + "\"";

            _svnReturnMessage += "\r\nAbout to commit";
            //below is only for debugging. Don't put the user/pwd in the log file!
            //_svnReturnMessage += string.Format("{0}Commit Command:{1}{2}", 
            //    "\r\n", commitCommand, "\r\n");
            m_SVNTimeout_Timer = new System.Timers.Timer(processTimeout);
            m_SVNTimeout_Timer.Elapsed += new ElapsedEventHandler(Timeout_Timer_Tick);
            m_SVNTimeout_Timer.Enabled = true;
            m_SVNTimeout_Timer.Start();

            _svnReturnMessage += ExternalProcessHelper.ExecuteProcessReturnMessage(false, commitCommand,
                svnLocation, workingDirectory, svnNonErrorsToExtract);

            _svnReturnMessage += "\r\nAfter commit";

            m_SVNTimeout_Timer.Enabled = false;
            NotifyDone();
        }

        private void Timeout_Timer_Tick(object sender, ElapsedEventArgs e)
        {
            m_SVNTimeout_Timer.Enabled = false;

            if (_numberRetries > _maxNumberRetries)
            {                
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = ".NET Runtime";
                    eventLog.WriteEntry("SVN commit timed out. You may need to manually check in files.  "
                    + "Location " + locationToCommit, EventLogEntryType.Error, 1000, 1);
                }

                Environment.Exit(-1);
            }
            else
            {
                _numberRetries++;
                //gotta be careful between timer and sleep
                m_SVNTimeout_Timer.Enabled = true;
                Thread.Sleep(_sleepBetweenCleanupAndCommit);
                _svnReturnMessage += string.Format(CultureInfo.InvariantCulture, "\r\nattempt{0}..cleaning up\r\n", _numberRetries) + ExternalProcessHelper.ExecuteProcessReturnMessage(false, cleanupCommand,
                    svnLocation, workingDirectory, svnNonErrorsToExtract);

                Thread.Sleep(_sleepBetweenCleanupAndCommit);

                _svnReturnMessage += string.Format(CultureInfo.InvariantCulture, "\r\nattempt{0}..committing\r\n", _numberRetries) + ExternalProcessHelper.ExecuteProcessReturnMessage(false, cleanupCommand,
                    svnLocation, workingDirectory, svnNonErrorsToExtract);


            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                m_SVNTimeout_Timer.Dispose();
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }
    }

    public class ClearDirectory : ProcessBaseWithHistory
    {
        public override void Execute()
        {
            string _directoryToClear = _processParams["DirectoryToClear"];
            bool _deleteSubDirectories = bool.Parse(_processParams["DeleteSubdirectories"] ?? "false");
            string _regularExpressionsList = _processParams["RegExFilesToExclude"] ?? "";
            System.IO.DirectoryInfo di = new DirectoryInfo(_directoryToClear);

            foreach (FileInfo file in di.GetFiles())
            {
                bool skip = false;
                if (!string.IsNullOrEmpty(_regularExpressionsList))
                {
                    foreach (string thisRegex in _regularExpressionsList.Split('_'))
                    {
                        System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(thisRegex);
                        if (rgx.IsMatch(file.Name))
                        {
                            skip = true;
                        }
                    }
                }

                if (skip)
                {
                    continue;
                }
                else
                {
                    file.Delete();
                }

            }

            if (_deleteSubDirectories)
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }

            NotifyDone();
        }
    }


    public class DeleteFile : ProcessBaseWithHistory
    {
        public override void Execute()
        {
            string fileToDelete = _processParams["FileToDelete"];
            File.Delete(fileToDelete);
            NotifyDone();
        }
    }

    public class FileCheckAndCopy : ProcessBaseWithHistory
    {
        string _resolvedDirectoryName = "";

        public string ResolvedDirectoryName
        {
            get
            {
                return _resolvedDirectoryName;
            }
        }

        public override void Execute()
        {
            FileExistenceChecker fileExistenceChecker = new FileExistenceChecker();
            fileExistenceChecker.ProcessParams = _processParams;
            fileExistenceChecker.Execute();

            DynamicDirectoryCopyFileProcess copyFiles = new DynamicDirectoryCopyFileProcess();
            copyFiles.ProcessParams = _processParams;
            copyFiles.Execute();
            _resolvedDirectoryName = fileExistenceChecker.ResolvedDirectoryName;
            NotifyDone();
        }
    }

    public class FileCopyOverrideDefault : ProcessBaseWithHistory
    {
        NameValueCollection _paramsToPass = new NameValueCollection();
        private string _resolvedNameOfDirectoryUsed;

        public string ResolvedNameOfDirectoryUsed
        {
            get
            {
                return _resolvedNameOfDirectoryUsed;
            }
        }

        public override void Execute()
        {
            string overrideDirectoryPattern = _processParams["OverrideSourceDirectoryPattern"] ?? "";
            string defaultDirectoryPattern = _processParams["DefaultSourceDirectoryPattern"] ?? "";
            int overrideprocessTimeToMonthOffset = int.Parse(_processParams["OverrideProcessTimeToMonthOffset"] ?? _processParams["ProcessTimeToMonthOffset"] ?? "-1");
            int defaultprocessTimeToMonthOffset = int.Parse(_processParams["DefaultProcessTimeToMonthOffset"] ?? _processParams["ProcessTimeToMonthOffset"] ?? "-1");
            string q1MonthMap = _processParams["Q1MonthMap"] ?? Q1Map;
            string q2MonthMap = _processParams["Q2MonthMap"] ?? Q2Map;
            string q3MonthMap = _processParams["Q3MonthMap"] ?? Q3Map;
            string q4MonthMap = _processParams["Q4MonthMap"] ?? Q4Map;
            int expectedNumberOfFiles = int.Parse(_processParams["ExpectedNumberOfFiles"] ?? "-1");
            string fileNameWildCard = _processParams["Wildcard"] ?? "";

            foreach (var k in _processParams.AllKeys)
            {
                _paramsToPass.Add(k, _processParams[k]);
            }

            DynamicFiscalNameResolver overrideResolver = new DynamicFiscalNameResolver(
                overrideDirectoryPattern,
                q1MonthMap,
                q2MonthMap,
                q3MonthMap,
                q4MonthMap,
                overrideprocessTimeToMonthOffset
                );


            string overrideFolder = overrideResolver.ResolvedName;
            _resolvedNameOfDirectoryUsed = overrideFolder;
            int numFilesOverride = CheckAndCopy(q1MonthMap, q2MonthMap, q3MonthMap, q4MonthMap,
                overrideprocessTimeToMonthOffset, expectedNumberOfFiles, fileNameWildCard, overrideFolder);

            if(numFilesOverride <= 0)
            {
                DynamicFiscalNameResolver defaultResolver = new DynamicFiscalNameResolver(
                    defaultDirectoryPattern,
                    q1MonthMap,
                    q2MonthMap,
                    q3MonthMap,
                    q4MonthMap,
                    defaultprocessTimeToMonthOffset
                    );

                _resolvedNameOfDirectoryUsed = defaultResolver.ResolvedName;

                int numFilesDefault = CheckAndCopy(q1MonthMap, q2MonthMap, q3MonthMap, q4MonthMap,
                    defaultprocessTimeToMonthOffset, expectedNumberOfFiles, 
                    fileNameWildCard, defaultResolver.ResolvedName);

                if(numFilesDefault == -1)
                {
                    _resolvedNameOfDirectoryUsed = "";
                    throw new Exception(string.Format("Did not find the expected number of files at "
                        + "override location of {0} or default location of {1}",
                        overrideFolder, defaultResolver.ResolvedName));
                }
            }

            NotifyDone();

        }

        private int CheckAndCopy(string q1MonthMap, string q2MonthMap, 
            string q3MonthMap, string q4MonthMap, int processTimeToMonthOffset, 
            int expectedNumberOfFiles, string fileNameWildCard, 
             string folder)
        {
            int tempReturn = -1;
            string timeOffset = _paramsToPass["ProcessTimeToMonthOffset"];

            if(timeOffset == null)
            {
                _paramsToPass.Add("ProcessTimeToMonthOffset", processTimeToMonthOffset.ToString());
            }
            else
            {
                _paramsToPass["ProcessTimeToMonthOffset"] = processTimeToMonthOffset.ToString();
            }

                if (Directory.Exists(folder))
            {
                _paramsToPass["SourceDirectoryPattern"] = folder;

                var files = Directory.GetFiles(folder, fileNameWildCard);
                int numberOfFilesFound = files.Length;
                //TODO: expected number of files should be one or more, right?
                if ((expectedNumberOfFiles > -1) &&
                    (numberOfFilesFound != expectedNumberOfFiles))
                {
                    tempReturn = -1;
                }
                else
                {
                    tempReturn = numberOfFilesFound;
                    DynamicDirectoryCopyFileProcess overrideCopyFileProcess =
                        new DynamicDirectoryCopyFileProcess();

                    overrideCopyFileProcess.ProcessParams = _paramsToPass;

                    overrideCopyFileProcess.Execute();
                }
            }

            return tempReturn;
        }
    }

    public class TimedFileCheckProcess : ProcessBaseWithHistory
    {
        private AutoResetEvent _waitHandle = new AutoResetEvent(false);
        string directoryName = "";
        string wildcardOrFileName = "";
        bool _errorFound = false;
        public override void Execute()
        {
            directoryName = _processParams["DirectoryName"] ?? "";
            wildcardOrFileName = _processParams["WildcardOrFileName"] ?? "";
            int checkIntervalInMinutes = int.Parse(_processParams["CheckIntervalInMinutes"] ?? "1");
            int maximumFileCheckAttempts = int.Parse(_processParams["MaximumFileCheckAttempts"] ?? "10");
            int expectedNumberOfFiles = int.Parse(_processParams["ExpectedNumberOfFiles"] ?? "1");

            TimedFileChecker timedFileChecker = new TimedFileChecker(checkIntervalInMinutes, maximumFileCheckAttempts,
                directoryName, wildcardOrFileName, expectedNumberOfFiles);

            timedFileChecker.Done += FileCheckDone;
            timedFileChecker.StartCheckingForFiles();
            _waitHandle.WaitOne();

            if (_errorFound)
            {
                throw new Exception(string.Format(@"Did not find file(s) in the expected time: {0}\{1}",
                    directoryName, wildcardOrFileName));
            }
        }

        private void FileCheckDone(object sender, EventArgs e)
        {
            TimedFileArgs timedFileArgs = (TimedFileArgs)e;

            if (!timedFileArgs.FoundFiles)
            {
                _errorFound = true;
                _waitHandle.Set();
                // throw new Exception(string.Format("Did not find file(s) in the expected time: {0}/{1}", directoryName, wildcardOrFileName));
            }
            else
            {
                _waitHandle.Set();
                NotifyDone();

            }
        }
    }
    public class TimedFileArgs : EventArgs
    {
        public bool FoundFiles { get; set; }
    }
    public class TimedFileChecker : IDisposable
    {
        private System.Timers.Timer m_Filecheck_Timer = new System.Timers.Timer();
        private int _checkIntervalInMinutes;
        private int _maximumFileCheckAttempts = 5;

        private int _currentNumberOfAttempts = 0;
        private string _directoryName;
        private string _wildcardOrFileName;
        private int _expectedNumberOfFiles = 1;
        public event EventHandler Done;
        private bool disposed = false;

        #region Properties
        public int CheckIntervalInMinutes
        {
            get
            {
                return _checkIntervalInMinutes;
            }

            set
            {
                _checkIntervalInMinutes = value;
            }
        }

        public int MaximumFileCheckAttempts
        {
            get
            {
                return _maximumFileCheckAttempts;
            }

            set
            {
                _maximumFileCheckAttempts = value;
            }
        }

        public string DirectoryName
        {
            get
            {
                return _directoryName;
            }

            set
            {
                _directoryName = value;
            }
        }

        public string WildcardOrFileName
        {
            get
            {
                return _wildcardOrFileName;
            }

            set
            {
                _wildcardOrFileName = value;
            }
        }

        public int ExpectedNumberOfFiles
        {
            get
            {
                return _expectedNumberOfFiles;
            }

            set
            {
                _expectedNumberOfFiles = value;
            }
        }
        #endregion

        public TimedFileChecker(int checkIntervalInMinutes, int maximumFileCheckAttempts,
            string directoryName, string wildcardOrFileName, int expectedNumberOfFiles)
        {
            _checkIntervalInMinutes = checkIntervalInMinutes;
            _maximumFileCheckAttempts = maximumFileCheckAttempts;
            _directoryName = directoryName;

            _wildcardOrFileName = wildcardOrFileName;
            _expectedNumberOfFiles = expectedNumberOfFiles;
        }
        public void StartCheckingForFiles()
        {
            if (_directoryName.EndsWith(@"\"))
            {
                _directoryName = _directoryName.TrimEnd(Path.DirectorySeparatorChar);
            }

            m_Filecheck_Timer.Interval = _checkIntervalInMinutes * 60000;
            m_Filecheck_Timer.Elapsed += Filecheck_Timer_Elapsed;
            m_Filecheck_Timer.Enabled = true;
            m_Filecheck_Timer.Start();
        }



        private void Filecheck_Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int numberOfFilesFound = 0;
            m_Filecheck_Timer.Enabled = false;
            _currentNumberOfAttempts++;

            //it's possible that the directory could be dynamically created
            //this isn't necessarily an error condition, as the directory may be created 
            //before the maximum iterations are over
            if (Directory.Exists(_directoryName))
            {
                var files = Directory.GetFiles(_directoryName, _wildcardOrFileName);
                numberOfFilesFound = files.Length;
            }


            if (numberOfFilesFound == _expectedNumberOfFiles)
            {
                TimedFileArgs timedFileArgsSuccess = new TimedFileArgs();
                timedFileArgsSuccess.FoundFiles = true;

                if (Done != null)
                {
                    Done.Invoke(this, timedFileArgsSuccess);
                }
            }
            else
            {
                if (_currentNumberOfAttempts == _maximumFileCheckAttempts)
                {
                    TimedFileArgs timedFileArgsFail = new TimedFileArgs();
                    timedFileArgsFail.FoundFiles = false;

                    if (Done != null)
                    {
                        Done.Invoke(this, timedFileArgsFail);
                    }
                }
                else
                {
                    //keep checking if we haven't reached the maximum
                    m_Filecheck_Timer.Enabled = true;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                m_Filecheck_Timer.Dispose();
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

    }
    public class FileExistenceChecker : ProcessBaseWithHistory
    {
        int _numberOfFilesFound;
        string _resolvedDirectoryName = "";
        public int NumberOfFilesFound
        {
            get
            {
                return _numberOfFilesFound;
            }
        }

        public string ResolvedDirectoryName
        {
            get
            {
                return _resolvedDirectoryName;
            }
        }

        public override void Execute()
        {
            string dynamicDirectoryPattern = _processParams["SourceDirectoryPattern"] ?? "";

            string fileNameWildcard = _processParams["Wildcard"];
            //default to leading zero on month

            int expectedNumberFiles = int.Parse(_processParams["ExpectedNumberOfFiles"] ?? "-1");

            int processTimeToMonthOffset = int.Parse(_processParams["ProcessTimeToMonthOffset"] ?? "-1");
            string q1MonthMap = Q1Map;
            string q2MonthMap = Q2Map;
            string q3MonthMap = Q3Map;
            string q4MonthMap = Q4Map;


            if (!string.IsNullOrEmpty(dynamicDirectoryPattern))
            {
                _resolvedDirectoryName = GetCurrentWorkingDirectory( dynamicDirectoryPattern,
                    q1MonthMap, q2MonthMap, q3MonthMap, q4MonthMap,
                    processTimeToMonthOffset);
            }
            else
            {
                throw new Exception("SourceDirectoryPattern needs to be provided in parameters. ");
            }


            var files = Directory.GetFiles(_resolvedDirectoryName, fileNameWildcard);
            _numberOfFilesFound = files.Length;
            //TODO: expected number of files should be one or more, right?
            if ((expectedNumberFiles > -1) &&
                (_numberOfFilesFound != expectedNumberFiles))
            {

                string sErrorParams = "\r\nProcess parameters:";

                foreach (var k in _processParams.AllKeys)
                {
                    sErrorParams += k + "\t" + _processParams[k] + "\r\n";
                }

                throw new Exception(String.Format("Number of files returned does not match "
                    + "expected number of files. Expected {0}, {1} files returned. {2}", expectedNumberFiles, 
                    files.Length, 
                    sErrorParams));
            }

            NotifyDone();
        }

        protected virtual string GetCurrentWorkingDirectory(string dynamicDirectoryPattern, string q1MonthMap,
            string q2MonthMap, string q3MonthMap, string q4MonthMap,
            int processTimeToMonthOffset)
        {
            DynamicFiscalNameResolver currentProcessingFolderBase = new DynamicFiscalNameResolver(
                dynamicDirectoryPattern,
                q1MonthMap,
                q2MonthMap,
                q3MonthMap,
                q4MonthMap,
                processTimeToMonthOffset
                );

            string workingDirectory = currentProcessingFolderBase.ResolvedName;
            return workingDirectory;
        }
    }

    public class CreateDynamicDirectoryAndCopyFiles : ProcessBaseWithHistory
    {
        public override void Execute()
        {
            CreateDynamicDirectoryProcess createDir = new CreateDynamicDirectoryProcess();
            createDir.ProcessParams = _processParams;
            createDir.Execute();

            string destinationDirectory = createDir.DestinationDirectory;

            DynamicDirectoryCopyFileProcess copyProcess = new DynamicDirectoryCopyFileProcess();

            if(_processParams["DestinationDirectory"] != null)
            {
                throw new Exception("DestinationDirectory should not be provided in parameters.");
            }

            _processParams.Add("DestinationDirectory", destinationDirectory);
            copyProcess.ProcessParams = _processParams;
            copyProcess.Execute();

            NotifyDone();

        }
    }
    public class CreateDynamicDirectoryProcess : ProcessBaseWithHistory
    {

        public string DestinationDirectory { get; private set; } = string.Empty;

        public override void Execute()
        {
            string dynamicDirectoryPattern = _processParams["DirectoryToCreatePattern"] ?? string.Empty;
            string q1MonthMap = Q1Map;
            string q2MonthMap = Q2Map;
            string q3MonthMap = Q3Map;
            string q4MonthMap = Q4Map;
            int processTimeToMonthOffset = int.Parse(_processParams["ProcessTimeToMonthOffset"]);

            DynamicFiscalNameResolver currentProcessingFolderBase = new DynamicFiscalNameResolver(
                dynamicDirectoryPattern,
                q1MonthMap,
                q2MonthMap,
                q3MonthMap,
                q4MonthMap,
                processTimeToMonthOffset
                );

            DestinationDirectory = currentProcessingFolderBase.ResolvedName;

            Directory.CreateDirectory(DestinationDirectory);

            NotifyDone();
        }
    }

    public class SystemPause : ProcessBaseWithHistory
    {
        private int _sleepTime = 0;
        public override void Execute()
        {
            _sleepTime = int.Parse(_processParams["SleepTimeInMilliseconds"] ?? "30000");
            Thread.Sleep(_sleepTime);
            NotifyDone();
        }
    }

    public class DynamicDirectoryCopyFileProcess : ProcessBaseWithHistory
    {
        protected NameValueCollection _filesProcessed = new NameValueCollection();
        string _resolvedDirectoryName = string.Empty;
        bool _createDirectoryIfNotExists = true;

        public override void Execute()
        {
            string dynamicDirectoryPattern, destinationDirectory, q1MonthMap, q2MonthMap, q3MonthMap, q4MonthMap, fileNameWildcard, wildCardExclusions, replaceFromString, replaceToString;
            int processTimeToMonthOffset, expectedNumberFiles;
            bool overWriteDestinationFile, isCopy;

            PopulateVariablesFromParams(out dynamicDirectoryPattern, out destinationDirectory,
                out q1MonthMap, out q2MonthMap, out q3MonthMap, out q4MonthMap, out processTimeToMonthOffset,
                out fileNameWildcard, out wildCardExclusions, out replaceFromString, out replaceToString,
                out overWriteDestinationFile, out expectedNumberFiles, out isCopy);

            _resolvedDirectoryName = GetCurrentWorkingDirectory(
                 dynamicDirectoryPattern,
                q1MonthMap, q2MonthMap, q3MonthMap, q4MonthMap,
                processTimeToMonthOffset);

            PerformCopy(destinationDirectory, fileNameWildcard, wildCardExclusions, replaceFromString, replaceToString, 
                expectedNumberFiles, overWriteDestinationFile, isCopy, _resolvedDirectoryName);

            NotifyDone();
        }

        protected virtual void PerformCopy(string destinationDirectory, string fileNameWildcard, string wildCardExclusions, string replaceFromString, string replaceToString, int expectedNumberFiles, bool overWriteDestinationFile, bool isCopy, string workingDirectory)
        {
            if (!Directory.Exists(destinationDirectory) && _createDirectoryIfNotExists)
            {
                Directory.CreateDirectory(destinationDirectory);
            }

            WildCardFileCopy wildCardFileCopy = new WildCardFileCopy(
                workingDirectory,
                fileNameWildcard,
                wildCardExclusions,
                destinationDirectory,
                replaceFromString,
                replaceToString,
                overWriteDestinationFile,
                expectedNumberFiles,
                isCopy
                );
            wildCardFileCopy.Copy(_filesProcessed);
        }

        protected void PopulateVariablesFromParams(out string dynamicDirectoryPattern, out string destinationDirectory, out string q1MonthMap, out string q2MonthMap, out string q3MonthMap, out string q4MonthMap, out int processTimeToMonthOffset, out string fileNameWildcard, out string wildCardExclusions, out string replaceFromString, out string replaceToString, out bool overWriteDestinationFile, out int expectedNumberFiles, out bool isCopy)
        {
            dynamicDirectoryPattern = _processParams["SourceDirectoryPattern"] ?? string.Empty;
            destinationDirectory = _processParams["DestinationDirectory"];
            processTimeToMonthOffset = int.Parse(_processParams["ProcessTimeToMonthOffset"]);
            fileNameWildcard = _processParams["Wildcard"];
            wildCardExclusions = _processParams["WildcardExclusions"] ?? "";
            replaceFromString = _processParams["ReplaceFromString"];
            replaceToString = _processParams["ReplaceToString"];
            overWriteDestinationFile = bool.Parse(_processParams["OverwriteDestinationFile"] ?? "true");
            expectedNumberFiles = int.Parse(_processParams["ExpectedNumberOfFiles"] ?? "-1");
            isCopy = bool.Parse(_processParams["IsCopy"] ?? "true");
            _createDirectoryIfNotExists = bool.Parse(_processParams["CreateDirectoryIfNotExists"] ?? "true");

            q1MonthMap = Q1Map;
            q2MonthMap = Q2Map;
            q3MonthMap = Q3Map;
            q4MonthMap = Q4Map;
        }

        protected virtual string GetCurrentWorkingDirectory( string dynamicDirectoryPattern, string q1MonthMap, 
            string q2MonthMap, string q3MonthMap, string q4MonthMap, 
            int processTimeToMonthOffset)
        {
            DynamicFiscalNameResolver currentProcessingFolderBase = new DynamicFiscalNameResolver(
                dynamicDirectoryPattern,
                q1MonthMap,
                q2MonthMap,
                q3MonthMap,
                q4MonthMap,
                processTimeToMonthOffset
                );

            string workingDirectory = currentProcessingFolderBase.ResolvedName;
            return workingDirectory;
        }
    }

    public class WildCardFileCopy
    {
        #region Fields
        private string _sourceDirectory;
        private string _sourceWildcard;
        private string[] _wildcardExclusions;
        private string _destinationDirectory;
        private string _regexFileRenamePattern;
        private string _fileNameReplaceToString;
        private bool _overWriteDestinationFile;
        private int _expectedNumberFiles;
        private bool _isCopy = true;
        #endregion

        #region Construtor
        public WildCardFileCopy(string sourceDirectory, string sourceWildCard,
            string wildcardExclusions,
            string destinationDirectory, string regexFileRenamePattern, 
            string fileNameReplaceToString, bool overWriteDestinatinoFile,
            int expectedNumberFiles, bool isCopy)
        {
            _sourceDirectory = sourceDirectory;
            _sourceWildcard = sourceWildCard;
            _destinationDirectory = destinationDirectory;
            _regexFileRenamePattern = regexFileRenamePattern;
            _fileNameReplaceToString = fileNameReplaceToString;
            _overWriteDestinationFile = overWriteDestinatinoFile;
            _expectedNumberFiles = expectedNumberFiles;

            if(!string.IsNullOrEmpty(wildcardExclusions))
            {
                _wildcardExclusions = wildcardExclusions.Split(' ');
            }

            _isCopy = isCopy;
            
            InitializeDirectories();
        }
        #endregion

        #region Private Methods
        private void InitializeDirectories()
        {
            _sourceDirectory += (_sourceDirectory.EndsWith(@"\") ? "" : @"\");
            _destinationDirectory += (_destinationDirectory.EndsWith(@"\") ? "" : @"\");
        }

        private void Copy(string inputFilePath, string outputFilePath, bool move)
        {
            int bufferSize = 1024 * 1024;

            using (FileStream outputFileStream = new FileStream(outputFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
            {
                using (FileStream fsInput = new FileStream(inputFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    outputFileStream.SetLength(fsInput.Length);
                    int bytesRead = -1;
                    byte[] bytes = new byte[bufferSize];

                    while ((bytesRead = fsInput.Read(bytes, 0, bufferSize)) > 0)
                    {
                        outputFileStream.Write(bytes, 0, bytesRead);
                    }
                }
            }

            if (move)
            {
                if (File.Exists(inputFilePath))
                    File.Delete(inputFilePath);
            }
        }
        #endregion

        public void Copy(NameValueCollection _filesProcessed)
        {            
            List<string> files = GetFileList();

            if ((_expectedNumberFiles > -1) && (files.Count != _expectedNumberFiles))
            {
                throw new Exception($"Number of files returned does not match expected number of files. Expected {_expectedNumberFiles}, {files.Count} files returned.");
            }

            int i = 0;

            foreach (var thisFile in files)
            {
                i++;
                //Only apply regex replace to file name                
                string newFileName = Regex.Replace(Path.GetFileName(thisFile), _regexFileRenamePattern, _fileNameReplaceToString);
                string newDestFile = _destinationDirectory + newFileName;

                         
                Copy(thisFile, newDestFile, !_isCopy);
                
                if (_filesProcessed != null)
                {
                    _filesProcessed.Add("File Name Before " + i.ToString(), thisFile);
                    _filesProcessed.Add("File Name After " + i.ToString(), newDestFile);
                }
            }
        }

        protected virtual List<string> GetFileList()
        {
            List<string> files = new List<string>();

            if (_wildcardExclusions != null)
            {
                files = Directory
                .EnumerateFiles(_sourceDirectory, _sourceWildcard)
                .Where(file => !_wildcardExclusions.Any(x => file.EndsWith(x, StringComparison.OrdinalIgnoreCase)))
                .ToList();
            }
            else
            {
                files = Directory
                .EnumerateFiles(_sourceDirectory, _sourceWildcard)
                .ToList();
            }

            return files;
        }
    }

    public class DynamicFiscalNameResolver
    {
        #region Fields    
        private string _dynamicDirectoryPattern;
        private string _q1MonthMap;
        private string _q2MonthMap;
        private string _q3MonthMap;
        private string _q4MonthMap;
        private int _processTimeToMonthOffset;
        private string _quarterString = string.Empty;

        DateTime _quarterDate;
        DateTime _processDate = DateTime.Now;

        int _quarterMonth;
        int _quarterDay;
        #endregion  
        public string ResolvedName { get; private set; } = string.Empty;

        #region Constructor
        public DynamicFiscalNameResolver(
            string dynamicDirectoryPattern,
            string q1MonthMap,
            string q2MonthMap,
            string q3MonthMap,
            string q4MonthMap,
            int processTimeToMonthOffset
            )
        {
            _dynamicDirectoryPattern = dynamicDirectoryPattern;
            _q1MonthMap = q1MonthMap;
            _q2MonthMap = q2MonthMap;
            _q3MonthMap = q3MonthMap;
            _q4MonthMap = q4MonthMap;
            _processTimeToMonthOffset = processTimeToMonthOffset;

            ResolvedName = GetNameResolved();
        }
        #endregion

        #region Private Methods
        private string GetNameResolved()
        {            
            GetQuarterDateAndMonth(out _quarterDate, out _quarterMonth, out _quarterDay);
            return GetName(GetQuarterString(_quarterMonth));            
        }

        private string GetName(string quarterString)
        {
            string tempDynamicName = _dynamicDirectoryPattern.Replace("{QuarterHolder}", quarterString);
            tempDynamicName = tempDynamicName.Replace("{YearHolderTwoDigit}", _quarterDate.ToString("yy"));
            tempDynamicName = tempDynamicName.Replace("{YearHolderFourDigit}", _quarterDate.ToString("yyyy"));
            tempDynamicName = tempDynamicName.Replace("{MonthHolderLeadingZero}", _quarterMonth.ToString().PadLeft(2, '0'));
            tempDynamicName = tempDynamicName.Replace("{MonthHolder}", _quarterMonth.ToString());
            tempDynamicName = tempDynamicName.Replace("{DayHolderLeadingZero}", _quarterDay.ToString().PadLeft(2, '0'));
            tempDynamicName = tempDynamicName.Replace("{DayHolder}", _quarterDay.ToString());
            tempDynamicName = tempDynamicName.Replace("{HourHolderLeadingZero}", _quarterDate.ToString("hh"));
            tempDynamicName = tempDynamicName.Replace("{HourHolder}", _quarterDate.ToString("%h"));
            tempDynamicName = tempDynamicName.Replace("{MinuteHolderLeadingZero}", _quarterDate.ToString("mm"));
            tempDynamicName = tempDynamicName.Replace("{MinuteHolder}", _quarterDate.ToString("%m"));
            tempDynamicName = tempDynamicName.Replace("{SecondHolderLeadingZero}", _quarterDate.ToString("ss"));
            tempDynamicName = tempDynamicName.Replace("{SecondHolder}", _quarterDate.ToString("%s"));

            tempDynamicName = tempDynamicName.Replace("{CurrentYearHolderTwoDigit}", _processDate.ToString("yy"));
            tempDynamicName = tempDynamicName.Replace("{CurrentYearHolderFourDigit}", _processDate.ToString("yyyy"));
            tempDynamicName = tempDynamicName.Replace("{CurrentMonthHolderLeadingZero}", _processDate.Month.ToString().PadLeft(2, '0'));
            tempDynamicName = tempDynamicName.Replace("{CurrentMonthHolder}", _processDate.Month.ToString());
            tempDynamicName = tempDynamicName.Replace("{CurrentDayHolderLeadingZero}", _processDate.Day.ToString().PadLeft(2, '0'));
            tempDynamicName = tempDynamicName.Replace("{CurrentDayHolder}", _processDate.Day.ToString());
            tempDynamicName = tempDynamicName.Replace("{CurrentDayHourHolderLeadingZero}", _processDate.ToString("hh"));
            tempDynamicName = tempDynamicName.Replace("{CurrentDayHourHolder}", _processDate.ToString("%h"));
            tempDynamicName = tempDynamicName.Replace("{CurrentDayMinuteHolderLeadingZero}", _processDate.ToString("mm"));
            tempDynamicName = tempDynamicName.Replace("{CurrentDayMinuteHolder}", _processDate.ToString("%m"));
            tempDynamicName = tempDynamicName.Replace("{CurrentDaySecondHolderLeadingZero}", _processDate.ToString("ss"));
            tempDynamicName = tempDynamicName.Replace("{CurrentDaySecondHolder}", _processDate.ToString("%s"));
            string tempResult = tempDynamicName;
            return tempResult;
        }

        private string GetQuarterString(int quarterMonth)
        {
            if (_q1MonthMap.Contains(quarterMonth.ToString()))
            {
                return "1";
            }

            if (_q2MonthMap.Contains(quarterMonth.ToString()))
            {
                return "2";
            }
            if (_q3MonthMap.Contains(quarterMonth.ToString()))
            {
                return "3";
            }
            if (_q4MonthMap.Contains(quarterMonth.ToString()))
            {
                return "4";                 
            }            
            throw new ArgumentOutOfRangeException("Unparsable quarter input");
        }

        private void GetQuarterDateAndMonth(out DateTime quarterDate, out int quarterMonth, out int quarterDay)
        {            
            quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            quarterMonth = quarterDate.Month;
            quarterDay = quarterDate.Day;
        }
        #endregion  
    }


    public class ExternalProcess : ProcessBaseWithHistory
    {
        protected string sError = string.Empty;

        public override void Execute()
        {            
            GetProcessParameters(out string processLocation, out bool useShellExecute, out string processArgs);
            RunProcess(useShellExecute, processArgs, processLocation);
            NotifyDone();
        }

        protected void GetProcessParameters(out string processLocation, out bool useShellExecute, out string processArgs)
        {
            processLocation = _processParams["ProcessLocation"];
            useShellExecute = bool.Parse(_processParams["UseShellExecute"] ?? "false");
            processArgs = _processParams["Arguments"] ?? "";
        }

        public virtual void RunProcess(bool useShellExecute, string processArgs, string processLocation)
        {
            ExternalProcessHelper.ExecuteProcess(useShellExecute, processArgs, processLocation);
        }

    }

    public static class VersionControlHelper
    {
        public static void RetrieveSVNInfo(string svnInfoFile, ref string svnUser, ref string svnPwd)
        {
            if (!File.Exists(svnInfoFile))
            {
                throw new FileNotFoundException("SVN Info File Not found. File Name: " + svnInfoFile);
            }

            if (string.IsNullOrEmpty(svnInfoFile))
            {
                throw new Exception("SVN Info File is not defined in the parameters.");
            }

            XElement root = XElement.Load(svnInfoFile);
            var thisUser = root.XPathSelectElements("//ServiceAccountName").Single();
            var thisPwd = root.XPathSelectElements("//ServiceAccountPassword").Single();

            svnUser = thisUser.Value;
            svnPwd = thisPwd.Value;
        }
    }
    public static class ExternalProcessHelper
    {
        #region Private Methods
        private static string RunProcess(bool useShellExecute, string processArgs, string processLocation, string workingDirectory, string nonErrorToExtract)
        {
            if (processLocation == null)
            {
                throw new Exception("ProcessLocation did not exist in parameter collection");
            }

            ProcessStartInfo startInfo = new ProcessStartInfo(processLocation, processArgs)
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = useShellExecute,
                WorkingDirectory = workingDirectory
            };

            string standardOutput = string.Empty;
            string errorOutput = string.Empty;

            using (Process exeProcess = Process.Start(startInfo))
            {
                standardOutput = exeProcess.StandardOutput.ReadToEnd();
                errorOutput = exeProcess.StandardError.ReadToEnd();
                exeProcess.WaitForExit();
            }

            if (errorOutput.Length > 0)
            {
                if (!NonErrorsExtracted(errorOutput, nonErrorToExtract))
                {
                    throw new Exception(errorOutput);
                }

            }

            return $"Standard Output\r\n {standardOutput} \r\nError Output\r\n {errorOutput}";
        }

        private static bool NonErrorsExtracted(string errorOutput, string nonErrorsToExtract)
        {
            bool response = true;
            var errorLines = errorOutput.Split("\n\r".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            //if one of the lines does not have the non-error to extract, the output has errors
            foreach (string line in errorLines)
            {
                if (!line.Contains(nonErrorsToExtract))
                {
                    response = false;
                    break;
                }
            }

            return response;
        }
        #endregion 
        public static void ExecuteProcess(bool useShellExecute, string processArgs, string processLocation)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(processLocation)
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = useShellExecute,
                Arguments = processArgs
            };

            if (processLocation == null)
            {
                throw new Exception("ProcessLocation did not exist in parameter collection");
            }

            string standardOutput = string.Empty;
            string errorOutput = string.Empty;

            using (Process exeProcess = Process.Start(startInfo))
            {
                standardOutput = exeProcess.StandardOutput.ReadToEnd();
                errorOutput = exeProcess.StandardError.ReadToEnd();
                exeProcess.WaitForExit();
            }

            if (errorOutput.Length > 0)
            {
                throw new Exception(errorOutput);
            }
        }


        public static string ExecuteProcessReturnMessage(bool useShellExecute, string processArgs,
            string processLocation, string workingDirectory, string nonErrorToExtract)
        {
            return RunProcess(useShellExecute, processArgs, processLocation, workingDirectory, nonErrorToExtract);
        }        
    }


    public class FileErrorChecker
    {
        #region Fields
        private string _fileToCheck;
        private string _errorList;
        private readonly char _errorIndicationDelimiter = '|';
        private bool _caseSensitive = false;
        #endregion

        public string FileContent { get; private set; }


        #region Constructors
        public FileErrorChecker(string errorList, string fileToCheck)
        {
            _errorList = errorList;
            _fileToCheck = fileToCheck;
        }

        public FileErrorChecker(string errorList, string fileToCheck, char errorIndicationDelimiter) 
            : this(errorList, fileToCheck)
        {            
            _errorIndicationDelimiter = errorIndicationDelimiter;
        }

        public FileErrorChecker(string errorList, string fileToCheck, char errorIndicationDelimiter, bool caseSensitive) 
            : this(errorList, fileToCheck, errorIndicationDelimiter)
        {            
            _caseSensitive = caseSensitive;
        }
        #endregion

        public bool FileContainsErrors()
        {
            bool tempResult = false;
            FileContent = System.IO.File.ReadAllText(_fileToCheck);

            foreach (string err in _errorList.Split(_errorIndicationDelimiter))
            {
                if(_caseSensitive)
                {
                    tempResult = FileContent.Contains(err, StringComparison.CurrentCulture);
                }
                else
                {
                    tempResult = FileContent.Contains(err, StringComparison.CurrentCultureIgnoreCase);
                }

                if (tempResult)
                {
                    break;
                }
            }
            return tempResult;
        }
    }

    public static class ProcessHelper
    {
        public static string WrapQuotes(string inputString)
        {
            if (!inputString.StartsWith("\""))
            {
                inputString = "\"" + inputString;
            }

            if (!inputString.EndsWith("\""))
            {
                inputString = inputString + "\"";
            }        
            return inputString;
        }
    }

    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) != -1;
        }
    }    
}
