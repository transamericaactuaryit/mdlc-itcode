﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Domain.ServiceModels;

namespace EnablingSystems
{
    public interface IModelManagerHttpClient
    {
        Task<ModelRequest> AddModelRequest(ModelRequest modelRequest);
        Task<InitialModelLookups> GetInitialModelLookups(string modelApplicationName, string modeltypeName, string modelGroupName);
        Task<ModelRequest> GetModelRequest(int id);
        Task<Model> GetModel(int id);
        Task<IEnumerable<Model>> GetModelsByModelRequestId(int id);
        Task<Model> UpdateModel(Model model);
        Task<IEnumerable<Model>> GetModelUnprocessed();
        Task<IEnumerable<ModelGroup>> GetModelGroups();
        Task<IEnumerable<ModelType>> GetModelTypesByModelGroupId(int modelGroupId);
        Task<bool> UpdateModelRequestsStatusByStatus(ModelRequestStatusEnums statusToChange, ModelRequestStatusEnums newStatus);
        Task<bool> UpdateModelStatus(int id, ModelStatusEnums newStatus, DateTime? timeStamp);
        Task<Model> GetLatestModelByModelTypeId(int modelTypeId);
        Task<string> CreateDynamicOnDemandConfiguration(OnDemandRun onDemandRun);
    }
}