﻿using Domain;
using Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace EnablingSystems
{
    public class ModelManagerHttpClient : IModelManagerHttpClient
    {
        private readonly HttpClient _httpClient;

        public ModelManagerHttpClient(HttpClient httpClient, Uri baseAddress)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = baseAddress;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<InitialModelLookups> GetInitialModelLookups(string modelApplicationName, string modeltypeName, string modelGroupName)
        {
            var path = $"api/model/initialmodellookups/modelapplication/{modelApplicationName}/modeltype/{modeltypeName}/modelgroup/{Uri.EscapeDataString(modelGroupName)}";

            InitialModelLookups initialModelLookups = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                initialModelLookups = await response.Content.ReadAsAsync<InitialModelLookups>();
            }

            return initialModelLookups;
        }

        public async Task<ModelRequest> GetModelRequest(int id)
        {
            var path = $"api/ModelRequest/{id}";

            ModelRequest modelRequest = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                modelRequest = await response.Content.ReadAsAsync<ModelRequest>();
            }

            return modelRequest;
        }

        public async Task<IEnumerable<Model>> GetModelUnprocessed()
        {
            var path = $"api/Model/Unprocessed";

            IEnumerable<Model> models = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                models = await response.Content.ReadAsAsync<IEnumerable<Model>>();
            }

            return models;
        }

        public async Task<ModelRequest> AddModelRequest(ModelRequest modelRequest)
        {
            var path = $"api/modelrequest";

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync(path, modelRequest);
            response.EnsureSuccessStatusCode();

            modelRequest = await response.Content.ReadAsAsync<ModelRequest>();

            return modelRequest;
        }

        public async Task<Model> GetModel(int id)
        {
            var path = $"api/Model/{id}";

            Model model = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                model = await response.Content.ReadAsAsync<Model>();
            }

            return model;
        }

        public async Task<IEnumerable<Model>> GetModelsByModelRequestId(int id)
        {
            var path = $"api/Model/modelrequest/{id}";

            IEnumerable<Model> models = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                models = await response.Content.ReadAsAsync<IEnumerable<Model>>();
            }

            return models;
        }

        public async Task<Model> UpdateModel(Model model)
        {
            var path = $"api/model/Update";

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync(path, model);
            response.EnsureSuccessStatusCode();

            model = await response.Content.ReadAsAsync<Model>();

            return model;
        }

        public async Task<IEnumerable<ModelGroup>> GetModelGroups()
        {
            var path = $"api/modelgroups";

            IEnumerable<ModelGroup> modelGroups = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                modelGroups = await response.Content.ReadAsAsync<IEnumerable<ModelGroup>>();
            }

            return modelGroups;
        }

        public async Task<IEnumerable<ModelType>> GetModelTypesByModelGroupId(int modelGroupId)
        {
            var path = $"api/modelgroup/{modelGroupId}/modeltypes";

            IEnumerable<ModelType> modelTypes = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                modelTypes = await response.Content.ReadAsAsync<IEnumerable<ModelType>>();
            }

            return modelTypes;
        }

        public async Task<bool> UpdateModelRequestsStatusByStatus(ModelRequestStatusEnums statusToChange, ModelRequestStatusEnums newStatus)
        {
            var path = $"api/modelrequests/UpdateStatusByStatus/{(int)statusToChange}";

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync(path, newStatus);
            response.EnsureSuccessStatusCode();

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateModelStatus(int id, ModelStatusEnums newStatus, DateTime? timeStamp)
        {
            var path = $"api/model/{id}/UpdateStatus/{(int)newStatus}";

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync(path, timeStamp);
            response.EnsureSuccessStatusCode();

            return response.IsSuccessStatusCode;
        }

        public async Task<Model> GetLatestModelByModelTypeId(int modelTypeId)
        {
            var path = $"api/model/modeltype/{modelTypeId}";

            Model model = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                model = await response.Content.ReadAsAsync<Model>();
            }

            return model;
        }

        public async Task<string> CreateDynamicOnDemandConfiguration(OnDemandRun onDemandRun)
        {
            var path = $"api/model/ondemand/create";

            string onDemandConfigUrl = "";
            HttpResponseMessage response = await _httpClient.PostAsJsonAsync(path, onDemandRun);
            if (response.IsSuccessStatusCode)
            {
                onDemandConfigUrl = await response.Content.ReadAsAsync<string>();
            }

            return onDemandConfigUrl;
        }
    }
}
