﻿using EnablingSystems.ProcessInterfaces;
using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using Domain;

namespace EnablingSystems.OneMDLC.History
{
    //Note that this is not in use. This references tables
    //that had a bigger scheme of being able to re-run jobs
    public class DashboardSaveToDB : IDashboard
    {

        string _connectionInformation;
        string _dashboardID;
        DateTime _startTime;
        DateTime _endTime;
        int _jobInstanceID;
        int _periodID = -1;
        int _historyID;
        string _processMessage;
        string _processName;
        bool _success;

        public string ConnectionInformation
        {
            get
            {
                return _connectionInformation;
            }

            set
            {
                _connectionInformation = value;
            }
        }

        public string DashboardID
        {
            get
            {
                return _dashboardID;
            }

            set
            {
                _dashboardID = value;
            }
        }

        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }

            set
            {
                _startTime = value;
            }
        }
        public DateTime EndTime
        {
            get
            {
                return _endTime;
            }

            set
            {
                _endTime = value;
            }
        }

        public int JobInstanceID
        {
            get
            {
                return _jobInstanceID;
            }

            set
            {
                _jobInstanceID = value;
            }
        }

        public string ProcessMessage
        {
            get
            {
                return _processMessage;
            }

            set
            {
                _processMessage = value;
            }
        }

        public string ProcessName
        {
            get
            {
                return _processName;
            }

            set
            {
                _processName = value;
            }
        }

        public bool Success
        {
            get
            {
                return _success;
            }

            set
            {
                _success = value;
            }
        }

        public int JobHistoryID
        {
            get
            {
                return _historyID;
            }
        }

        public void Save()
        {
            GetCurrentPeriodID();
            _historyID = AddJobHistoryGetID();
        }

        private void GetCurrentPeriodID()
        {
            using (SqlConnection connection = new SqlConnection(_connectionInformation))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT MPP.PeriodID FROM "
                            + "ModelProcessingPeriod MPP "
                            + "INNER JOIN ModelType MT "
                            + "ON MPP.ModelTypeID = MT.ModelTypeID "
                            + "INNER JOIN JobInstance JI "
                            + "on JI.ModelTypeID = MT.ModelTypeID "
                            + "where JobInstanceID = @JobInstanceID "
                            + "and MPP.IsCurrent = 1", connection))
                {
                    cmd.Parameters.AddWithValue("@JobInstanceID", _jobInstanceID);
                    connection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _periodID = reader.GetInt32(reader.GetOrdinal("PeriodID"));
                        }
                    }
                }
            }

            if(_periodID == -1)
            {
                throw new Exception("Could not find Period ID for JobInstanceID " + _jobInstanceID.ToString());
            }
        }

        private int AddJobHistoryGetID()
        {
            int modified = 0;
            using (SqlConnection sqlCon = new SqlConnection(_connectionInformation))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO JobHistory([PeriodID],"
                    + "[JobInstanceID],[DashboardText], [Success],"
                    + "[StartTime], [EndTime],"
                    + "[WhoSubmitted] , [Message] ) "
                    + "output INSERTED.JobHistoryID VALUES(@periodid,@JobInstanceID, @DashboardText, @Success, "
                    + "@StartTime, @EndTime,@WhoSubmitted, @Message  )", sqlCon))
                {
                    cmd.Parameters.AddWithValue("@periodid", _periodID);
                    cmd.Parameters.AddWithValue("@JobInstanceID", _jobInstanceID);
                    cmd.Parameters.AddWithValue("@DashboardText", _processName);
                    cmd.Parameters.AddWithValue("@Success", _success);
                    cmd.Parameters.AddWithValue("@StartTime", _startTime);
                    cmd.Parameters.AddWithValue("@EndTime", _endTime);
                    cmd.Parameters.AddWithValue("@WhoSubmitted", "System");
                    //TODO:property here..
                    cmd.Parameters.AddWithValue("@Message", "");
                    sqlCon.Open();

                    modified = (int)cmd.ExecuteScalar();                                    
                }
            }
            return modified;
        }
    }

    public class JobHistorySaveToDB : IJobHistory
    {
        private readonly Model _model;
        private string _connectionInformation;

        public JobHistorySaveToDB(Model model)
        {
            _model = model;
        }

        //ConnectionInformation will have the URL of the web api root, which will just be http://localhost:8020 unless we move the Web Api to another server. SJO 8/9/2018
        public string ConnectionInformation
        {
            set
            {
                _connectionInformation = value;
            }
        }

        public void SaveHistory(int jobHistoryID, NameValueCollection processHistoryItems)
        {
            foreach (string item in processHistoryItems)
            {
                SaveHistoryToDb(jobHistoryID, item, processHistoryItems[item]);
            }

        }

        private void SaveHistoryToDb(int jobHistoryID, string item, string itemValue)
        {
            //ELMAHLogger.LogError();
            //TODO: Richard            
        }
    }
}
