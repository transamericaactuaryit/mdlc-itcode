﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity.Configuration;
using Reporting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace EnablingSystems.Utilities
{



    class Program
    {
        class AppSettingAttributes
        {
            public string _xlogPath { get; set; }
            public string _xlogSource { get; set; }
            public string _xreportedEntriesLoc { get; set; }
            public string _xerrorCountFile { get; set; }
            public int _xmaxErrorsPerDay { get; set; }

        }

        static string _errorToEmailAddress;
        static string _errorEmailFromAddress;
        static string _emailServer;
        static void Main(string[] args)
        {
            _errorToEmailAddress = ConfigurationManager.AppSettings["ErrorEmailToAddress"] ?? "";
            _errorEmailFromAddress = ConfigurationManager.AppSettings["ErrorEmailFromAddress"] ?? "";
            _emailServer = ConfigurationManager.AppSettings["EmailServer"] ?? "";

            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");

            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            ExceptionManager exceptionManager = exceptionFactory.CreateManager();


            try
            {
               



                foreach (var item in retrieveAppSettings())
                {

                    //Console.WriteLine(item._xlogPath + " " + item._xlogSource);
                    //Console.WriteLine(item._reportedEntFileLocation);
                    //Console.WriteLine("---------------------------------------------------------------");
                    int currentErrCount = RefreshCurrentCount(item._xerrorCountFile);

                    if (currentErrCount < item._xmaxErrorsPerDay)
                    {
                        try
                        { 
                            int numErrorsFound = ScanEventLogsAndReport(item._xlogPath, item._xlogSource,
                                item._xreportedEntriesLoc, item._xmaxErrorsPerDay);
                            UpdateErrorsPerDay(item._xerrorCountFile, numErrorsFound);
                        }
                        catch(Exception ex)
                        {
                            exceptionManager.HandleException(ex, "MainExceptionPolicy");
                        }
                    }                                        
                }
            }
            catch (Exception ex)
            {
                string errorLog = $"Error in Event Log Scan -{Environment.MachineName} - {ex.Message}";
                ELMAHLogger.LogError(errorLog, "AlfaEventLogScan");
                
                if ((!string.IsNullOrEmpty(_errorToEmailAddress)) &&
                    (!string.IsNullOrEmpty(_errorEmailFromAddress)) &&
                    (!string.IsNullOrEmpty(_emailServer)))
                {
                    TAEmailer emailer = 
                        new TAEmailer(
                            _errorToEmailAddress, 
                            _errorEmailFromAddress,
                            string.Empty, 
                            $"Error in Event Log Scan -{Environment.MachineName}", 
                            ex.Message, 
                            false, 
                            _emailServer);
                    emailer.SendEmail();
                }

               
            }
        }


        private static List<AppSettingAttributes> retrieveAppSettings()
        {

            //linq code used to loop through AppConfig.Settings.
            string[] xPaths = ConfigurationManager.AppSettings.AllKeys
                           .Where(key => key.StartsWith("logXPath"))
                           .Select(key => ConfigurationManager.AppSettings[key])
                           .ToArray();

            string[] xSource = ConfigurationManager.AppSettings.AllKeys
                             .Where(key => key.StartsWith("logSource"))
                             .Select(key => ConfigurationManager.AppSettings[key])
                             .ToArray();

            string[] xReportedEntriesFileLoc = ConfigurationManager.AppSettings.AllKeys
                             .Where(key => key.StartsWith("reportedEntriesFileLocation"))
                             .Select(key => ConfigurationManager.AppSettings[key])
                             .ToArray();

            string[] xErrorCountFile = ConfigurationManager.AppSettings.AllKeys
                             .Where(key => key.StartsWith("ErrorCountFile"))
                             .Select(key => ConfigurationManager.AppSettings[key])
                             .ToArray();

            string[] xMaxErrorsPerDay = ConfigurationManager.AppSettings.AllKeys
                             .Where(key => key.StartsWith("MaximumErrorsPerDay"))
                             .Select(key => ConfigurationManager.AppSettings[key])
                             .ToArray();


            List<AppSettingAttributes> output1 = (from n1 in xPaths.Select((item, index) => new { item, index })
                                                  join n2 in xSource.Select((item, index) => new { item, index }) on n1.index equals n2.index
                                                  join n3 in xReportedEntriesFileLoc.Select((item, index) => new { item, index }) on n2.index equals n3.index
                                                  join n4 in xErrorCountFile.Select((item, index) => new { item, index }) on n3.index equals n4.index
                                                  join n5 in xMaxErrorsPerDay.Select((item, index) => new { item, index }) on n4.index equals n5.index
                                                  select new AppSettingAttributes
                                                  {
                                                      _xlogPath = n1.item,
                                                      _xlogSource = n2.item,
                                                      _xreportedEntriesLoc = n3.item,
                                                      _xerrorCountFile = n4.item,
                                                      _xmaxErrorsPerDay = int.Parse(n5.item)
                                                  }
                           ).ToList<AppSettingAttributes>();

            return output1;
        }

        private static int ScanEventLogsAndReport(string logXPath, string logSource,
            string reportedEntriesFileLocation, int maxErrorsPerDay)
        {
            int numErrors = 0;
            EventLogScan thisScan = new EventLogScan(logXPath, logSource, reportedEntriesFileLocation);
            thisScan.QueryEventLog();
            
            foreach (var thisItem in thisScan.EventList)
            {
                numErrors++;
                if (numErrors <= maxErrorsPerDay)
                {
                    string errorLog = $@"Level: {thisItem.Level}\r\n
                            Log Name: {thisItem.LogName}\r\n
                            Machine: {thisItem.MachineName}\r\n
                            Level: {thisItem.Level.ToString()}\r\n
                            Event ID: {thisItem.Id.ToString()}\r\n
                            Description: {thisItem.FormatDescription()}\r\n
                            Details: {thisItem.ToXml()}\r\n";

                    ELMAHLogger.LogError(errorLog, "AlfaEventLogScan");

                    if ((!string.IsNullOrEmpty(_errorToEmailAddress)) &&
                        (!string.IsNullOrEmpty(_errorEmailFromAddress)) &&
                        (!string.IsNullOrEmpty(_emailServer)))
                    {                                                                        
                        TAEmailer emailer = 
                            new TAEmailer(
                                _errorToEmailAddress, 
                                _errorEmailFromAddress,
                                string.Empty, 
                                $"Event Log Item Found - {Environment.MachineName}",
                                errorLog, 
                                false, 
                                _emailServer);
                        emailer.SendEmail();
                    }
                }
            }
            return numErrors;
        }


        private static void SendMaxNotification(string logXPath, string logSource, int maxErrors)
        {
            string message = "Encountered the maximum errros per day.  "+
                            "No more notifications will be sent today\r\n"+
                            $"Log source: {logSource}.\r\n"+
                            $"Log Query: {logXPath}.\r\n"+
                            $"Max errors per day: {maxErrors.ToString()}.\r\n"+
                            $"Environment: {Environment.MachineName}";

            ELMAHLogger.LogError(message, "AlfaEventLogScan");

            if ((!string.IsNullOrEmpty(_errorToEmailAddress)) &&
                (!string.IsNullOrEmpty(_errorEmailFromAddress)) &&
                (!string.IsNullOrEmpty(_emailServer)))
            {                                
                TAEmailer emailer = 
                    new TAEmailer(
                        _errorToEmailAddress, 
                        _errorEmailFromAddress,
                        string.Empty, 
                        $"Error in Event Log Scan - {Environment.MachineName}", message, false, _emailServer);
                emailer.SendEmail();
            }
        }
        private static void UpdateErrorsPerDay(string errorCountStateFile, int numErr)
        {
            int currErrorCount = 0;

            string fileDir = Path.GetDirectoryName(errorCountStateFile);

            //check to see if directories exist.  Create if they don't
            if (Directory.Exists(fileDir) == false)
                Directory.CreateDirectory(fileDir);

            if (!File.Exists(errorCountStateFile))
            {
                using (StreamWriter fileWrite =
                    new System.IO.StreamWriter(errorCountStateFile))
                {
                    fileWrite.WriteLine(numErr.ToString(numErr.ToString()));
                }
            }
            else
            {
                using (StreamReader file =
                        new System.IO.StreamReader(errorCountStateFile))
                {

                    if (int.TryParse(file.ReadLine(), out currErrorCount))
                    {
                        numErr = numErr + currErrorCount;
                    }
                }

                using (StreamWriter fileWrite =
                        new System.IO.StreamWriter(errorCountStateFile))
                {
                    fileWrite.WriteLine(numErr.ToString(numErr.ToString()));
                }
            }

        }

        private static int RefreshCurrentCount(string errorFile)
        {
            int numErr = 0;

            string fileDir = Path.GetDirectoryName(errorFile);

            //check to see if directories exist.  Create if they don't
            if (Directory.Exists(fileDir) == false)
                Directory.CreateDirectory(fileDir);

            if (!File.Exists(errorFile))
            {
                using (StreamWriter fileWrite =
                    new System.IO.StreamWriter(errorFile))
                {
                    fileWrite.WriteLine(numErr.ToString(numErr.ToString()));
                }
            }
            else
            {
                if (DateTime.Now.Day != File.GetLastWriteTime(errorFile).Day)
                {
                    File.WriteAllText(errorFile, "0");
                }
            }

            using (StreamReader file =
                     new System.IO.StreamReader(errorFile))
            {
                if (int.TryParse(file.ReadLine(), out numErr))
                {
                    return numErr;
                }
            }

            return numErr;
        }
    }
}
