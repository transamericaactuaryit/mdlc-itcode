﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using EnablingSystems.Utilities;
using EnablingSystems.Processes;
using System.Globalization;

namespace EnablingSystems.MDLC.Processes
{
    class Program
    {
        static void Main(string[] args)
        {
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            NameValueCollection settingsCollection = new NameValueCollection();
            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            ExceptionManager exceptionManager = exceptionFactory.CreateManager();

            string environment = DetermineEnvironment(args);
            LoadParams(environment, settingsCollection);

            string _errorToEmailAddress = settingsCollection["ErrorEmailToAddress"] ?? "";
            string _errorEmailFromAddress = settingsCollection["ErrorEmailFromAddress"] ?? "";
            string _emailServer = settingsCollection["EmailServer"] ?? "";
            string _environment = settingsCollection["Environment"] ?? "DEV";
            string _metaConfigSwitchLocation = settingsCollection["MetaConfigSwitchLocation"] ?? @"E:\MgAlfa\OnDemandSwitchFiles";

            string _jobAggregatorLocation = settingsCollection["JobAggregatorLocation"]
        ?? @"E:\AutomationCode\General\Job_Aggregator\AggregatorHost.exe";


            string _sensOnDemandFileLocation = settingsCollection["SensOnDemandFileLocation"]
                    ?? @"C:\Temp2\OnDemand";
            string _sensArchiveOnDemandFileLocation = settingsCollection["SensArchiveOnDemandFileLocation"]
                    ?? @"C:\Temp2\OnDemand\Archive";

            string _nonsensOnDemandFileLocation = settingsCollection["NonSensOnDemandFileLocation"]
                   ?? @"C:\Temp2\OnDemand";
            string _nonsenseArchiveOnDemandFileLocation = settingsCollection["NonSensArchiveOnDemandFileLocation"]
                    ?? @"C:\Temp2\OnDemand\Archive";
            string onDemandWorkAreaPath = settingsCollection["OnDemandWorkArea"];

            string onDemandFile = "";

            try
            {

                onDemandFile = args[1].ToUpper().Trim();


                //Here, we need to do a check to see if file is Sensitivity or Non-Sensitivity.  Call
                //whichever method based on answer.
                string fileType = WhichFileType(onDemandFile, _sensOnDemandFileLocation, _nonsensOnDemandFileLocation).ToString();


                switch (fileType)
                {
                    case "S": //Signifies file as Sensitivity
                        OnDemandModelRun(onDemandFile, _sensOnDemandFileLocation, _sensArchiveOnDemandFileLocation, onDemandWorkAreaPath, _metaConfigSwitchLocation, environment);
                        break;
                    case "N": //Signifies file as Non-Sensitivity
                        OnDemandModelRun(onDemandFile, _nonsensOnDemandFileLocation, _nonsenseArchiveOnDemandFileLocation, onDemandWorkAreaPath, _metaConfigSwitchLocation, environment);
                        break;
                    case "":
                        throw new System.InvalidOperationException("OnDemand file is neither a Sensitivity or NonSensitivity.");
                        
                    default:
                        break;
                }
                

            }

            catch (Exception ex)
            {
                exceptionManager.HandleException(ex, "MainExceptionPolicy");

                //Anything other than 0 will tell scheduler that something went wrong
                Environment.Exit(-1);
            }

            Environment.Exit(0);

        }

        private static void ArchiveFile(string fileToArchive, string archiveFileLocation)
        {
            if(!archiveFileLocation.EndsWith(@"\"))
            {
                archiveFileLocation += @"\";
            }

            //check to see if directories exist.  Create if they don't
            if (!Directory.Exists(archiveFileLocation))
            {
                Directory.CreateDirectory(archiveFileLocation);
            }

            if (!string.IsNullOrEmpty(archiveFileLocation))
            {
                string archiveFileName = string.Concat(
                     archiveFileLocation,
                     Path.GetFileNameWithoutExtension(fileToArchive),
                     DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                     Path.GetExtension(fileToArchive)
                    );
                File.Move(fileToArchive, archiveFileName);
            }
            else
            {
                File.Delete(fileToArchive);
            }
        }


        private static void OnDemandModelRun(string onDemandFile, string onDemandFileLocation, string archiveOnDemandFileLocation, 
            string onDemandWorkArea, string switchFileLocation, string environment)
        {
            if (!onDemandFileLocation.EndsWith(@"\"))
            {
                onDemandFileLocation += @"\";
            }

            if (!onDemandWorkArea.EndsWith(@"\"))
            {
                onDemandWorkArea += @"\";
            }

            if (!onDemandWorkArea.EndsWith(@"\"))
            {
                onDemandWorkArea += @"\";
            }

            string fileDir = Path.GetDirectoryName(onDemandWorkArea);

            System.IO.FileInfo file = new System.IO.FileInfo(onDemandWorkArea);

            //check to see if directories exist.  Create if they don't
            if (Directory.Exists(fileDir) == false)
                file.Directory.Create();

            OnDemandFileParse onDemandParse = new OnDemandFileParse(onDemandFileLocation + onDemandFile, onDemandWorkArea);
            onDemandParse.MetaConfigSwitcherDirectory = switchFileLocation;
            onDemandParse.Execute();

            ArchiveFile(onDemandFileLocation + onDemandFile, archiveOnDemandFileLocation);

            //this should be the "pre" file, not post
            string metaConfigToRun = onDemandParse.MetaConfigFileToUpdate;

            AlfaProcessAggregator alfaProcessAggregator = new AlfaProcessAggregator();
            alfaProcessAggregator.MetaConfigFile = metaConfigToRun;

            alfaProcessAggregator.Environment = environment;
            alfaProcessAggregator.SetupAutomations();
            alfaProcessAggregator.RunAutomations();
        }

        private static string WhichFileType(string onDemandFile, string _sensFileLocation, string _nonsensFileLocation)
        {
            string indicatorType = "";

            if (!_sensFileLocation.EndsWith(@"\"))
            {
                _sensFileLocation += @"\";
            }

            bool sensitivity = File.Exists(_sensFileLocation + onDemandFile);

            if (!_nonsensFileLocation.EndsWith(@"\"))
            {
                _nonsensFileLocation += @"\";
            }

            bool nonsensitivity = File.Exists(_nonsensFileLocation + onDemandFile);

            if(sensitivity == true)
            {
                indicatorType = "S";
            }
            else if(nonsensitivity == true)
            {
                indicatorType = "N";
            }
            else
            {
                indicatorType = "";
            }

            return indicatorType;
        }


        private static void LoadParams(string environment, NameValueCollection settingsCollection)
        {

            foreach (string key in ConfigurationManager.AppSettings)
            {
                if (!key.Contains("Dashboard"))
                {
                    string settingValue = ConfigurationManager.AppSettings[key];

                    string keyUpper = key.ToUpper();
                    //check if it's a configuration depenedent 
                    //on environment
                    if ((keyUpper.Contains("DEV_")) ||
                        (keyUpper.Contains("INT_")) ||
                        (keyUpper.Contains("PROD_"))||
                        (keyUpper.Contains("TEST_")))
                    {
                        string settingEnv = keyUpper.Split('_')[0];

                        if (settingEnv == environment)
                        {
                            //strip out the environment declaration of the config key,
                            //as the process should not have any knowledge of environment
                            string newKey = key.Replace(settingEnv + "_", "");

                            if (settingsCollection[newKey] == null)
                            {
                                settingsCollection.Add(newKey, settingValue);
                            }
                        }
                        else
                        {
                            if(settingsCollection[key] == null)
                            {
                                settingsCollection.Add(key, settingValue);
                            }
                        }
                    }
                    else
                    {
                        if (settingsCollection[key] == null)
                        {
                            settingsCollection.Add(key, settingValue);
                        }
                    }
                }
            }

        }


        private static string DetermineEnvironment(string[] args)
        {
            string env = "DEV";
            if (args.Length > 0)
            {
                env = args[0].ToUpper(CultureInfo.InvariantCulture).Trim();

                if ((env == "PROD") ||
                    (env == "INT") ||
                    (env == "DEV") ||
                    (env == "TEST"))

                {
                    return env;
                }
            }
            return env;
        }
    }
}
