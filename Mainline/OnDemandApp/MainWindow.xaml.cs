﻿using Domain;
using Domain.ServiceModels;
using EnablingSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Deployment.Application;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace OnDemandApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// Note: this is a interim solution so I kept most of the logic in here rather than in specific classes. SJO 9/10
    /// </summary>
    public partial class MainWindow : Window
    {
        private ModelManagerHttpClient _modelManagerHttpClient;
        private ModelGroup _selectedModelGroup;
        private bool _inputRequired;

        public ObservableCollection<EnvironmentUrls> Environments { get; set; }
        public ObservableCollection<ModelGroup> ModelGroups { get; set; }
        public ObservableCollection<ModelType> Sensitivities { get; set; }
        public ObservableCollection<ModelType> NonSensitivities { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            SetVersion();
            LoadEnvironments();
            LoadOffsets();

            StatusBarDisplayMessage($"Welcome {Environment.UserName}");
        }

        private void SetVersion()
        {
            var versionNumber = ApplicationDeployment.IsNetworkDeployed
                ? ApplicationDeployment.CurrentDeployment.CurrentVersion
                : Assembly.GetExecutingAssembly().GetName().Version;

            VersionLabel.Content = $"Version: {versionNumber}";
        }

        private void LoadEnvironments()
        {
            var appSettingIdentifier = "_EnvironmentUrl";
            var environments = new ObservableCollection<EnvironmentUrls>();
            foreach (var key in ConfigurationManager.AppSettings.AllKeys.Where(s => s.Contains(appSettingIdentifier)))
            {
                environments.Add(new EnvironmentUrls
                {
                    Name = key.Replace(appSettingIdentifier, ""),
                    URL = ConfigurationManager.AppSettings.Get(key)
                });
            }

            EnvironmentControl.ItemsSource = environments;
        }

        private async void LoadModelGroups(EnvironmentUrls environmentUrl)
        {
            try
            {
                _modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), new Uri(environmentUrl.URL));
                var modelGroups = await _modelManagerHttpClient.GetModelGroups();
                ModelGroups = new ObservableCollection<ModelGroup>(modelGroups.OrderBy(m => m.Name));

                ModelGroupControl.ItemsSource = ModelGroups;
            }
            catch (HttpRequestException ex)
            {
                StatusBarDisplayMessage($"ERROR: Unable to connect to the WebAPI at {environmentUrl.URL}", StatusBarMessageType.Error);
            }
            catch (Exception ex)
            {
                StatusBarDisplayMessage($"Exception: {ex.Message}", StatusBarMessageType.Error);
            }
        }

        private void LoadOffsets()
        {
            MonthOffsetControl.ItemsSource = new ObservableCollection<int>
            {
                -12,
                -11,
                -10,
                -9,
                -8,
                -7,
                -6,
                -5,
                -4,
                -3,
                -2,
                -1,
                0,
                1,
                2,
                3
            };

            ValdateOffsetControl.ItemsSource = new ObservableCollection<int>
            {
                -12,
                -11,
                -10,
                -9,
                -8,
                -7,
                -6,
                -5,
                -4,
                -3,
                -2,
                -1,
                0
            };
        }

        private async void LoadModelTypes(ModelGroup modelGroup)
        {
            _selectedModelGroup = modelGroup;

            var modelTypes = await _modelManagerHttpClient.GetModelTypesByModelGroupId(modelGroup.ID);

            Sensitivities = new ObservableCollection<ModelType>(modelTypes.Where(m => m.Tags == "Sensitivity").OrderBy(m => m.Name));
            NonSensitivities = new ObservableCollection<ModelType>(modelTypes.Where(m => m.Tags == "NonSensitivity").OrderBy(m => m.Name));

            //Binding here as binding via XAML did not work, may have time to figure out later.  
            SensitivitiesControl.ItemsSource = Sensitivities;
            NonSensitivitiesControl.ItemsSource = NonSensitivities;
        }

        private void StatusBarDisplayMessage(string message, StatusBarMessageType messageType = StatusBarMessageType.Default)
        {
            Brush messageBrush;

            switch (messageType)
            {
                case StatusBarMessageType.Message:
                    messageBrush = Brushes.DarkBlue;
                    break;
                case StatusBarMessageType.Error:
                    messageBrush = Brushes.Maroon;
                    break;
                case StatusBarMessageType.Default:
                default:
                    messageBrush = Brushes.Black;
                    break;
            }

            StatusBarMessage.Foreground = messageBrush;
            StatusBarMessage.Text = message;
        }

        private void ClearAllControls(bool clearCollections = false)
        {
            if (clearCollections)
            {
                if (ModelGroups != null) ModelGroups.Clear();
                if (Sensitivities != null) Sensitivities.Clear();
                if (NonSensitivities != null) NonSensitivities.Clear();
            }

            ClearModelSelections();
            RunDefinitionControl.Text = "";
            MonthOffsetControl.SelectedIndex = -1;
            ValdateOffsetControl.SelectedIndex = -1;
            PostRunOnlyControl.IsChecked = false;
            EmergencyRunControl.IsChecked = false;
        }

        private void ClearModelSelections()
        {
            SensitivitiesControl.UnselectAll();
            NonSensitivitiesControl.UnselectAll();
        }

        private async Task<bool> ModelCreationValidation(List<Model> models, ModelType modelType)
        {
            Model model = await _modelManagerHttpClient.GetLatestModelByModelTypeId(modelType.ID);
            //if no entries for this model type exists in the queue then create one.
            if (model == null)
            {
                if (_inputRequired)
                {
                    var ondemandConfigLocation = await GenerateXML(modelType);
                    if (ondemandConfigLocation != "")
                    {
                        AddModel(models, modelType, ondemandConfigLocation);
                        return true;
                    }
                }
                else
                {
                    AddModel(models, modelType, "");
                }

                return false;
            }

            MessageBoxResult userResponse;

            switch (model.ModelStatusID)
            {
                case (int)ModelStatusEnums.NotStarted:
                case (int)ModelStatusEnums.InQueue:
                    var modelRequest = await _modelManagerHttpClient.GetModelRequest(model.ModelRequestID);
                    if (modelRequest.CreatedBy == Environment.UserName)
                    {
                        if (_inputRequired)
                        {
                            //do you want to overwrite?
                            userResponse = MessageBox.Show($"There is already a model, {modelType.Name}, in the queue that you created. Do you wish to overwrite the inputs?",
                                                            "Override Existing Model Inputs?",
                                                            MessageBoxButton.YesNo);

                            if (userResponse == MessageBoxResult.Yes)
                            {
                                //Since the job has not executed yet we can overwrite the xml.
                                var onDemandUrl = await GenerateXML(modelType);
                                return onDemandUrl != "";
                            }
                            else if (userResponse == MessageBoxResult.No)
                                return false;
                        }
                        else
                        {
                            userResponse = MessageBox.Show($"There is already a model, {modelType.Name}, in the queue that you created. Try again later.",
                                "Override Existing Model Inputs?",
                                MessageBoxButton.OK);
                            if (userResponse == MessageBoxResult.OK)
                                return false;
                        }
                    }

                    userResponse = MessageBox.Show($"The model, {modelType.Name}, has been submitted by another user, {modelRequest.CreatedBy}, and is awaiting execution. Try again later.", "Model Has Already Been Submitted.", MessageBoxButton.OK);
                    if (userResponse == MessageBoxResult.OK)
                        return false;

                    break;
                case (int)ModelStatusEnums.Executing:
                    userResponse = MessageBox.Show($"The model, {modelType.Name}, is currently being executed and cannot be added to queue at this time. Try again later.", "Model Is Currently Executing.", MessageBoxButton.OK);
                    if (userResponse == MessageBoxResult.OK)
                        return false;

                    break;
                default:
                    var ondemandConfigLocation = "";
                    if (_inputRequired)
                    {
                        ondemandConfigLocation = await GenerateXML(modelType);
                        if (ondemandConfigLocation == "") return false;
                    }

                    AddModel(models, modelType, ondemandConfigLocation);

                    break;
            }

            return true;
        }

        private async Task<string> GenerateXML(ModelType modelType)
        {
            var ondemandRun = new OnDemandRun
            {
                Environment = (EnvironmentControl.SelectedItem as EnvironmentUrls).Name,
                Configuration = modelType.ConfigurationFile,
                RunDefinition = RunDefinitionControl.Text,
                ProcessTimeToMonthOffset = MonthOffsetControl.SelectedItem.ToString(),
                ProcessTimeToMonthOffsetForValDate = ValdateOffsetControl.SelectedItem != null ? ValdateOffsetControl.SelectedItem.ToString() : "",
                SubmittedBy = Environment.UserName.ToString(),
                SubmittedDate = DateTime.Now
            };

            var ondemandConfigLocation = await _modelManagerHttpClient.CreateDynamicOnDemandConfiguration(ondemandRun);
            if (ondemandConfigLocation.Contains("Error:"))
            {
                var userResponse = MessageBox.Show($"There was an error with, {modelType.Name}, could not create the OnDemand Config file. Please contact support.", "Model Error!.", MessageBoxButton.OK);
                if (userResponse == MessageBoxResult.OK)
                    return "";
            }

            return ondemandConfigLocation;
        }

        private void AddModel(List<Model> models, ModelType modelType, string ondemandConfigLocation)
        {
            var model = new Model
            {
                Name = modelType.Name,
                CreatedDate = DateTime.Now,
                Priority = EmergencyRunControl.IsChecked.Value ? 1 : 999,
                PostOnlyRun = PostRunOnlyControl.IsChecked,
                ModelApplicationID = (int)ModelApplicationEnums.ALFA,
                ModelStatusID = (int)ModelStatusEnums.NotStarted,
                ModelTypeID = modelType.ID
            };

            if (!string.IsNullOrEmpty(ondemandConfigLocation))
                model.OnDemandConfigurationFile = ondemandConfigLocation;

            models.Add(model);
        }

        private async void CreateModelRequest(List<Model> models)
        {
            var modelRequest = new ModelRequest
            {
                Name = $"{_selectedModelGroup.Name} - {Environment.UserName}", //may get this from UI as user input. SJO 7/17
                CreatedBy = Environment.UserName,
                CreatedDate = DateTime.Now,
                ModelRequestStatusID = (int)ModelRequestStatusEnums.NotStarted,
                Models = models
            };

            ClearAllControls();

            StatusBarDisplayMessage($"Saving your Model Request...");

            var updatedModelRequest = await _modelManagerHttpClient.AddModelRequest(modelRequest);

            StatusBarDisplayMessage($"Model Request Saved! Your Model Request ID is {updatedModelRequest.ID}");

        }

        private bool Validate()
        {
            var isValid = true;
            var message = "";

            if (EnvironmentControl.SelectedItem == null)
            {
                message += "Environment. \n";
                isValid = false;
            }

            if (ModelGroupControl.SelectedItem == null)
            {
                message += "Model Group. \n";
                isValid = false;
            }

            if (SensitivitiesControl.SelectedItem == null && NonSensitivitiesControl.SelectedItem == null)
            {
                message += "Both Sensitivities and Non Sensitivities cannot be blank. \n";
                isValid = false;
            }

            if (!string.IsNullOrEmpty(RunDefinitionControl.Text) &&
                !Regex.IsMatch(RunDefinitionControl.Text, @"[0-9\-\,]"))
            {
                message += "Run Definition: Please enter some combination of numbers, commas, and dashes. \n";
                isValid = false;
            }

            if (MonthOffsetControl.SelectedItem == null)
            {
                message += "Month Offset cannot be blank. \n";
                isValid = false;
            }

            if (!(PostRunOnlyControl.IsChecked.HasValue ? PostRunOnlyControl.IsChecked.Value : false))
            {
                if (string.IsNullOrEmpty(RunDefinitionControl.Text))
                {
                    message += "Run Definition cannot be blank. \n";
                    isValid = false;
                }

                if (ValdateOffsetControl.SelectedItem == null)
                {
                    message += "Valdate Offset cannot be blank. \n";
                    isValid = false;
                }
            }

            if (!isValid)
            {
                MessageBox.Show(message, "Please Fix", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return isValid;
        }

        private void CheckForModelTypesThatDontRequireInput()
        {
            var statusBarMessage = "";
            if (SensitivitiesControl.SelectedItems.Cast<ModelType>().Any(m => m.Name.ToLower().Contains("_and_".ToLower())) ||
                NonSensitivitiesControl.SelectedItems.Cast<ModelType>().Any(m => m.Name.ToLower().Contains("_and_".ToLower())))
            {
                InputPanelControl.Visibility = Visibility.Hidden;
                _inputRequired = false;
                statusBarMessage = "TIP! You have selected a special Model Type that does not require inputs. " +
                                    "Deselect it if you require to run other Model Types instead.";
                DeselectAnyModelTypesThatRequireInput(SensitivitiesControl.SelectedItems);
                DeselectAnyModelTypesThatRequireInput(NonSensitivitiesControl.SelectedItems);
            }
            else
            {
                InputPanelControl.Visibility = Visibility.Visible;
                _inputRequired = true;
            }

            StatusBarDisplayMessage(statusBarMessage, StatusBarMessageType.Message);
        }

        private void DeselectAnyModelTypesThatRequireInput(IList selectedItems)
        {
            foreach (ModelType modelType in new ArrayList(selectedItems))
            {
                if (!modelType.Name.ToLower().Contains("_and_".ToLower()))
                {
                    selectedItems.Remove(modelType);
                }
            }
        }

        #region Events
        private void ClearSelectionsButton_Click(object sender, RoutedEventArgs e)
        {
            ClearModelSelections();
        }

        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            if (_inputRequired && !Validate()) return;

            var models = new List<Model>();

            foreach (ModelType modelType in SensitivitiesControl.SelectedItems)
            {
                await ModelCreationValidation(models, modelType);
            }

            foreach (ModelType modelType in NonSensitivitiesControl.SelectedItems)
            {
                await ModelCreationValidation(models, modelType);
            }

            if (models.Count != 0)
                CreateModelRequest(models);
            else
                StatusBarDisplayMessage($"Your Model Request was not saved.");
        }

        private void EnvironmentControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearAllControls(true);
            var environmentUrls = (sender as ComboBox).SelectedItem as EnvironmentUrls;
            LoadModelGroups(environmentUrls);
        }

        private void ModelGroupControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var modelGroup = (sender as ComboBox).SelectedItem as ModelGroup;
            if (modelGroup == null) return;

            LoadModelTypes(modelGroup);
        }

        private void SensitivitiesControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckForModelTypesThatDontRequireInput();
        }

        private void NonSensitivitiesControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckForModelTypesThatDontRequireInput();
        }

        #endregion
    }

    //Only used with in this interim app so leave here.
    public class EnvironmentUrls
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }

    internal enum StatusBarMessageType
    {
        Default,
        Message,
        Error
    }
}
