﻿using System;
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Dts.Runtime;
using EnablingSystems.MDLC.Processes;



namespace EnablingSystems.MDLC.Processes
{
    public class ETL : ProcessBaseWithHistory
    {
        //private NameValueCollection _processParams = new NameValueCollection();
                      
        SaveHistoryStatistics s = new SaveHistoryStatistics();
        List<PackageWarnings> warningsCol = new List<PackageWarnings>();

        //public NameValueCollection ProcessParams
        //{
        //    set
        //    {
        //        _processParams = value;
        //    }
        //    get
        //    {
        //        return _processParams;
        //    }
        //}

        

       
        public override void Execute()
        {
            
            string pkgLocation = _processParams["PackagePath"];
            string pkgResultsLogLocation = _processParams["LogFileLoc"];

            Package pkg;
            Application app;
            DTSExecResult pkgResults;
            Variables vars;
           

            MyEventListener eventListener = new MyEventListener();

            app = new Application();
            pkg = app.LoadPackage(pkgLocation, eventListener);

            vars = pkg.Variables;

           
            pkgResults = pkg.Execute(null, vars, eventListener, null, null);
            s.ExecutionDuration = pkg.ExecutionDuration;
            s.StartTime = pkg.StartTime;
            s.EndTime = pkg.StopTime;

            if (pkgResults == DTSExecResult.Success)
                s.PackageStatus = "Package ran successfully";
            else
               s.PackageStatus = "Package failed";

            

                if (pkg.Warnings.Count >= 2)
                {
                    
                PackageWarnings pkgWarning1 = new PackageWarnings()
                {
                    PkgWarningsCount = pkg.Warnings.Count,
                    WarningCode = pkg.Warnings[0].WarningCode.ToString(),
                    WarningDesc = pkg.Warnings[0].Description,
                    HelpFile = pkg.Warnings[0].HelpFile,
                    IdOfInterface = pkg.Warnings[0].IDOfInterfaceWithWarning
                };

                warningsCol.Add(pkgWarning1);
                
                }
                else
                    Console.WriteLine("Test that Warnings can be added to the collection, FAILED");


            SaveHistory(warningsCol,pkgResultsLogLocation);
        }

        public void SaveHistory(List<PackageWarnings> pw, string strLogWriteTo)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("*************Package Statistics****************");
            str.AppendLine("Start Time: " + s.StartTime.ToString());
            str.AppendLine("End Time: " + s.EndTime.ToString());
            str.AppendLine("Execution Duration: " + s.ExecutionDuration);
            str.AppendLine("Package Results: " + s.PackageStatus);

            //foreach(var row in pw)
            //{
            //    str.AppendLine("Package warnings count after: " + row.PkgWarningsCount.ToString());
            //    //    Console.WriteLine("Description {0}", package.Warnings[0].Description);
            //    //    Console.WriteLine("WarningCode {0}", package.Warnings[0].WarningCode);
            //    //    Console.WriteLine("HelpContext {0}", package.Warnings[0].HelpContext);
            //    //    Console.WriteLine("HelpFile {0}", package.Warnings[0].HelpFile);
            //    //    Console.WriteLine("IDOfInterfaceWithWarning {0}", package.Warnings[0].IDOfInterfaceWithWarning);
            //}

            using(StreamWriter wtr = new StreamWriter(strLogWriteTo))
            {
                wtr.Write(str);
            }
        }
    }

    public class MyEventListener : DefaultEvents
    {
        private StringBuilder _packageResults;

        public StringBuilder PackageResults
        {
            set
            {
                _packageResults = value;
            }
            get
            {
                return _packageResults;
            }
        }
        public override bool OnError(DtsObject source, int errorCode, string subComponent,
          string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            
            // Add application-specific diagnostics here.  
            PackageResults.AppendLine("Error in " + source + "/" + subComponent + ":" + description);
            return false;
        }
    }

    public class SaveHistoryStatistics
    {
        public int ExecutionDuration { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string PackageStatus { get; set; }
                
    }

    public class PackageWarnings
    {
        public int PkgWarningsCount { get; set; }
        public string WarningDesc { get; set; }
        public string WarningCode { get; set; }
        public string HelpFile { get; set; }
        public string IdOfInterface { get; set; }

         
    }
}
