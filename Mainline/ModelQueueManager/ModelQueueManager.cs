﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnablingSystems.ProcessInterfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;


namespace EnablingSystems.Processes
{
    public class QueueManager
    {
        private IModelAggregatorQuery _modelAggregatorQuery;
        private ExceptionManager _threadLaunchExceptionHandler;
        private LogSource _logSource;
        private static object _queueLock = new object();
        private static object _exceptionHandlerLock = new object();
        private AutoResetEvent _waitHandle = new AutoResetEvent(false);
        private string _exceptionPolicyName = "";
        public QueueManager(IModelAggregatorQuery modelAggregatorQuery,
            ExceptionManager exceptionManager, string exceptionPolicyName, LogSource logSource)
        {
            _modelAggregatorQuery = modelAggregatorQuery;
            _threadLaunchExceptionHandler = exceptionManager;
            _exceptionPolicyName = exceptionPolicyName;
            _logSource = logSource;
        }

        public void ProcessQueue()
        {
            foreach (IModelAggregator thisAggregator in _modelAggregatorQuery.ModelsToProcess)
            {
                ProcessModels(thisAggregator);
            }
        }

        private void ProcessModels(IModelAggregator thisAggregator)
        {
            List<Task> automationList = new List<Task>();

            if (thisAggregator.Queue.SlotAvailable())
            {
                //TODO: Verify model status labels
                var modelsToRun = thisAggregator.ModelStates
                    .Where(x => x.ModelStatus == "In Queue")
                    .OrderBy(p => p.ModelPriority).ToArray();

                if (modelsToRun.Count() > 0)
                {
                    for (int index = 0; index < modelsToRun.Count(); index++)
                    {

                        //Check again, as we're submitting models within this loop
                        if (thisAggregator.Queue.SlotAvailable())
                        {
                            var thisModel = modelsToRun[index];

                            thisModel.SetModelStatus("Processing");

                            lock (_queueLock)
                            {
                                thisAggregator.Queue.TakeSlot();
                            }

                            var thisAutomationTask = Task.Run(() => ProcessAutomation(thisModel, thisAggregator));
                            automationList.Add(thisAutomationTask);
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                Task.WaitAll(automationList.ToArray());
                //Notify Done here?
                //Maybe we don't want to do this..processing tied up for other queues, waiting for this set to complete
            }
        }

        private void ProcessAutomation(IModelState thisModel, IModelAggregator thisAggregator)
        {
            //TODO: Account for this
            // string switchLocation = ConfigurationManager.AppSettings["MetaConfigSwitchLocation"] ?? @"C:\MgAlfa\OnDemandSwitchFiles");
            string switchLocation = @"C:\MgAlfa\OnDemandSwitchFiles";
            try
            {
                AlfaProcessAggregator alfaProcessAggregator = new AlfaProcessAggregator(switchLocation);

                //TODO - resolve to XML
                alfaProcessAggregator.MetaConfigFile = thisModel.MetaConfigFile;
                alfaProcessAggregator.CheckForSwtich();
                //TODO: Make this Dynamic
                alfaProcessAggregator.Environment = "DEV";
                alfaProcessAggregator.SetupAutomations();
                alfaProcessAggregator.RunAutomations();

                lock (_queueLock)
                {
                    thisAggregator.Queue.ReleaseSlot();
                }

                _waitHandle.Set();

            }
            catch (Exception eX)
            {
                //it's possible that _threadLaunchExceptionHandler is null, 
                //if so, we want to throw
                bool exceptionHandled = false;

                thisModel.SetModelStatus("error");

                lock (_queueLock)
                {
                    thisAggregator.Queue.ReleaseSlot();
                }

                if (_threadLaunchExceptionHandler != null)
                {
                    lock (_exceptionHandlerLock)
                    {
                        ApplyOverrideEmailIfExists(thisAggregator);
                        _threadLaunchExceptionHandler.HandleException(eX, _exceptionPolicyName);
                    }
                    exceptionHandled = true;
                }

                _waitHandle.Set();

                if (!exceptionHandled)
                {
                    throw eX;
                }
            }


        }

        private void ApplyOverrideEmailIfExists(IModelAggregator thisAggregator)
        {
            if(!string.IsNullOrEmpty(thisAggregator.ErrorEmailAddress))
            {
                foreach (var thisListener in _logSource.Listeners)
                {
                    if (thisListener is TAEmailTraceListener)
                    {
                        ((TAEmailTraceListener)thisListener).OverrideToAddress = thisAggregator.ErrorEmailAddress;

                    }
                }
            }
        }
    }
}
