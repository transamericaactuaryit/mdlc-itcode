﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnablingSystems.Utilities;
using System.Configuration;
using System.IO;

namespace EnablingSystems.Utilities
{
    class Program
    {
        static void Main(string[] args)
        {
            string errorCountStateFile = ConfigurationManager.AppSettings["ErrorCountStateFile"] 
                ?? Directory.GetCurrentDirectory() + @"\ErrorCountPerDay.txt";
            int maxErrorsPerDay = int.Parse(ConfigurationManager.AppSettings["MaximumErrorsPerDay"]
                ?? "5");
            int maxDaysToKeepScreenshots =  int.Parse(ConfigurationManager.AppSettings["MaxDaysToKeepScreenshots"]
                ?? "7");
            string screenShotPath = ConfigurationManager.AppSettings["ScreenShotPath"] ?? "";
            string textToFindList = ConfigurationManager.AppSettings["TextToFindList"] ?? "";
            string lastErrorTimeFileLocation = ConfigurationManager.AppSettings["LastErrorTimeFileLocation"] ?? "";
            int notifyTimeSpanHours = int.Parse(ConfigurationManager.AppSettings["NotifyTimeSpanHours"] ?? "2");
            int notifyTimeSpanMinutes = int.Parse(ConfigurationManager.AppSettings["NotifyTimeSpanMinutes"] ?? "0");

            string errorToEmailAddress = ConfigurationManager.AppSettings["ErrorEmailToAddress"] ?? "";
            string errorEmailFromAddress = ConfigurationManager.AppSettings["ErrorEmailFromAddress"] ?? "";
            string emailSubject = ConfigurationManager.AppSettings["ErrorEmailSubject"] ?? "Error found";
            string errorEmailBody = ConfigurationManager.AppSettings["ErrorEmailBody"] ?? "Possible Error";
            string emailServer = ConfigurationManager.AppSettings["EmailServer"] ?? "";
            string environment = ConfigurationManager.AppSettings["Environment"] ?? "DEV";
            string tesseractDataPath = ConfigurationManager.AppSettings["TesseractDataPath"] ?? "";

            int currentErrCount = 0;
            try
            {
                currentErrCount = RefreshCurrentCount(errorCountStateFile);
            }
            catch
            {
                currentErrCount = 0;
            }
            

            if (!Directory.Exists(screenShotPath))
            {
                Directory.CreateDirectory(screenShotPath);
            }

            if (currentErrCount < maxErrorsPerDay)
            {
                errorEmailBody += "\r\nMachine Name: " + Environment.MachineName;
                DesktopTextAlert dta = new DesktopTextAlert(screenShotPath, textToFindList,
                    lastErrorTimeFileLocation, notifyTimeSpanHours, notifyTimeSpanMinutes,
                    emailSubject, errorEmailBody,
                    errorToEmailAddress, errorEmailFromAddress, emailServer, environment, tesseractDataPath);
                if (dta.ErrorsFound)
                {
                    try
                    {
                        IncrementErrorsPerDay(errorCountStateFile);
                    }
                    finally
                    {
                        Environment.Exit(-1);
                    }
                    
                }
                else
                {
                    try
                    {
                        DeleteOldScreenshots(screenShotPath, maxDaysToKeepScreenshots);
                    }
                    finally
                    {
                        Environment.Exit(0);
                    }
                    
                }
            }

        }

        private static void DeleteOldScreenshots(string screenShotPath, int maxDaysToKeepScreenshots)
        {
            string[] files = Directory.GetFiles(screenShotPath);
            maxDaysToKeepScreenshots = maxDaysToKeepScreenshots * -1;

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastWriteTime < DateTime.Now.AddDays(maxDaysToKeepScreenshots))
                    fi.Delete();
            }
        }

        private static void IncrementErrorsPerDay(string errorCountStateFile)
        {
            int numErr;
            using (StreamReader file =
                    new System.IO.StreamReader(errorCountStateFile))
            {

                if (int.TryParse(file.ReadLine(), out numErr))
                {
                    numErr++;
                }
            }

            using (StreamWriter fileWrite =
                    new System.IO.StreamWriter(errorCountStateFile))
            {
                fileWrite.WriteLine(numErr.ToString(numErr.ToString()));
            }
        }

        private static int RefreshCurrentCount(string errorFile)
        {
            int numErr = 0;

            if (!File.Exists(errorFile))
            {
                using (StreamWriter fileWrite =
                    new System.IO.StreamWriter(errorFile))
                {
                    fileWrite.WriteLine(numErr.ToString(numErr.ToString()));
                }
            }
            else
            {
                if (DateTime.Now.Day != File.GetLastWriteTime(errorFile).Day)
                {
                    using (StreamWriter fileWrite =
                        new System.IO.StreamWriter(errorFile))
                    {
                        fileWrite.WriteLine(numErr.ToString(numErr.ToString()));
                    }
                }
            }

            using (StreamReader file =
                     new System.IO.StreamReader(errorFile))
            {
                if (int.TryParse(file.ReadLine(), out numErr))
                {
                    return numErr;
                }
            }

            return numErr;
        }
    }
}
