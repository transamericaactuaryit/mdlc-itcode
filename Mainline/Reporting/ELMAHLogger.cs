﻿using Elmah;

namespace Reporting
{
    public static class ELMAHLogger
    {
        public static void LogError(string errorText, string appName)
        {
            Error e = new Error(new Elmah.ApplicationException(errorText))
            {
                ApplicationName = appName
            };
            
            ErrorLog.GetDefault(null).Log(e);
        }

        public static void LogModelError(int modelId, string errorText, string appName)
        {
            Error e = new Error(new Elmah.ApplicationException(errorText))
            {
                ApplicationName = appName
            };

            ErrorLog.GetDefault(null).Log(e);
        }
    }
}
