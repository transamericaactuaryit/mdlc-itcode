﻿using Domain;
using EnablingSystems.MDLC.Processes;
using EnablingSystems.Processes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace EnablingSystems
{
    public class ModelManagerPriorityQueueManager
    {
        private readonly IModelManagerHttpClient _modelManagerHttpClient;
        private readonly string _environment;
        private readonly int _maxConcurrent;
        private readonly string _onDemandWorkArea;
        private ExceptionManager _threadLaunchExceptionHandler;
        private readonly string _exceptionPolicyName;
        private LogSource _logSource;
        private BlockingCollection<KeyValuePair<int, Model>> _queue = new BlockingCollection<KeyValuePair<int, Model>>(new ConcurrentModelPriorityQueue<int, Model>());

        public ModelManagerPriorityQueueManager(IModelManagerHttpClient modelManagerHttpClient, 
            ExceptionManager exceptionManager,
            LogSource logSource,
            string exceptionPolicyName,
            string environment,
            int maxConcurrent,
            string onDemandWorkArea)
        {
            _modelManagerHttpClient = modelManagerHttpClient;
            _threadLaunchExceptionHandler = exceptionManager;
            _logSource = logSource;
            _exceptionPolicyName = exceptionPolicyName;
            _environment = environment;
            _maxConcurrent = maxConcurrent;
            _onDemandWorkArea = onDemandWorkArea;
        }

        public async Task Process()
        {
            ProduceQueue(new CancellationToken(false));

            await ConsumeQueue();
        }

        //The CancellationToken can be passed in if something goes wrong, since this is called from a window service
        //this will be on window service failing.
        public void ProduceQueue(CancellationToken token)
        {
            Task.Run(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    var models = _modelManagerHttpClient.GetModelUnprocessed().Result.ToList();

                    foreach (var model in models)
                    {
                        var updatedModel = _modelManagerHttpClient.UpdateModelStatus(model.ID, ModelStatusEnums.InQueue, null).Result;

                        var priority = model.Priority == 1 ? model.Priority : model.ModelType.Score;

                        _queue.Add(new KeyValuePair<int, Model>(priority, model));
                    }

                    //lets sleep for alittle each time we check for new items. 
                    Thread.Sleep(60000);  
                };
            });
        }

        private async Task ConsumeQueue()
        {
            var automationTasks = new List<Task>();

            while (true)
            {
                var foundOne = _queue.TryTake(out KeyValuePair<int, Model> queueItem);

                //Check for an item, if empty then it returns a -1.
                if (!foundOne) continue; 

                //This will end the wait time if the task runs quicker than wait time. 
                //Exceptions will stop the wait as well. 
                CancellationTokenSource ts = new CancellationTokenSource();

                var automationTask = Task.Run(() => {
                                                        ProcessAutomations(queueItem.Value);
                                                        ts.Cancel();                    
                                                    });
                //Wait for 1 hr so that the models can be processed in Alfa and in order based on model type score or priority. 
                //Cancellation token used if the process finishes earlier than the wait time.
                //Future enhancement would be to build in a checker for the Alfa temp files based on runs provided for a more accurate reading. SJO 7/25
                automationTask.Wait(3600000, ts.Token);
                automationTasks.Add(automationTask);

                //Ensures that the amount of models running at one time is restricted by the max concurrent number. 
                if (automationTasks.Count() == _maxConcurrent)
                {
                    Task finishedTask = await Task.WhenAny(automationTasks.ToArray());
                    automationTasks.Remove(finishedTask);
                }
            }
        }

        private void ProcessAutomations(Model model)
        {
            try
            {
                var updated = _modelManagerHttpClient.UpdateModelStatus(model.ID, ModelStatusEnums.Executing, DateTime.Now).Result;

                //not passing in as need a new instance each time since it can run on different threads
                var alfaProcessAggregator = new AlfaProcessAggregator(model);
                //TODO:Richard pass in httpContext as well



                if (string.IsNullOrEmpty(model.OnDemandConfigurationFile))
                {
                    alfaProcessAggregator.MetaConfigFile = model.ModelType.ConfigurationFile;
                }
                else
                {
                    var onDemandModelGroupWorkArea = _onDemandWorkArea.EndsWith(@"\") ? _onDemandWorkArea : _onDemandWorkArea + @"\";
                    onDemandModelGroupWorkArea = $"{onDemandModelGroupWorkArea}{model.ModelType.ModelGroup.Name.Replace(" ", "")}\\"; 
                    OnDemandFileParse onDemandParse = new OnDemandFileParse(model.OnDemandConfigurationFile, onDemandModelGroupWorkArea);
                    onDemandParse.Execute();

                    alfaProcessAggregator.MetaConfigFile = onDemandParse.MetaConfigFileToUpdate;
                }
                
                alfaProcessAggregator.PostModelOnly = model.PostOnlyRun.HasValue ? model.PostOnlyRun.Value : false;
                alfaProcessAggregator.Environment = _environment;
                alfaProcessAggregator.SetupAutomations();
                alfaProcessAggregator.RunAutomations();

                updated = _modelManagerHttpClient.UpdateModelStatus(model.ID, ModelStatusEnums.Complete, DateTime.Now).Result;
            }
            catch (Exception ex)
            {
                var updated = _modelManagerHttpClient.UpdateModelStatus(model.ID, ModelStatusEnums.Error, DateTime.Now).Result;

                if (_threadLaunchExceptionHandler != null)
                {
                   ApplyOverrideEmailIfExists(model.ModelType.ModelGroup.Email);
                   _threadLaunchExceptionHandler.HandleException(ex, _exceptionPolicyName);
                }
            }
        }

        private void ApplyOverrideEmailIfExists(string modelGroupEmail)
        {
            if (!string.IsNullOrEmpty(modelGroupEmail))
            {
                foreach (var thisListener in _logSource.Listeners)
                {
                    if (thisListener is TAEmailTraceListener)
                    {
                        ((TAEmailTraceListener)thisListener).OverrideToAddress = modelGroupEmail;
                    }
                }
            }
        }
    }
}
