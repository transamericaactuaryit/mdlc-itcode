﻿using Domain;
using EnablingSystems.MDLC.Processes;
using EnablingSystems.Processes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EnablingSystems
{
    //Not used but leaving here while we test the new queue
    public class ModelManagerDBQueueManager
    {
        private readonly IModelManagerHttpClient _modelManagerHttpClient;
        private readonly string _environment;
        private readonly int _maxConcurrent;
        private readonly string _onDemandWorkArea;
        private ExceptionManager _threadLaunchExceptionHandler;
        private readonly string _exceptionPolicyName;
        private LogSource _logSource;
        BlockingCollection<Model> _queue;

        public ModelManagerDBQueueManager(IModelManagerHttpClient modelManagerHttpClient, 
            ExceptionManager exceptionManager,
            LogSource logSource,
            string exceptionPolicyName,
            string environment,
            int maxConcurrent,
            string onDemandWorkArea)
        {
            _modelManagerHttpClient = modelManagerHttpClient;
            _threadLaunchExceptionHandler = exceptionManager;
            _logSource = logSource;
            _exceptionPolicyName = exceptionPolicyName;
            _environment = environment;
            _maxConcurrent = maxConcurrent;
            _onDemandWorkArea = onDemandWorkArea;
            _queue = new BlockingCollection<Model>(_maxConcurrent);
        }

        public async Task Process()
        {
            ProduceQueue(new CancellationToken(false));

            await ConsumeQueue();
        }

        private async Task ConsumeQueue()
        {
            var automationTasks = new List<Task>();

            //The _queue.GetConsumingEnumerable() is self blocking so it will block and wait if nothing is in the queue.
            foreach (var model in _queue.GetConsumingEnumerable())
            {
                //This will end the wait time if the task runs quicker than wait time. 
                //Exceptions will stop the wait as well. 
                CancellationTokenSource ts = new CancellationTokenSource();

                var automationTask = Task.Run(() => {
                                                        ProcessAutomations(model);
                                                        ts.Cancel();                    
                                                    });
                //Wait for 1 hr so that the models can be processed in Alfa and in order based on model type score or priority. 
                //Cancellation token used if the process finishes earlier than the wait time.
                //Future enhancement would be to build in a checker for the Alfa temp files based on runs provided for a more accurate reading. SJO 7/25
                automationTask.Wait(3600000, ts.Token);
                automationTasks.Add(automationTask);

                if (automationTasks.Count() == _maxConcurrent)
                {
                    Task finishedTask = await Task.WhenAny(automationTasks.ToArray());
                    automationTasks.Remove(finishedTask);
                }
            }
        }

        private void ProcessAutomations(Model model)
        {
            Model updatedModel = new Model();
            try
            {
                model.StartTime = DateTime.Now;
                model.ModelStatusID = (int)ModelStatusEnums.Executing;
                updatedModel = _modelManagerHttpClient.UpdateModel(model).Result;

                //not passing in as need a new instance each time since it can run on different threads
                var alfaProcessAggregator = new AlfaProcessAggregator(model);

                if (string.IsNullOrEmpty(model.OnDemandConfigurationFile))
                {
                    alfaProcessAggregator.MetaConfigFile = model.ModelType.ConfigurationFile;
                }
                else
                {
                    var onDemandModelGroupWorkArea = _onDemandWorkArea.EndsWith(@"\") ? _onDemandWorkArea : _onDemandWorkArea + @"\";
                    onDemandModelGroupWorkArea = $"{onDemandModelGroupWorkArea}{model.ModelType.ModelGroup.Name.Replace(" ", "")}\\"; 
                    OnDemandFileParse onDemandParse = new OnDemandFileParse(model.OnDemandConfigurationFile, onDemandModelGroupWorkArea);
                    onDemandParse.Execute();

                    alfaProcessAggregator.MetaConfigFile = onDemandParse.MetaConfigFileToUpdate;
                }
                
                alfaProcessAggregator.PostModelOnly = model.PostOnlyRun.HasValue ? model.PostOnlyRun.Value : false;
                alfaProcessAggregator.Environment = _environment;
                alfaProcessAggregator.SetupAutomations();
                alfaProcessAggregator.RunAutomations();

                updatedModel.CompletedTime = DateTime.Now;
                updatedModel.ModelStatusID = (int)ModelStatusEnums.Complete;
                updatedModel = _modelManagerHttpClient.UpdateModel(updatedModel).Result;
            }
            catch (Exception ex)
            {
                updatedModel.CompletedTime = DateTime.Now;
                updatedModel.ModelStatusID = (int)ModelStatusEnums.Error;
                updatedModel = _modelManagerHttpClient.UpdateModel(updatedModel).Result;

                if (_threadLaunchExceptionHandler != null)
                {
                   ApplyOverrideEmailIfExists(model.ModelType.ModelGroup.Email);
                   _threadLaunchExceptionHandler.HandleException(ex, _exceptionPolicyName);
                }
            }
        }

        //An upper bound is added to queue collection based on the max concurrent. This is self blocking  
        //so it will block on _queue.Add() until the consumer has taken a model from the queue.
        //The CancellationToken can be passed in if something goes wrong, since this is called from a window service
        //this will be on window service failing.
        public void ProduceQueue(CancellationToken token)
        {
            Task.Run(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    var models = _modelManagerHttpClient.GetModelUnprocessed().Result.ToList();

                    foreach (var model in models)
                    {
                        model.ModelStatusID = (int)ModelStatusEnums.InQueue;
                        var updatedModel = _modelManagerHttpClient.UpdateModel(model).Result;

                        _queue.Add(model); //self blocking if upper bound is reached.
                    }

                };
            });
        }

        private void ApplyOverrideEmailIfExists(string modelGroupEmail)
        {
            if (!string.IsNullOrEmpty(modelGroupEmail))
            {
                foreach (var thisListener in _logSource.Listeners)
                {
                    if (thisListener is TAEmailTraceListener)
                    {
                        ((TAEmailTraceListener)thisListener).OverrideToAddress = modelGroupEmail;
                    }
                }
            }
        }
    }
}
