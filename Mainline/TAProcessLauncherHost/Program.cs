﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace EnablingSystems.MDLC.Processes
{
    class Program
    {
        static void Main(string[] args)
        {
            //DetermineEnvironment(args);
            string environment = DetermineEnvironment(args);
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            IUnityContainer container = new UnityContainer().LoadConfiguration(section, "main");

            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);
            container.RegisterInstance(logWriter);

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            ExceptionManager exceptionManager = exceptionFactory.CreateManager();

            try
            {
                var thisDashboard =
                    container.Resolve<EnablingSystems.ProcessInterfaces.IDashboard>("ConcreteDashboard");

                var thisHistory =
                    container.Resolve<EnablingSystems.ProcessInterfaces.IJobHistory>("ConcreteJobHistory");

                var thisProcess =
                    container.Resolve<EnablingSystems.ProcessInterfaces.IProcess>("ConcreteProcess");

                var thisProcessLauncher =
                    container.Resolve<EnablingSystems.ProcessInterfaces.IProcessLauncher>("ConcreteProcessLauncher");

                thisProcess.JobHistory = thisHistory;

                thisDashboard.DashboardID = ConfigurationManager.AppSettings["DashboardID"];
                thisDashboard.ProcessName = ConfigurationManager.AppSettings["DashboardProcessName"];

                thisProcessLauncher.Environment = environment;
                thisProcessLauncher.Dashboard = thisDashboard;
                thisProcessLauncher.Process = thisProcess;
                thisProcessLauncher.RunProcess();


            }
            catch (Exception ex)
            {
                exceptionManager.HandleException(ex, "MainExceptionPolicy");

                //Anything other than 0 will tell scheduler that something went wrong
                Environment.Exit(-1);
            }

           Environment.Exit(0);
        }

        /// <summary>
        /// If this executable is called with  arguments,
        /// the first argument will be the environment declaration.
        /// If it is present, the "Environment" configuration will be changed
        /// to that argument's value. Otherwise, it will take the value from the 
        /// configuration file. This allows support for both production 
        /// environment switching and debugging within visual studio
        /// </summary>
        /// <param name="args"></param>
        private static string DetermineEnvironment(string[] args)
        {
            string env = "DEV";
            if(args.Length > 0)
            {
                env = args[0].ToUpper().Trim();

                if ((env == "PROD") ||
                    (env == "INT") ||
                    (env == "DEV") ||
                    (env == "TEST"))
                {
                        return env;
                    }
            }
            return env;
        }

    }
}
