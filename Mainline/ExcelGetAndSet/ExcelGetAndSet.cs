﻿using ExcelUtilities;
using System.Collections.Specialized;

namespace EnablingSystems.MDLC.Processes
{
    public class ExcelCopyPasteOneToManyProcess : ProcessBaseWithHistory
    {
        private NameValueCollection _cellsCopied;
        public NameValueCollection CellsCopied
        {
            get
            {
                return _cellsCopied;
            }
        }
        public override void Execute()
        {
            CopyPaste();
            //SaveHistory();
            NotifyDone();
        }

        private void CopyPaste()
        {
            ExcelCopyPasteOneToMany excelCopyRange = new ExcelCopyPasteOneToMany();
            _cellsCopied = excelCopyRange.CellsCopied;
            excelCopyRange.SourceWorkbookLocation = _processParams["SourceWorkbookLocation"];
            excelCopyRange.SourceSheetName = _processParams["SourceSheetName"];
            excelCopyRange.SourceRow = int.Parse(_processParams["SourceRow"]);
            excelCopyRange.SourceColumn = _processParams["SourceColumn"];

            excelCopyRange.DestinationWorkbookLocation = _processParams["DestinationWorkbookLocation"];
            excelCopyRange.DestinationSheetName = _processParams["DestinationSheetName"];
            excelCopyRange.DestinationStartRangeRow = int.Parse(_processParams["DestinationRangeStartRow"]);
            excelCopyRange.DestinationStartRangeColumn = _processParams["DestinationRangeStartColumn"];
            excelCopyRange.DestinationEndRangeRow = int.Parse(_processParams["DestinationEndRangeRow"]);
            excelCopyRange.DestinationEndRangeColumn = _processParams["DestinationEndRangeColumn"];

            excelCopyRange.CopyOneToManyValue();
        }
    }
    public class ExcelCopyPasteRangeProcess : ProcessBaseWithHistory
    {
        private NameValueCollection _cellsCopied;
        public NameValueCollection CellsCopied
        {
            get
            {
                return _cellsCopied;
            }
        }
        public override void Execute()
        {
            CopyAndPasteExcelRange();
            //SaveHistory();
            NotifyDone();
        }

        private void CopyAndPasteExcelRange()
        {
            ExcelCopyPasteRange excelCopyRange = new ExcelCopyPasteRange();
            _cellsCopied = excelCopyRange.CellsCopied;
            excelCopyRange.SourceWorkbookLocation = _processParams["SourceWorkbookLocation"];
            excelCopyRange.SourceSheetName = _processParams["SourceSheetName"];
            excelCopyRange.SourceStartRangeRow = int.Parse(_processParams["SourceRangeStartRow"]);
            excelCopyRange.SourceStartRangeColumn = _processParams["SourceRangeStartColumn"];
            excelCopyRange.SourceEndRangeRow = int.Parse(_processParams["SourceEndRangeRow"]);
            excelCopyRange.SourceEndRangeColumn = _processParams["SourceEndRangeColumn"];

            excelCopyRange.DestinationWorkbookLocation = _processParams["DestinationWorkbookLocation"];
            excelCopyRange.DestinationSheetName = _processParams["DestinationSheetName"];
            excelCopyRange.DestinationStartRangeRow = int.Parse(_processParams["DestinationRangeStartRow"]);
            excelCopyRange.DestinationStartRangeColumn = _processParams["DestinationRangeStartColumn"];
            excelCopyRange.DestinationEndRangeRow = int.Parse(_processParams["DestinationEndRangeRow"]);
            excelCopyRange.DestinationEndRangeColumn = _processParams["DestinationEndRangeColumn"];

            excelCopyRange.CopyValuesFromAndToRange();
        }
    }

}
