﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    public static class UnitTestHelpers
    {
        public static Mock<DbSet<T>> CreateMockSet<T>(IQueryable<T> dataForDbSet) where T : class
        {
            var dbsetMock = new Mock<DbSet<T>>();

            dbsetMock.As<IQueryable<T>>().Setup(m => m.Provider)
                .Returns(dataForDbSet.Provider);
            dbsetMock.As<IQueryable<T>>().Setup(m => m.Expression)
                .Returns(dataForDbSet.Expression);
            dbsetMock.As<IQueryable<T>>().Setup(m => m.ElementType)
                .Returns(dataForDbSet.ElementType);
            dbsetMock.As<IQueryable<T>>().Setup(m => m.GetEnumerator())
                .Returns(dataForDbSet.GetEnumerator());
            return dbsetMock;
        }
    }
}
