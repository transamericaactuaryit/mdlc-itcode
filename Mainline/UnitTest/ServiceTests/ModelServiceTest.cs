﻿using DataAccess.Interfaces;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTest.ServiceTests
{
    [TestClass]
    public class ModelServiceTest
    {
        [TestMethod]
        public void CanGetModelTypeByName()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RateAttribution"
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelTypeRepository.Get("RateAttribution", "Fair Value")).Returns(testCollection.First());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetModelType("RateAttribution", "Fair Value");

            Assert.AreEqual(1, results.ID);
            Assert.AreEqual("RateAttribution", results.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetModelTypedByNameHasNoMatch()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RateAttribution"
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelTypeRepository.Get(It.IsAny<string>(), It.IsAny<string>())).Throws(new InvalidOperationException());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetModelType("App Does Not Exist", "No Existing Model Group");
        }

        [TestMethod]
        public void CanGetModelApplicationByName()
        {
            var testCollection = new List<ModelApplication>()
            {
                new ModelApplication
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelApplication
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelApplicationRepository.Get("ALFA")).Returns(testCollection.First());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetModelApplication("ALFA");

            Assert.AreEqual(1, results.ID);
            Assert.AreEqual("ALFA", results.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetModelApplicationByNameHasNoMatch()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelApplicationRepository.Get(It.IsAny<string>())).Throws(new InvalidOperationException());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetModelApplication("App Does Not Exist");
        }

        [TestMethod]
        public void CanGetModelStatusById()
        {
            var testCollection = new List<ModelStatus>()
            {
                new ModelStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelStatusRepository.Get(1)).Returns(testCollection.First());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetStatus(1);

            Assert.AreEqual(1, results.ID);
            Assert.AreEqual("Not Started", results.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetModelStatusByIdHasNoMatch()
        {
            var testCollection = new List<ModelRequestStatus>()
            {
                new ModelRequestStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelStatusRepository.Get(It.IsAny<int>())).Throws(new InvalidOperationException());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetStatus(1);
        }

        [TestMethod]
        public void CanGetModel()
        {
            var testCollection = new List<Model>()
            {
                new Model
                {
                    Name = "BOPParallel",
                    CreatedDate = DateTime.Now,
                    StartTime = DateTime.Now,
                    Priority = 10,
                    ModelRequestID = 1,
                    ModelApplicationID = 1,
                    ModelStatusID = 2,
                    ModelTypeID = 1
                }
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRepository.Get(It.IsAny<int>())).Returns(testCollection.First());

            var service = new ModelService(mockUOW.Object);
            var results = service.Get(testCollection.First().ID);

            Assert.AreEqual("BOPParallel", results.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetModelByIdHasNoMatch()
        {
            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRepository.Get(It.IsAny<int>())).Throws(new InvalidOperationException());

            var service = new ModelService(mockUOW.Object);
            var results = service.Get(1);
        }

        [TestMethod]
        public void CanGetModelsByModelRequestId()
        {
            var testCollection = new List<Model>()
            {
                new Model
                {
                    ModelRequestID = 1,
                    Name = "BOPParallel",
                    CreatedDate = DateTime.Now,
                    StartTime = DateTime.Now,
                    Priority = 10,
                    ModelApplicationID = 1,
                    ModelStatusID = 2,
                    ModelTypeID = 1
                },
                new Model
                {
                    ModelRequestID = 1,
                    Name = "RateAttribution",
                    CreatedDate = DateTime.Now,
                    StartTime = DateTime.Now,
                    Priority = 10,
                    ModelApplicationID = 1,
                    ModelStatusID = 2,
                    ModelTypeID = 1
                }
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRepository.GetAllByModelRequestId(It.IsAny<int>())).Returns(testCollection);

            var service = new ModelService(mockUOW.Object);
            var results = service.GetAllByModelRequestId(1);

            Assert.AreEqual(2, results.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetModelsByModelRequestIdHasNoMatch()
        {
            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRepository.GetAllByModelRequestId(It.IsAny<int>())).Throws(new InvalidOperationException());

            var service = new ModelService(mockUOW.Object);
            var results = service.GetAllByModelRequestId(1);
        }

        [TestMethod]
        public void CanGetModelEmail()
        {
            var testCollection = new List<Model>()
            {
                new Model
                {
                    ModelRequestID = 1,
                    Name = "BOPParallel",
                    CreatedDate = DateTime.Now,
                    StartTime = DateTime.Now,
                    Priority = 10,
                    ModelApplicationID = 1,
                    ModelStatusID = 2,
                    ModelTypeID = 1,
                    ModelType = new ModelType
                    {
                        ID = 1,
                        ModelGroup = new ModelGroup
                        {
                            Email = "simon.ovens@transamerica.com"
                        }
                    }
                }
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelGroupRepository.Get(It.IsAny<int>())).Returns(testCollection.First().ModelType.ModelGroup);

            var service = new ModelService(mockUOW.Object);
            var result = service.GetModelEmail(testCollection.First());

            Assert.AreEqual("simon.ovens@transamerica.com", result);
        }

        [TestMethod]
        public void CanGetLatestByModelTypeId()
        {
            var earlierDate = DateTime.Now.AddDays(-5);
            var mostRecentDate = DateTime.Now.AddDays(-2);

            var models = new List<Model>()
            {
                new Model
                        {
                            ID = 1,
                            Name = "Base",
                            CreatedDate = earlierDate,
                            ModelTypeID = 1
                        },
                new Model
                        {
                            ID = 2,
                            Name = "Base",
                            CreatedDate = mostRecentDate,
                            ModelTypeID = 1
                        },
                new Model
                        {
                            ID = 3,
                            Name = "Some Other Model",
                            CreatedDate = DateTime.Now,
                            ModelTypeID = 2
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRepository.GetLatestByModelTypeId(It.IsAny<int>())).Returns(models.Single(m => m.CreatedDate == mostRecentDate));

            var service = new ModelService(mockUOW.Object);
            var results = service.GetLatestByModelTypeId(1);

            Assert.AreEqual(2, results.ID);
            Assert.AreEqual(mostRecentDate, results.CreatedDate);
        }


    }
}
