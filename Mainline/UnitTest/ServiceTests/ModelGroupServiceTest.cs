﻿using DataAccess.Interfaces;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTest.ServiceTests
{
    [TestClass]
    public class ModelGroupServiceTest
    {
        [TestMethod]
        public void CanGetModelGroups()
        {
            var testCollection = new List<ModelGroup>()
            {
                new ModelGroup
                        {
                            ID = 1,
                            Name = "Fair Value"
                        },
                new ModelGroup
                        {
                            ID = 2,
                            Name = "DAC"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelGroupRepository.Get()).Returns(testCollection);

            var service = new ModelGroupService(mockUOW.Object);
            var results = service.Get();

            Assert.AreEqual(1, results.First().ID);
            Assert.AreEqual(2, results.Count());
        }

        [TestMethod]
        public void CanGetModelTypesByModelGroupId()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                {
                    Name = "DACBase",
                    ModelGroupID = 1
                },
                new ModelType
                {
                    Name = "FVRates",
                    ModelGroupID = 2
                },
                new ModelType
                {
                    Name = "DACVol",
                    ModelGroupID = 1
                },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelTypeRepository.GetByModelGroupId(It.IsAny<int>())).Returns(testCollection.Where(m => m.ModelGroupID == 1));

            var service = new ModelGroupService(mockUOW.Object);
            var results = service.GetModelTypesByModelGroupId(1);

            Assert.AreEqual("DACBase", results.First().Name);
            Assert.AreEqual(2, results.Count());
        }



    }
}
