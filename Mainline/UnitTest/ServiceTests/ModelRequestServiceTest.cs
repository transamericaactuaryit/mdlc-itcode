﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.Interfaces;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service;

namespace UnitTest
{
    [TestClass]
    public class ModelRequestServiceTest
    {
        [TestMethod]
        public void CanAddModelRequest()
        {
            var testModelRequest = new ModelRequest
            {
                Name = "Model Request 1"
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Add(testModelRequest)).Returns(testModelRequest);

            var service = new ModelRequestService(mockUOW.Object);
            var returnObj = service.Add(testModelRequest);

            Assert.AreEqual("Model Request 1", returnObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelRequest()
        {
            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Add(It.IsAny<ModelRequest>())).Throws(new ArgumentException());

            var service = new ModelRequestService(mockUOW.Object);
            service.Add(new ModelRequest());

        }

        [TestMethod]
        public void CanDeleteModelRequest()
        {
            var testModelRequest = new ModelRequest
            {
                ID = 1,
                Name = "Model Request 1"
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Delete(testModelRequest)).Returns(testModelRequest);

            var service = new ModelRequestService(mockUOW.Object);
            var isSuccessful = service.Delete(1);

            Assert.IsTrue(isSuccessful);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelRequest()
        {
            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Delete(It.IsAny<ModelRequest>())).Throws(new ArgumentException());

            var service = new ModelRequestService(mockUOW.Object);
            service.Delete(1);
        }

        [TestMethod]
        public void CanGetModelRequestById()
        {
            var testCollection = new List<ModelRequest>()
            {
                new ModelRequest
                        {
                            ID = 1,
                            Name = "Model Request 1"
                        },
                new ModelRequest
                        {
                            ID = 2,
                            Name = "Model Request 2"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Get(1, false)).Returns(testCollection.First());

            var service = new ModelRequestService(mockUOW.Object);
            var results = service.Get(1, false);

            Assert.AreEqual(1, results.ID);
            Assert.AreEqual("Model Request 1", results.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelRequest()
        {
            var testCollection = new List<ModelRequest>()
            {
                new ModelRequest
                        {
                            ID = 1,
                            Name = "Model Request 1"
                        },
                new ModelRequest
                        {
                            ID = 2,
                            Name = "Model Request 2"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Get(It.IsAny<int>(), false)).Throws(new InvalidOperationException());

            var service = new ModelRequestService(mockUOW.Object);
            var results = service.Get(3, false);
        }

        [TestMethod]
        public void CanGetAllModelRequests()
        {
            var testCollection = new List<ModelRequest>()
            {
                new ModelRequest
                        {
                            ID = 1,
                            Name = "Model Request 1"
                        },
                new ModelRequest
                        {
                            ID = 2,
                            Name = "Model Request 2"
                        },
                new ModelRequest
                        {
                            ID = 3,
                            Name = "Model Run 3"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Get()).Returns(testCollection);

            var service = new ModelRequestService(mockUOW.Object);
            var results = service.Get();

            Assert.AreEqual(3, results.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ExceptionThrownWhenGetAllModelRequestsIsEmpty()
        {
            var testCollection = new List<ModelRequest>();

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestRepository.Get()).Throws(new Exception());

            var service = new ModelRequestService(mockUOW.Object);
            var results = service.Get();
        }

        [TestMethod]
        public void CanGetModelRequestStatusById()
        {
            var testCollection = new List<ModelRequestStatus>()
            {
                new ModelRequestStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestStatusRepository.Get(1)).Returns(testCollection.First());

            var service = new ModelRequestService(mockUOW.Object);
            var results = service.GetStatus(1);

            Assert.AreEqual(1, results.ID);
            Assert.AreEqual("Not Started", results.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetModelStatusByIdHasNoMatch()
        {
            var testCollection = new List<ModelRequestStatus>()
            {
                new ModelRequestStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            };

            var mockUOW = new Mock<IUnitOfWork>();
            mockUOW.Setup(m => m.ModelRequestStatusRepository.Get(It.IsAny<int>())).Throws(new InvalidOperationException());

            var service = new ModelRequestService(mockUOW.Object);
            var results = service.GetStatus(1);
        }
    }
}
