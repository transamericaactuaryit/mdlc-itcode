﻿using System;
using System.IO;
using System.Collections.Specialized;
using EnablingSystems.MDLC.Processes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Reporting;

namespace UnitTest
{
    public static class ThisDirectory
    {
        public static void CreateDirectoryIfNotExists(string strDir)
        {
            if (Directory.Exists(strDir) == false)
                Directory.CreateDirectory(strDir);
        }
    }

    [TestClass]
    public class UnitTest1
    {

        //relative path directory store.
        string strDriveLetter = @"../../TestDir/";

        public string DriveLetter 
        {
            get
            {
                return strDriveLetter;
            }
        }

        [ClassInitialize()]
        public static void InitializeEvent(TestContext context)
        {
            //relative path directory store.
            UnitTest1 val = new UnitTest1();
            ThisDirectory.CreateDirectoryIfNotExists(val.DriveLetter);
        }

        [TestMethod]        
        public void LogBasicELMAHError()
        {
            string msgText = "This is a test Elmah.ApplicationException";
            ELMAHLogger.LogError(msgText, "UnitTest1");                        
        }

        [TestMethod]
        public void LogModelEMAHError()
        {
            string msgText = "Test ELMAH ModelError";
            int modelId = 1;
            ELMAHLogger.LogModelError(modelId, msgText, "UnitTest1");
        }

        [TestMethod]
        public void DynamicDirectoryCopyFileProcess_ShouldCopyFileProcess()
        {
            //This test creates Inforce and BOP directories with a file called TEDFileOverrideQ118.Ail2.  The
            //File TEDFileOverrideQ118.Ail2 is copied from source C:\Source\OneMDLCCode\UnitTest\bin\TestDir\ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q118\AILFiles\BOP 
            //to destination C:\Source\OneMDLCCode\UnitTest\bin\TestDir\Destination_DynamicDirectoryCopyFileProcess_PositiveTest and file name is shaved
            //and copied into destination folder as TEDFileOverride.Ail2.  The assert checks to see if destination
            //file yields TEDFileOverride.Ail2 name.  After which cleanup to files and directories takes place.

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;            
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;            
            int defaultQuarterYr = defaultQuarterDate.Year;


            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string quarterString = string.Empty;
            string defaultQuarterString = string.Empty;
            


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");
            string strBOPPath = strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + quarterString + quarterYr2Digit + @"\AILFiles\BOP";
            string strInforcePath = strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + quarterString + quarterYr2Digit + @"\AILFiles\Inforce";
            string strBOPPathAndFile = strBOPPath + @"\TEDFileOverride" + monthDay + ".Ail2";
            string strInforcePathAndFile = strInforcePath + @"\TEDFileOverride" + monthDay + ".Ail2";

            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");
            string strDefaultBOPPath = strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\BOP";
            string strDefaultInforcePath = strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\Inforce";
            string strDefaultBOPPathAndFile = strDefaultBOPPath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDefaultInforcePathAndFile = strDefaultInforcePath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDestination = strDriveLetter + @"Destination_DynamicDirectoryCopyFileProcess_PositiveTest";


            //check to see if directories exist.  Create if they don't
            ThisDirectory.CreateDirectoryIfNotExists(strBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDestination);
            
            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            var inforcePathFile = new FileInfo(strInforcePathAndFile);
            var inforceCreate = inforcePathFile.Create();

            // you can use dispose here, for it returns filestream
            inforceCreate.Dispose();

            var defaultBOPPathFile = new FileInfo(strDefaultBOPPathAndFile);
            var defaultBOPCreate = defaultBOPPathFile.Create();

            defaultBOPCreate.Dispose();

            var defaultInforcePathFile = new FileInfo(strDefaultInforcePathAndFile);
            var defaultInforceCreate = defaultInforcePathFile.Create();

            defaultInforceCreate.Dispose();


            NameValueCollection _params = new NameValueCollection();
            _params.Add("OverrideSourceDirectoryPattern", strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce");
            _params.Add("ProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");
            _params.Add("DestinationDirectory", strDestination);
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", @"Q\d{3}"); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");
            _params.Add("SourceDirectoryPattern", strDriveLetter + @"ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");

            DynamicDirectoryCopyFileProcess ddcfp = new DynamicDirectoryCopyFileProcess();
            ddcfp.ProcessParams = _params;
            ddcfp.Execute();


            string strDestinationFile = Path.GetFileName(Directory.GetFiles(strDestination).FirstOrDefault());


            Assert.AreEqual("TEDFileOverride.Ail2", strDestinationFile);

            RemoveDirectory(strDestination);
        }

        [TestMethod]
        public void DynamicDirectoryCopyFileProcess_ShouldNotCopyFileProcess()
        {
            //This test creates Inforce and BOP directories with a file called TEDFileOverrideQ118.Ail2.  The
            //File TEDFileOverrideQ118.Ail2 is copied from source C:\Source\OneMDLCCode\UnitTest\bin\TestDir\ait10\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q118\AILFiles\BOP 
            //to destination C:\Source\OneMDLCCode\UnitTest\bin\TestDir\Destination_DynamicDirectoryCopyFileProcess_PositiveTest and file name is shaved
            //and copied into destination folder as TEDFileOverride.Ail2.  The assert checks to see if destination
            //file does not yield TEDFileOverrideQ118.Ail2 name.  After which cleanup to files and directories takes place.
            //Since the file name in destination does not match it is a successful FAIL test.

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterDay = defaultQuarterDate.Day;
            int defaultQuarterYr = defaultQuarterDate.Year;

            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string quarterString = string.Empty;
            string defaultQuarterString = string.Empty;            


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");
            string strBOPPath = strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + quarterString + quarterYr2Digit + @"\AILFiles\BOP";
            string strInforcePath = strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + quarterString + quarterYr2Digit + @"\AILFiles\Inforce";
            string strBOPPathAndFile = strBOPPath + @"\TEDFileOverride" + monthDay + ".Ail2";
            string strInforcePathAndFile = strInforcePath + @"\TEDFileOverride" + monthDay + ".Ail2";

            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");
            string strDefaultBOPPath = strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\BOP";
            string strDefaultInforcePath = strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\Inforce";
            string strDefaultBOPPathAndFile = strDefaultBOPPath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDefaultInforcePathAndFile = strDefaultInforcePath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDestination = strDriveLetter + @"Destination_DynamicDirectoryCopyFileProcess_PositiveTest";



            //check to see if directories exist.  Create if they don't
            ThisDirectory.CreateDirectoryIfNotExists(strBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDestination);
            


            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            var inforcePathFile = new FileInfo(strInforcePathAndFile);
            var inforceCreate = inforcePathFile.Create();

            // you can use dispose here, for it returns filestream
            inforceCreate.Dispose();

            var defaultBOPPathFile = new FileInfo(strDefaultBOPPathAndFile);
            var defaultBOPCreate = defaultBOPPathFile.Create();

            defaultBOPCreate.Dispose();

            var defaultInforcePathFile = new FileInfo(strDefaultInforcePathAndFile);
            var defaultInforceCreate = defaultInforcePathFile.Create();

            defaultInforceCreate.Dispose();


            NameValueCollection _params = new NameValueCollection();
            _params.Add("OverrideSourceDirectoryPattern", strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");
    
            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce");
            _params.Add("ProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");
            _params.Add("DestinationDirectory", strDestination);
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", @"Q\d{3}"); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");
            _params.Add("SourceDirectoryPattern", strDriveLetter + @"ait11\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");

            DynamicDirectoryCopyFileProcess ddcfp = new DynamicDirectoryCopyFileProcess();
            ddcfp.ProcessParams = _params;
            ddcfp.Execute();


            string strDestinationFile = Path.GetFileName(Directory.GetFiles(strDestination).FirstOrDefault());
            string strSourceFileExpected = "TEDFileOverrideQ" + defaultQuarterString + defaultQuarterYr2Digit + "Ail2";


            Assert.AreNotEqual(strSourceFileExpected, strDestinationFile);

            RemoveDirectory(strDestination);
        }

        [TestMethod]

        public void DynamicFiscalNameResolver_ShouldResolveFiscalName()
        {
 
            //This test checks to resolves fiscal naming based on taking current date and adding offset
            //to determine {QuarterHolder}{YearHolderFourDigits}{MonthHolder}{DayHolder} in file path
            //pattern.  The assert checks to see if that directory file path exists, which it is created.
            //We successfully pass the positive test.  Lastly, cleanup is done.

            string dynamicDir = strDriveLetter + @"Temp\Archive\{YearHolderFourDigit}\FairValue\BOPParallel\Q{QuarterHolder}\{YearHolderFourDigit}{MonthHolder}{DayHolder}"
                + "_CurrentDay_{CurrentYearHolderFourDigit}{CurrentMonthHolder}{CurrentDayHolder}";

            NameValueCollection createDirectoryParams = new NameValueCollection();
            createDirectoryParams.Add("DynamicDirectory", dynamicDir);

            createDirectoryParams.Add("CopyFromDirectory", strDriveLetter + @"TEMP\TestSource\");
            createDirectoryParams.Add("CopyFromWildcard", "*.*");
            createDirectoryParams.Add("Q1MonthMap", "1,2,3");
            createDirectoryParams.Add("Q2MonthMap", "4,5,6");
            createDirectoryParams.Add("Q3MonthMap", "7,8,9");
            createDirectoryParams.Add("Q4MonthMap", "10,11,12");
            createDirectoryParams.Add("ProcessTimeToMonthOffset", "-1");

            DynamicFiscalNameResolver currentProcessingFolder = new DynamicFiscalNameResolver(
               dynamicDir,
               createDirectoryParams["Q1MonthMap"],
               createDirectoryParams["Q2MonthMap"],
               createDirectoryParams["Q3MonthMap"],
               createDirectoryParams["Q4MonthMap"],
               int.Parse(createDirectoryParams["ProcessTimeToMonthOffset"])
               );

            System.IO.Directory.CreateDirectory(currentProcessingFolder.ResolvedName);

            bool isThere = Directory.Exists(currentProcessingFolder.ResolvedName);

            Assert.IsTrue(isThere);

            RemoveDirectory(currentProcessingFolder.ResolvedName);
           
        }

        [TestMethod]

        public void DynamicFiscalNameResolver_ShouldFailToResolveFiscalName()
        {

            //This test checks to resolves fiscal naming based on taking current date and adding offset
            //to determine {QuarterHolder}{YearHolderFourDigits}{MonthHolder}{DayHolder} in file path
            //pattern.  The assert checks to see if that directory file path exists, which it doesn't
            //because we fail to create it and successfully fails the test.  Lastly, cleanup is done.

            string dynamicDir = strDriveLetter + @"Temp1\Archive\{YearHolderFourDigit}\FairValue\BOPParallel\Q{QuarterHolder}\{YearHolderFourDigit}{MonthHolder}{DayHolder}"
                + "_CurrentDay_{CurrentYearHolderFourDigit}{CurrentMonthHolder}{CurrentDayHolder}";

            NameValueCollection createDirectoryParams = new NameValueCollection();
            createDirectoryParams.Add("DynamicDirectory", dynamicDir);

            createDirectoryParams.Add("CopyFromDirectory", strDriveLetter + @"TEMP\TestSource\");
            createDirectoryParams.Add("CopyFromWildcard", "*.*");
            createDirectoryParams.Add("Q1MonthMap", "1,2,3");
            createDirectoryParams.Add("Q2MonthMap", "4,5,6");
            createDirectoryParams.Add("Q3MonthMap", "7,8,9");
            createDirectoryParams.Add("Q4MonthMap", "10,11,12");
            createDirectoryParams.Add("ProcessTimeToMonthOffset", "-1");

            DynamicFiscalNameResolver currentProcessingFolder = new DynamicFiscalNameResolver(
               dynamicDir,
               createDirectoryParams["Q1MonthMap"],
               createDirectoryParams["Q2MonthMap"],
               createDirectoryParams["Q3MonthMap"],
               createDirectoryParams["Q4MonthMap"],
               int.Parse(createDirectoryParams["ProcessTimeToMonthOffset"])
               );


          
            bool isThere = Directory.Exists(currentProcessingFolder.ResolvedName);

            Assert.IsFalse(isThere);


            
        }

        [TestMethod]

        public void CopyFileProcess_ShouldCopyFile()
        {
            //This test creates 4 folders for current Date year in zAlfa Project folder from 
            //2018Q1_BOPParallel through 2018Q4_BOPParallel along with .adb2 files in each of
            //these folders.  Creates a text file called 'mySingleFile.txt' out in Projects folder.  
            //Creates a destination folder if it does not already exist and copies 'mySingleFile.txt' 
            //from source to destination directory and does cleanup to remove everything.
            
            //Arrange
            DateTime today = DateTime.Now;

            string baseDir = strDriveLetter + @"Temp\ValProd\" + today.ToString("yyyy") + "M"
                + today.Month.ToString().PadLeft(2, '0')
                + @"\Riders\zAlfa\Projects\";

            CreateTempADB2Files(baseDir);


            using (File.Create(baseDir + "mysingleFile.txt")) { }


            string directoryDest = strDriveLetter + @"Temp\ProdDestination";


            ThisDirectory.CreateDirectoryIfNotExists(directoryDest);
           


            string fileDestination = strDriveLetter + @"Temp\ProdDestination\mysingleFile.txt";

            NameValueCollection theseParams = new NameValueCollection();
            theseParams.Add("SourceFile", @baseDir + "mysingleFile.txt");
            theseParams.Add("DynamicDirectoryPattern", @"");
            theseParams.Add("DestinationFile", fileDestination);



            CopyFileProcess singFileCopyFileProcess =
                new CopyFileProcess();

            singFileCopyFileProcess.ProcessParams = theseParams;

            //ACT
            singFileCopyFileProcess.Execute();

            //Assert
            //use File.IO namespace to count files

            string strDocPath = Path.GetDirectoryName(fileDestination);


            int docCount = Directory.GetFiles(strDocPath, "*.txt",
            SearchOption.TopDirectoryOnly).Length;

            Assert.AreEqual(1, docCount);

            //Clean Up


            RemoveDirectory(baseDir);

        }


        [TestMethod]
        public void CopyFileProcess_ShouldFailToCopyFile()
        {
            //This test creates 4 folders for current Date year in zAlfa Project folder from 
            //2018Q1_BOPParallel through 2018Q4_BOPParallel along with .adb2 files in each of
            //these folders.  Creates a text file called 'mySingleFile.txt' out in Projects folder.  
            //does not create destination folder and fails to copy file 'mySingleFile.txt' 
            //from source to destination directory because destination does not exists.  Cleanup
            //occurs to directories and files.

            Exception _Exception = null;

            string baseDir = "";
            string parentDir = strDriveLetter + @"Temp\ProdDestination";
            try
            {
                //Arrange
                DateTime today = DateTime.Now;

                baseDir = strDriveLetter + @"Temp\ValProd\" + today.ToString("yyyy") + "M"
                    + today.Month.ToString().PadLeft(2, '0')
                    + @"\Riders\zAlfa\Projects\";

                CreateTempADB2Files(baseDir);

                using (File.Create(baseDir + "mysingleFile.txt")) { }


                string fileDestination = strDriveLetter + @"Temp\ProdDestination\mysingleFile.txt";

                NameValueCollection theseParams = new NameValueCollection();
                theseParams.Add("SourceFile", @baseDir + "mysingleFile.txt");
                theseParams.Add("DynamicDirectoryPattern", @"");
                theseParams.Add("DestinationFile", fileDestination);
                theseParams.Add("OverwriteDestinationFile", "false");

                CopyFileProcess singFileCopyFileProcess =
                    new CopyFileProcess();

                singFileCopyFileProcess.ProcessParams = theseParams;

                //ACT
                singFileCopyFileProcess.Execute();
                RemoveDirectory(parentDir);
            }

            catch (Exception ex)
            {
                _Exception = ex;
            }
            finally
            {
                Assert.IsNotNull(_Exception);
                Assert.IsInstanceOfType(_Exception, typeof(System.IO.IOException));
 
               
            }




        }

        [TestMethod]

        public void ClearDirectory_ShouldClearDirectory()
        {
            //This test creates a directory and clears specific directory including files.  
                        
            //Arrange
            NameValueCollection _params = new NameValueCollection();

            _params.Add("DirectoryToClear", strDriveLetter + @"Temp/TedDir");
            _params.Add("RegExFilesToExclude", @"\d+");


            string FolderPath = strDriveLetter + @"Temp/TedDir";
            
            Directory.CreateDirectory(Path.GetFullPath(FolderPath));

            ClearDirectory clear = new ClearDirectory();
            clear.ProcessParams = _params;



            DirectoryInfo di = new DirectoryInfo(FolderPath);


            var f = new FileInfo(FolderPath + @"/Test.txt");
            var fs = f.Create();

            // you can use dispose here, for it returns filestream
            fs.Dispose();
          

            //Act
            clear.Execute();

            var intFileCountAfter = di.GetFiles("*.*")
                .Select(file => file.Name).Count();

            //Assert
            Assert.AreEqual(0, intFileCountAfter);

            //Remove the directory
 
            RemoveDirectory(FolderPath);
        }

        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException), "ProcessLocation did not exist in parameter collection")]

        public void ClearDirectory_ShouldGenerateExpectedException()
        {

            //This test generates an exception as it does not find the location of the directory.  

            //Arrange
            NameValueCollection _params = new NameValueCollection();

            _params.Add("DirectoryToClear", strDriveLetter + @"Temp\TedDir");
            _params.Add("RegExFilesToExclude", @"\d+");

            ClearDirectory clear = new ClearDirectory();
            clear.ProcessParams = _params;


            string FolderPath = strDriveLetter + @"Temp\TedDir";
            DirectoryInfo di = new DirectoryInfo(FolderPath);

            //Act
            clear.Execute();

            var intFileCountAfter = di.GetFiles("*.*")
                .Select(file => file.Name).Count();

            //Ass
            Assert.AreEqual(0, intFileCountAfter);
        }

        [TestMethod]

        public void DeleteFile_ShouldDeleteFile()
        {

            //This test successfully creates a folder and file and successfully
            //deletes the file and lastly cleanup is done to remove folders.


            string myFilePath = strDriveLetter + @"Temp\DeleteFile\TestFileSuccess.txt";            

            string strDocPath = Path.GetDirectoryName(myFilePath);

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strDocPath);
                      
            var f = new FileInfo(myFilePath);
            var fs = f.Create();

            // you can use dispose here, for it returns filestream
            fs.Dispose();

            // your code using s   
            NameValueCollection _params = new NameValueCollection();
            _params.Add("FileToDelete", myFilePath);


            DeleteFile df = new DeleteFile();
            df.ProcessParams = _params;
            df.Execute();
            int intFileCountAfter = Directory.GetFiles(strDocPath, "*.txt",
                  SearchOption.TopDirectoryOnly).Length;
            Assert.AreEqual(0, intFileCountAfter);



            RemoveDirectory(strDocPath);
        }

        [TestMethod]
 
        public void DeleteFile_ShouldNotDeleteFile()
        {

            //This test creates a file and directory and
            //deletes the file but we assert that there should be still 1 file
            //left and there is none, so it fails.  Lastly cleanup is done to remove folders.

            string myFilePath = strDriveLetter + @"Temp\DeleteFile\TestFileSuccess.txt";            

            string strDocPath = Path.GetDirectoryName(myFilePath);

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strDocPath);

 
            var f = new FileInfo(myFilePath);
            var fs = f.Create();

            // you can use dispose here, for it returns filestream
            fs.Dispose();

            // your code using s   
            NameValueCollection _params = new NameValueCollection();
            _params.Add("FileToDelete", myFilePath);

            DeleteFile df = new DeleteFile();
            df.ProcessParams = _params;
            df.Execute();
            int intFileCountAfter = Directory.GetFiles(strDocPath, "*.txt",
                  SearchOption.TopDirectoryOnly).Length;

            Assert.AreNotEqual(1, intFileCountAfter);


            RemoveDirectory(strDocPath);
        }

        [TestMethod]

        public void FileExistenceChecker_ShouldRunFileExistenceChecker()
        {
            //This test checks to resolves fiscal naming based on taking current date and adding offset
            //to determine {YearHolderFourDigit}\Q{QuarterHolder}{YearHolderTwoDigit} in file path
            //pattern.  A file is created and the assert checks to see if that file exists in file path.
            //The file count matches the expected count and we successfully pass the positive test.  
            //Lastly, cleanup is done.

            int _processTimeToMonthOffset = -1;


            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            int quarterMonth = quarterDate.Month;            
            int quarterYr = quarterDate.Year;
            string quarterString = string.Empty;
            //datetime object based on - month

 
            //string parentDir = strDriveLetter + @"ait";

            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }

            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");


            string strPath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\sharedactuarial\EquityRisk\" + quarterYr.ToString() + @"\" + monthDay + @"\RN\Stochastic\TCM";
            string strPathAndFile = strPath + @"\TCMALFA1_RN_TED.AGU";
            
            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strPath);
                       
            var f = new FileInfo(strPathAndFile);
            var fs = f.Create();

            // you can use dispose here, for it returns filestream
            fs.Dispose();

            NameValueCollection col = new NameValueCollection();

            col.Add("SourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\sharedactuarial\EquityRisk\{YearHolderFourDigit}\Q{QuarterHolder}{YearHolderTwoDigit}\RN\Stochastic\TCM");
            col.Add("Wildcard", "TCMALFA1_RN_TED.AGU");
            col.Add("ExpectedNumberOfFiles", "1");
            col.Add("ProcessTimeToMonthOffset", _processTimeToMonthOffset.ToString());
            col.Add("Q1MonthMap", "1,2,3");
            col.Add("Q2MonthMap", "4,5,6");
            col.Add("Q3MonthMap", "7,8,9");
            col.Add("Q4MonthMap", "10,11,12");



            FileExistenceChecker fec = new FileExistenceChecker();
            fec.ProcessParams = col;
            fec.Execute();            

            int intFileCount = Directory.GetFiles(strPath, "*.AGU",
                 SearchOption.TopDirectoryOnly).Length;

            Assert.AreEqual(1, intFileCount);


            RemoveDirectory(strPath);


        }

        [TestMethod]

        public void FileExistenceChecker_ShouldNotRunFileExistenceChecker()
        {
            //This test checks to resolves fiscal naming based on taking current date and adding offset
            //to determine {YearHolderFourDigit}\Q{QuarterHolder}{YearHolderTwoDigit} in file path
            //pattern.  A file is created and the assert checks to see if that file exists in file path.
            //The file count matches the expected count and we successfully pass the positive test.  
            //Lastly, cleanup is done.

            int _processTimeToMonthOffset = -1;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            int quarterMonth = quarterDate.Month;            
            int quarterYr = quarterDate.Year;
            string quarterString = string.Empty;            


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");


            string strPath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\sharedactuarial\EquityRisk\" + quarterYr.ToString() + @"\" + monthDay + @"\RN\Stochastic\TCM";
            string strPathAndFile = strPath + @"\TCMALFA1_RN_TED1.AGU";

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strPath);
            
            var f = new FileInfo(strPathAndFile);
            var fs = f.Create();

            // you can use dispose here, for it returns filestream
            fs.Dispose();

            NameValueCollection col = new NameValueCollection();

            col.Add("SourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\sharedactuarial\EquityRisk\{YearHolderFourDigit}\Q{QuarterHolder}{YearHolderTwoDigit}\RN\Stochastic\TCM");
            col.Add("Wildcard", "TCMALFA1_RN_TED1.AGU");
            col.Add("ExpectedNumberOfFiles", "1");
            col.Add("ProcessTimeToMonthOffset", _processTimeToMonthOffset.ToString());
            col.Add("Q1MonthMap", "1,2,3");
            col.Add("Q2MonthMap", "4,5,6");
            col.Add("Q3MonthMap", "7,8,9");
            col.Add("Q4MonthMap", "10,11,12");


            FileExistenceChecker fec = new FileExistenceChecker();
            fec.ProcessParams = col;
            fec.Execute();            

            int intFileCount = Directory.GetFiles(strPath, "*.AGU",
                 SearchOption.TopDirectoryOnly).Length;

            Assert.AreNotEqual(0, intFileCount);


            RemoveDirectory(strPath);


        }

        [TestMethod]

        public void FileCopyOverrideDefault_ShouldRunFileCopyOverrideDefault()
        {
            //This test does Fiscal Name Resolver for source BOP directory and Inforce
            //directory by resolving \Q{QuarterHolder}{YearHolderTwoDigit} with respective 
            //quarter and year holder two digits.  Creates a file called TEDFileOverrideQ118.Ail2
            //(Q118 may differ based on current date).  A destination directory is created and 
            //the file is written to the destination folder with Quarter/Yr removed as a result
            //of the ReplaceFrom and ReplaceTo strings.  Lastly, we assert that the destination folder
            //file matches expected file name of TEDFileOverride.Ail2, which it does resulting in a
            //positive test.  Lastly, directories and files are removed.

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterYr = defaultQuarterDate.Year;


            string parentDir = strDriveLetter + @"ait";
            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string quarterString = string.Empty;
            string defaultQuarterString = string.Empty;
            

            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");

            string strBOPPath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q" + quarterString + quarterYr2Digit + @"\AILFiles\BOP";
            string strInforcePath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q" + quarterString + quarterYr2Digit + @"\AILFiles\Inforce";
            string strBOPPathAndFile = strBOPPath + @"\TEDFileOverride" + monthDay + ".Ail2";
            string strInforcePathAndFile = strInforcePath + @"\TEDFileOverride" + monthDay + ".Ail2";

            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");

            string strDefaultBOPPath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\BOP";
            string strDefaultInforcePath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\Inforce";
            string strDefaultBOPPathAndFile = strDefaultBOPPath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDefaultInforcePathAndFile = strDefaultInforcePath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";

            string strDestination = strDriveLetter + @"Destination";


            //check to see if directories exist.  Create if they don't
            ThisDirectory.CreateDirectoryIfNotExists(strBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDestination);
            

            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            var inforcePathFile = new FileInfo(strInforcePathAndFile);
            var inforceCreate = inforcePathFile.Create();

            // you can use dispose here, for it returns filestream
            inforceCreate.Dispose();

            var defaultBOPPathFile = new FileInfo(strDefaultBOPPathAndFile);
            var defaultBOPCreate = defaultBOPPathFile.Create();

            defaultBOPCreate.Dispose();

            var defaultInforcePathFile = new FileInfo(strDefaultInforcePathAndFile);
            var defaultInforceCreate = defaultInforcePathFile.Create();

            defaultInforceCreate.Dispose();


            NameValueCollection _params = new NameValueCollection();

            _params.Add("OverrideSourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");

            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce");
            _params.Add("ProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");

            _params.Add("DestinationDirectory", strDriveLetter + @"Destination");
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", @"Q\d{3}"); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");

            FileCopyOverrideDefault fcod = new FileCopyOverrideDefault();
            fcod.ProcessParams = _params;
            fcod.Execute();

            string strSourceFileName = Path.GetFileName(strBOPPathAndFile);
            bool blncheck = strSourceFileName.Contains(monthDay);

            // Only get files that begin with the letter "c."
            string[] dirs = Directory.GetFiles(strDestination, "*.Ail2");
            string strDestinationFileName = "";
            foreach (string dir in dirs)
            {
                strDestinationFileName = Path.GetFileName(dir);
            }

            Assert.AreEqual("TEDFileOverride.Ail2", strDestinationFileName);


          
            RemoveDirectory(parentDir);


        }


        [TestMethod]

        public void FileCopyOverrideDefault_ShouldFailToRunFileCopyOverrideDefault()
        {
            //This test does Fiscal Name Resolver for source BOP directory and Inforce
            //directory by resolving \Q{QuarterHolder}{YearHolderTwoDigit} with respective 
            //quarter and year holder two digits.  Creates a file called TEDFileOverrideQ118.Ail2
            //(Q118 may differ based on current date).  A destination directory is created and 
            //the file is written to the destination folder with Quarter/Yr removed as a result
            //of the ReplaceFrom and ReplaceTo strings.  Lastly, we assert that the destination folder
            //file does not match expected file name of "TEDFileOverrideQ118.Ail2", which it does not resulting in a
            //negative test.  Lastly, directories and files are removed.

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterDay = quarterDate.Day;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterDay = defaultQuarterDate.Day;
            int defaultQuarterYr = defaultQuarterDate.Year;


            string parentDir = strDriveLetter + @"ait";
            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string quarterString = "";
            string defaultQuarterString = "";
            //datetime object based on - month


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");

            string strBOPPath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + quarterString + quarterYr2Digit + @"\AILFiles\BOP";
            string strInforcePath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + quarterString + quarterYr2Digit + @"\AILFiles\Inforce";
            string strBOPPathAndFile = strBOPPath + @"\TEDFileOverride" + monthDay + ".Ail2";
            string strInforcePathAndFile = strInforcePath + @"\TEDFileOverride" + monthDay + ".Ail2";

            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");

            string strDefaultBOPPath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\BOP";
            string strDefaultInforcePath = strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\Inforce";
            string strDefaultBOPPathAndFile = strDefaultBOPPath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDefaultInforcePathAndFile = strDefaultInforcePath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";

            string strDestination = strDriveLetter + @"Destination_Negative";



            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDestination);
           


            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            var inforcePathFile = new FileInfo(strInforcePathAndFile);
            var inforceCreate = inforcePathFile.Create();

            // you can use dispose here, for it returns filestream
            inforceCreate.Dispose();

            var defaultBOPPathFile = new FileInfo(strDefaultBOPPathAndFile);
            var defaultBOPCreate = defaultBOPPathFile.Create();

            defaultBOPCreate.Dispose();

            var defaultInforcePathFile = new FileInfo(strDefaultInforcePathAndFile);
            var defaultInforceCreate = defaultInforcePathFile.Create();

            defaultInforceCreate.Dispose();


            NameValueCollection _params = new NameValueCollection();

            _params.Add("OverrideSourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");

            _params.Add("DefaultSourceDirectoryPattern", strDriveLetter + @"ait\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce");
            _params.Add("ProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");

            _params.Add("DestinationDirectory", strDriveLetter + @"Destination_Negative");
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", @"Q\d{3}"); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");

            FileCopyOverrideDefault fcod = new FileCopyOverrideDefault();
            fcod.ProcessParams = _params;
            fcod.Execute();

            string strSourceFileName = Path.GetFileName(strBOPPathAndFile);
            bool blncheck = strSourceFileName.Contains(monthDay);

            // Only get files that begin with the letter "c."
            string[] dirs = Directory.GetFiles(strDestination, "*.Ail2");
            string strDestinationFileName = "";
            foreach (string dir in dirs)
            {
                strDestinationFileName = Path.GetFileName(dir);
            }

            Assert.AreNotEqual("TEDFileOverrideQ118.Ail2", strDestinationFileName);


            
            RemoveDirectory(parentDir);
        }

        [TestMethod]
      
        public void FileCheckAndCopy_ShouldRunFileCheckAndCopy()
        {
            //This test does Fiscal Name Resolver for source BOP directory and Inforce
            //directory by resolving \Q{QuarterHolder}{YearHolderTwoDigit} with respective 
            //quarter and year holder two digits.  Creates a file called TEDFileOverrideQ118.Ail2
            //(Q118 may differ based on current date).  A destination directory is created and 
            //the file is written to the destination folder with Quarter/Yr removed as a result
            //of the ReplaceFrom and ReplaceTo strings.  Lastly, we assert that the source folder matches
            //what we are expecting.  Lastly, directories and files are removed.

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterDay = quarterDate.Day;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterDay = defaultQuarterDate.Day;
            int defaultQuarterYr = defaultQuarterDate.Year;


            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string parentDir = strDriveLetter + @"ait4";

            string quarterString = "";
            string defaultQuarterString = "";
            //datetime object based on - month


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");

            string strBOPPath = strDriveLetter + @"ait4\Actuary\TCMActuary\Valtest\InForce_File\Q" + quarterString + quarterYr2Digit + @"\AILFiles\BOP";
            string strInforcePath = strDriveLetter + @"ait4\Actuary\TCMActuary\Valtest\InForce_File\Q" + quarterString + quarterYr2Digit + @"\AILFiles\Inforce";
            string strBOPPathAndFile = strBOPPath + @"\TEDFileOverride" + monthDay + ".Ail2";
            string strInforcePathAndFile = strInforcePath + @"\TEDFileOverride" + monthDay + ".Ail2";

            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");

            string strDefaultBOPPath = strDriveLetter + @"ait4\Actuary\TCMActuary\Valtest\InForce_File\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\BOP";
            string strDefaultInforcePath = strDriveLetter + @"ait4\Actuary\TCMActuary\Valtest\InForce_File\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\Inforce";
            string strDefaultBOPPathAndFile = strDefaultBOPPath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
            string strDefaultInforcePathAndFile = strDefaultInforcePath + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";

            string strDestination = strDriveLetter + @"Destination_FileCheckAndCopy_PositiveTest";


            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDestination);
            
            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            //// you can use dispose here, for it returns filestream

            bopCreate.Dispose();


            var inforcePathFile = new FileInfo(strInforcePathAndFile);
            var inforceCreate = inforcePathFile.Create();

            // you can use dispose here, for it returns filestream
            inforceCreate.Dispose();

            var defaultBOPPathFile = new FileInfo(strDefaultBOPPathAndFile);
            var defaultBOPCreate = defaultBOPPathFile.Create();

            defaultBOPCreate.Dispose();

            var defaultInforcePathFile = new FileInfo(strDefaultInforcePathAndFile);
            var defaultInforceCreate = defaultInforcePathFile.Create();

            defaultInforceCreate.Dispose();


            NameValueCollection _params = new NameValueCollection();

            _params.Add("SourceDirectoryPattern", strDriveLetter + @"ait4\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("ProcessTimeToMonthOffset", "-1");

            _params.Add("DestinationDirectory", strDriveLetter + @"Destination_FileCheckAndCopy_PositiveTest");
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("ReplaceFromString", @"Q\d{3}"); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");

            FileCheckAndCopy fcc = new FileCheckAndCopy();
            fcc.ProcessParams = _params;
            fcc.Execute();



 
            Assert.AreEqual(strBOPPath, fcc.ResolvedDirectoryName);


           
            RemoveDirectory(parentDir);


        }

        [TestMethod]

        public void FileCheckAndCopy_ShouldFailFileCheckAndCopy()
        {
            //This test does Fiscal Name Resolver for source BOP directory and Inforce
            //directory by resolving \Q{QuarterHolder}{YearHolderTwoDigit} with respective 
            //quarter and year holder two digits.  Creates a file called TEDFileOverrideQ118.Ail2
            //(Q118 may differ based on current date).  A destination directory is created and 
            //the file is written to the destination folder with Quarter/Yr removed as a result
            //of the ReplaceFrom and ReplaceTo strings.  Lastly, we assert that the source folder does not match
            //what we are expecting and results in a negative test.  Lastly, directories and files are removed.

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterDay = quarterDate.Day;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterDay = defaultQuarterDate.Day;
            int defaultQuarterYr = defaultQuarterDate.Year;


            string strBOPPathBeforeFormats = strDriveLetter + @"ait8\Actuary\TCMActuary\ValProd\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP";
            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string parentDir = strDriveLetter + @"ait8";
            string quarterString = "";
            string defaultQuarterString = "";
            //datetime object based on - month


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");

            string strBOPPath = strDriveLetter + @"ait8\Actuary\TCMActuary\Valtest\InForce_File\Q" + quarterString + quarterYr2Digit + @"\AILFiles\BOP";
            string strInforcePath = strDriveLetter + @"ait8\Actuary\TCMActuary\Valtest\InForce_File\Q" + quarterString + quarterYr2Digit + @"\AILFiles\Inforce";
            string strBOPPathAndFile = strBOPPath + @"\TEDFileOverride1" + monthDay + ".Ail2";
            string strInforcePathAndFile = strInforcePath + @"\TEDFileOverride1" + monthDay + ".Ail2";

            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");

            string strDefaultBOPPath = strDriveLetter + @"ait8\Actuary\TCMActuary\Valtest\InForce_File\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\BOP";
            string strDefaultInforcePath = strDriveLetter + @"ait8\Actuary\TCMActuary\Valtest\InForce_File\Q" + defaultQuarterString + defaultQuarterYr2Digit + @"\AILFiles\Inforce";
            string strDefaultBOPPathAndFile = strDefaultBOPPath + @"\TEDFileOverride1" + defaultMonthDay + ".Ail2";
            string strDefaultInforcePathAndFile = strDefaultInforcePath + @"\TEDFileOverride1" + defaultMonthDay + ".Ail2";

            string strDestination = strDriveLetter + @"Destination_FileCheckAndCopy_PositiveTest";


            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultBOPPath);
            ThisDirectory.CreateDirectoryIfNotExists(strInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDefaultInforcePath);
            ThisDirectory.CreateDirectoryIfNotExists(strDestination);


            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            var inforcePathFile = new FileInfo(strInforcePathAndFile);
            var inforceCreate = inforcePathFile.Create();

            // you can use dispose here, for it returns filestream
            inforceCreate.Dispose();

            var defaultBOPPathFile = new FileInfo(strDefaultBOPPathAndFile);
            var defaultBOPCreate = defaultBOPPathFile.Create();

            defaultBOPCreate.Dispose();

            var defaultInforcePathFile = new FileInfo(strDefaultInforcePathAndFile);
            var defaultInforceCreate = defaultInforcePathFile.Create();

            defaultInforceCreate.Dispose();


            NameValueCollection _params = new NameValueCollection();

            _params.Add("SourceDirectoryPattern", strDriveLetter + @"ait8\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP");
            _params.Add("ProcessTimeToMonthOffset", "-1");

            _params.Add("DestinationDirectory", strDriveLetter + @"Destination_FileCheckAndCopy_PositiveTest");
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("ReplaceFromString", @"Q\d{3}"); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");

            FileCheckAndCopy fcc = new FileCheckAndCopy();
            fcc.ProcessParams = _params;
            fcc.Execute();

            Assert.AreNotEqual(strBOPPathBeforeFormats, fcc.ResolvedDirectoryName);


           RemoveDirectory(parentDir);


        }

        [TestMethod]

        public void FileErrorChecker_WithError_ShouldRunFileErrorCheckerWithError()
        {
            //This test creates a directory and a file and writes a delimited set of rows
            //to the file with pipes being the accepted delimiter.  However, a "," is used
            //inadvertantly causing an error to be detected and the Assert is expecting
            //the sError string to return a written error.  Lastly, the file and directories
            //are cleared out.
            string _errorIndicatorList = ",";

            string _sourceFile = strDriveLetter + @"Temp\FileErrorChecker_WithError\error.log";
            string sError = "";

            ThisDirectory.CreateDirectoryIfNotExists(Path.GetDirectoryName(_sourceFile));
           

            if (!File.Exists(_sourceFile))
            {
                using (StreamWriter sw = File.CreateText(_sourceFile))
                {
                    sw.WriteLine("Hello|Jane|Commet");

                    sw.WriteLine("Jasper|Creek,Shed");
                }
            }



            FileErrorChecker fileErrorChecker = new FileErrorChecker(_errorIndicatorList,
               _sourceFile);

            if (fileErrorChecker.FileContainsErrors())
            {

                sError += "\r\n" + System.IO.File.ReadAllText(@_sourceFile);
            }

            Assert.IsNotNull(sError);

            File.Delete(_sourceFile);

            RemoveDirectory(Path.GetDirectoryName(_sourceFile));
        }

        [TestMethod]

        public void FileErrorChecker_WithoutError_ShouldRunFileErrorCheckerWithoutError()
        {
            //This test creates a directory and a file and writes a delimited set of rows
            //to the file with a pipe delimiter being the accepted delimiter.  All rows contain just
            //pipe delimiters resulting in no errors and the Assert expects none resulting in a 
            //positive test.  Lastly, the file and directories are cleared out.
            string _errorIndicatorList = ",";

            string _sourceFile = strDriveLetter + @"Temp\FileErrorChecker_WithoutError\error.log";
            string sError = "";


            ThisDirectory.CreateDirectoryIfNotExists(Path.GetDirectoryName(_sourceFile));
           

            if (!File.Exists(_sourceFile))
            {
                using (StreamWriter sw = File.CreateText(_sourceFile))
                {
                    sw.WriteLine("Hello|Jeff|Jone");
                }
            }





            FileErrorChecker fileErrorChecker = new FileErrorChecker(_errorIndicatorList,
               _sourceFile);

            if (fileErrorChecker.FileContainsErrors())
            {
                sError += "\r\n" + System.IO.File.ReadAllText(@_sourceFile);
            }


            Assert.AreEqual("", sError);


            
            RemoveDirectory(Path.GetDirectoryName(_sourceFile));
        }

        [TestMethod]

        public void ProcessHelper_WrapQuotes_ShouldWrapQuotes()
        {


            //This test checks directory to see if it starts with and ends with "\" and since it does not
            //it adds it to the beginning of the path string and does an assert expecting that
            //the directory match that with the "\" at the beginning and end.  Lastly, directories are removed
            string _dir = strDriveLetter + @"ProcessHelper_WrapQuotes_PositiveTest";

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_dir);           

            string _compareTo = ProcessHelper.WrapQuotes(_dir);

            string _compare = "\"" + _dir + "\"";

            Assert.AreEqual(_compare, _compareTo);


            RemoveDirectory(_dir);

        }


        [TestMethod]

        public void ProcessHelper_WrapQuotes_ShouldNotWrapQuotes()
        {

            //This test checks directory to see if it starts with and ends with "\" and since it does not
            //it adds it to the beginning of the path string and does an assert expecting that
            //the directory not match that because of extra "\" at the beginning and end.  Lastly, directories are removed

            string _dir = strDriveLetter + @"ProcessHelper_WrapQuotes_NegativeTest";

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_dir);

            string _compareTo = ProcessHelper.WrapQuotes(_dir);

            string _compare = @"\" + _dir + @"\";

            Assert.AreNotEqual(_compare, _compareTo);


            RemoveDirectory(_dir);
        }

        [TestMethod]
        
        public void StringExtensions_Contains_ShouldContainStringExtensions()
        {

            //This test creates a directory called StringExtensions_Contain_PositiveTest
            // and runs a test to see if the directory string contains the word "Contain"
            //and since it does, it asserts as true resulting in a positive test.  Lastly,
            //directory removed.
            string _dir = strDriveLetter + @"StringExtensions_Contain_PositiveTest";

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_dir);


            bool _isTrue = StringExtensions.Contains(_dir, "Contain", 0);

            Assert.IsTrue(_isTrue);


            RemoveDirectory(_dir);
        }

        [TestMethod]
        
        public void StringExtensions_Contains_ShouldNotContainStringExtensions()
        {

            //This test creates a directory called StringExtensions_Contain_PositiveTest
            //and runs a test to see if the directory string contains the word "NotContain"
            //and since it does not, it asserts as false resulting in a negative test.  Lastly,
            //directory removed.
            string _dir = strDriveLetter + @"StringExtensions_Contain_NegativeTest";

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_dir);

            bool _isFalse = StringExtensions.Contains(_dir, "NotContain", 0);

            Assert.IsFalse(_isFalse);


            RemoveDirectory(_dir);
        }

        [TestMethod]

        public void WildcardFileCopy_Copy_ShouldRunWildCardFileCopy()
        {

            //This test uses the Fiscal Name Resolver to identify the Q{QuarterHolder}{YearHolderTwoDigit}
            //in the file path, creates the directory structure and creates a file called "TEDFileOverrideQ118.Ail2"
            //(Note: Q118 could differ based on date test is ran).  Also, a destination folder is created and the file uses
            //the WildCardFileCopy to copy the file in source "TEDFileOverrideQ118.Ail2" to destination path as "TEDFileOverride.Ail2"
            //An assert to check for equal expected results yields a succesful positive test.  
            //Lastly, directories and files are cleared out.

            string _wildCardExclusions = "";

            string parentDir = strDriveLetter + @"ait2";
            string _sourceDirectory = strDriveLetter + @"ait2\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP";
            string _defaultSourceDirectory = strDriveLetter + @"ait2\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce";
            string _sourceWildcard = "TEDFileOverride*.Ail2";

            string _destinationDirectory = strDriveLetter + @"Destination_WildCardCopy_PositiveTest";
            string _regexFileRenamePattern = @"Q\d{3}";
            string _fileNameReplaceToString = "";
            bool _overWriteDestinatinoFile = true;
            int _expectedNumberFiles = 1;
            bool _isCopy = true;

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterDay = quarterDate.Day;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterDay = defaultQuarterDate.Day;
            int defaultQuarterYr = defaultQuarterDate.Year;


            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string quarterString = "";
            string defaultQuarterString = "";
            //datetime object based on - month


            if (quarterMonth >= 1 && quarterMonth <= 3)
            {
                quarterString = "1";
            }
            else if (quarterMonth >= 4 && quarterMonth <= 6)
            {
                quarterString = "2";
            }
            else if (quarterMonth >= 7 && quarterMonth <= 9)
            {
                quarterString = "3";
            }
            else
            {
                quarterString = "4";
            }

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }

            string monthDay = "Q" + quarterString + quarterDate.ToString("yy");
            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");
            string strBOPPathAndFile = _sourceDirectory + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";


            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_sourceDirectory);
            ThisDirectory.CreateDirectoryIfNotExists(_destinationDirectory);
            
            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            WildCardFileCopy wcfc = new WildCardFileCopy(_sourceDirectory, _sourceWildcard,
            _wildCardExclusions, _destinationDirectory, _regexFileRenamePattern,
            _fileNameReplaceToString, _overWriteDestinatinoFile,
            _expectedNumberFiles, _isCopy);



            NameValueCollection _params = new NameValueCollection();
            _params.Add("OverrideSourceDirectoryPattern", _sourceDirectory);
            _params.Add("DefaultSourceDirectoryPattern", _sourceDirectory);
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultSourceDirectoryPattern", _defaultSourceDirectory);
            _params.Add("ProcessTimeToMonthOffset", _processTimeToMonthOffset.ToString());
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");
            _params.Add("DestinationDirectory", _destinationDirectory);
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", _regexFileRenamePattern); //in the case of dates in a file name.
            _params.Add("ReplaceToString", _fileNameReplaceToString);


            wcfc.Copy(_params);

            // Only get files that begin with the letter "c."
            string[] dirs = Directory.GetFiles(_destinationDirectory, "*.Ail2");
            string strDestinationFileName = "";
            foreach (string dir in dirs)
            {
                strDestinationFileName = Path.GetFileName(dir);
            }

            Assert.AreEqual("TEDFileOverride.Ail2", strDestinationFileName);


            RemoveDirectory(parentDir);


        }

        [TestMethod]

        public void WildcardFileCopy_Copy_ShouldNotRunWildcardFileCopy()
        {
            //This test uses the Fiscal Name Resolver to identify the Q{QuarterHolder}{YearHolderTwoDigit}
            //in the file path, creates the directory structure and creates a file called "TEDFileOverrideQ118.Ail2"
            //(Note: Q118 could differ based on date test is ran).  Also, a destination folder is created and the file uses
            //the WildCardFileCopy to copy the file in source "TEDFileOverrideQ118.Ail2" to destination path as "TEDFileOverride.Ail2"
            //An assert to check for not equal yields a succesful negative test.  Lastly, directories and files are cleared out.

            string _wildCardExclusions = string.Empty;

            string parentDir = strDriveLetter + @"ait3";
            string _sourceDirectory = strDriveLetter + @"ait3\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP";
            string _defaultSourceDirectory = strDriveLetter + @"ait3\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce";
            string _sourceWildcard = "TEDFileOverride*.Ail2";

            string _destinationDirectory = strDriveLetter + @"Destination_WildCardCopy_NegativeTest";
            string _regexFileRenamePattern = @"Q\d{3}";
            string _fileNameReplaceToString = string.Empty;
            bool _overWriteDestinatinoFile = true;
            int _expectedNumberFiles = 1;
            bool _isCopy = true;

            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;
            int quarterDay = quarterDate.Day;
            int quarterYr = quarterDate.Year;

            int defaultQuarterMonth = defaultQuarterDate.Month;
            int defaultQuarterDay = defaultQuarterDate.Day;
            int defaultQuarterYr = defaultQuarterDate.Year;


            string quarterYr2Digit = quarterYr.ToString().Substring(2, 2);
            string defaultQuarterYr2Digit = defaultQuarterYr.ToString().Substring(2, 2);

            string defaultQuarterString = string.Empty;

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }
            
            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");
            string strBOPPathAndFile = _sourceDirectory + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
           
            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_sourceDirectory);
            ThisDirectory.CreateDirectoryIfNotExists(_destinationDirectory);


            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            WildCardFileCopy wcfc = new WildCardFileCopy(_sourceDirectory, _sourceWildcard,
            _wildCardExclusions, _destinationDirectory, _regexFileRenamePattern,
            _fileNameReplaceToString, _overWriteDestinatinoFile,
            _expectedNumberFiles, _isCopy);


            NameValueCollection _params = new NameValueCollection();
            _params.Add("OverrideSourceDirectoryPattern", _sourceDirectory);
            _params.Add("DefaultSourceDirectoryPattern", _sourceDirectory);
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultSourceDirectoryPattern", _defaultSourceDirectory);
            _params.Add("ProcessTimeToMonthOffset", _processTimeToMonthOffset.ToString());
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");
            _params.Add("DestinationDirectory", _destinationDirectory);
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", _regexFileRenamePattern); //in the case of dates in a file name.
            _params.Add("ReplaceToString", _fileNameReplaceToString);


            wcfc.Copy(_params);

            // Only get files that begin with the letter "c."
            string[] dirs = Directory.GetFiles(_destinationDirectory, "*.Ail2");
            string strDestinationFileName = string.Empty;
            foreach (string dir in dirs)
            {
                strDestinationFileName = Path.GetFileName(dir);
            }

            Assert.AreNotEqual("TEDFileOverrideQ118.Ail2", strDestinationFileName);


            RemoveDirectory(parentDir);


        }

        [TestMethod]
        [Ignore]
        [ExpectedException(typeof(System.Exception), "'ipconfg' is not recognized as an internal or external command, operable program or batch file.")]

        public void ExternalProcess_ShouldGenerateExpectedException()
        {            
            NameValueCollection theseParams = new NameValueCollection();
            theseParams.Add("ProcessLocation", "../../TestFixtures/Resources/TestCommandFailure.cmd");
            theseParams.Add("ShellExecute", "true");
            theseParams.Add("Arguments", "ipconfg");
            ExternalProcess thisProcess = new ExternalProcess();
            thisProcess.ProcessParams = theseParams;
            thisProcess.Execute();
        }

        [TestMethod]
        [Ignore]
        public void ExternalProcess_ShouldRunExternalProcess()
        {
            NameValueCollection theseParams = new NameValueCollection();
            theseParams.Add("ProcessLocation", "../../TestFixtures/Resources/TestCommand.cmd");
            theseParams.Add("ShellExecute", "true");
            theseParams.Add("Arguments", "");
            ExternalProcess thisProcess = new ExternalProcess();
            thisProcess.ProcessParams = theseParams;
            thisProcess.Execute();
        }

        [TestMethod]
        [Ignore]
        public void ExternalProcessHelper_ShouldRunExternalProcessHelper()
        {           
            ExternalProcessHelper.ExecuteProcess(false, "", "../../TestFixtures/Resources/TestCommand.cmd");
        }

        [TestMethod]
        [Ignore]
        [ExpectedException(typeof(Exception), "'ipconfg' is not recognized as an internal or external command, operable program or batch file.")]
        public void ExternalProcessHelper_ShouldGenerateExpectedException()
        {

            //This test creates a batch command file and generates an exception when executing the batch command.  
            //It is suppose to be running ipconfig but it is spelled wrong so the expected exception occurs.
            string strDirectory = strDriveLetter + @"Local\ExternalProcessHelper_ExpectedException\";
            string strFile = strDirectory + @"MyCommand.cmd";



            ThisDirectory.CreateDirectoryIfNotExists(strDirectory);
           
            if (!File.Exists(strFile))
            {
                string[] lines = { "ipconfg" };
                // WriteAllLines creates a file, writes a collection of strings to the file,
                // and then closes the file.  You do NOT need to call Flush() or Close().
                File.WriteAllLines(strFile, lines);

            }

            ExternalProcessHelper.ExecuteProcess(false, string.Empty, strFile);


            RemoveDirectory(strDirectory);

        }

        [TestMethod]

        public void CreateDynamicDirectoryAndCopyFiles_ShouldCreateDynamicDirectoryAndCopyFiles()
        {

            //This test successfully creates a source and destination location and uses offset to determine {QuarterHolder}{YearHolderTwoDigit}
            //and creates a file and copies from source to destination since expected file count matches actual file count.
            string parentDir = strDriveLetter + @"ait6";
            string _sourceDirectory = strDriveLetter + @"ait6\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP";
            string _defaultSourceDirectory = strDriveLetter + @"ait6\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce";
            string _destinationDirectory = strDriveLetter + @"Destination_CreateDynamicDirectoryAndCopyFiles_PositiveTest";
            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;            
            int quarterYr = quarterDate.Year;            
            int defaultQuarterMonth = defaultQuarterDate.Month;            
            int defaultQuarterYr = defaultQuarterDate.Year;                       
            
            string defaultQuarterString = string.Empty; 

            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }            
            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");

            string strBOPPathFormatted = strDriveLetter + @"ait6\Actuary\TCMActuary\Valtest\InForce_File_Negative\" + defaultMonthDay + @"\AILFiles\BOP";
            string strBOPPathAndFileFormatted = strDriveLetter + @"ait6\Actuary\TCMActuary\Valtest\InForce_File_Negative\" + defaultMonthDay + @"\AILFiles\BOP\TEDFileOverrideQ417.Ail2";            

            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(strBOPPathFormatted);
            ThisDirectory.CreateDirectoryIfNotExists(_destinationDirectory);
            
            var bopPathFile = new FileInfo(strBOPPathAndFileFormatted);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            NameValueCollection _params = new NameValueCollection();
            _params.Add("DirectoryToCreatePattern", strBOPPathFormatted);
            _params.Add("DefaultSourceDirectoryPattern", strBOPPathFormatted);
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultSourceDirectoryPattern", _defaultSourceDirectory);
            _params.Add("ProcessTimeToMonthOffset", _processTimeToMonthOffset.ToString());
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");
            _params.Add("SourceDirectoryPattern", strBOPPathFormatted);
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", ""); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");

            CreateDynamicDirectoryAndCopyFiles create = new CreateDynamicDirectoryAndCopyFiles();
            create.ProcessParams = _params;
            create.Execute();



            RemoveDirectory(parentDir);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Number of files returned does not match expected number of files. Expected 1, 0 files returned.")]
        public void CreateDynamicDirectoryAndCopyFiles_ExpectedException()
        {
            //This test creates source directory and replaces {QuarterHolder}{YearHolder} with offset month and quarter and year is determined 
            //in source directory structure.  Does the same thing with the destination directory.  It is expecting 1 file to be found when 0 files
            //are returned.  This generates an exception.


            string parentDir = strDriveLetter + @"ait7";
            string _sourceDirectory = strDriveLetter + @"ait7\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP";
            string _defaultSourceDirectory = strDriveLetter + @"ait7\Actuary\TCMActuary\Valtest\InForce_File\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\Inforce";
            string _destinationDirectory = strDriveLetter + @"Destination_CreateDynamicDirectoryAndCopyFiles_PositiveTest";
            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);

            int quarterMonth = quarterDate.Month;                        

            int defaultQuarterMonth = defaultQuarterDate.Month;                        
            
            string defaultQuarterString = string.Empty;
            //datetime object based on - month


            if (defaultQuarterMonth >= 1 && defaultQuarterMonth <= 3)
            {
                defaultQuarterString = "1";
            }
            else if (defaultQuarterMonth >= 4 && defaultQuarterMonth <= 6)
            {
                defaultQuarterString = "2";
            }
            else if (defaultQuarterMonth >= 7 && defaultQuarterMonth <= 9)
            {
                defaultQuarterString = "3";
            }
            else
            {
                defaultQuarterString = "4";
            }
            
            string defaultMonthDay = "Q" + defaultQuarterString + defaultQuarterDate.ToString("yy");

            string strBOPPathFormatted = strDriveLetter + @"ait7\Actuary\TCMActuary\Valtest\InForce_File_Negative\" + defaultMonthDay + @"\AILFiles\BOP";            

            string strBOPPathAndFile = _sourceDirectory + @"\TEDFileOverride" + defaultMonthDay + ".Ail2";
           
            //check to see if directories exist.  Create if they don't

            ThisDirectory.CreateDirectoryIfNotExists(_sourceDirectory);
            ThisDirectory.CreateDirectoryIfNotExists(_destinationDirectory);


            var bopPathFile = new FileInfo(strBOPPathAndFile);
            var bopCreate = bopPathFile.Create();

            // you can use dispose here, for it returns filestream
            bopCreate.Dispose();

            NameValueCollection _params = new NameValueCollection();
            _params.Add("DirectoryToCreatePattern", strBOPPathFormatted);
            _params.Add("DefaultSourceDirectoryPattern", strBOPPathFormatted);
            _params.Add("OverrideProcessTimeToMonthOffset", "-1");
            _params.Add("DefaultSourceDirectoryPattern", _defaultSourceDirectory);
            _params.Add("ProcessTimeToMonthOffset", _processTimeToMonthOffset.ToString());
            _params.Add("DefaultProcessTimeToMonthOffset", "-3");
            _params.Add("SourceDirectoryPattern", strBOPPathFormatted);
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");
            _params.Add("ExpectedNumberOfFiles", "1");
            _params.Add("Wildcard", "TEDFileOverride*.Ail2");
            _params.Add("WildcardExclusions", "");
            _params.Add("ReplaceFromString", ""); //in the case of dates in a file name.
            _params.Add("ReplaceToString", "");

            CreateDynamicDirectoryAndCopyFiles create = new CreateDynamicDirectoryAndCopyFiles();
            create.ProcessParams = _params;
            create.Execute();


            RemoveDirectory(parentDir);


        }

        [TestMethod]

        public void CreateDynamicDirectoryProcess_ShouldCreateDynamicDirectory()
        {
            //This test creates a dynamic directory by determining {QuarterHolder}{YearHolderTwoDigit} by 
            //taking the Date Months and adding the offset and determining what to replace {QuarterHolder}{YearHolderTwoDigit}
            //with.  The folder is created as C:\Source\OneMDLCCode\UnitTest\bin\TestDir\ait14\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q118\AILFiles\BOP
            
            int _processTimeToMonthOffset = -1;
            int _defaultProcessTimeToMonthOffset = -3;

            DateTime _processDate = DateTime.Now;
            DateTime quarterDate = _processDate.AddMonths(_processTimeToMonthOffset);
            DateTime defaultQuarterDate = _processDate.AddMonths(_defaultProcessTimeToMonthOffset);
            
            int defaultQuarterMonth = defaultQuarterDate.Month;

            string strDirectoryToCreatePattern = strDriveLetter + @"ait14\Actuary\TCMActuary\Valtest\InForce_File_Negative\Q{QuarterHolder}{YearHolderTwoDigit}\AILFiles\BOP";
                       
            NameValueCollection _params = new NameValueCollection();
            _params.Add("DirectoryToCreatePattern", strDirectoryToCreatePattern);
            _params.Add("ProcessTimeToMonthOffset", "-1");
            _params.Add("Q1MonthMap", "1,2,3");
            _params.Add("Q2MonthMap", "4,5,6");
            _params.Add("Q3MonthMap", "7,8,9");
            _params.Add("Q4MonthMap", "10,11,12");

            CreateDynamicDirectoryProcess create = new CreateDynamicDirectoryProcess();
            create.ProcessParams = _params;
            create.Execute();



        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {

            UnitTest.UnitTest1 prop = new UnitTest.UnitTest1();
            string str = prop.strDriveLetter;


            string[] stringArray = { str + @"ait", str + @"ait1", str + @"ait2", str + @"ait3", str + @"ait4", str + @"ait5", str + @"ait6",
                                     str + @"ait7", str + @"ait10", str + @"ait11",str + @"ait14",str + @"TEMP\Archive", str + @"TEMP\ValProd",str + @"Destination",
                                     str + @"Destination_CreateDynamicDirectoryAndCopyFiles_PositiveTest",
                                     str + @"Destination_DynamicDirectoryCopyFileProcess_PositiveTest",
                                     str + @"Destination_FileCheckAndCopy_PositiveTest", str + @"Destination_Negative",
                                     str + @"Destination_WildCardCopy_NegativeTest", str + @"Destination_WildCardCopy_PositiveTest",
                                     str + @"Local\ExternalProcessHelper_ExpectedException",str + @"Local\ExternalProcess_ExpectedException", str + @"Local", str + @"Temp"};

            foreach (string item in stringArray)
            {
                if (Directory.Exists(item))

                    RemoveDirectory(item);
            }

        }

        private static void CreateTempADB2Files(string baseDir)
        {
            DateTime today = DateTime.Now;
            Directory.CreateDirectory(baseDir + today.ToString("yyyy") + "Q1" + "_BOPParallel");
            Directory.CreateDirectory(baseDir + today.ToString("yyyy") + "Q2" + "_BOPParallel");
            Directory.CreateDirectory(baseDir + today.ToString("yyyy") + "Q3" + "_BOPParallel");
            Directory.CreateDirectory(baseDir + today.ToString("yyyy") + "Q4" + "_BOPParallel");

            using (File.Create(baseDir +
                today.ToString("yyyy") + "Q1" + @"_BOPParallel\" + "VDA.104.0003d." +
                today.ToString("yyyy") + "Q1.adb2")) { }
            using (File.Create(baseDir +
                today.ToString("yyyy") + "Q2" + @"_BOPParallel\" + "VDA.104.0003d." +
                today.ToString("yyyy") + "Q2.adb2")) { }
            using (File.Create(baseDir +
                today.ToString("yyyy") + "Q3" + @"_BOPParallel\" + "VDA.104.0003d." +
                today.ToString("yyyy") + "Q3.adb2")) { }
            using (File.Create(baseDir +
                today.ToString("yyyy") + "Q4" + @"_BOPParallel\" + "VDA.104.0003d." +
                today.ToString("yyyy") + "Q4.adb2")) { }

        }

        private static void RemoveDirectory(string strDirectory)
        {
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            DirectoryInfo directory = new DirectoryInfo(strDirectory);
            foreach (FileInfo file in directory.GetFiles())
            {
                file.Delete();
            }
            directory.Delete(true);
        }



    }
}
