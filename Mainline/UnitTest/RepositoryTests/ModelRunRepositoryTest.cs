﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelRunRepositoryTest
    {
        [TestMethod]
        public void CanAddModelRun()
        {
            var testModelRun = new ModelRun
            {
                Run = "10"
            };

            var mockSet = new Mock<DbSet<ModelRun>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelRun = mockContext.Setup(m => m.ModelRun).Returns(mockSet.Object);

            var repo = new ModelRunRepository(mockContext.Object);
            repo.Add(testModelRun);

            mockSet.Verify(m => m.Add(It.IsAny<ModelRun>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelRun()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRunRepository(mockContext.Object);
            repo.Add(new ModelRun());
        }

        [TestMethod]
        public void CanDeleteModelRun()
        {
            var testModelRun = new ModelRun
            {
                ID = 1,
                Run = "10"
            };

            var mockSet = new Mock<DbSet<ModelRun>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRun).Returns(mockSet.Object);

            var repo = new ModelRunRepository(mockContext.Object);
            repo.Delete(testModelRun);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelRun>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelRun()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRunRepository(mockContext.Object);
            repo.Delete(new ModelRun());
        }

        [TestMethod]
        public void CanGetModelRunById()
        {
            var ModelRun = new List<ModelRun>()
            {
                new ModelRun
                        {
                            ID = 1,
                            Run = "10"
                        },
                new ModelRun
                        {
                            ID = 2,
                            Run = "42"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRun);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRun).Returns(mockSet.Object);

            var repo = new ModelRunRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("10", returnedTestObj.Run);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelRun()
        {
            var applications = new List<ModelRun>()
            {
                new ModelRun
                        {
                            ID = 1,
                            Run = "10"
                        },
                new ModelRun
                        {
                            ID = 2,
                            Run = "42"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRun).Returns(mockSet.Object);

            var repo = new ModelRunRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetAllModelRun()
        {
            var ModelRun = new List<ModelRun>()
            {
                new ModelRun
                        {
                            ID = 1,
                            Run = "10"
                        },
                new ModelRun
                        {
                            ID = 2,
                            Run = "42"
                        },
                new ModelRun
                        {
                            ID = 3,
                            Run = "100"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRun);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRun).Returns(mockSet.Object);

            var repo = new ModelRunRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelRun()
        {
            var ModelRun = new List<ModelRun>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRun);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRun).Returns(mockSet.Object);

            var repo = new ModelRunRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
