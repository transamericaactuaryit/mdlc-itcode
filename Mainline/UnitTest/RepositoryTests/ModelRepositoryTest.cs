﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelRepositoryTest
    {
        [TestMethod]
        public void CanAddModel()
        {
            var testModel = new Model
            {
                Name = "Rates Attribute"
            };

            var mockSet = new Mock<DbSet<Model>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModel = mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            repo.Add(testModel);

            mockSet.Verify(m => m.Add(It.IsAny<Model>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModel()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRepository(mockContext.Object);
            repo.Add(new Model());
        }

        [TestMethod]
        public void CanDeleteModel()
        {
            var testModel = new Model
            {
                ID = 1,
                Name = "Test Model"
            };

            var mockSet = new Mock<DbSet<Model>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            repo.Delete(testModel);

            mockSet.Verify(m => m.Remove(It.IsAny<Model>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModel()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRepository(mockContext.Object);
            repo.Delete(new Model());
        }

        [TestMethod]
        public void CanGetModelById()
        {
            var Model = new List<Model>()
            {
                new Model
                        {
                            ID = 1,
                            Name = "Test Model 1"
                        },
                new Model
                        {
                            ID = 2,
                            Name = "Test Model 2"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(Model);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("Test Model 1", returnedTestObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModel()
        {
            var applications = new List<Model>()
            {
                new Model
                        {
                            ID = 1,
                            Name = "Test Model 1"
                        },
                new Model
                        {
                            ID = 2,
                            Name = "Test Model 2"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }


        [TestMethod]
        public void CanGetLatestByModelTypeId()
        {
            var earlierDate = DateTime.Now.AddDays(-5);
            var mostRecentDate = DateTime.Now.AddDays(-2);

            var models = new List<Model>()
            {
                new Model
                        {
                            ID = 1,
                            Name = "Base",
                            CreatedDate = earlierDate,
                            ModelTypeID = 1
                        },
                new Model
                        {
                            ID = 2,
                            Name = "Base",
                            CreatedDate = mostRecentDate,
                            ModelTypeID = 1
                        },
                new Model
                        {
                            ID = 3,
                            Name = "Some Other Model",
                            CreatedDate = DateTime.Now,
                            ModelTypeID = 2
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(models);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            var returnedTestObj = repo.GetLatestByModelTypeId(1);

            Assert.AreEqual(2, returnedTestObj.ID);
            Assert.AreEqual(mostRecentDate.ToString(), returnedTestObj.CreatedDate.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetLatestByModelTypeIdHasNoMatch()
        {
            var models = new List<Model>()
            {
                new Model
                        {
                            ID = 1,
                            Name = "Base",
                            CreatedDate = DateTime.Now,
                            ModelTypeID = 1
                        },
                new Model
                        {
                            ID = 2,
                            Name = "Base",
                            CreatedDate = DateTime.Now,
                            ModelTypeID = 1
                        },
                new Model
                        {
                            ID = 3,
                            Name = "Some Other Model",
                            CreatedDate = DateTime.Now,
                            ModelTypeID = 2
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(models);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            var returnedTestObj = repo.GetLatestByModelTypeId(3);
        }

        [TestMethod]
        public void CanGetModelsByGetAllByModelRunId()
        {
            var models = new List<Model>{
                        new Model
                                {
                                    ID = 1,
                                    Name = "Test Model 1",
                                    ModelRequestID = 1
                                },
                        new Model
                                {
                                    ID = 2,
                                    Name = "Test Model 2",
                                    ModelRequestID = 1
                                },
                        new Model
                                {
                                    ID = 3,
                                    Name = "Test Model 3",
                                    ModelRequestID = 1
                                }
                 }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(models);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            var returnedTestObjs = repo.GetAllByModelRequestId(1);

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ExceptionThrownWhenGetAllByModelRequestIdHasNoMatchModelRun()
        {
            var models = new List<Model>{
                        new Model
                                {
                                    ID = 1,
                                    Name = "Test Model 1",
                                    ModelRequestID = 1
                                },
                        new Model
                                {
                                    ID = 2,
                                    Name = "Test Model 2",
                                    ModelRequestID = 1
                                },
                        new Model
                                {
                                    ID = 3,
                                    Name = "Test Model 3",
                                    ModelRequestID = 1
                                }
                 }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(models);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.Model).Returns(mockSet.Object);

            var repo = new ModelRepository(mockContext.Object);
            var returnedTestObj = repo.GetAllByModelRequestId(2);
        }

    }
}
