﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelRequestRepositoryTest
    {
        [TestMethod]
        public void CanAddModelRequest()
        {
            var testModelRequest = new ModelRequest
            {
                Name = "Model Run 1"
            };

            var mockSet = new Mock<DbSet<ModelRequest>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelRequest = mockContext.Setup(m => m.ModelRequest).Returns(mockSet.Object);

            var repo = new ModelRequestRepository(mockContext.Object);
            repo.Add(testModelRequest);

            mockSet.Verify(m => m.Add(It.IsAny<ModelRequest>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelRequest()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRequestRepository(mockContext.Object);
            repo.Add(new ModelRequest());
        }

        [TestMethod]
        public void CanDeleteModelRequest()
        {
            var testModelRequest = new ModelRequest
            {
                ID = 1,
                Name = "Test ModelRequest"
            };

            var mockSet = new Mock<DbSet<ModelRequest>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequest).Returns(mockSet.Object);

            var repo = new ModelRequestRepository(mockContext.Object);
            repo.Delete(testModelRequest);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelRequest>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelRequest()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRequestRepository(mockContext.Object);
            repo.Delete(new ModelRequest());
        }

        [TestMethod]
        public void CanGetModelRequestById()
        {
            var testCollection = new List<ModelRequest>()
            {
                new ModelRequest
                        {
                            ID = 1,
                            Name = "Test ModelRequest 1"
                        },
                new ModelRequest
                        {
                            ID = 2,
                            Name = "Test ModelRequest 2"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
        
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequest).Returns(mockSet.Object);

            var repo = new ModelRequestRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1, false);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("Test ModelRequest 1", returnedTestObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchingModelRequest()
        {
            var applications = new List<ModelRequest>()
            {
                new ModelRequest
                        {
                            ID = 1,
                            Name = "Test ModelRequest 1"
                        },
                new ModelRequest
                        {
                            ID = 2,
                            Name = "Test ModelRequest 2"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequest).Returns(mockSet.Object);

            var repo = new ModelRequestRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3, false);
        }

        [TestMethod]
        public void CanGetAllModelRequestsByGet()
        {
            var ModelRequests = new List<ModelRequest>{
                        new ModelRequest
                                {
                                    ID = 1,
                                    Name = "Test ModelRequest 1"
                                },
                        new ModelRequest
                                {
                                    ID = 2,
                                    Name = "Test ModelRequest 2"
                                },
                        new ModelRequest
                                {
                                    ID = 3,
                                    Name = "Test ModelRequest 3"
                                }
                 }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRequests);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequest).Returns(mockSet.Object);

            var repo = new ModelRequestRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ExceptionThrownWhenGetReturnsNoModelRequests()
        {
            var ModelRequests = new List<ModelRequest>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRequests);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequest).Returns(mockSet.Object);

            var repo = new ModelRequestRepository(mockContext.Object);
            var returnedTestObj = repo.Get();
        }
    }
}
