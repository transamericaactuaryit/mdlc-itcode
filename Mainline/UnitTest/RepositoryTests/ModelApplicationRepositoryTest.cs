﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelApplicationRepositoryTest
    {
        [TestMethod]
        public void CanAddModelApplication()
        {
            var testModelApplication = new ModelApplication
            {
                Name = "ALFA"
            };

            var mockSet = new Mock<DbSet<ModelApplication>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelApplication = mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            repo.Add(testModelApplication);

            mockSet.Verify(m => m.Add(It.IsAny<ModelApplication>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelApplication()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelApplicationRepository(mockContext.Object);
            repo.Add(new ModelApplication());
        }

        [TestMethod]
        public void CanDeleteModelApplication()
        {
            var testModelApplication = new ModelApplication
            {
                ID = 1,
                Name = "ALFA"
            };

            var mockSet = new Mock<DbSet<ModelApplication>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            repo.Delete(testModelApplication);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelApplication>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelApplication()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelApplicationRepository(mockContext.Object);
            repo.Delete(new ModelApplication());
        }

        [TestMethod]
        public void CanGetModelApplicationById()
        {
            var applications = new List<ModelApplication>()
            {
                new ModelApplication
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelApplication
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("ALFA", returnedTestObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelApplication()
        {
            var applications = new List<ModelApplication>()
            {
                new ModelApplication
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelApplication
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetModelApplicationByName()
        {
            var testCollection = new List<ModelApplication>()
            {
                new ModelApplication
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelApplication
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            var returnedTestObj = repo.Get("AXIS");

            Assert.AreEqual(2, returnedTestObj.ID);
            Assert.AreEqual("AXIS", returnedTestObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetByNameHasNoMatch()
        {
            var testCollection = new List<ModelApplication>()
            {
                new ModelApplication
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelApplication
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            var returnedTestObj = repo.Get("No Existing Name");
        }

        [TestMethod]
        public void CanGetAllModelApplications()
        {
            var applications = new List<ModelApplication>()
            {
                new ModelApplication
                        {
                            ID = 1,
                            Name = "ALFA"
                        },
                new ModelApplication
                        {
                            ID = 2,
                            Name = "AXIS"
                        },
                new ModelApplication
                        {
                            ID = 3,
                            Name = "SOMEOTHER"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelApplications()
        {
            var applications = new List<ModelApplication>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelApplication).Returns(mockSet.Object);

            var repo = new ModelApplicationRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
