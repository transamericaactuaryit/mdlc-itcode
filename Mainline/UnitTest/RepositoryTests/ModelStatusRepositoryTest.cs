﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelStatusRepositoryTest
    {
        [TestMethod]
        public void CanAddModelStatus()
        {
            var testModelStatus = new ModelStatus
            {
                Status = "Complete"
            };

            var mockSet = new Mock<DbSet<ModelStatus>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelStatus = mockContext.Setup(m => m.ModelStatus).Returns(mockSet.Object);

            var repo = new ModelStatusRepository(mockContext.Object);
            repo.Add(testModelStatus);

            mockSet.Verify(m => m.Add(It.IsAny<ModelStatus>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelStatus()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelStatusRepository(mockContext.Object);
            repo.Add(new ModelStatus());
        }

        [TestMethod]
        public void CanDeleteModelStatus()
        {
            var testModelStatus = new ModelStatus
            {
                ID = 1,
                Status = "Complete"
            };

            var mockSet = new Mock<DbSet<ModelStatus>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelStatus).Returns(mockSet.Object);

            var repo = new ModelStatusRepository(mockContext.Object);
            repo.Delete(testModelStatus);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelStatus>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelStatus()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelStatusRepository(mockContext.Object);
            repo.Delete(new ModelStatus());
        }

        [TestMethod]
        public void CanGetModelStatusById()
        {
            var modelStatus = new List<ModelStatus>()
            {
                new ModelStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelStatus
                        {
                            ID = 2,
                            Status = "Complete"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(modelStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelStatus).Returns(mockSet.Object);

            var repo = new ModelStatusRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("Not Started", returnedTestObj.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelStatus()
        {
            var testCollection = new List<ModelStatus>()
            {
                new ModelStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelStatus).Returns(mockSet.Object);

            var repo = new ModelStatusRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetAllModelStatus()
        {
            var modelStatus = new List<ModelStatus>()
            {
                new ModelStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelStatus
                        {
                            ID = 2,
                            Status = "Started"
                        },
                new ModelStatus
                        {
                            ID = 3,
                            Status = "Completed"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(modelStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelStatus).Returns(mockSet.Object);

            var repo = new ModelStatusRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelStatus()
        {
            var modelStatus = new List<ModelStatus>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(modelStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelStatus).Returns(mockSet.Object);

            var repo = new ModelStatusRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
