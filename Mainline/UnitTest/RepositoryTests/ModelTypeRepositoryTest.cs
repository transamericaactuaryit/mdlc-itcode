﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelTypeRepositoryTest
    {
        [TestMethod]
        public void CanAddModelType()
        {
            var testModelType = new ModelType
            {
                Name = "RatesAttribution"
            };

            var mockSet = new Mock<DbSet<ModelType>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelType = mockContext.Setup(m => m.ModelType).Returns(mockSet.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            repo.Add(testModelType);

            mockSet.Verify(m => m.Add(It.IsAny<ModelType>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelType()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelTypeRepository(mockContext.Object);
            repo.Add(new ModelType());
        }

        [TestMethod]
        public void CanDeleteModelType()
        {
            var testModelType = new ModelType
            {
                ID = 1
            };

            var mockSet = new Mock<DbSet<ModelType>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSet.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            repo.Delete(testModelType);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelType>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelType()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelTypeRepository(mockContext.Object);
            repo.Delete(new ModelType());
        }

        [TestMethod]
        public void CanGetModelTypeById()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RatesAttribution"
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSet.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("RatesAttribution", returnedTestObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelType()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RatesAttribution"
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSet.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetModelTypeByNameAndModelGroup()
        {
            var modelType = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RatesAttribution",
                            ModelGroupID = 1
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base",
                           ModelGroupID = 1
                        },
            }.AsQueryable();

            var modelGroup = new List<ModelGroup>()
            {
                new ModelGroup
                {
                    ID = 1,
                    Name = "Fair Value"
                }
            }.AsQueryable();

            var mockSetModelType = UnitTestHelpers.CreateMockSet(modelType);
            var mockSetModelGroup = UnitTestHelpers.CreateMockSet(modelGroup);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSetModelType.Object);
            mockContext.Setup(m => m.ModelGroup).Returns(mockSetModelGroup.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            var returnedTestObj = repo.Get("RatesAttribution", "Fair Value");

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("RatesAttribution", returnedTestObj.Name);            
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetByNameHasNoMatchModelType()
        {
            var modelType = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RatesAttribution",
                            ModelGroupID = 1
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base",
                           ModelGroupID = 1
                        },
            }.AsQueryable();

            var modelGroup = new List<ModelGroup>()
            {
                new ModelGroup
                {
                    ID = 1,
                    Name = "Fair Value"
                }
            }.AsQueryable();

            var mockSetModelType = UnitTestHelpers.CreateMockSet(modelType);
            var mockSetModelGroup = UnitTestHelpers.CreateMockSet(modelGroup);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSetModelType.Object);
            mockContext.Setup(m => m.ModelGroup).Returns(mockSetModelGroup.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            var returnedTestObj = repo.Get("No Existing Name", "No Existing Model Group");
        }

        [TestMethod]
        public void CanGetAllModelTypes()
        {
            var testCollection = new List<ModelType>()
            {
                new ModelType
                        {
                            ID = 1,
                            Name = "RatesAttribution"
                        },
                new ModelType
                        {
                            ID = 2,
                            Name = "Base"
                        },
                new ModelType
                        {
                            ID = 3,
                            Name = "BOPParallel"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSet.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelTypes()
        {
            var applications = new List<ModelType>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelType).Returns(mockSet.Object);

            var repo = new ModelTypeRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
