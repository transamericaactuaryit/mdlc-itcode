﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelGroupRepositoryTest
    {
        [TestMethod]
        public void CanAddModelGroup()
        {
            var testModelGroup = new ModelGroup
            {
                Name = "Fair Value"
            };

            var mockSet = new Mock<DbSet<ModelGroup>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelGroup = mockContext.Setup(m => m.ModelGroup).Returns(mockSet.Object);

            var repo = new ModelGroupRepository(mockContext.Object);
            repo.Add(testModelGroup);

            mockSet.Verify(m => m.Add(It.IsAny<ModelGroup>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelGroup()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelGroupRepository(mockContext.Object);
            repo.Add(new ModelGroup());
        }

        [TestMethod]
        public void CanDeleteModelGroup()
        {
            var testModelGroup = new ModelGroup
            {
                ID = 1,
                Name = "Fair Value"
            };

            var mockSet = new Mock<DbSet<ModelGroup>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelGroup).Returns(mockSet.Object);

            var repo = new ModelGroupRepository(mockContext.Object);
            repo.Delete(testModelGroup);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelGroup>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelGroup()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelGroupRepository(mockContext.Object);
            repo.Delete(new ModelGroup());
        }

        [TestMethod]
        public void CanGetModelGroupById()
        {
            var testCollection = new List<ModelGroup>()
            {
                new ModelGroup
                        {
                            ID = 1,
                            Name = "Fair Value"
                        },
                new ModelGroup
                        {
                            ID = 2,
                            Name = "DAC"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelGroup).Returns(mockSet.Object);

            var repo = new ModelGroupRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("Fair Value", returnedTestObj.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelGroup()
        {
            var testCollection = new List<ModelGroup>()
            {
                new ModelGroup
                        {
                            ID = 1,
                            Name = "Fair Value"
                        },
                new ModelGroup
                        {
                            ID = 2,
                            Name = "DAC"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelGroup).Returns(mockSet.Object);

            var repo = new ModelGroupRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetAllModelGroups()
        {
            var testCollection = new List<ModelGroup>()
            {
                new ModelGroup
                        {
                            ID = 1,
                            Name = "Fair Value"
                        },
                new ModelGroup
                        {
                            ID = 2,
                            Name = "DAC"
                        },
                new ModelGroup
                        {
                            ID = 2,
                            Name = "Model Group 3"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(testCollection);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelGroup).Returns(mockSet.Object);

            var repo = new ModelGroupRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelGroups()
        {
            var applications = new List<ModelGroup>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelGroup).Returns(mockSet.Object);

            var repo = new ModelGroupRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
