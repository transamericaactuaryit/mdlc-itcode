﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelRunStatusRepositoryTest
    {
        [TestMethod]
        public void CanAddModelRunStatus()
        {
            var testModelRunStatus = new ModelRunStatus
            {
                Status = "Complete"
            };

            var mockSet = new Mock<DbSet<ModelRunStatus>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelRunStatus = mockContext.Setup(m => m.ModelRunStatus).Returns(mockSet.Object);

            var repo = new ModelRunStatusRepository(mockContext.Object);
            repo.Add(testModelRunStatus);

            mockSet.Verify(m => m.Add(It.IsAny<ModelRunStatus>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelRunStatus()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRunStatusRepository(mockContext.Object);
            repo.Add(new ModelRunStatus());
        }

        [TestMethod]
        public void CanDeleteModelRunStatus()
        {
            var testModelRunStatus = new ModelRunStatus
            {
                ID = 1,
                Status = "Complete"
            };

            var mockSet = new Mock<DbSet<ModelRunStatus>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRunStatus).Returns(mockSet.Object);

            var repo = new ModelRunStatusRepository(mockContext.Object);
            repo.Delete(testModelRunStatus);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelRunStatus>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelRunStatus()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRunStatusRepository(mockContext.Object);
            repo.Delete(new ModelRunStatus());
        }

        [TestMethod]
        public void CanGetModelRunStatusById()
        {
            var ModelRunStatus = new List<ModelRunStatus>()
            {
                new ModelRunStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRunStatus
                        {
                            ID = 2,
                            Status = "Complete"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRunStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRunStatus).Returns(mockSet.Object);

            var repo = new ModelRunStatusRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("Not Started", returnedTestObj.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelRunStatus()
        {
            var applications = new List<ModelRunStatus>()
            {
                new ModelRunStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRunStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRunStatus).Returns(mockSet.Object);

            var repo = new ModelRunStatusRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetAllModelRunStatus()
        {
            var ModelRunStatus = new List<ModelRunStatus>()
            {
                new ModelRunStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRunStatus
                        {
                            ID = 2,
                            Status = "Started"
                        },
                new ModelRunStatus
                        {
                            ID = 3,
                            Status = "Completed"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRunStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRunStatus).Returns(mockSet.Object);

            var repo = new ModelRunStatusRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelRunStatus()
        {
            var ModelRunStatus = new List<ModelRunStatus>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRunStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRunStatus).Returns(mockSet.Object);

            var repo = new ModelRunStatusRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
