﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTest
{
    [TestClass]
    public class ModelRequestStatusRepositoryTest
    {
        [TestMethod]
        public void CanAddModelRequestStatus()
        {
            var testModelRequestStatus = new ModelRequestStatus
            {
                Status = "Complete"
            };

            var mockSet = new Mock<DbSet<ModelRequestStatus>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            var mockedModelRequestStatus = mockContext.Setup(m => m.ModelRequestStatus).Returns(mockSet.Object);

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            repo.Add(testModelRequestStatus);

            mockSet.Verify(m => m.Add(It.IsAny<ModelRequestStatus>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenAddingInvalidModelRequestStatus()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            repo.Add(new ModelRequestStatus());
        }

        [TestMethod]
        public void CanDeleteModelRequestStatus()
        {
            var testModelRequestStatus = new ModelRequestStatus
            {
                ID = 1,
                Status = "Complete"
            };

            var mockSet = new Mock<DbSet<ModelRequestStatus>>();
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequestStatus).Returns(mockSet.Object);

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            repo.Delete(testModelRequestStatus);

            mockSet.Verify(m => m.Remove(It.IsAny<ModelRequestStatus>()), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDeletingInvalidModelRequestStatus()
        {
            var mockContext = new Mock<ModelManagerDbContext>();

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            repo.Delete(new ModelRequestStatus());
        }

        [TestMethod]
        public void CanGetModelRequestStatusById()
        {
            var ModelRequestStatus = new List<ModelRequestStatus>()
            {
                new ModelRequestStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 2,
                            Status = "Complete"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRequestStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequestStatus).Returns(mockSet.Object);

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            var returnedTestObj = repo.Get(1);

            Assert.AreEqual(1, returnedTestObj.ID);
            Assert.AreEqual("Not Started", returnedTestObj.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ExceptionThrownWhenGetIdHasNoMatchModelRequestStatus()
        {
            var applications = new List<ModelRequestStatus>()
            {
                new ModelRequestStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 2,
                            Status = "Completed"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(applications);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequestStatus).Returns(mockSet.Object);

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            var returnedTestObj = repo.Get(3);
        }

        [TestMethod]
        public void CanGetAllModelRequestStatus()
        {
            var ModelRequestStatus = new List<ModelRequestStatus>()
            {
                new ModelRequestStatus
                        {
                            ID = 1,
                            Status = "Not Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 2,
                            Status = "Started"
                        },
                new ModelRequestStatus
                        {
                            ID = 3,
                            Status = "Completed"
                        },
            }.AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRequestStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequestStatus).Returns(mockSet.Object);

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(3, returnedTestObjs.Count());
        }

        [TestMethod]
        public void GetAllReturnsEmptyListModelRequestStatus()
        {
            var ModelRequestStatus = new List<ModelRequestStatus>().AsQueryable();

            var mockSet = UnitTestHelpers.CreateMockSet(ModelRequestStatus);
            var mockContext = new Mock<ModelManagerDbContext>();
            mockContext.Setup(m => m.ModelRequestStatus).Returns(mockSet.Object);

            var repo = new ModelRequestStatusRepository(mockContext.Object);
            var returnedTestObjs = repo.Get();

            Assert.AreEqual(0, returnedTestObjs.Count());
        }
    }
}
