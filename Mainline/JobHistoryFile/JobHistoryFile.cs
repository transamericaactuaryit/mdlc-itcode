﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnablingSystems.ProcessInterfaces;
using System.Collections.Specialized;
using System.IO;

namespace EnablingSystems.OneMDLC.History
{
    public class JobHistoryFile : IJobHistory
    {
        private string _connectionInformation;
        public string ConnectionInformation
        {
            set
            {
                _connectionInformation = value;
            }
        }
        //Note: passing in jobhistoryID, at some point in the future, 
        //we might save history to a database, 
        public void SaveHistory(int jobHistoryID, NameValueCollection processHistoryItems)
        {
            if (string.IsNullOrEmpty(_connectionInformation))
            {
                throw new Exception("Job History Connection is not present");
            }

            if (!File.Exists(_connectionInformation))
            {
                var connectionDirectory = _connectionInformation.Remove(_connectionInformation.LastIndexOf(@"\"));
                //create directory if not exists.
                Directory.CreateDirectory(connectionDirectory);
            }

            using (System.IO.StreamWriter file =
                new StreamWriter(_connectionInformation))
            {
                foreach (string item in processHistoryItems)
                {
                    file.WriteLine(item + ":" + processHistoryItems[item] ?? "");
                }
            }
        }
    }
}
