﻿using EnablingSystems;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Configuration;
using System.Net.Http;
using System.ServiceProcess;
using System.Threading;

namespace ModelManager
{
    public struct ErrorsPerDay
    {
        public DateTime CurrentDate { get; set; }
        public int NumberOfErrors { get; set; }
    }

    public partial class ModelQueue_Service : ServiceBase
    {
        private Thread starterThread;
        private int m_Max_Errors_Per_Day;
        private ErrorsPerDay m_ErrorsPerDay = new ErrorsPerDay();
        private ExceptionManager _exceptionManager;
        LogSource _logSource;
        private string _exceptionPolicyName = "MainExceptionPolicy";
        //TODO: Replace this with real

        public ModelQueue_Service()
        {
            this.ServiceName = "Transamerica Model Queue";
            this.EventLog.Log = "Application";

            // These Flags set whether or not to handle that specific
            //  type of event. Set to true if you need it, false otherwise.
            this.CanHandlePowerEvent = true;
            this.CanHandleSessionChangeEvent = true;
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;

            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
            m_Max_Errors_Per_Day = int.Parse(ConfigurationManager.AppSettings["MaxErrorsPerDay"] ?? "100");

            m_ErrorsPerDay.CurrentDate = DateTime.Now;
            m_ErrorsPerDay.NumberOfErrors = 0;

            InitializeExceptionManager();

            var baseAddress = new Uri(ConfigurationManager.AppSettings["WebApiBaseAddress"]);
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseAddress);
            var queueManager = new ModelManagerPriorityQueueManager(modelManagerHttpClient,
                                        _exceptionManager,
                                        _logSource,
                                        _exceptionPolicyName,
                                        ConfigurationManager.AppSettings["Environment"],
                                        int.Parse(ConfigurationManager.AppSettings["MaxConcurrent"]),
                                        ConfigurationManager.AppSettings["OnDemandWorkArea"]);

            try
            {
                var delayTask = queueManager.Process();
                //queueManager.Process().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                //if new day, reset number of errors
                if ((DateTime.Now - m_ErrorsPerDay.CurrentDate).TotalDays >= 1)
                {
                    m_ErrorsPerDay.CurrentDate = DateTime.Now;
                    m_ErrorsPerDay.NumberOfErrors = 0;
                }

                string errorMessage = ex.Message;
                m_ErrorsPerDay.NumberOfErrors++;

                if (m_ErrorsPerDay.NumberOfErrors > m_Max_Errors_Per_Day)
                {
                    errorMessage += "Maximum error reached. Service shutting down.";
                    //TODO: Email notification here?
                    _exceptionManager.HandleException(ex, _exceptionPolicyName);

                    //cancel the queue to release processes.
                    queueManager.ProduceQueue(new CancellationToken(true));

                    //ExitCode 13816 = An unknown error has occurred
                    ExitCode = 13816;
                    Stop();
                    throw;
                }
            }
        }

        private void InitializeExceptionManager()
        {
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");

            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);
            Logger.Writer.TraceSources.TryGetValue("Exceptions", out _logSource);

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            _exceptionManager = exceptionFactory.CreateManager();
        }

        //private void Model_Timer_Tick(object sender, ElapsedEventArgs e)
        //{
        //    m_Model_Ping_Timer.Enabled = false;
        //    try
        //    {
        //        //if new day, reset number of errors
        //        if ((DateTime.Now - m_ErrorsPerDay.CurrentDate).TotalDays >= 1)
        //        {
        //            m_ErrorsPerDay.CurrentDate = DateTime.Now;
        //            m_ErrorsPerDay.NumberOfErrors = 0;
        //        }

        //        //TODO: Exception Policy name to be configurable
        //        QueueManager queueManager = new QueueManager(_memModelQuery, _exceptionManager, "MainExceptionPolicy",_logSource);
        //        _memModelQuery.FindUnProcessedModels();
        //        queueManager.ProcessQueue();

        //        m_Model_Ping_Timer.Enabled = true;
        //    }
        //    //Errors generated from model automation processes should not bubble up to here
        //    //error counts should be limited to this windows service
        //    catch (Exception eX)
        //    {
        //        bool shutdownService = false;
        //        string errorMessage = eX.Message;
        //        m_ErrorsPerDay.NumberOfErrors++;

        //        if (m_ErrorsPerDay.NumberOfErrors > m_Max_Errors_Per_Day)
        //        {
        //            errorMessage += "Maximum error reached. Service shutting down.";
        //            shutdownService = true;
        //            //TODO: Email notification here?
        //        }



        //        if (shutdownService)
        //        {
        //            this.OnStop();
        //        }
        //        else
        //        {
        //            m_Model_Ping_Timer.Enabled = true;
        //        }
        //    }
        //}

        protected override void OnStop()
        {
        }
    }

}
