﻿using Domain;
using EnablingSystems.ProcessInterfaces;

namespace EnablingSystems.Processes
{
    public class ModelRequestProcessor
    {
        private readonly ModelRequest _modelRequest;
        private readonly ModelManagerHttpClient _modelManagerHttpClient;
        private readonly IAutomationAggregator _automationAggregator;

        public ModelRequestProcessor(ModelRequest modelRequest, 
            ModelManagerHttpClient modelManagerHttpClient,
            IAutomationAggregator automationAggregator)
        {
            _modelRequest = modelRequest;
            _modelManagerHttpClient = modelManagerHttpClient;
            _automationAggregator = automationAggregator;
        }

        public void PreProcess()
        {
            foreach(var model in _modelRequest.Models)
            {
                //Mark previous staged model as completed
                //var previousModelStage =_modelManagerHttpClient.CompleteModelStep(model).Result;
                ////Clone model which creates next stage and set start time
                //var newModel = _modelManagerHttpClient.CloneModel(previousModelStage, true).Result;

                //_automationAggregator.MetaConfigFile = model.PreConfigurationFile;
                //_automationAggregator.SetupAutomations();
                //_automationAggregator.RunAutomations();

                ////Mark cloned model as completed
                //var newModelCompleted = _modelManagerHttpClient.CompleteModelStep(newModel).Result;
                ////Clone the completed model for next stage
                //var nextModelToProcess = _modelManagerHttpClient.CloneModel(newModelCompleted, true).Result;

            }
        }
    }
}
