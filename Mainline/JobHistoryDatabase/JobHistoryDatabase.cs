﻿using Domain;
using EnablingSystems.ProcessInterfaces;
using GemBox.Spreadsheet;
using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;

namespace EnablingSystems.MDLC.History
{

    public class DashboardDatabase : IDashboard
    {

        string _connectionInformation;
        string _dashboardID;
        DateTime _startTime;
        DateTime _endTime;
        int _jobInstanceID;
        //int _periodID = -1;
        int _historyID;
        string _processMessage;
        string _processName;
        bool _success;

        public string ConnectionInformation
        {
            get
            {
                return _connectionInformation;
            }

            set
            {
                _connectionInformation = value;
            }
        }

        public string DashboardID
        {
            get
            {
                return _dashboardID;
            }

            set
            {
                _dashboardID = value;
            }
        }

        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }

            set
            {
                _startTime = value;
            }
        }
        public DateTime EndTime
        {
            get
            {
                return _endTime;
            }

            set
            {
                _endTime = value;
            }
        }

        public int JobInstanceID
        {
            get
            {
                return _jobInstanceID;
            }

            set
            {
                _jobInstanceID = value;
            }
        }

        public string ProcessMessage
        {
            get
            {
                return _processMessage;
            }

            set
            {
                _processMessage = value;
            }
        }

        public string ProcessName
        {
            get
            {
                return _processName;
            }

            set
            {
                _processName = value;
            }
        }

        public bool Success
        {
            get
            {
                return _success;
            }

            set
            {
                _success = value;
            }
        }

        public int JobHistoryID
        {
            get
            {
                return _historyID;
            }
        }

        public void Save()
        {
            //GetCurrentPeriodID();
            _historyID = AddJobHistoryGetID();
        }

        //private void GetCurrentPeriodID()
        //{
        //    using (SqlConnection connection = new SqlConnection(_connectionInformation))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("SELECT MPP.PeriodID FROM "
        //                    + "ModelProcessingPeriod MPP "
        //                    + "INNER JOIN ModelType MT "
        //                    + "ON MPP.ModelTypeID = MT.ModelTypeID "
        //                    + "INNER JOIN JobInstance JI "
        //                    + "on JI.ModelTypeID = MT.ModelTypeID "
        //                    + "where JobInstanceID = @JobInstanceID "
        //                    + "and MPP.IsCurrent = 1", connection))
        //        {
        //            cmd.Parameters.AddWithValue("@JobInstanceID", _jobInstanceID);
        //            connection.Open();
        //            using (var reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    _periodID = reader.GetInt32(reader.GetOrdinal("PeriodID"));
        //                }
        //            }
        //        }
        //    }

        //    if (_periodID == -1)
        //    {
        //        throw new Exception("Could not find Period ID for JobInstanceID " + _jobInstanceID.ToString());
        //    }
        //}

        private int AddJobHistoryGetID()
        {
            using (SqlConnection sqlCon = new SqlConnection(_connectionInformation))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO JobHistory("
                    + "[DashboardID], [DashboardText], [Success],"
                    + "[StartTime], [EndTime],"
                    + "[Message] ) "
                    + "output INSERTED.JobHistoryID VALUES( @DashboardID, @DashboardText, @Success, "
                    + "@StartTime, @EndTime, @Message  )", sqlCon))
                {
                    cmd.Parameters.AddWithValue("@DashboardID", _dashboardID);
                    cmd.Parameters.AddWithValue("@DashboardText", _processName);
                    cmd.Parameters.AddWithValue("@Success", _success);
                    cmd.Parameters.AddWithValue("@StartTime", _startTime);
                    cmd.Parameters.AddWithValue("@EndTime", _endTime);
                    //TODO:property here..
                    cmd.Parameters.AddWithValue("@Message", "");
                    sqlCon.Open();

                    int modified = (int)cmd.ExecuteScalar();

                    
                    return modified;
                }
            }
        }
    }
    public class JobHistoryDatabase : IJobHistory
    {
        private readonly Model _model;
        private string _connectionInformation;

        public JobHistoryDatabase(Model model)
        {
            _model = model;
        }

        //ConnectionInformation will have the URL of the web api root, which will just be http://localhost:8020 unless we move the Web Api to another server. SJO 8/9/2018
        public string ConnectionInformation
        {
            set
            {
                _connectionInformation = value;
            }
        }

        public void SaveHistory(int jobHistoryID, NameValueCollection processHistoryItems)
        {
            foreach (string item in processHistoryItems)
            {
                SaveHistoryToDb(jobHistoryID, item, processHistoryItems[item]);
            }

        }

        private void SaveHistoryToDb(int jobHistoryID, string item, string itemValue)
        {
            //Use modelmanagerhttpclient with _model
        }
    }

    public class CreateHistoryExcel
    {
        private string _dashboardID = "";
        private string _connectionInformation;
        private string _fileLocation = @"C:\Temp\processHistory.xlsx";
        private int _daysBackToReport;
        public string ConnectionInformation
        {
            set
            {
                _connectionInformation = value;
            }
        }
        public string DashboardID
        {
            set
            {
                _dashboardID = value;
            }
        }

        public int DaysBackToReport
        {
            set
            {
                _daysBackToReport = value;
            }
        }

        public string FileLocation
        {

            set
            {
                _fileLocation = value;
            }
        }

        public void CreateDashboardReport()
        {
            //TODO:Alter the next two lines
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            SpreadsheetInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = FreeLimitReachedAction.ContinueAsTrial;

            DataSet ds = GetReportsPerUser();
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = null;

            string currentProcess = "";
            int row = 1;
            foreach(DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["DashboardText"].ToString() != currentProcess)
                {
                    currentProcess = dr["DashboardText"].ToString();
                    ws = ef.Worksheets.Add(currentProcess);
                    ws.PrintOptions.FitWorksheetWidthToPages = 1;
                    //TODO: Make these configurable
                    ws.Columns[0].Width = 20 * 256;
                    ws.Columns[1].Width = 20 * 256;
                    ws.Columns[2].Width = 50 * 256;
                    FormatHeaderCells(ws);

                    row++;
                    ws.Cells[row, 0].Value = "Start Time";
                    ws.Cells[row, 1].Value = ds.Tables[0].Rows[0]["StartTime"].ToString();
                    row++;
                    ws.Cells[row, 0].Value = "Start Time";
                    ws.Cells[row, 1].Value = ds.Tables[0].Rows[0]["StartTime"].ToString();

                    row = 1;
                }

                //ws.Cells[row, 0].Value = dr["StartTime"].ToString();
                //ws.Cells[row, 1].Value = dr["EndTime"].ToString();
                ws.Cells[row, 0].Value = dr["PropertyName"].ToString();
                ws.Cells[row, 1].Value = dr["PropertyValue"].ToString();
                row++;
            }



            ef.Save(_fileLocation);
        }

        private void FormatHeaderCells(ExcelWorksheet ws)
        {
            CellStyle tmpStyle = new CellStyle();
            tmpStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            tmpStyle.VerticalAlignment = VerticalAlignmentStyle.Center;
           // tmpStyle.FillPattern.SetSolid(Color.Chocolate);
            tmpStyle.Font.Weight = ExcelFont.BoldWeight;
           // tmpStyle.Font.Color = Color.White;
            tmpStyle.WrapText = true;
           // tmpStyle.Borders.SetBorders(MultipleBorders.Right | MultipleBorders.Top, GemBox.Spreadsheet.SpreadsheetColor.FromName., LineStyle.Thin);

            ws.Cells.GetSubrangeAbsolute(0, 0, 0, 3).Style = tmpStyle;
            //ws.Cells[0, 0].Value = "Start Time";
            //ws.Cells[0, 1].Value = "End Time";
            ws.Cells[0, 0].Value = "Name";
            ws.Cells[0, 1].Value = "Value";

        }

        private DataSet GetReportsPerUser()
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection connection = new
                        SqlConnection(_connectionInformation))
            {
                SqlCommand command =
                                new SqlCommand("GetCurrentHistoryPerModel", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter dashIDParam = new SqlParameter();
                dashIDParam.ParameterName = "@DashboardID";
                dashIDParam.SqlDbType = SqlDbType.NVarChar;
                dashIDParam.Direction = ParameterDirection.Input;
                dashIDParam.Value = _dashboardID;
                command.Parameters.Add(dashIDParam);

                SqlParameter daysBackParam = new SqlParameter();
                daysBackParam.ParameterName = "@DaysBack";
                daysBackParam.SqlDbType = SqlDbType.Int;
                daysBackParam.Direction = ParameterDirection.Input;
                daysBackParam.Value = _daysBackToReport;
                command.Parameters.Add(daysBackParam);

                SqlDataAdapter da = new
                                SqlDataAdapter(command);
                da.Fill(dataSet,
                                "report");
            }
            return dataSet;
        }

    }

}