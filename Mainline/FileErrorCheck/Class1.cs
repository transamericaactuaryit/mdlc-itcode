﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnablingSystems.Utilities
{
    public class FileErrorChecker
    {
        string _fileToCheck;
        string _errorList;
        char _errorIndicationDelimiter = '|';

        public FileErrorChecker(string errorList, string fileToCheck)
        {
            _errorList = errorList;
            _fileToCheck = fileToCheck;
        }


        public bool FileContainsErrors()
        {
            bool tempResult = false;
            string contents = System.IO.File.ReadAllText(_fileToCheck);

            foreach (string err in _errorList.Split(_errorIndicationDelimiter))
            {
                if (contents.Contains(err))
                {
                    tempResult = true;
                    break;
                }
            }

            return tempResult;
        }
    }
}
