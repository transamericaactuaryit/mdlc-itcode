﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Drawing.Text;
using System.IO;
using System.Globalization;
using System.Diagnostics.Eventing.Reader;

namespace EnablingSystems.Utilities
{
    public class DesktopTextAlert
    {

        private string _screenShotPath = "";
        private string _textToFindList = "";
        private string _lastErrorTimeFileLocation = "";
        private int _notifyTimeSpanHours;
        private int _notifyTimeSpanMinutes;
        private string _lastErrorTimeFormat = "yyyyMMddHHmmssfff";
        private string _emailSubject = "";
        private string _emailBody = "";
        private string _errorToEmailAddress = "";
        private string _errorFromEmailAddress = "";
        private string _emailServer = "";
        private string _environment = "DEV";
        private string _tesseractDataPath = "";
        private bool _errorsFound = false;
        private bool _deleteScreenShot = true;

        public bool ErrorsFound
        {
            get
            {
                return _errorsFound;
            }
        }

        public bool DeleteScreenShot
        {
            get
            {
                return _deleteScreenShot;
            }

            set
            {
                _deleteScreenShot = value;
            }
        }

        public DesktopTextAlert(string screenShotPath, string textToFindList, string lastErrorTimeFileLocation,
            int notifyTimeSpanHours, int notifyTimeSpanMinutes,
            string emailSubject, string emailBody,
            string errorToEmailAddress, string errorFromEmailAddress, 
            string emailServer, string environment, string tesseractDataPath)
        {
            _screenShotPath = screenShotPath;
            _textToFindList = textToFindList;
            _lastErrorTimeFileLocation = lastErrorTimeFileLocation;
            _notifyTimeSpanHours = notifyTimeSpanHours;
            _notifyTimeSpanMinutes = notifyTimeSpanMinutes;
            _emailSubject = emailSubject;
            _emailBody = emailBody;
            _errorToEmailAddress = errorToEmailAddress;
            _errorFromEmailAddress = errorFromEmailAddress;
            _emailServer = emailServer;
            _environment = environment;
            _tesseractDataPath = tesseractDataPath;
            DetectErrors();
        }

        public void DetectErrors()
        {
            if (OkToDetectErrors())
            {
                ScreenShot sshot = new ScreenShot(_screenShotPath);
                sshot.CaptureScreen();


                ImageTextDetector itd = new ImageTextDetector(_textToFindList, sshot.ScreenShotFileName);
                itd.TesseractDataPath = _tesseractDataPath;
                itd.DetectText();

                if (itd.ImageContainsText)
                {
                    _errorsFound = true;
                    File.WriteAllText(_lastErrorTimeFileLocation,
                        DateTime.Now.ToString(_lastErrorTimeFormat));

                    SendNotification(sshot.ScreenShotFileName);
                }
            }
        }

        private void SendNotification(string fileName)
        {
            if ((!string.IsNullOrEmpty(_errorToEmailAddress)) &&
                (!string.IsNullOrEmpty(_errorFromEmailAddress)) &&
                (!string.IsNullOrEmpty(_emailServer)))
            {
                TAEmailer emailer = new TAEmailer(_errorToEmailAddress, _errorFromEmailAddress,
                    "", _emailSubject, _emailBody, false, _emailServer);

                if((!string.IsNullOrEmpty(fileName)) &&
                    (File.Exists(fileName)))
                {
                    emailer.Attachments.Add(fileName);
                }

                emailer.SendEmail();
            }
        }

        private bool OkToDetectErrors()
        {
            if(!File.Exists(_lastErrorTimeFileLocation))
            {
                return true;
            }
            else
            {
                string thisDate = File.ReadAllText(_lastErrorTimeFileLocation);
                DateTime lastErrTime = DateTime.ParseExact(thisDate, _lastErrorTimeFormat, CultureInfo.InvariantCulture);
                lastErrTime = lastErrTime.AddHours(_notifyTimeSpanHours).AddMinutes(_notifyTimeSpanMinutes);

                if (lastErrTime < DateTime.Now)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public class ScreenShot
    {
        private string _tempImageFileLocation;
        private string _screenShotFileName = "";

        public string ScreenShotFileName
        {
            get
            {
                return _screenShotFileName;
            }
        }

        public ScreenShot(string tempImageFileLocation)
        {
            _tempImageFileLocation = tempImageFileLocation;
            if(!_tempImageFileLocation.EndsWith(@"\"))
            {
                _tempImageFileLocation += @"\";
            }
        }

        public void CaptureScreen()
        {
            Rectangle bounds = Screen.GetBounds(Point.Empty);

            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    //g.InterpolationMode = InterpolationMode.High;
                    //g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    //g.SmoothingMode = SmoothingMode.AntiAlias;
                    //g.TextRenderingHint = TextRenderingHint.AntiAlias;
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }

                _screenShotFileName = string.Format("{0}_screenshot_{1}.bmp", _tempImageFileLocation, 
                    DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                bitmap.Save(_screenShotFileName,  System.Drawing.Imaging.ImageFormat.Tiff);
            }
        }
    }
    public class ImageTextDetector
    {
        private string textToFindList = "";
        private string _tempScreenShotSaveLocation;
        private string _tesseractDataPath = "";
        private bool _imageContainsText = false;
        private List<String> _textFound = new List<String>();

        public string ErrorTextList
        {
            get
            {
                return textToFindList;
            }

            set
            {
                textToFindList = value;
            }
        }

        public string TempScreenShotSaveLocation
        {
            get
            {
                return _tempScreenShotSaveLocation;
            }

            set
            {
                _tempScreenShotSaveLocation = value;
            }
        }

        public bool ImageContainsText
        {
            get
            {
                return _imageContainsText;
            }
        }

        public List<string> TextFoundInImage
        {
            get
            {
                return _textFound;
            }
        }

        public string TesseractDataPath
        {
            get
            {
                return _tesseractDataPath;
            }

            set
            {
                _tesseractDataPath = value;
            }
        }

        public ImageTextDetector(string textToFineList, string imageLocation)
        {
            textToFindList = textToFineList;
            _tempScreenShotSaveLocation = imageLocation;
        }


        public void DetectText()
        {
            var image = new Bitmap(_tempScreenShotSaveLocation);
            string dataPath;

            if(string.IsNullOrEmpty(_tesseractDataPath))
            {
                dataPath = System.IO.Directory.GetCurrentDirectory();
            }
            else
            {
                dataPath = _tesseractDataPath;
            }
            // var ocr = new Tesseract.TesseractEngine(@"\\crdc3050c2\TA_Users\jireisner\visual studio 2015\Projects\DialogBoxErrorDetection\DialogBoxErrorDetector\bin\Debug", 
            var ocr = new Tesseract.TesseractEngine(dataPath,
                 "eng", EngineMode.Default);

            var result = ocr.Process(image);
            var resultText = result.GetText().ToLower();

            foreach(string thisText in textToFindList.Split('|'))
            {
                if(resultText.Contains(thisText.ToLower()))
                {
                    _imageContainsText = true;
                    TextFoundInImage.Add(thisText);
                }
            }
        }
    }


}
