﻿using System.Collections.Generic;
using Domain;

namespace Service.Interfaces
{
    public interface IModelRequestService
    {
        ModelRequest Add(ModelRequest ModelRequest);
        bool Delete(int id);
        IEnumerable<ModelRequest> Get();
        ModelRequest Get(int id, bool loadAllChildEntities = true);
        ModelRequestStatus GetStatus(int id);
        void UpdateStatusByStatus(ModelRequestStatusEnums statusToChange, ModelRequestStatusEnums newStatus);
    }
}