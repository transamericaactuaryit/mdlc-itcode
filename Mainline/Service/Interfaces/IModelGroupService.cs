﻿using System.Collections.Generic;
using Domain;

namespace Service.Interfaces
{
    public interface IModelGroupService
    {
        IEnumerable<ModelGroup> Get();
        IEnumerable<ModelType> GetModelTypesByModelGroupId(int modelGroupId);
    }
}