﻿using Domain;
using Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface IModelService
    {
        Model Get(int id);
        ModelStatus GetStatus(int id);
        ModelApplication GetModelApplication(string name);
        ModelType GetModelType(string name, string modelGroupName);
        Model UpdateModel(Model model);
        Model UpdateModelStatus(int id, ModelStatusEnums status, DateTime? timeStamp);
        IEnumerable<Model> GetAllByModelRequestId(int modelRequestId);
        IEnumerable<Model> GetUnprocessed();
        string GetModelEmail(Model model);
        Model GetLatestByModelTypeId(int modelTypeId);
        string CreateDynamicOnDemandConfiguration(OnDemandRun onDemandRun);
    }
}