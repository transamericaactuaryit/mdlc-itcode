﻿using DataAccess.Interfaces;
using Domain;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service
{
    public class ModelGroupService : IModelGroupService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ModelGroupService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<ModelGroup> Get()
        {
            return _unitOfWork.ModelGroupRepository.Get();
        }

        public IEnumerable<ModelType> GetModelTypesByModelGroupId(int modelGroupId)
        {
            return _unitOfWork.ModelTypeRepository.GetByModelGroupId(modelGroupId);
        }
        
    }
}
