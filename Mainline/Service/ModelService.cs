﻿using DataAccess.Interfaces;
using Domain;
using Domain.ServiceModels;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;

namespace Service
{
    public class ModelService : IModelService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ModelService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ModelType GetModelType(string name, string modelGroupName)
        {
            return _unitOfWork.ModelTypeRepository.Get(name, modelGroupName);
        }

        public ModelApplication GetModelApplication(string name)
        {
            return _unitOfWork.ModelApplicationRepository.Get(name);
        }

        public ModelStatus GetStatus(int id)
        {
            return _unitOfWork.ModelStatusRepository.Get(id);
        }

        public Model Get(int id)
        {
            return _unitOfWork.ModelRepository.Get(id);
        }

        public IEnumerable<Model> GetAllByModelRequestId(int modelRequestId)
        {
            return _unitOfWork.ModelRepository.GetAllByModelRequestId(modelRequestId);
        }

        public Model UpdateModel(Model model)
        {
            _unitOfWork.ModelRepository.Update(model);

            _unitOfWork.SaveChanges();

            return model;
        }

        public Model UpdateModelStatus(int id, ModelStatusEnums status, DateTime? timeStamp)
        {
            var model = _unitOfWork.ModelRepository.UpdateStatus(id, status, timeStamp);

            _unitOfWork.SaveChanges();

            return model;
        }

        public IEnumerable<Model> GetUnprocessed()
        {
            return _unitOfWork.ModelRepository.GetUnprocessed();
        }

        public string GetModelEmail(Model model)
        {
            var modelType = model.ModelType;
            if (modelType == null)
            {
                modelType = _unitOfWork.ModelTypeRepository.Get(model.ModelTypeID);
            }

            return _unitOfWork.ModelGroupRepository.Get(modelType.ModelGroupID).Email;
        }

        public Model GetLatestByModelTypeId(int modelTypeId)
        {
            return _unitOfWork.ModelRepository.GetLatestByModelTypeId(modelTypeId);
        }

        //Used by the OnDemand App: OnDemand App is an interim solution so not going to clean this up or write unit tests just yet. SJO 8/15/18
        public string CreateDynamicOnDemandConfiguration(OnDemandRun onDemandRun)
        {
            string onDemandConfigLocation;

            try
            {
                var insertIndex = onDemandRun.Configuration.LastIndexOf(@"\");
                onDemandConfigLocation = onDemandRun.Configuration.Insert(insertIndex + 1, @"OnDemand\");

                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = ("    "),
                    CloseOutput = true,
                    OmitXmlDeclaration = true
                };

                using (XmlWriter writer = XmlWriter.Create(onDemandConfigLocation, settings))
                {
                    // Next show the object title and check state for each item selected.
                    writer.WriteStartElement("OnDemandRuns");

                    writer.WriteStartElement("OnDemandRun");

                    writer.WriteElementString("Environment", onDemandRun.Environment);
                    writer.WriteElementString("Configuration", onDemandRun.Configuration);
                    writer.WriteElementString("RunDefinition", onDemandRun.RunDefinition);
                    writer.WriteElementString("ProcessTimeToMonthOffset", onDemandRun.ProcessTimeToMonthOffset);
                    writer.WriteElementString("ProcessTimeToMonthOffsetForValDate", onDemandRun.ProcessTimeToMonthOffsetForValDate);
                    writer.WriteElementString("SubmittedBy", onDemandRun.SubmittedBy);
                    writer.WriteElementString("SubmittedDate", onDemandRun.SubmittedDate.ToString());
                    writer.WriteEndElement();
                    writer.Flush();

                    writer.WriteEndElement();
                }
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message}";
            }

            return onDemandConfigLocation;
        }
    }
}
