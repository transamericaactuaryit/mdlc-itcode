﻿using DataAccess.Interfaces;
using Domain;
using Reporting;
using Service.Interfaces;
using System;
using System.Collections.Generic;


namespace Service
{
    public class ModelRequestService : IModelRequestService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ModelRequestService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<ModelRequest> Get()
        {
            return _unitOfWork.ModelRequestRepository.Get();
        }

        public ModelRequest Get(int id, bool loadAllChildEntities = true)
        {
            return _unitOfWork.ModelRequestRepository.Get(id, loadAllChildEntities);
        }

        public ModelRequestStatus GetStatus(int id)
        {
            return _unitOfWork.ModelRequestStatusRepository.Get(id);
        }

        public ModelRequest Add(ModelRequest modelRequest)
        {
            var newModelRequest = _unitOfWork.ModelRequestRepository.Add(modelRequest);

            _unitOfWork.SaveChanges();

            return newModelRequest;

        }

        public void UpdateStatusByStatus(ModelRequestStatusEnums statusToChange, ModelRequestStatusEnums newStatus)
        {
            _unitOfWork.ModelRequestRepository.UpdateStatusByStatus(statusToChange, newStatus);

            _unitOfWork.SaveChanges();
        }

        public bool Delete(int id)
        {
            try
            {
                var modelRequest = _unitOfWork.ModelRequestRepository.Get(id);
                _unitOfWork.ModelRequestRepository.Delete(modelRequest);
                _unitOfWork.SaveChanges();

                return true;
            }
            catch (InvalidOperationException ex)
            {
                ELMAHLogger.LogError(ex.Message, "ModelRequestService");               
                return false;
            }
        }
    }
}
