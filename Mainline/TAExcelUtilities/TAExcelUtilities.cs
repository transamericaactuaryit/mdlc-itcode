﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GemBox.Spreadsheet;
using System.Collections.Specialized;

namespace ExcelUtilities
{
    public class ExcelCopyPasteRange
    {
        private string _sourceWorkbookLocation;
        private string _sourceSheetName;

        private string _destWorkbookLocation;
        private string _destSheetName;

        private int _sourceStartRangeRow;
        private string _sourceStartRangeColumn;
        private int _sourceEndRangeRow;
        private string _sourceEndRangeColumn;

        private int _destStartRangeRow;
        private string _destStartRangeColumn;
        private int _destEndRangeRow;
        private string _destEndRangeColumn;

        private NameValueCollection _cellsCopied = new NameValueCollection();

        #region Properties
        public string SourceSheetName
        {
            get
            {
                return _sourceSheetName;
            }

            set
            {
                _sourceSheetName = value;
            }
        }

        public string SourceWorkbookLocation
        {
            get
            {
                return _sourceWorkbookLocation;
            }

            set
            {
                _sourceWorkbookLocation = value;
            }
        }

        public string DestinationSheetName
        {
            get
            {
                return _destSheetName;
            }

            set
            {
                _destSheetName = value;
            }
        }

        public string DestinationWorkbookLocation
        {
            get
            {
                return _destWorkbookLocation;
            }

            set
            {
                _destWorkbookLocation = value;
            }
        }

        public int SourceStartRangeRow
        {
            get
            {
                return _sourceStartRangeRow;
            }

            set
            {
                _sourceStartRangeRow = value;
            }
        }

        public string SourceStartRangeColumn
        {
            get
            {
                return _sourceStartRangeColumn;
            }

            set
            {
                _sourceStartRangeColumn = value;
            }
        }

        public int SourceEndRangeRow
        {
            get
            {
                return _sourceEndRangeRow;
            }

            set
            {
                _sourceEndRangeRow = value;
            }
        }

        public string SourceEndRangeColumn
        {
            get
            {
                return _sourceEndRangeColumn;
            }

            set
            {
                _sourceEndRangeColumn = value;
            }
        }

        public int DestinationStartRangeRow
        {
            get
            {
                return _destStartRangeRow;
            }

            set
            {
                _destStartRangeRow = value;
            }
        }

        public string DestinationStartRangeColumn
        {
            get
            {
                return _destStartRangeColumn;
            }

            set
            {
                _destStartRangeColumn = value;
            }
        }

        public int DestinationEndRangeRow
        {
            get
            {
                return _destEndRangeRow;
            }

            set
            {
                _destEndRangeRow = value;
            }
        }

        public string DestinationEndRangeColumn
        {
            get
            {
                return _destEndRangeColumn;
            }

            set
            {
                _destEndRangeColumn = value;
            }
        }

        public NameValueCollection CellsCopied
        {
            get
            {
                return _cellsCopied;
            }
        }

        #endregion Properties

        public void CopyValuesFromAndToRange()
        {
            //SpreadsheetInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = FreeLimitReachedAction.ContinueAsTrial;
            SpreadsheetInfo.SetLicense("EGHB-9SAD-P0EZ-OWD3");
            ExcelFile wbSource = ExcelFile.Load(_sourceWorkbookLocation);
            ExcelWorksheet wsSource = wbSource.Worksheets[_sourceSheetName];

            ExcelFile wbDest = ExcelFile.Load(_destWorkbookLocation);
            ExcelWorksheet wsDest = wbDest.Worksheets[_destSheetName];

            CellRange sourceRange = wsSource.Cells.GetSubrange(
                _sourceStartRangeColumn + _sourceStartRangeRow,
                _sourceEndRangeColumn + _sourceEndRangeRow);


            CellRange destRange = wsDest.Cells.GetSubrange(
                _destStartRangeColumn + _destStartRangeRow,
                _destEndRangeColumn + _destEndRangeRow);

            List<ExcelCell> sourceCells = new List<ExcelCell>();
            List<ExcelCell> destCells = new List<ExcelCell>();

            for (int sourceRow = sourceRange.FirstRowIndex; sourceRow <= sourceRange.LastRowIndex; sourceRow++)
            {
                for (int sourceColumn = sourceRange.FirstColumnIndex; sourceColumn <= sourceRange.LastColumnIndex; sourceColumn++)
                {
                    ExcelCell cell = sourceRange[sourceRow - sourceRange.FirstRowIndex, sourceColumn - sourceRange.FirstColumnIndex];
                    _cellsCopied.Add("Source Cells", "");
                    _cellsCopied.Add(cell.Name, (cell.Value == null ? "" : cell.Value.ToString()));
                    sourceCells.Add(cell);
                }
            }

            for (int r=destRange.FirstRowIndex; r <= destRange.LastRowIndex; r++)
            {
                for (int c=destRange.FirstColumnIndex; c<=destRange.LastColumnIndex; c++)
                {
                    ExcelCell cell = destRange[r - destRange.FirstRowIndex, c - destRange.FirstColumnIndex];
                    //_cellsCopied.Add("Destination Cells:", "");
                    //_cellsCopied.Add(cell.Name, (cell.Value == null ? "": cell.Value.ToString()));
                    destCells.Add(cell);
                }
            }

            //TODO: Check for improper number on both sides
            for (int i = 0; i < sourceCells.Count; i++)
            {
                ExcelCell newCell = sourceCells[i];
                destCells[i].Value = newCell.Value;
            }

            wbDest.Save(_destWorkbookLocation);


        }

    }

    public class ExcelCopyPasteOneToMany
    {
        private string _sourceWorkbookLocation;
        private string _sourceSheetName;

        private string _destWorkbookLocation;
        private string _destSheetName;

        private int _sourceRow;
        private string _sourceColumn;

        private int _destStartRangeRow;
        private string _destStartRangeColumn;
        private int _destEndRangeRow;
        private string _destEndRangeColumn;

        private NameValueCollection _cellsCopied = new NameValueCollection();

        #region Properties
        public string SourceSheetName
        {
            get
            {
                return _sourceSheetName;
            }

            set
            {
                _sourceSheetName = value;
            }
        }

        public string SourceWorkbookLocation
        {
            get
            {
                return _sourceWorkbookLocation;
            }

            set
            {
                _sourceWorkbookLocation = value;
            }
        }

        public string DestinationSheetName
        {
            get
            {
                return _destSheetName;
            }

            set
            {
                _destSheetName = value;
            }
        }

        public string DestinationWorkbookLocation
        {
            get
            {
                return _destWorkbookLocation;
            }

            set
            {
                _destWorkbookLocation = value;
            }
        }

        public int SourceRow
        {
            get
            {
                return _sourceRow;
            }

            set
            {
                _sourceRow = value;
            }
        }

        public string SourceColumn
        {
            get
            {
                return _sourceColumn;
            }

            set
            {
                _sourceColumn = value;
            }
        }

        public int DestinationStartRangeRow
        {
            get
            {
                return _destStartRangeRow;
            }

            set
            {
                _destStartRangeRow = value;
            }
        }

        public string DestinationStartRangeColumn
        {
            get
            {
                return _destStartRangeColumn;
            }

            set
            {
                _destStartRangeColumn = value;
            }
        }

        public int DestinationEndRangeRow
        {
            get
            {
                return _destEndRangeRow;
            }

            set
            {
                _destEndRangeRow = value;
            }
        }

        public string DestinationEndRangeColumn
        {
            get
            {
                return _destEndRangeColumn;
            }

            set
            {
                _destEndRangeColumn = value;
            }
        }

        public NameValueCollection CellsCopied
        {
            get
            {
                return _cellsCopied;
            }
        }

        #endregion Properties

        public void CopyOneToManyValue()
        {
            //SpreadsheetInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = FreeLimitReachedAction.ContinueAsTrial;
            SpreadsheetInfo.SetLicense("EGHB-9SAD-P0EZ-OWD3");
            ExcelFile wbSource = ExcelFile.Load(_sourceWorkbookLocation);
            ExcelWorksheet wsSource = wbSource.Worksheets[_sourceSheetName];

            ExcelFile wbDest = ExcelFile.Load(_destWorkbookLocation);
            ExcelWorksheet wsDest = wbDest.Worksheets[_destSheetName];

            ExcelCell sourceCell = wsSource.Cells[_sourceColumn + _sourceRow.ToString()];

            _cellsCopied.Add(sourceCell.Name, (sourceCell.Value == null ? "" : sourceCell.Value.ToString()));

            CellRange destRange = wsDest.Cells.GetSubrange(
                _destStartRangeColumn + _destStartRangeRow,
                _destEndRangeColumn + _destEndRangeRow);
            List<ExcelCell> destCells = new List<ExcelCell>();

            for (int r = destRange.FirstRowIndex; r <= destRange.LastRowIndex; r++)
            {
                for (int c = destRange.FirstColumnIndex; c <= destRange.LastColumnIndex; c++)
                {
                    ExcelCell cell = destRange[r - destRange.FirstRowIndex, c - destRange.FirstColumnIndex];
                    //_cellsCopied.Add("Destination Cells:", "");
                    //_cellsCopied.Add(cell.Name, (cell.Value == null ? "" : cell.Value.ToString()));
                    destCells.Add(cell);
                }
            }

            //TODO: Check for improper number on both sides
            for (int i = 0; i < destCells.Count; i++)
            {
                destCells[i].Value = sourceCell.Value;
            }

            wbDest.Save(_destWorkbookLocation);


        }

    }

}
