﻿
namespace EnablingSystems.MDLC.Processes
{
    public class MGAlfaProcessorMock : MGAlfaExternalProcess
    {
        public string ErrorContent
        {
            get
            {
                return sError;
            }
        }
        public override void Execute()
        {
            _errorIndicatorList = _processParams["ErrorIndicatorList"];
            CheckAlfaLogContentForErrors();
        }
         
    }
}
