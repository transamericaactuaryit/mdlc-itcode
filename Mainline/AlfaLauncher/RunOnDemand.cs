﻿using System;
using System.Diagnostics;

namespace EnablingSystems.MDLC.Processes
{
    public class RunModelOnDemand
    {
        string _jobAggregatorLocation = "";
        string _metaconfigDirectoryLocation = "";

        public string JobAggregatorLocation
        {
            set
            {
                _jobAggregatorLocation = value;
            }
        }

        public string MetaconfigDirectoryLocation
        {
            set
            {
                _metaconfigDirectoryLocation = value;
            }
        }

        public RunModelOnDemand(string jobAggregatorLocation,
            string metaconfigDirectoryLocation)
        {
            _jobAggregatorLocation = jobAggregatorLocation;
            _metaconfigDirectoryLocation = metaconfigDirectoryLocation;
        }

        public void ProcessOnDemandModel()
        {
            ExecuteProcess(false, _metaconfigDirectoryLocation, _jobAggregatorLocation);
        }

        public  void ExecuteProcess(bool useShellExecute, string processArgs,
                string processLocation)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardOutput = true;

                if (processLocation == null)
                {
                    throw new Exception("ProcessLocation did not exist in parameter collection");
                }

                startInfo.FileName = processLocation;
                startInfo.UseShellExecute = useShellExecute;
                startInfo.Arguments = processArgs;

                string standardOutput = "";
                string errorOutput = "";

                using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
                {
                    standardOutput = exeProcess.StandardOutput.ReadToEnd();
                    errorOutput = exeProcess.StandardError.ReadToEnd();
                    exeProcess.WaitForExit();
                }

                if (errorOutput.Length > 0)
                {
                    throw new Exception(errorOutput);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("Method Name: ExecuteProcess", "RunOnDemand");
                throw;
            }
        }
    }
}
