﻿using System;
using System.IO;
using System.Configuration;
using EnablingSystems.Utilities;
using Reporting;

namespace EnablingSystems.MDLC.Processes
{
    public class AlfaLauncher
    {
        static void Main(string[] args)
        {
            string _errorToEmailAddress = ConfigurationManager.AppSettings["ErrorEmailToAddress"] ?? "";
            string _errorEmailFromAddress = ConfigurationManager.AppSettings["ErrorEmailFromAddress"] ?? "";
            string _emailServer = ConfigurationManager.AppSettings["EmailServer"] ?? "";
            try
            {
                ProcessAlfaTriggers();
            }
            catch (Exception ex)
            {
                ELMAHLogger.LogError($"Error in Alfa Launcher - {Environment.MachineName} - {ex.Message}", "AlfaLauncher");

                if ((!string.IsNullOrEmpty(_errorToEmailAddress)) &&
                    (!string.IsNullOrEmpty(_errorEmailFromAddress)) &&
                    (!string.IsNullOrEmpty(_emailServer)))
                {
                    TAEmailer emailer = 
                        new TAEmailer(
                            _errorToEmailAddress, 
                            _errorEmailFromAddress,
                            string.Empty, 
                            "Error in Alfa Launcher - " + Environment.MachineName, 
                            $"{ex.Message}\r\nMachine Name: {Environment.MachineName}", 
                            false, 
                            _emailServer);
                    emailer.SendEmail();
                }

            }
        }


        private static void ProcessAlfaTriggers()
        {
            string _alfaTriggerWorkingDirectory = ConfigurationManager.AppSettings["AlfaTriggerFileLocation"]
            ?? @"E:\MGALFA\AlfaRunTriggerFiles";

            string _archiveLocation = ConfigurationManager.AppSettings["ArchiveAlfaTriggerLocation"]
            ?? @"";

            if (!_archiveLocation.EndsWith(@"\"))
            {
                _archiveLocation += @"\";
            }

            var files = Directory.GetFiles(_alfaTriggerWorkingDirectory, "*.txt");

            if(files.Length > 0)
            {
                var file = files[0];

                RunAlfa runAlfa = new RunAlfa(file);

                if (!string.IsNullOrEmpty(_archiveLocation))
                {
                    string archiveFileName = string.Concat(
                         _archiveLocation,
                         Path.GetFileNameWithoutExtension(file),
                         DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                         Path.GetExtension(file)
                        );
                    File.Move(file, archiveFileName);
                }
                else
                {
                    File.Delete(file);
                }

                runAlfa.ProcessAlfaRun();
            }

            //Note: Commented out below, as we were encoutering issues
            //when multiple alfa instances were executed in sequence.
            //we believe that multiple instances may have been 
            //competing for the same temp files. 
            //we switched to launching one instance per scheduled task iteration (above)
            //foreach (var file in files)
            //{
            //    RunAlfa runAlfa = new RunAlfa(file);

            //    if (!string.IsNullOrEmpty(_archiveLocation))
            //    {
            //        string archiveFileName = string.Concat(
            //             _archiveLocation,
            //             Path.GetFileNameWithoutExtension(file),
            //             DateTime.Now.ToString("yyyyMMddHHmmssfff"),
            //             Path.GetExtension(file)
            //            );
            //        File.Move(file, archiveFileName);
            //    }
            //    else
            //    {
            //        File.Delete(file);
            //    }

            //    runAlfa.ProcessAlfaRun();
            //}
        }
    }
}
