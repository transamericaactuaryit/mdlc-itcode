﻿using EnablingSystems.Mocks;
using EnablingSystems.OneMDLC.History;
using EnablingSystems.ProcessInterfaces;
using EnablingSystems.Utilities;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using Reporting;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading;

namespace EnablingSystems.MDLC.Processes
{
    public class RunAlfa
    {
        private string _paramsFile;
        private NameValueCollection _alfaRunParameters = new NameValueCollection();
        private IJobHistory _jobHistory;
        private IDashboard _dashboard;

        #region Constructor
        public RunAlfa(string paramsFile)
        {
            _paramsFile = paramsFile;
            LoadParams();
            RegisterUnityTypes();

            _dashboard.ConnectionInformation = _alfaRunParameters["DashboardConnection"] ?? _alfaRunParameters["JobHistoryConnection"];
            _dashboard.JobInstanceID = int.Parse(_alfaRunParameters["JobInstanceID"] ?? "-1");
            _dashboard.DashboardID = _alfaRunParameters["DashboardID"];
            _dashboard.ProcessName = _alfaRunParameters["DashboardProcessName"];

            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            
            Logger.SetLogWriter(new LogWriterFactory().Create());

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            ExceptionManager exceptionManager = exceptionFactory.CreateManager();
        }
        #endregion

        #region Private Methods
        private void RegisterUnityTypes()
        {
            UnityContainer _unityContainer;
            _unityContainer = new UnityContainer();

            bool usedbJobHistory = bool.Parse(_alfaRunParameters["UseDBJobHistory"] ?? "false");
            if (usedbJobHistory)
            {
                _unityContainer.RegisterType<IJobHistory, JobHistorySaveToDB>();
                _unityContainer.RegisterType<IDashboard, DashboardSaveToDB>();
            }
            else
            {
                _unityContainer.RegisterType<IJobHistory, JobHistoryFile>();
                _unityContainer.RegisterType<IDashboard, DashboardMock>();
            }
                                                
            _jobHistory = _unityContainer.Resolve<IJobHistory>();
            _jobHistory.ConnectionInformation = _alfaRunParameters["JobHistoryConnection"];
            _dashboard = _unityContainer.Resolve<IDashboard>();                                    
        }

        private void LoadParams()
        {
            string[] lines = File.ReadAllLines(_paramsFile);
            var dict = lines.Select(l => l.Split('=')).ToDictionary(a => a[0], a => a[1]);

            foreach (var kvp in dict)
            {
                _alfaRunParameters.Add(kvp.Key.ToString(), kvp.Value?.ToString());
            }
        }
        private void RunThisAlfaInstance()
        {
            DateTime processStartTime = DateTime.Now;
            DateTime processFinishTime;

            MGAlfaExternalProcess alfaProcess = new MGAlfaExternalProcess()
            {
                JobHistory = _jobHistory,
                ProcessParams = _alfaRunParameters,
                ProcessName = _alfaRunParameters["ProcessName"]
            };
            
            try
            {
                alfaProcess.Execute();
            }
            catch (Exception eX)
            {
                ELMAHLogger.LogError($"Error in Alfa Launcher - {Environment.MachineName} - {eX.Message}", "AlfaLauncher");

                if ((!string.IsNullOrEmpty(_alfaRunParameters["ToAddress"])) &&
                    (!string.IsNullOrEmpty(_alfaRunParameters["FromAddress"])) &&
                    (!string.IsNullOrEmpty(_alfaRunParameters["SmtpServer"])))
                {
                    TAEmailer emailer =
                        new TAEmailer(
                            _alfaRunParameters["ToAddress"],
                            _alfaRunParameters["FromAddress"],
                            string.Empty,
                            $"Error in Alfa Launcher - {Environment.MachineName}",
                            $"{eX.Message}\r\nMachine Name: {Environment.MachineName}",
                            false,
                            _alfaRunParameters["SmtpServer"]);
                    emailer.SendEmail();
                }

                Environment.Exit(-1);
            }
            processFinishTime = DateTime.Now;

            //TODO: Refactor how JobHistoryDB.Save() works
            _dashboard.StartTime = processStartTime;
            _dashboard.EndTime = processFinishTime;
            _dashboard.Success = true;
            _dashboard.ProcessMessage = "Completed successfully";
            _dashboard.Save();
            alfaProcess.SaveHistory(_dashboard.JobHistoryID);
        }
        #endregion

        public void ProcessAlfaRun()
        {
            var thread = new Thread(() => RunThisAlfaInstance());
            thread.Start();
        }        
    }
}
