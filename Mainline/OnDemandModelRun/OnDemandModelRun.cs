﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EnablingSystems.MDLC.Processes
{

    public class OnDemandFileParse
    {
        private string _itemToParse;
        //private string _archiveDirectory;
        private string _metaConfigFileToUpdate;
        private string _configWorkingArea; //destination folder
       

        private bool _processAtDirectoryLevel = false;
        private string _metaConfigSwitcherDirectory;

        public bool ProcessAtDirectoryLevel
        {
            get
            {
                return _processAtDirectoryLevel;
            }

            set
            {
                _processAtDirectoryLevel = value;
            }
        }

        public string MetaConfigFileToUpdate
        {
            get
            {
                return _metaConfigFileToUpdate;
            }

        }

        public string WorkingConfigArea
        {
            get
            {
                return _configWorkingArea;
            }
        }

        public string MetaConfigSwitcherDirectory
        {
            get
            {
                return _metaConfigSwitcherDirectory;
            }

            set
            {
                _metaConfigSwitcherDirectory = value;
            }
        }

        public OnDemandFileParse(string itemToParse)
        {
            _itemToParse = itemToParse;
        }

        public OnDemandFileParse(string itemToParse, string configWorkingArea)
        {
            _itemToParse = itemToParse;
            _configWorkingArea = configWorkingArea;
            
        }

        public void Execute()
        {
            if (_processAtDirectoryLevel)
            {
                RunOnDemandModelsDirectoryLevel();
            }
            else
            {
                RunOnDemandModelFileLevel();
            }

        }

        public void ArchiveFile(string fileToArchive)
        {
            var currentFolder = Directory.GetParent(fileToArchive);
            var archiveFileLocation = currentFolder.FullName + @"\Archive\";
            //check to see if directories exist.  Create if they don't
            if (!Directory.Exists(archiveFileLocation))
            {
                Directory.CreateDirectory(archiveFileLocation);
            }

            if (!string.IsNullOrEmpty(archiveFileLocation))
            {
                string archiveFileName = string.Concat(
                     archiveFileLocation,
                     Path.GetFileNameWithoutExtension(fileToArchive),
                     DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                     Path.GetExtension(fileToArchive)
                    );
                File.Move(fileToArchive, archiveFileName);
            }
            else
            {
                File.Delete(fileToArchive);
            }
        }

        private void RunOnDemandModelFileLevel()
        {
            ParseAndUpdateFile(_itemToParse);

        }

        public void RunOnDemandModelsDirectoryLevel()
        {
            var files = Directory.GetFiles(_itemToParse, "*.xml");

            foreach(string file in files)
            {
                ParseAndUpdateFile(file);
            }
        }

        private void ParseAndUpdateFile(string file)
        {
            XElement root = XElement.Load(file);

            var onDemands = from onDemandRuns in root.Elements()
                            select onDemandRuns;

            foreach (var thisElement in onDemands)
            {
                string origConfigFile = thisElement.Elements("Configuration").Single().Value;
                string runDefinition = thisElement.Elements("RunDefinition").Single().Value;
                string processTimeToMonthOffset = thisElement.Elements("ProcessTimeToMonthOffset").Single().Value;
                string processValDateMonthOffset = thisElement.Elements("ProcessTimeToMonthOffsetForValDate").Single().Value;

                string copiedMetaConfig = CopyMetaConfigToWorkingArea(origConfigFile);

                UpdateFile(copiedMetaConfig, runDefinition, processTimeToMonthOffset, processValDateMonthOffset);

                ArchiveFile(file);

                _metaConfigFileToUpdate = copiedMetaConfig;
            }
        }

        private void CreateSwitcherFile(string postFile)
        {
            string postMetaName = Path.GetFileName(postFile);
            postMetaName = postMetaName.Replace(".xml", ".txt");

            string switcherDirectory = _metaConfigSwitcherDirectory.EndsWith(@"\") ? _metaConfigSwitcherDirectory : _metaConfigSwitcherDirectory + @"\";
            //create directory if not there
            if(!Directory.Exists(switcherDirectory))
            {
                Directory.CreateDirectory(switcherDirectory);
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@switcherDirectory + postMetaName))
            {

                file.WriteLine(postFile);
            }
        }

        private static void UpdateFile(string configurationFile, string runDefinition, string processTimeToMonthOffset, string processValDateMonthOffset)
        {
            XElement root =
            XElement.Load(configurationFile);

            var runScenarios = new List<XElement>();
            var processValDateMonthOffsets = new List<XElement>();

            //not a required field
            if (!string.IsNullOrEmpty(runDefinition))
                runScenarios = root.XPathSelectElements(@"//*/SetAlfaProcessTrigger/add[@key='RunScenarios']").ToList();

            //not a required field
            if (!string.IsNullOrEmpty(processValDateMonthOffset))
                processValDateMonthOffsets = root.XPathSelectElements("//*/*[@key='ProcessTimeToMonthOffsetForValDate']").ToList();
            
            var processTimeOffsets = root.XPathSelectElements("//*/*[@key='ProcessTimeToMonthOffset']").ToList();
            var overrideProcessTimeOffsets = root.XPathSelectElements("//*/*[@key='OverrideProcessTimeToMonthOffset']").ToList();
            var defaultProcessTimeToMonthOffset = root.XPathSelectElements("//*/*[@key='DefaultProcessTimeToMonthOffset']").ToList();
            var periodSpecificProcessTimeToMonthOffset = root.XPathSelectElements("//*/*[@PeriodSpecificProcessTimeToMonthOffset]").ToList();

            foreach (var childElement in processTimeOffsets)
            {
                childElement.Attribute("value").Value = processTimeToMonthOffset;
            }
                        
            foreach (var childElement in processValDateMonthOffsets)
            {
                childElement.Attribute("value").Value = processValDateMonthOffset;
            }

            foreach (var childElement in overrideProcessTimeOffsets)
            {
                childElement.Attribute("value").Value = processTimeToMonthOffset;
            }

            foreach(var childElement in defaultProcessTimeToMonthOffset)
            {
                childElement.Attribute("value").Value = processTimeToMonthOffset;
            }

            //Note that below is a little different because the attribute is at the 'Process' tag
            foreach (var childElement in periodSpecificProcessTimeToMonthOffset)
            {
                childElement.Attribute("PeriodSpecificProcessTimeToMonthOffset").Value = processTimeToMonthOffset;
            }

            foreach (var childElement in runScenarios)
            {
                var dashboard = childElement.Parent.XPathSelectElements(@"add[@key='DashboardProcessName']").Single();
                if(dashboard.Attribute("value").Value != "ScenarioFileImport")
                    childElement.Attribute("value").Value = runDefinition;
            }

            root.Save(configurationFile);

            UpdateOnDemandComputedElements(configurationFile);

        }

        private static void UpdateOnDemandComputedElements(string configurationFile)
        {
            XElement root = XElement.Load(configurationFile);
            var configs = from configSettings in root.Elements()
                          select configSettings;
            //<Process> level
            foreach (var thisElement in configs)
            {
                //automation type level, ex <FileCopyOverrideDefault>
                foreach (var thisAutomation in thisElement.Elements())
                {
                    foreach (var thisAttribute in thisAutomation.Attributes())
                    {
                        string attribName = thisAttribute.Name.ToString();

                        if (attribName.ToLower().Contains("ondemandcompute"))
                        {
                            OnDemandComputeElement computeElement = new OnDemandComputeElement();
                            computeElement.AutomationTypeName = thisAutomation.Name.ToString();
                            string[] tmpAttribute = thisAttribute.Name.ToString().Split('_');
                            computeElement.KeyToUpdate = tmpAttribute[0];
                            computeElement.UpdateToValue = thisAttribute.Value.ToString();

                            foreach (var thisConfigParam in thisAutomation.Elements())
                            {
                                foreach (var thisSubAttrib in thisConfigParam.Attributes())
                                {
                                    if (thisSubAttrib.Value.ToLower() == computeElement.KeyToUpdate.ToLower())
                                    {
                                        thisSubAttrib.NextAttribute.Value = (int.Parse(thisSubAttrib.NextAttribute.Value) +
                                            int.Parse(computeElement.UpdateToValue)).ToString();
                                    }
                                }
                            }
                        }

                    }
                }
            }

            root.Save(configurationFile);
        }


        private string CopyMetaConfigToWorkingArea(string fileToUpdate)
        {
            //here we open up the OnDemand GUI Trigger file and capture the
            //Configuration node and copy from a to b and return the file name.

            if (!_configWorkingArea.EndsWith(@"\"))
            {
                _configWorkingArea += @"\";
            }

            string destPath = _configWorkingArea + Path.GetFileName(fileToUpdate);
            File.Copy(fileToUpdate, destPath, true);

            return destPath;
        }
    }

    public class OnDemandComputeElement
    {
        string _automationTypeName;
        string _keyToUpdate;
        string _updateToValue;

        public string AutomationTypeName
        {
            get
            {
                return _automationTypeName;
            }

            set
            {
                _automationTypeName = value;
            }
        }

        public string KeyToUpdate
        {
            get
            {
                return _keyToUpdate;
            }

            set
            {
                _keyToUpdate = value;
            }
        }

        public string UpdateToValue
        {
            get
            {
                return _updateToValue;
            }

            set
            {
                _updateToValue = value;
            }
        }
    }
    public class OnDemandModelRun
    {
        string _jobAggregatorLocation = string.Empty;
        string _metaconfigDirectoryLocation = string.Empty;
        string _environment = string.Empty;

        public string JobAggregatorLocation
        {
            get
            {
                return _jobAggregatorLocation;
            }
            set
            {
                _jobAggregatorLocation = value;
            }
        }

        public string MetaconfigDirectoryLocation
        {
            get
            {
                return _metaconfigDirectoryLocation;
            }
            set
            {
                _metaconfigDirectoryLocation = value;
            }
        }

        public OnDemandModelRun(string jobAggregatorLocation, string environment, 
            string metaconfigDirectoryLocation)
        {
            _jobAggregatorLocation = jobAggregatorLocation;
            _environment = environment;
            _metaconfigDirectoryLocation = metaconfigDirectoryLocation;
        }

        public void ProcessOnDemandModel()
        {
            ExecuteProcess(false, $"{_environment} {_metaconfigDirectoryLocation}", 
                _jobAggregatorLocation);
        }

        public static void ExecuteProcess(bool useShellExecute, string processArgs,
                string processLocation)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    FileName = processLocation ?? throw new ArgumentNullException("processLocation did not exist in parameter collection"),
                    UseShellExecute = useShellExecute,
                    Arguments = processArgs
                };

                //string standardOutput = "";
                string errorOutput = "";

                using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
                {
                    //standardOutput = exeProcess.StandardOutput.ReadToEnd();
                    errorOutput = exeProcess.StandardError.ReadToEnd();
                    exeProcess.WaitForExit();
                }

                if (errorOutput.Length > 0)
                {
                    throw new InvalidDataException(errorOutput);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("Method Name: ExecuteProcess", "RunOnDemand");
                throw;
            }
        }
    }
}
