﻿using System;
using System.IO;
using EnablingSystems.ProcessInterfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnablingSystems.Mocks
{
    public class FileBasedModelQueue : IModelExecutionQueue
    {
        private string _queueFileLocation;
        private int _currentCount = 0;
        private int _maxConcurrent = 2;


        public FileBasedModelQueue(string queueFileLocation)
        {
            this.QueueControlConnectionInformation = queueFileLocation;
        }
        public string ExecutionMachine
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int MaxConcurrentModels
        {
            get
            {
                return _maxConcurrent;
            }

            set
            {
                _maxConcurrent = value;
            }
        }

        public string QueueControlConnectionInformation
        {
            set
            {
                _queueFileLocation = value;
                CreateFileIfNotExists();
            }
        }

        private void CreateFileIfNotExists()
        {
            if (!File.Exists(_queueFileLocation))
            {
                using (StreamWriter sw = new StreamWriter(_queueFileLocation, true))
                {
                    sw.WriteLine("0");
                }

            }
        }

        public int SlotsUsed
        {
            get
            {
                return GetCurrentCount();
            }

        }

        public int ReleaseSlot()
        {
            CheckConnection();
            _currentCount = GetCurrentCount();

            _currentCount--;
            UpdateCount(_currentCount);
            return _currentCount;
        }

        private void UpdateCount(int currentCount)
        {
            using (System.IO.StreamWriter file =
                 new System.IO.StreamWriter(_queueFileLocation))
            {
                file.WriteLine(currentCount);
            }
        }

        private int GetCurrentCount()
        {
            string line;

            using (StreamReader reader = new StreamReader(_queueFileLocation))
            {
                line = reader.ReadLine();
            }

            int currentCount = int.Parse(line);
            return currentCount;
        }

        private void CheckConnection()
        {
            if (String.IsNullOrEmpty(_queueFileLocation))
            {
                throw new
                    Exception("QueueControlConnectionInformation is not set");
            }
        }

        public bool SlotAvailable()
        {
            int proposedCount = _currentCount + 1;
            return proposedCount <= _maxConcurrent;
        }

        public int TakeSlot()
        {
            if (SlotAvailable())
            {
                _currentCount++;
                UpdateCount(_currentCount);
                return _currentCount;
            }
            else
            {
                throw new Exception("Slot is not avaialable");
            }
        }
    }


    public class InMemModelQuery : IModelAggregatorQuery
    {
        string _connectionInfo = "";
        List<IModelAggregator> _modelsToProcess = new List<IModelAggregator>();
        public string ConnectionInformation
        {
            set
            {
                _connectionInfo = value;
            }
        }

        public List<IModelAggregator> ModelsToProcess
        {
            get
            {
                return _modelsToProcess;
            }
        }

        public void FindUnProcessedModels()
        {
            _modelsToProcess = CreateTestAggregators();
        }

        private List<IModelAggregator> CreateTestAggregators()
        {
            List<InMemIModelAggregator> returnAggregators = new List<InMemIModelAggregator>();
            IModelExecutionQueue modelQueue = new FileBasedModelQueue("Queue.txt");
            modelQueue.MaxConcurrentModels = 3;

            InMemIModelAggregator testAggregator1 = new InMemIModelAggregator();
            testAggregator1.Envrionment = "Dev";
            testAggregator1.ErrorEmailAddress = "simon.ovens@transamerica.com";
            testAggregator1.Queue = modelQueue;

            InMemModelState modelState1 = new InMemModelState();
            modelState1.ModelPriority = 3;
            modelState1.ModelIdentifier = "BOP Parallel";
            modelState1.MetaConfigFile = @"\\ccnp-fileprint\data\TA Tech FES\TIME Program\Projects\IN-4818 - Model Development Lifecycle (MDLC) Implementation\Development\UnitTestData\BOPParallel.xml";
            modelState1.SetModelStatus("In Queue");
            modelState1.ModelType = "Alfa";
            testAggregator1.ModelStates.Add(modelState1);

            InMemModelState modelState2 = new InMemModelState();
            modelState2.ModelPriority = 2;
            modelState2.ModelIdentifier = "Fail Process";
            modelState2.MetaConfigFile = @"\\ccnp-fileprint\data\TA Tech FES\TIME Program\Projects\IN-4818 - Model Development Lifecycle (MDLC) Implementation\Development\UnitTestData\FailProcess.xml";
            modelState2.SetModelStatus("In Queue");
            modelState2.ModelType = "Alfa";
            testAggregator1.ModelStates.Add(modelState2);

            InMemModelState modelState3 = new InMemModelState();
            modelState3.ModelPriority = 1;
            modelState3.ModelIdentifier = "Cross";
            modelState3.MetaConfigFile = @"\\ccnp-fileprint\data\TA Tech FES\TIME Program\Projects\IN-4818 - Model Development Lifecycle (MDLC) Implementation\Development\UnitTestData\Cross.xml";
            modelState3.SetModelStatus("In Queue");
            modelState3.ModelType = "Alfa";
            testAggregator1.ModelStates.Add(modelState3);



            returnAggregators.Add(testAggregator1);

            //_modelsToProcess.Add(returnAggregators);

            List<IModelAggregator> returnValue = new List<IModelAggregator>(returnAggregators);
            return returnValue;
            //return returnAggregators.Cast<IModelAggregator>.ToList();
        }
    }

    public class InMemIModelAggregator : IModelAggregator
    {
        string _envrionment = "";
        string _errorEmail = "";
        IModelExecutionQueue _modelQueue;


        List<IModelState> _modelStates = new List<IModelState>();
        public string Envrionment
        {
            get
            {
                return _envrionment;
            }

            set
            {
                _envrionment = value;
            }
        }

        public string ErrorEmailAddress
        {
            get
            {
                return _errorEmail;
            }

            set
            {
                _errorEmail = value;
            }
        }


        public List<IModelState> ModelStates
        {
            get
            {
                return _modelStates;
            }

            set
            {
                _modelStates = value;
            }
        }

        public IModelExecutionQueue Queue
        {
            get
            {
                return _modelQueue;
            }

            set
            {
                _modelQueue = value;
            }
        }
    }

    public class InMemModelState : IModelState
    {
        string _metaConfigFile = "";
        string _modelIdentifier = "";
        int _modelPriority = 0;
        string _modelStatus = "";
        string _modelType = "";
        public string ConnectionInformation
        {
            set
            {
                throw new NotImplementedException();
            }
        }

        public string MetaConfigFile
        {
            get
            {
                return _metaConfigFile;
            }

            set
            {
                _metaConfigFile = value;
            }
        }

        public string ModelIdentifier
        {
            get
            {
                return _modelIdentifier;
            }

            set
            {
                _modelIdentifier = value;
            }
        }

        public int ModelPriority
        {
            get
            {
                return _modelPriority;
            }

            set
            {
                _modelPriority = value;
            }
        }

        public string ModelStatus
        {
            get
            {
                return _modelStatus;
            }
        }

        public string ModelType
        {
            get
            {
                return _modelType;
            }

            set
            {
                _modelType = value;
            }
        }

        public void SetModelStatus(string modelStatus)
        {
            _modelStatus = modelStatus;
        }
    }
}
