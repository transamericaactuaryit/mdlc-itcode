﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Security;
using System.Xml.XPath;
using EnablingSystems.Utilities;

namespace SVN_Password_Encryption_Utility
{
    public partial class Form1 : Form
    {
        private string _errors = "";
        private string _path = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtErrors.Text = "";
            string defaultFolder = ConfigurationManager.AppSettings["DefaultConfigurationLocation"] ?? "";

            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;

            if (!(string.IsNullOrEmpty(defaultFolder)) &&
                (Directory.Exists(defaultFolder)))
            {
                folderDlg.SelectedPath = defaultFolder;
            }

            DialogResult result = folderDlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                _path = folderDlg.SelectedPath;
                lblFolder.Text = _path;
            }
        }

        private void UpdateFiles()
        {

            if (String.IsNullOrEmpty(_path))
            {
                MessageBox.Show("Please select a folder");
                return;
            }

            if (String.IsNullOrEmpty(txtUnEncryptPWD.Text.Trim()))
            {
                MessageBox.Show("Please provide a password");
                return;
            }

            SecureString newSecurePwd = DPAPI_Utilities.ToSecureString(txtUnEncryptPWD.Text.Trim());
            string encryptPwd = DPAPI_Utilities.EncryptString(newSecurePwd);

            foreach (var fileInfo in Directory.GetFiles(_path, "*.xml",
                SearchOption.AllDirectories))
            {
                UpdateSVNUserPassword(fileInfo, "", encryptPwd);
            }

            if (String.IsNullOrEmpty(_errors))
            {
                MessageBox.Show("Files have been updated.");
            }
            else
            {
                txtErrors.Text = _errors;
                MessageBox.Show("File have been updated. Please review errors.");
            }
        }


        private void UpdateSVNUserPassword(string fileName, string svnUser, string svnPassword)
        {
            try
            {
                XElement root =
                    XElement.Load(@fileName);
                var svnPasswordUnEncrypt = root.XPathSelectElements("//ServiceAccountPassword").Single();
                svnPasswordUnEncrypt.Value = svnPassword;

                root.Save(@fileName);

            }
            catch (Exception eX)
            {
                _errors += "\r\nError\r\nFile: " + fileName
                    + "\r\nError:" + eX.Message
                    + "\r\n";
            }

        }


        //public static SecureString ToSecureString(string input)
        //{
        //    SecureString secure = new SecureString();
        //    foreach (char c in input)
        //    {
        //        secure.AppendChar(c);
        //    }
        //    secure.MakeReadOnly();
        //    return secure;
        //}

        //public static string ToInsecureString(SecureString input)
        //{
        //    string returnValue = string.Empty;
        //    IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
        //    try
        //    {
        //        returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
        //    }
        //    finally
        //    {
        //        System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
        //    }
        //    return returnValue;
        //}

        //public static string EncryptString(System.Security.SecureString input)
        //{

        //    byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
        //        System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
        //        null,
        //        System.Security.Cryptography.DataProtectionScope.LocalMachine);
        //    return Convert.ToBase64String(encryptedData);
        //}

        //public static SecureString DecryptString(string encryptedData)
        //{
        //    try
        //    {
        //        byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
        //            Convert.FromBase64String(encryptedData),
        //            null,
        //            System.Security.Cryptography.DataProtectionScope.CurrentUser);
        //        return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
        //    }
        //    catch
        //    {
        //        return new SecureString();
        //    }
        //}

        private void button1_Click_1(object sender, EventArgs e)
        {
            UpdateFiles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string svnInfoFile = @"E:\AutomationCode\General\Job_Aggregator\SVN_Info\SVNInfo.xml";
            string svnUser = "";
            string svnPwd = "";

            RetrieveSVNInfo(svnInfoFile, ref svnUser, ref svnPwd);
            MessageBox.Show("SVNUser is: " + svnUser + " SVNPWD is : " + svnPwd);
        }

        private void RetrieveSVNInfo(string svnInfoFile, ref string svnUser, ref string svnPwd)
        {
            XElement root =
                XElement.Load(svnInfoFile);
            var thisUser = root.XPathSelectElements("//ServiceAccountName").Single();
            var thisPwd = root.XPathSelectElements("//ServiceAccountPassword").Single();

            svnUser = thisUser.Value;
            svnPwd = thisPwd.Value;
        }
    }
}
