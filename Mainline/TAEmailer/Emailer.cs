﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EnablingSystems.Utilities
{
    public class TAEmailer
    {
        private List<string> _attachments = new List<string>();
        private string _body;
        private string _ccAddress;
        private string _fromAddress;
        private bool _isHtml;
        private string _smtpServer;
        private string _subject;
        private string _toAddress;

        #region properties
        public List<string> Attachments
        {
            get
            {
                return _attachments;
            }
        }

        public string Body
        {
            get
            {
                return _body;
            }

            set
            {
                _body = value;
            }
        }

        public string CcAddress
        {
            get
            {
                return _ccAddress;
            }

            set
            {
                _ccAddress = value;
            }
        }

        public string FromAddress
        {
            get
            {
                return _fromAddress;
            }

            set
            {
                _fromAddress = value;
            }
        }

        public bool IsHtml
        {
            get
            {
                return _isHtml;
            }

            set
            {
                _isHtml = value;
            }
        }

        public string SmtpServer
        {
            get
            {
                return _smtpServer;
            }

            set
            {
                _smtpServer = value;
            }
        }

        public string Subject
        {
            get
            {
                return _subject;
            }

            set
            {
                _subject = value;
            }
        }

        public string ToAddress
        {
            get
            {
                return _toAddress;
            }

            set
            {
                _toAddress = value;
            }
        }

        #endregion properties

        public TAEmailer(string toAddress, string fromAddress,
            string ccAddress, string subject, string body,
            bool isHtml, string smtpServer)
        {
            _toAddress = toAddress;
            _fromAddress = fromAddress;
            _ccAddress = ccAddress;
            _subject = subject;
            _body = body;
            _isHtml = isHtml;
            _smtpServer = smtpServer;
        }

        public void SendEmail()
        {
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

            mailMessage.To.Add(_toAddress);

            if (!string.IsNullOrEmpty(_ccAddress))
            {
                mailMessage.CC.Add(_ccAddress);
            }

            if (!string.IsNullOrEmpty(_subject))
            {
                mailMessage.Subject = _subject;
            }

            foreach(string attachmentLocation in _attachments)
            {
                mailMessage.Attachments.Add(new System.Net.Mail.Attachment(attachmentLocation));
            }

            mailMessage.Body = _body;
            mailMessage.From = new System.Net.Mail.MailAddress(_fromAddress);
            mailMessage.IsBodyHtml = _isHtml;

            System.Net.Mail.SmtpClient smtp;

            smtp = new System.Net.Mail.SmtpClient(_smtpServer);

            smtp.Send(mailMessage);
            mailMessage.Dispose();
            smtp.Dispose();
        }
    }
}
