﻿using Domain;
using EnablingSystems;
using System;
using System.Configuration;
using System.Net.Http;

namespace ModelRequestInitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseWebAddress = "";

            try
            {
                baseWebAddress = ConfigurationManager.AppSettings["ModelManagerWebApiBaseUrl"];
            }
            catch (ConfigurationErrorsException ex)
            {
                throw new ConfigurationErrorsException(ex.Message);
            }

            var isUpdated = new ModelManagerHttpClient(new HttpClient(), new Uri(baseWebAddress)).UpdateModelRequestsStatusByStatus(ModelRequestStatusEnums.NotStarted, ModelRequestStatusEnums.InQueue).Result;

            //Data was added to DB successfully so send success message back to Control-M
            //TODO: Add logging.
        }
    }
}
