﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Xml;
using System.Configuration;
using System.Security.Principal;
using System.IO;

namespace EnablingSystems.Utilities
{
    public class EventLogScan
    {
        private string _xPathQuery;
        private string _reportedEntriesFileLocation;
        private string _logSource;
        private bool _foundEvents = false;
        private List<EventRecord> _eventList = new List<EventRecord>();

        #region Properties
        public string XPathQuery
        {
            get
            {
                return _xPathQuery;
            }
        }


        public bool FoundEvents
        {
            get
            {
                return _foundEvents;
            }

        }


        public List<EventRecord> EventList
        {
            get
            {
                return _eventList;
            }
        }

        #endregion Properties

        public EventLogScan(string xPathQuery, string logSource,
            string reportedEntriesFileLocation)
        {
            _xPathQuery = xPathQuery;
            _logSource = logSource;
            _reportedEntriesFileLocation = reportedEntriesFileLocation;

            if(!Directory.Exists(Path.GetDirectoryName(_reportedEntriesFileLocation)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_reportedEntriesFileLocation));
            }

            if(!File.Exists(_reportedEntriesFileLocation))
            {
               // File.Create(_reportedEntriesFileLocation);
                using (StreamWriter sw = new StreamWriter(_reportedEntriesFileLocation, true))
                {
                    //create to the file
                }

            }

        }

        public void QueryEventLog()
        {
            var elQuery = new EventLogQuery(_logSource, PathType.LogName, _xPathQuery);
            var elReader = new System.Diagnostics.Eventing.Reader.EventLogReader(elQuery);
            

            for (EventRecord eventInstance = elReader.ReadEvent();
                null != eventInstance; eventInstance = elReader.ReadEvent())
            {

                if(!ReportedPreviously(eventInstance))
                {
                    _eventList.Add(eventInstance);
                }
            }

            AddToReportedList(_eventList);

        }

        //Consider removing this out of here, and into the particular scanner
        private void AddToReportedList(List<EventRecord> _eventList)
        {            
            using (StreamWriter sw = new StreamWriter(new FileStream(_reportedEntriesFileLocation, FileMode.Append, FileAccess.Write)))
            {
                foreach (var thisItem in _eventList)
                {
                    //sw.WriteLine(thisItem.ActivityId.ToString());
                    sw.WriteLine(thisItem.RecordId.ToString());                    
                }                
            }
        }

        private bool ReportedPreviously(EventRecord eventInstance)
        {
            string thisGuid = "";
            bool reportedPreviously = false;

            string[] lines = File.ReadAllLines(_reportedEntriesFileLocation); 

            for (int x = 0; x < lines.Length - 1; x++)
            {
                thisGuid = lines[x];
                if (thisGuid == eventInstance.ActivityId.ToString())
                {
                    reportedPreviously = true;
                    break; // exit loop if found
                }
            }

            return reportedPreviously;
        }
    }
}
