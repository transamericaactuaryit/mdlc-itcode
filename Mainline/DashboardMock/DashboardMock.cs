﻿using System;
using System.Collections.Specialized;
using System.IO;
using EnablingSystems.ProcessInterfaces;

namespace EnablingSystems.Mocks
{
    public class DashboardMock : IDashboard
    {
        public string ConnectionInformation
        {
            get;set;
        }

        public string DashboardID
        {
            get; set;
        }

        public DateTime EndTime
        {
            get; set;
        }

        public int JobHistoryID
        {
            get { return -1; }
        }

        public int JobInstanceID
        {
            get;set;
        }

        public string ProcessMessage
        {
            get; set;
        }

        public string ProcessName
        {
            get; set;
        }

        public DateTime StartTime
        {
            get; set;
        }

        public bool Success
        {
            get; set;
        }

        public void Save()
        {

        }
    }

    //public class JobHistoryMock : IJobHistory
    //{
    //    private string _connectionInformation;
    //    public string ConnectionInformation
    //    {
    //        set
    //        {
    //            _connectionInformation = value;
    //        }
    //    }

    //    public void SaveHistory(int jobHistoryID, NameValueCollection processHistoryItems)
    //    {
    //        using (System.IO.StreamWriter file =
    //            new StreamWriter(_connectionInformation))
    //        {
    //            foreach (string item in processHistoryItems)
    //            {
    //                file.WriteLine(item + ":" + processHistoryItems[item]);
    //            }
    //        }
    //    }
    //}
}
