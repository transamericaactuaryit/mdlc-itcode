﻿using System.Diagnostics;
using System;
using System.IO;
using System.Collections.Specialized;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnablingSystems.MDLC.Processes;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TestSupport.TraceListeners;
using EnablingSystems.ProcessInterfaces;

namespace Microsoft.Practices.EnterpriseLibrary.Logging.TestSupport.TraceListeners
{
    public class MockTraceListener : TraceListener

    {

        public object tracedData = null;

        public string tracedSource = null;

        public TraceEventType? tracedEventType = null;

        public bool wasDisposed = false;



        private static object traceRequestMonitor = new object();

        private static int processedTraceRequests = 0;

        private static List<LogEntry> entries = new List<LogEntry>();

        private static List<MockTraceListener> instances = new List<MockTraceListener>();

        private static List<Guid> transferGuids = new List<Guid>();



        public MockTraceListener()

            : this("")

        {

        }



        public MockTraceListener(string name)

        {

            this.Name = name;

        }



        public static void Reset()

        {

            lock (traceRequestMonitor)

            {

                entries.Clear();

                instances.Clear();

                transferGuids.Clear();

                processedTraceRequests = 0;

            }

        }



        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)

        {

            lock (traceRequestMonitor)

            {

                tracedData = data;

                tracedSource = source;

                tracedEventType = eventType;

                MockTraceListener.Entries.Add(data as LogEntry);

                MockTraceListener.Instances.Add(this);

                processedTraceRequests++;

            }

        }



        public override void TraceTransfer(TraceEventCache eventCache, string source, int id, string message, Guid relatedActivityId)

        {

            lock (traceRequestMonitor)

            {

                MockTraceListener.transferGuids.Add(relatedActivityId);

            }

        }



        public override void Write(string message)

        {

            throw new Exception("The method or operation is not implemented.");

        }



        public override void WriteLine(string message)

        {

            throw new Exception("The method or operation is not implemented.");

        }



        public static List<LogEntry> Entries

        {

            get { return entries; }

        }



        public static List<MockTraceListener> Instances

        {

            get { return instances; }

        }



        public static List<Guid> TransferGuids

        {

            get { return transferGuids; }

        }



        public static int ProcessedTraceRequests

        {

            get { lock (traceRequestMonitor) { return processedTraceRequests; } }

        }



        public static LogEntry LastEntry

        {

            get { return entries.Count > 0 ? entries[entries.Count - 1] : null; }

        }



        protected override void Dispose(bool disposing)

        {

            base.Dispose(disposing);



            wasDisposed = true;

        }

    }
}
namespace ProcessUnitTests
{

   

    [TestClass]
    public class ProcessUnitTests
    {
        string _environment = "DEV";
        LogWriter logWriter;
        [TestInitialize]
        public void Init()
        {
            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);
        }


        

        public virtual void LoadParams(IProcess process)
        {

            NameValueCollection theseParams = new NameValueCollection();

            foreach (string key in ConfigurationManager.AppSettings)
            {
                if (!key.Contains("Dashboard"))
                {
                    string settingValue = ConfigurationManager.AppSettings[key];

                    string keyUpper = key.ToUpper();
                    //check if it's a configuration depenedent 
                    //on environment
                    if ((keyUpper.Contains("DEV_")) ||
                        (keyUpper.Contains("INT_")) ||
                        (keyUpper.Contains("PROD_")))
                    {
                        string settingEnv = keyUpper.Split('_')[0];

                        if (settingEnv == _environment)
                        {
                            //strip out the environment declaration of the config key,
                            //as the process should not have any knowledge of environment
                            string newKey = key.Replace(settingEnv + "_", "");
                            AddToProcessParams(theseParams, newKey, settingValue);
                        }
                    }
                    //must be a configuration item that is not dependent on 
                    //environment, so add it
                    else
                    {
                        AddToProcessParams(theseParams, key, settingValue);
                    }

                }
            }

            process.ProcessParams = theseParams;
        }

        private static void AddToProcessParams(NameValueCollection theseParams, string key, string settingValue)
        {
            if (settingValue != null)
            {
                theseParams.Add(key, settingValue);
            }
        }
    }
}
