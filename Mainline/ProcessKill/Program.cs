﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using EnablingSystems.MDLC.Processes;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;

namespace EnablingSystems.Utilities
{
    class Program
    {
        static void Main(string[] args)
        {
            int intSleepInterval = int.Parse(ConfigurationManager.AppSettings["TimerInterval"].ToString());

            
            string argProcessName = args[0].ToString();
            string strMachineName = Environment.MachineName;
           
            
            
            try
            {
                var processsToZap = Process.GetProcesses().
                                 Where(pr => pr.ProcessName.ToUpper() == argProcessName.ToUpper());

                foreach (var process in processsToZap)
                {
                    process.Kill();
                }

                System.Threading.Thread.Sleep(intSleepInterval);

                var processToZap2ndPass = Process.GetProcesses().
                                          Where(pr2 => pr2.ProcessName.ToUpper() == argProcessName.ToUpper());

                if (processToZap2ndPass.Count() > 0)
                {
                    string strSubject = ConfigurationManager.AppSettings["subject"] + " " + argProcessName + ".exe.  Environment: " + strMachineName;
                    string strBody = ConfigurationManager.AppSettings["body"] + " " + argProcessName;


                    NameValueCollection col = new NameValueCollection();
                    col.Add("toAddress", ConfigurationManager.AppSettings["toAddress"]);
                    col.Add("fromAddress", ConfigurationManager.AppSettings["fromAddress"]);
                    col.Add("smtpServer", ConfigurationManager.AppSettings["smtpAddress"]);
                    col.Add("ccAddress", ConfigurationManager.AppSettings["ccAddress"]);
                    col.Add("subject", strSubject);
                    col.Add("body", strBody);



                    EmailProcess mail = new EmailProcess(null);

                    mail.ProcessParams = col;
                    mail.Execute();


                }
            }
            catch
            {
               

                //Anything other than 0 will tell scheduler that something went wrong
                Environment.Exit(-1);
            }

            Environment.Exit(0);


        }

                        
    }
}
