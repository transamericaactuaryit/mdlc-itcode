﻿using Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace EnablingSystems.Processes
{
    public class ModelRequestAggregator : IModelRequestAggregator
    {
        private string _modelRequestConfigLocation = "";
        private readonly IModelManagerHttpClient _modelManagerHttpClient;

        private string Environment { get; set; }

        public ModelRequestAggregator(string modelRequestConfigLocation, IModelManagerHttpClient modelManagerHttpClient)
        {
            _modelRequestConfigLocation = modelRequestConfigLocation;
            _modelManagerHttpClient = modelManagerHttpClient;
        }

        public ModelRequest Process()
        {
            if (string.IsNullOrEmpty(_modelRequestConfigLocation) ||
                !File.Exists(_modelRequestConfigLocation))
                throw new FileNotFoundException("The model run config Location is null or does not exist.");


            XElement xelement = XElement.Load(_modelRequestConfigLocation);
            var configs = from configSettings in xelement.Elements()
                          select configSettings;

            List<Model> models = SerializeModels(configs);
            ModelRequest modelRequest = SerializeModelRequest(xelement, models);

            modelRequest = _modelManagerHttpClient.AddModelRequest(modelRequest).Result;

            RenameConfig(_modelRequestConfigLocation);

            return modelRequest;
        }

        private void RenameConfig(string modelRequestConfigLocation)
        {
            File.Move(modelRequestConfigLocation, modelRequestConfigLocation.Replace(".xml", DateTime.Now.ToString("yyyyMMddHHmmss")));
        }

        private List<Model> SerializeModels(IEnumerable<XElement> configs)
        {
            var models = new List<Model>();

            foreach (var element in configs)
            {
                var elementValue = element.Value;

                var initialLookups = _modelManagerHttpClient.GetInitialModelLookups(GetAttributeValue(element, "modelapplication"), elementValue, GetAttributeValue(element, "modelgroup")).Result;
                models.Add(new Model
                {
                    Name = elementValue,
                    CreatedDate = DateTime.Now,
                    Priority = GetAttributeValue(element, "priority") != "" ? int.Parse(GetAttributeValue(element, "priority")) : 999, //this can be used as override, setting all the same inititally. SJO 6/27
                    ModelTypeID = initialLookups.ModelType.ID,
                    ModelApplicationID = initialLookups.ModelApplication.ID,
                    ModelStatusID = initialLookups.ModelStatus.ID,
                    PostOnlyRun = GetAttributeValue(element, "postmodelrun") != "" ? bool.Parse(GetAttributeValue(element, "postmodelrun")) : false
                });
            }

            return models;
        }

        private ModelRequest SerializeModelRequest(XElement xelement, List<Model> models)
        {
            return new ModelRequest
            {
                Name = GetAttributeValue(xelement, "name"),
                CreatedBy = GetAttributeValue(xelement, "createdby"),
                CreatedDate = DateTime.Now,
                StartTime = DateTime.Now,
                Models = models,
                ModelRequestStatusID = int.Parse(GetAttributeValue(xelement, "status"))
            };
        }

        private string GetAttributeValue(XElement xelement, string attributeName)
        {
            var attribute = xelement.Attributes().SingleOrDefault(a => a.Name.ToString().ToLower() == attributeName);
            if (attribute != default(XAttribute))
                return attribute.Value;
            else
                return string.Empty;
        }
    }
}
