﻿using Domain;

namespace EnablingSystems.Processes
{
    public interface IModelRequestAggregator
    {
        ModelRequest Process();
    }
}