﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using GemBox.Spreadsheet;

namespace EnablingSystems.MDLC.Processes
{
    public class TextReportConsolidation : ProcessBaseWithHistory
    {
        private string _reportLogFileDirectory;
        private string _reportOutputFile;
        private int _tabCount;
        private List<string> _workSheetNames = new List<string>();
        ExcelFile _wbLogReport;
        public override void Execute()
        {
            _reportLogFileDirectory = _processParams["LogFileDirectory"];
            _reportOutputFile = _processParams["OutputFile"];

            if ((string.IsNullOrEmpty(_reportLogFileDirectory)) ||
                (string.IsNullOrEmpty(_reportOutputFile)))
            {
                throw new Exception("LogFileDirectory and OutputDirectory"
                    + " must be in the parameter list");
            }

            CreateReports();

            NotifyDone();
        }

        private void CreateReports()
        {

            string[] files =
                Directory.GetFiles(_reportLogFileDirectory, "*.txt",
                SearchOption.TopDirectoryOnly);

            SpreadsheetInfo.SetLicense("EGHB-9SAD-P0EZ-OWD3");
            _wbLogReport = new ExcelFile();

            foreach (var file in files)
            {
                CreateLogTab(file);
            }

            _wbLogReport.Save(_reportOutputFile);
        }

        private void CreateLogTab(string file)
        {
            string sheetName = Path.GetFileNameWithoutExtension(file);
            sheetName = sheetName.Length <= 30 ? sheetName : sheetName.Substring(0, 30);

            _tabCount++;
            //guarantee uniqueness of sheet name
            if (_workSheetNames.Contains(sheetName))
                sheetName = sheetName.Substring(0, 24) + _tabCount.ToString();

            _workSheetNames.Add(sheetName);

            ExcelWorksheet ws = _wbLogReport.Worksheets.Add(sheetName);

            int row = 0;
            foreach (string line in File.ReadLines(file))
            {
                //string[] nameValue = line.Split(':');
                int splitter = line.IndexOf(':');
                if(splitter > 0)
                {
                    string varName = line.Substring(0, line.IndexOf(':'));
                    string varValue = line.Substring(line.IndexOf(':') + 1);
                    ws.Cells[row, 0].Value = varName;
                    ws.Cells[row, 1].Value = varValue;
                }
                else
                {
                    ws.Cells[row, 0].Value = line;
                }
                row++;
            }

            ws.PrintOptions.FitWorksheetWidthToPages = 1;
        }
    }
}
