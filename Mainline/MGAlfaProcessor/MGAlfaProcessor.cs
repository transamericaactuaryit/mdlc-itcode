﻿using EnablingSystems.Utilities;
using Reporting;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace EnablingSystems.MDLC.Processes
{
    //This specialized version is for copying from a 'Base' model to a sensitivity folder
    //skipping some of the files that are produced from a model run
    public class MGAlfaBaseModelDynamicDirectoryCopyFileProcess : DynamicDirectoryCopyFileProcess
    {
        string _regularExpressionsList;

        public override void Execute()
        {
            _regularExpressionsList = _processParams["RegExFilesToExclude"];
            base.Execute();
        }

        protected override void PerformCopy(string destinationDirectory, string fileNameWildcard, 
            string wildCardExclusions, string replaceFromString, string replaceToString, 
            int expectedNumberFiles, bool overWriteDestinationFile, bool isCopy, string workingDirectory)
        {
           MGAlfaBaseWildCardFileCopyForSensitivity wildCardFileCopy =  new MGAlfaBaseWildCardFileCopyForSensitivity(
                workingDirectory,
                fileNameWildcard,
                wildCardExclusions,
                destinationDirectory,
                replaceFromString,
                replaceToString,
                overWriteDestinationFile,
                expectedNumberFiles,
                isCopy
                );
            wildCardFileCopy.RegularExpressionsList = _regularExpressionsList;
            wildCardFileCopy.Copy(_filesProcessed);
        }
    }
    public class MGAlfaBaseWildCardFileCopyForSensitivity : WildCardFileCopy
    {
        public string RegularExpressionsList { get; set; }
        public MGAlfaBaseWildCardFileCopyForSensitivity(string sourceDirectory, string sourceWildCard, string wildcardExclusions,
            string destinationDirectory, string regexFileRenamePattern, string fileNameReplaceToString, bool overWriteDestinatinoFile,
            int expectedNumberFiles, bool isCopy) :
            base(sourceDirectory, sourceWildCard, wildcardExclusions,
                destinationDirectory, regexFileRenamePattern, fileNameReplaceToString, overWriteDestinatinoFile,
                expectedNumberFiles, isCopy) { }

        protected override List<string> GetFileList()
        {
            List<string> baseFileList = base.GetFileList();
            List<string> filesToExclude = new List<string>();
            //do a special loop to get rid of files that we don't want to move
            //(and can't be identified just by the file's extension)
            string[] regexForExclude = RegularExpressionsList.Split('_');

            foreach(string thisFile in baseFileList)
            {
                foreach(string thisRegex in regexForExclude)
                {
                    System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(thisRegex);
                    if(rgx.IsMatch(thisFile))
                    {
                        filesToExclude.Add(thisFile);
                    }
                }
            }

            foreach(string fileToExclude in filesToExclude)
            {
                baseFileList.Remove(fileToExclude);
            }

            return baseFileList;

        }
    }


    public class SetAlfaProcessTrigger : ProcessBaseWithHistory
    {
        public override void Execute()
        {
            if (!_processParams.AllKeys.Contains("AlfaTriggerFileLocation"))
            {
                throw new Exception("Configuration must contain AlfaTriggerFileLocation");
            }

            if (!_processParams.AllKeys.Contains("ModelName"))
            {
                throw new Exception("Configuration must contain ModelName");
            }

            string alfaTriggerFileLocation = _processParams["AlfaTriggerFileLocation"];

            if (!alfaTriggerFileLocation.EndsWith(@"\"))
            {
                alfaTriggerFileLocation += @"\";
            }

            using (System.IO.StreamWriter file =
            new StreamWriter(@alfaTriggerFileLocation + _processParams["ModelName"] + ".txt"))
            {
                foreach (string key in _processParams)
                {
                    file.WriteLine($"{key}={_processParams[key]}");
                }

                //Default setting to use the Process Name as a way handle certain scenarios.
                file.WriteLine(string.Format("{0}={1}", "ProcessName", ProcessName));
            }
        }
    }
    public class MGAlfaExternalProcess : ExternalProcess
    {
        private int _blankOutputReRuns = 0;
        private string _logFileContent = string.Empty;
        protected string _errorIndicatorList = string.Empty;
        private string _errorFileSaveLocation = string.Empty;
        private int _sleepTimePostProcessing = 60000;
        
        public NameValueCollection ScenariosRun { get; } = new NameValueCollection();

        public string ModelAinReName { get; private set; } = string.Empty;

        public override void SaveHistory(int jobHistoryID)
        {
            _processHistory.Add("Alfa Run Log", _logFileContent);

            base.SaveHistory(jobHistoryID);

        }

        public override void Execute()
        {
            string thisModelName = _processParams["ModelName"] ?? @_errorFileSaveLocation;
            //Default to 2 minutes
            _sleepTimePostProcessing = int.Parse(_processParams["SleepTimePostProcessing"] ?? "120000");

            CheckParams();
            RenameModelAin();

            _errorIndicatorList = _processParams["ErrorIndicatorList"] ?? "Wrn|Err|ValidError|::error|TskErr";

            _errorFileSaveLocation = _processParams["ErrorFileSaveLocation"] ?? Directory.GetCurrentDirectory();

            string runScenarios = _processParams["RunScenarios"];
            ScenariosRun.Add("Initial ", "Scenarios:");
            ScenariosRun.Add("Scenarios: ", runScenarios);

            RunModel(runScenarios);

            if (!string.IsNullOrEmpty(sError))
            {
                using (StreamWriter file = new StreamWriter(@_errorFileSaveLocation, false))
                {
                    file.Write(sError);
                }
                string errorLog = $"Error found in processing {thisModelName} Environment: {Environment.MachineName} Error: {sError}";
                ELMAHLogger.LogError(errorLog, "MGAlfaProcessor");
                SendEmail(errorLog, "Please check the attached log for errors and/or warnings", false, _errorFileSaveLocation);

                //Do not throw exception, as we want post-processing to occur (archiving of model output so actuaries can investigate)                
            }

            var postProcessingTriggerFile = _processParams["PostProcessingTriggerFile"];

            if (postProcessingTriggerFile != null)
            {
                using (StreamWriter file = new StreamWriter(postProcessingTriggerFile, true))
                {
                    file.WriteLine(ModelAinReName);
                }
            }

            if (string.IsNullOrEmpty(sError))
            {
                SendEmail(_processParams["Subject"] + " Environment: " + Environment.MachineName,
                _processParams["Body"],
                bool.Parse(_processParams["IsHTML"] ?? "false"));
            }

            NotifyDone();
        }

        //if it's a scenario, don't re-name
        private void RenameModelAin()
        {
            if(ProcessName == "Scenario Import")
            {
                ModelAinReName = _processParams["ModelAinLocation"];
                return;
            }
            
            string _modelPath = Path.GetDirectoryName(_processParams["ModelAinLocation"]);
            ModelAinReName = $@"{_modelPath}\{_processParams["ModelName"]}.ain2";                
            CopyViaStream(_processParams["ModelAinLocation"], ModelAinReName);                            
        }

        private void CopyViaStream(string inFilePath, string outFilePath)
        {
            if(File.Exists(outFilePath))
            {
                File.Delete(outFilePath);
            }

            using (Stream inStream = File.Open(inFilePath, FileMode.Open))
            {
                using (Stream outStream = File.Create(outFilePath))
                {
                    while (inStream.Position < inStream.Length)
                    {
                        outStream.WriteByte((byte)inStream.ReadByte());
                    }
                }
            }
        }
        protected void CheckAlfaLogContentForErrors()
        {

            FileErrorChecker fileErrorChecker = new FileErrorChecker(_errorIndicatorList,
                _processParams["AlfaLogFile"]);

            if(fileErrorChecker.FileContainsErrors())
            {
                sError += "\r\n" + System.IO.File.ReadAllText(@_processParams["AlfaLogFile"]);
            }

            _logFileContent += fileErrorChecker.FileContent;
        }

        private void CheckParams()
        {            
            //Need to re-name here
            if (!File.Exists(_processParams["ModelAinLocation"] ?? string.Empty))
            {
                Exception eX = new FileNotFoundException("Model AIN: "+_processParams["ModelAinLocation"]+" not found");
                eX.Data.Add("ModelAinLocation", _processParams["ModelAinLocation"] ?? string.Empty);
                throw eX;
            }

            if (!Directory.Exists(_processParams["ErrorOutputSizeLocation"] ?? string.Empty))
            {
                Exception eX = new DirectoryNotFoundException("Output directory: "+_processParams["ErrorOutputSizeLocation"] ?? string.Empty+" not found");
                eX.Data.Add("ErrorOutputSizeLocation", _processParams["ErrorOutputSizeLocation"] ?? string.Empty);
                throw eX;
            }

        }

        private void RunModel(string runScenarios)
        {
            //In Alfa version 11, a scenario import returned À, even if the scenario import was successful
            string nonErrorsToExtract = _processParams["NonErrorsToExtract"] ?? "";
            //need to change the OpenModel here
            string openModelString = "OpenModel \"" + ModelAinReName + "\"";
            string valDate = GetValDateString();
            string closeScript = "Close";
            string runScenariosStr = "Run " + runScenarios;

            if (!File.Exists(_processParams["ScriptRunFile"]))
            {
                using (File.Create(_processParams["ScriptRunFile"])) { }
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(_processParams["ScriptRunFile"]))
            {
                file.WriteLine(openModelString);
                file.WriteLine(valDate);
                file.WriteLine(runScenariosStr);
                file.WriteLine(closeScript);
            }
            //run the actual process                
            bool useCreds = _processParams["CredentialFile"] == null ? false : true;


            if (useCreds)
            {
                string workingDirectory = Path.GetDirectoryName(_processParams["ModelAinLocation"]);
                string baseCredFile = _processParams["CredentialFile"];

                //TODO:Make sure baseCredFile Exists
                string workingDirectorySlash = workingDirectory.EndsWith(@"\") ? workingDirectory : workingDirectory + @"\";

                string newCredFile = workingDirectorySlash + Path.GetFileName(baseCredFile);
                File.Copy(baseCredFile, newCredFile, true);

                //This class inherits from ExternalProcess, which expects _processParams["Arguments"]
                //we separate the script file (because we write to it) from the other Arguments,
                //so we re-format  _processParams["Arguments"] here before we call base                  
                string arguments = string.Format(@"/Script={0} /CredentialFile={1}",
                        Path.GetFileName(_processParams["ScriptRunFile"]).Replace("\"", "")
                        , Path.GetFileName(newCredFile).Replace("\"", ""));


                //TODO: nonErrorsToExtract isn't really necessary
                RunAlfaProcess(false, arguments, _processParams["ProcessLocation"], workingDirectory, nonErrorsToExtract);

            }
            else
            {
                string arguments = string.Format(@"/Script={0} ", _processParams["ScriptRunFile"]);
                RunAlfaProcess(false, _processParams["ScriptRunFile"], _processParams["ProcessLocation"]);
            }
            _logFileContent += $"\r\nAlfa log for scenarios: {runScenarios}\r\n";
            CheckAlfaLogContentForErrors();
            SearchAndReprocessBlankOutput();
        }

        //Alfa in version 11 was returning odd unicode characters in the error output. 
        //examples: └└└ and ÀÀÀ
        private void RunAlfaProcess(bool useShellExecute, string processArgs, string processLocation, string workingDirectory, string nonErrorToExtract)
        {
            if (processLocation == null)
            {
                throw new Exception("ProcessLocation did not exist in parameter collection");
            }

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                WorkingDirectory = workingDirectory,
                FileName = processLocation,
                UseShellExecute = useShellExecute,
                Arguments = processArgs
            };            

            string standardOutput = string.Empty;
            string errorOutput = string.Empty;

            using (Process exeProcess = Process.Start(startInfo))
            {
                standardOutput = exeProcess.StandardOutput.ReadToEnd();
                errorOutput = exeProcess.StandardError.ReadToEnd();
                exeProcess.WaitForExit();
            }
        }


        public void RunAlfaProcess(bool useShellExecute, string processArgs, string processLocation)
        {
            if (processLocation == null)
            {
                throw new Exception("ProcessLocation did not exist in parameter collection");
            }

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                FileName = processLocation,
                UseShellExecute = useShellExecute,
                Arguments = processArgs
            };

            string standardOutput = string.Empty;
            string errorOutput = string.Empty;

            using (Process exeProcess = Process.Start(startInfo))
            {
                standardOutput = exeProcess.StandardOutput.ReadToEnd();
                errorOutput = exeProcess.StandardError.ReadToEnd();
                exeProcess.WaitForExit();
            }
        }

        private void SearchAndReprocessBlankOutput()
        {
            long fileSizeThresholdMin = long.Parse(_processParams["ErrorOutputSizeMinimumInBytes"]);
            long fileSizeThresholdMax = long.Parse(_processParams["ErrorOutputSizeMaximumInBytes"] ?? "3229696");
            string outputDirectory = _processParams["ErrorOutputSizeLocation"];
            string outputFileWildCard = _processParams["ErrorOutputSizeWildcard"];

            InterrogateOutputDirectory(outputDirectory, outputFileWildCard, fileSizeThresholdMin, fileSizeThresholdMax);
        }

        private void InterrogateOutputDirectory(string outputDirectory, string outputFileWildCard, long fileSizeMinThreshold, long fileSizeThresholdMax)
        {
            string[] filesToEvaluate = Directory.GetFiles(outputDirectory, outputFileWildCard);
            List<string> ScenariousToReRun = new List<string>();

            foreach (string file in filesToEvaluate)
            {
                FileInfo fileInfo = new FileInfo(file);

                if (fileInfo.Length <= fileSizeMinThreshold)
                {
                    ScenariousToReRun.Add(ParseOutRunNumberFromFile(fileInfo.Name));
                }

                if (fileInfo.Length >= fileSizeThresholdMax)
                {
                    ScenariousToReRun.Add(ParseOutRunNumberFromFile(fileInfo.Name));
                }
            }
            NotifyAndReRunScenarios(ScenariousToReRun);
        }

        private void NotifyAndReRunScenarios(List<string> scenariousToReRun)
        {
            //create string 2,3
            if (scenariousToReRun.Count > 0)
            {
                _blankOutputReRuns++;

                string errorScenarios = string.Empty;

                foreach (string thisBlankScenario in scenariousToReRun)
                {
                    errorScenarios += $"{thisBlankScenario},";
                }

                errorScenarios = errorScenarios.TrimEnd(',');

                if (_blankOutputReRuns > 1)
                {
                    string errorScenariosMsg = $"The following scenario(s) have incomplete output after attempting to re-run:\r\n{errorScenarios}";
                        

                    //write an error file too
                    sError += $"\r\n{errorScenariosMsg}";
                    
                }
                else
                {
                    ScenariosRun.Add("Scenarios ", "Re-run (blank output)");

                    SendEmail("ARX2 file incomplete. Model Name: " + _processParams["ModelName"]
                        + " Environment: " + Environment.MachineName,
                        String.Format("Run(s) {0} resulted in incomplete output. "
                        + "Run(s) {0} is being systematically re-run", errorScenarios),
                        bool.Parse(_processParams["IsHTML"] ?? "false"));
                    RunModel(errorScenarios);
                    ScenariosRun.Add("Scenario " + errorScenarios, " re-ran");
                }


            }
        }

        /// <summary>
        /// Assumes that the file name is in the format of
        /// Q117_Ifrs_Spoyr2.Run.009.Arx2
        /// </summary>
        /// <param name="name"></param>
        /// <returns>remove leading zeros and return</returns>
        private string ParseOutRunNumberFromFile(string name)
        {            
            return name.Split('.')[2].TrimStart('0');            
        }


        private void SendEmail(string subject, string body, bool isHTML)
        {
            string toAddress = _processParams["ToAddress"];
            string fromAddress = _processParams["FromAddress"];
            string smtpServer = _processParams["SmtpServer"];
            string ccAddress = _processParams["CcAddress"] ?? string.Empty;

            ELMAHLogger.LogError(subject, "MGAlfaProcessor");

            TAEmailer emailer = new TAEmailer(toAddress, fromAddress, ccAddress, subject, body, isHTML, smtpServer);
            emailer.SendEmail();
        }

        private void SendEmail(string subject, string body, bool isHTML, string errorFilelocation)
        {
            string toAddress = _processParams["ToAddress"];
            string fromAddress = _processParams["FromAddress"];
            string smtpServer = _processParams["SmtpServer"];            
            string ccAddress = _processParams["CcAddress"] ?? string.Empty;
            
            TAEmailer emailer = new TAEmailer(toAddress, fromAddress, ccAddress, subject, body, isHTML, smtpServer);

            if (File.Exists(errorFilelocation))
            {
                emailer.Attachments.Add(errorFilelocation);
            }
            emailer.SendEmail();
        }

        private string GetValDateString()
        {
            int monthOffset = int.Parse(_processParams["ProcessTimeToMonthOffsetForValDate"] ?? "0");
            DateTime processDate = DateTime.Now.AddMonths(monthOffset);
            
            return $"ValDate {processDate.Month.ToString().PadLeft(2, '0')}/{processDate.Year.ToString()}"; ;
        }

        public override void NotifyDone()
        {
            //sometimes the .ain file is still in use when we archive
            //pausing here so Alfa has a chance to release the file
            //Thread.Sleep(_sleepTimePostProcessing);
            base.NotifyDone();
        }

        public MGAlfaExternalProcess() : base() {}        
    }
}
