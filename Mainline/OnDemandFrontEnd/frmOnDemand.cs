﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.DirectoryServices.AccountManagement;
using System.Collections;

namespace OnDemand
{
    public partial class frmOnDemand : Form
    {
        public frmOnDemand()
        {
                        
            InitializeComponent();       
        }

        enum TypeOf
        {
            NonSensFile,
            SensFile            
        }

        private void InitializeCells()
        {
            chkSensitivityConfiguration.SelectedItems.Clear();
            txtRunDefinition.Text = null;          
            cmbProcessTimeToMonthOffset.Text="-1";
            cmbValdateOffset.Text = "0";
            cmbEnvironment.Text = "DEV";
            
        }

        private bool ValidateText(object sender, EventArgs e)
        {
            TextBox txtBox = sender as TextBox;
            String strpattern = @"[0-9\-\,]"; //Pattern is Ok
          
            Regex regex = new Regex(strpattern);
            if (!regex.Match(sender.ToString()).Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsExistingSensitivity(string startFolder, string strFileName)
        {

            return File.Exists(startFolder + strFileName + ".xml");         
        }

        private void AuthorizeUser()
        {
            UserPrincipal up = UserPrincipal.Current;

            bool isAuthorized = false;

            PrincipalSearchResult<Principal> groups = up.GetAuthorizationGroups();

            object[] objarValue = new object[1];
            string adGrp = "TA Fin VAFVModelers";
            //string adGrp = "TA Fin VAFVModelers";
             //<!--<add key ="ADGroup" value="TA Fin VAFVModelers"/>-->

            IEnumerable<Principal> query = groups.Where(grp => (grp.SamAccountName.Equals(adGrp)));

            ArrayList groups2 = new ArrayList();

            
            foreach (GroupPrincipal grp in query)
            {
               
                //objarValue[0] = grp.SamAccountName.ToString();


                //groups2.Add(objarValue[0].ToString());
                isAuthorized = true;
            }

            if (isAuthorized == false)
            {
                MessageBox.Show("Access denied.", "Unauthorized!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //AuthorizeUser();
            
            InitializeCells();
            btnUncheckAll.Text = "Check All";


            
            //LoadCheckBoxLists(TypeOf.NonSensFile.ToString());
            //LoadCheckBoxLists(TypeOf.SensFile.ToString());
           
            

            btnUncheckAll_Click(sender, e);
        }

       
        private void LoadCheckBoxLists(string arg)
        {
            SensitivityMapping mapping = new SensitivityMapping();
            string filePath = ConfigurationManager.AppSettings[arg];
            using (var reader = new StreamReader(filePath))
            {
                List<SensitivityMapping> FileName = new List<SensitivityMapping>();


                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    mapping.sFileName = values[0];

                    if (arg == "SensFile")
                    {
                        chkSensitivityConfiguration.Items.Add(mapping.sFileName);
                    }
                    else
                    {
                        chkNonSensitivityConfiguration.Items.Add(mapping.sFileName);
                    }
                    
                }


            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string environment = cmbEnvironment.Text.ToString();
            string sensitivityConfigPath = "";
            string nonSensitivityConfigPath = "";
            string sensSubmittedXMLFilePath="";  
            string nonSensSubmittedXMLFilePath="";
            string triggerFileArchiveTo = "";

            switch (environment)
            {
                case "DEV":
                    sensitivityConfigPath = ConfigurationManager.AppSettings["SensPathDev"];
                    nonSensitivityConfigPath = ConfigurationManager.AppSettings["NonSensPathDev"];
                    sensSubmittedXMLFilePath = ConfigurationManager.AppSettings["SensOnDemandPathAndFileNameDev"];
                    nonSensSubmittedXMLFilePath = ConfigurationManager.AppSettings["NonSensOnDemandPathAndFileNameDev"];
                    triggerFileArchiveTo = ConfigurationManager.AppSettings["ArchiveTriggerLocDev"];
                    break;
                case "TEST":
                    sensitivityConfigPath = ConfigurationManager.AppSettings["SensPathTest"];
                    nonSensitivityConfigPath = ConfigurationManager.AppSettings["NonSensPathTest"];
                    sensSubmittedXMLFilePath = ConfigurationManager.AppSettings["SensOnDemandPathAndFileNameTest"];
                    nonSensSubmittedXMLFilePath = ConfigurationManager.AppSettings["NonSensOnDemandPathAndFileNameTest"];
                    triggerFileArchiveTo = ConfigurationManager.AppSettings["ArchiveTriggerLocTest"];
                    break;
                case "INT":
                    sensitivityConfigPath = ConfigurationManager.AppSettings["SensPathInt"];
                    nonSensitivityConfigPath = ConfigurationManager.AppSettings["NonSensPathInt"];
                    sensSubmittedXMLFilePath = ConfigurationManager.AppSettings["SensOnDemandPathAndFileNameInt"]; //+ "OnDemandRequest_" + string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now) + ".xml";
                    nonSensSubmittedXMLFilePath = ConfigurationManager.AppSettings["NonSensOnDemandPathAndFileNameInt"];
                    triggerFileArchiveTo = ConfigurationManager.AppSettings["ArchiveTriggerLocInt"];
                    break;
                case "PRD":
                    sensSubmittedXMLFilePath = ConfigurationManager.AppSettings["SensOnDemandPathAndFileNamePrd"];// + "OnDemandRequest_" + string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now) + ".xml";
                    nonSensSubmittedXMLFilePath = ConfigurationManager.AppSettings["NonSensOnDemandPathAndFileNamePrd"];
                    sensitivityConfigPath = ConfigurationManager.AppSettings["SensPathPrd"];
                    nonSensitivityConfigPath = ConfigurationManager.AppSettings["NonSensPathPrd"];
                    triggerFileArchiveTo = ConfigurationManager.AppSettings["ArchiveTriggerLocPrd"];
                    break;
                default:
                    break;
            }

                      
            try
            {
                if(cmbEnvironment.Text.ToString() =="" || cmbEnvironment.Text == null)
                {
                    MessageBox.Show("You must choose an environment!", "Required Selection!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbEnvironment.Focus();
                }
                else if (chkSensitivityConfiguration.CheckedItems.Count == 0 && chkNonSensitivityConfiguration.CheckedItems.Count == 0)
                {
                    MessageBox.Show("You must check at least one model from either of the two lists!", "Required Selection!",MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtRunDefinition.Text.ToString() == "" || txtRunDefinition.Text == null)
                {
                    MessageBox.Show("Run Definition required.", "Required Input!",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtRunDefinition.Focus();
                }
                else if (txtRunDefinition.Text.ToString().Any(s => (!Regex.IsMatch(txtRunDefinition.Text.ToString(), @"[0-9\-\,]"))))
                {
                    
                    MessageBox.Show("Please enter some combination of numbers, commas, and dashes","Required Input!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtRunDefinition.Focus();
                }
                else if (cmbProcessTimeToMonthOffset.Text.ToString() == "" || cmbProcessTimeToMonthOffset.Text == null)
                {
                    MessageBox.Show("Process Time To Month Offset required!", "Required Input!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbProcessTimeToMonthOffset.Focus();
                }
                else
                {
                    if(cmbProcessTimeToMonthOffset.Text == "-1" && cmbValdateOffset.Text == "0")
                    {
                        DialogResult dialogResult = MessageBox.Show("You have chosen to submit using the default values for 'Month Offset To Quarterly Directories' and 'Valdate Month Offset'.  Continue?", "Submit with Default Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            //Process any non-sensitivities
                           string strShowSavedMessage1 = GenerateNonSensitivitiesOnDemandFiles(nonSensitivityConfigPath, nonSensSubmittedXMLFilePath, triggerFileArchiveTo);
                    
                            //Process any sensitivities
                           string strShowSavedMessage2 = GenerateSensitivitiesOnDemandFiles(sensitivityConfigPath, sensSubmittedXMLFilePath, triggerFileArchiveTo);

                            //Only show submitted successfully if "Override" or ""
                            if (strShowSavedMessage1 == "YES" || strShowSavedMessage2 == "YES")
                            {
                                MessageBox.Show("The OnDemand request has been submitted!", "Submitted!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            MessageBox.Show("Revise 'Month Offset To Quarterly Directories' and/or 'Valdate Month Offset'!", "Revise!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        //Process any non-sensitivities
                        string strShowSavedMessage3 = GenerateNonSensitivitiesOnDemandFiles(nonSensitivityConfigPath, nonSensSubmittedXMLFilePath, triggerFileArchiveTo);

                        //Process any sensitivities
                        string strShowSavedMessage4 = GenerateSensitivitiesOnDemandFiles(sensitivityConfigPath, sensSubmittedXMLFilePath, triggerFileArchiveTo);

                        //Only show submitted successfully if "Override" or ""
                        if (strShowSavedMessage3 == "YES" || strShowSavedMessage4 == "YES")
                        {
                            MessageBox.Show("The OnDemand request has been submitted!", "Submitted!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        // MessageBox.Show("The OnDemand request has been submitted!", "Submitted!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                   

                }
            }
            catch(Exception ex)
            {
                string strDestPath = "";
                
                if(!string.IsNullOrEmpty(sensSubmittedXMLFilePath))
                    strDestPath = sensSubmittedXMLFilePath;

                 if(!string.IsNullOrEmpty(nonSensSubmittedXMLFilePath))
                    strDestPath = nonSensSubmittedXMLFilePath;

                //check for exception could not find part of the path.  When found,
                //set custom exception to permission denied error.
                if (ex.HResult == -2147024893)
                {
                    MessageBox.Show("Permission denied to: " + strDestPath , "Permission Denied!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
        }

        private string GenerateNonSensitivitiesOnDemandFiles(string nonSensitivityConfigPath, string NonSensSubmittedXMLFilePath, string triggerArchiveTo)
        {
            string strDialogRequest = "";
            string strIterationFile = "";
            string strShowSubmittedSave = "";

            foreach (object itemChecked in chkNonSensitivityConfiguration.CheckedItems)
            {
                strDialogRequest = "";

                if (IsExistingSensitivity(NonSensSubmittedXMLFilePath, itemChecked.ToString()) == true)
                {
                    DialogResult dialogResult = MessageBox.Show("There is already a non-sensitivity file called " + itemChecked.ToString() + ".xml in the submitted location.  Do you wish to override?", "Override Existing File?", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        strDialogRequest = "Override";
                    }
                    else
                    {
                        strDialogRequest = "Don't Override";
                    }

                }

                string file = "";

                if (strDialogRequest == "" || strDialogRequest == "Override")
                {
                    file = itemChecked.ToString() + ".xml";
                    strIterationFile = NonSensSubmittedXMLFilePath + file;

                    GenerateXML(strIterationFile, nonSensitivityConfigPath, itemChecked.ToString());

                  
                    ArchiveTriggerFile(strIterationFile, triggerArchiveTo, file);

                    strShowSubmittedSave = "YES";
                }

               
            }

            return strShowSubmittedSave;
        }

        private string GenerateSensitivitiesOnDemandFiles(string sensitivityConfigPath, string SensSubmittedXMLFilePath, string triggerArchiveTo)
        {
            string strDialogRequest = "";
            string strIterationFile = "";
            string strShowSubmittedSave = "";
            
            foreach (object itemChecked in chkSensitivityConfiguration.CheckedItems)
            {
                strDialogRequest = "";

                if (IsExistingSensitivity(SensSubmittedXMLFilePath, itemChecked.ToString()) == true)
                {
                    DialogResult dialogResult = MessageBox.Show("There is already a sensitivity file called " + itemChecked.ToString() + ".xml in the submitted location.  Do you wish to override?", "Override Existing File?", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        strDialogRequest = "Override";
                    }
                    else
                    {
                        strDialogRequest = "Don't Override";
                    }

                }

                string file="";
                if (strDialogRequest == "" || strDialogRequest == "Override")
                {
                    
                    file = itemChecked.ToString() + ".xml";
                    strIterationFile = SensSubmittedXMLFilePath + file;

                    GenerateXML(strIterationFile, sensitivityConfigPath, itemChecked.ToString());
                  
                    ArchiveTriggerFile(strIterationFile, triggerArchiveTo, file);

                    strShowSubmittedSave = "YES";
                }

                
            }

            return strShowSubmittedSave;
            
        }

        private void GenerateXML(string iterationFile,string configPath, string itemChecked)
        {
            
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("    ");
            settings.CloseOutput = true;
            settings.OmitXmlDeclaration = true;
            
            
            using (XmlWriter writer = XmlWriter.Create(iterationFile, settings))
            {
                                
                // Next show the object title and check state for each item selected.

                writer.WriteStartElement("OnDemandRuns");
               
                writer.WriteStartElement("OnDemandRun");
                writer.WriteElementString("Environment", cmbEnvironment.Text.ToString());
                
                writer.WriteElementString("Configuration", configPath + itemChecked.ToString() + "Pre.xml");
                
               
                writer.WriteElementString("RunDefinition", txtRunDefinition.Text);
                writer.WriteElementString("ProcessTimeToMonthOffset", cmbProcessTimeToMonthOffset.Text.ToString());
                writer.WriteElementString("ProcessTimeToMonthOffsetForValDate", cmbValdateOffset.Text.ToString());
                writer.WriteElementString("SubmittedBy", Environment.UserName.ToString());
                writer.WriteElementString("SubmittedDate", System.DateTime.Now.ToString());
                writer.WriteEndElement();
                writer.Flush();

               
                writer.WriteEndElement();

            }
        }

        private void ArchiveTriggerFile(string strIterationFile, string triggerArchiveTo, string fileToArchive)
        {
            string archiveFileName = string.Concat(
                     triggerArchiveTo,
                     "GUI_" + Path.GetFileNameWithoutExtension(fileToArchive),
                     DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                     Path.GetExtension(fileToArchive)
                    );
            File.Copy(strIterationFile, archiveFileName);
        }

        private void btnUncheckAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnUncheckAll.Text == "Uncheck All")
                {
                    foreach (int i in chkSensitivityConfiguration.CheckedIndices)
                    {
                        chkSensitivityConfiguration.SetItemCheckState(i, CheckState.Unchecked);
                    }

                   

                    btnUncheckAll.Text = "Check All";
                }
                else
                {
                    for (int i = 0; i < chkSensitivityConfiguration.Items.Count; i++)
                    {
                        chkSensitivityConfiguration.SetItemCheckState(i, CheckState.Checked);
                    }

                  
                    btnUncheckAll.Text = "Uncheck All";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Form.ActiveForm.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine("btnClose_Click() Error: " + ex.Message);
            }
        }
    }

    public class SensitivityMapping
    {
        public string sFileName { get; set; }
        public string sFilePath { get; set; }

    }
}
