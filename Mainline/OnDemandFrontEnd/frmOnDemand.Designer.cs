﻿namespace OnDemand
{
    partial class frmOnDemand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkSensitivityConfiguration = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtRunDefinition = new System.Windows.Forms.TextBox();
            this.cmbProcessTimeToMonthOffset = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.cmbEnvironment = new System.Windows.Forms.ComboBox();
            this.btnUncheckAll = new System.Windows.Forms.Button();
            this.chkNonSensitivityConfiguration = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbValdateOffset = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkSensitivityConfiguration
            // 
            this.chkSensitivityConfiguration.FormattingEnabled = true;
            this.chkSensitivityConfiguration.Location = new System.Drawing.Point(26, 310);
            this.chkSensitivityConfiguration.Name = "chkSensitivityConfiguration";
            this.chkSensitivityConfiguration.Size = new System.Drawing.Size(416, 394);
            this.chkSensitivityConfiguration.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Sensitivities:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtRunDefinition);
            this.groupBox1.Location = new System.Drawing.Point(465, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(201, 54);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Run Definition:";
            // 
            // txtRunDefinition
            // 
            this.txtRunDefinition.Location = new System.Drawing.Point(13, 21);
            this.txtRunDefinition.Name = "txtRunDefinition";
            this.txtRunDefinition.Size = new System.Drawing.Size(124, 20);
            this.txtRunDefinition.TabIndex = 3;
            // 
            // cmbProcessTimeToMonthOffset
            // 
            this.cmbProcessTimeToMonthOffset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcessTimeToMonthOffset.FormattingEnabled = true;
            this.cmbProcessTimeToMonthOffset.Items.AddRange(new object[] {
            "",
            "-12",
            "-11",
            "-10",
            "-9",
            "-8",
            "-7",
            "-6",
            "-5",
            "-4",
            "-3",
            "-2",
            "-1",
            "0",
            "1",
            "2",
            "3"});
            this.cmbProcessTimeToMonthOffset.Location = new System.Drawing.Point(82, 24);
            this.cmbProcessTimeToMonthOffset.Name = "cmbProcessTimeToMonthOffset";
            this.cmbProcessTimeToMonthOffset.Size = new System.Drawing.Size(70, 21);
            this.cmbProcessTimeToMonthOffset.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Environment:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbProcessTimeToMonthOffset);
            this.groupBox2.Location = new System.Drawing.Point(465, 128);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 58);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Month Offset to Quarterly Directories:";
            // 
            // btnSubmit
            // 
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Location = new System.Drawing.Point(717, 19);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(100, 23);
            this.btnSubmit.TabIndex = 5;
            this.btnSubmit.Text = "Submit Job";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(717, 48);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmbEnvironment
            // 
            this.cmbEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEnvironment.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbEnvironment.FormattingEnabled = true;
            this.cmbEnvironment.Items.AddRange(new object[] {
            "",
            "DEV",
            "INT",
            "PRD",
            "TEST"});
            this.cmbEnvironment.Location = new System.Drawing.Point(109, 6);
            this.cmbEnvironment.Name = "cmbEnvironment";
            this.cmbEnvironment.Size = new System.Drawing.Size(70, 21);
            this.cmbEnvironment.TabIndex = 0;
            // 
            // btnUncheckAll
            // 
            this.btnUncheckAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUncheckAll.Location = new System.Drawing.Point(364, 29);
            this.btnUncheckAll.Name = "btnUncheckAll";
            this.btnUncheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnUncheckAll.TabIndex = 2;
            this.btnUncheckAll.Text = "Uncheck All";
            this.btnUncheckAll.UseVisualStyleBackColor = true;
            this.btnUncheckAll.Click += new System.EventHandler(this.btnUncheckAll_Click);
            // 
            // chkNonSensitivityConfiguration
            // 
            this.chkNonSensitivityConfiguration.FormattingEnabled = true;
            this.chkNonSensitivityConfiguration.Location = new System.Drawing.Point(23, 85);
            this.chkNonSensitivityConfiguration.Name = "chkNonSensitivityConfiguration";
            this.chkNonSensitivityConfiguration.Size = new System.Drawing.Size(416, 184);
            this.chkNonSensitivityConfiguration.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Select  Non-Sensitivities:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbValdateOffset);
            this.groupBox3.Location = new System.Drawing.Point(465, 205);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(201, 58);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Valdate Offset:";
            // 
            // cmbValdateOffset
            // 
            this.cmbValdateOffset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbValdateOffset.FormattingEnabled = true;
            this.cmbValdateOffset.Items.AddRange(new object[] {
            "",
            "-12",
            "-11",
            "-10",
            "-9",
            "-8",
            "-7",
            "-6",
            "-5",
            "-4",
            "-3",
            "-2",
            "-1",
            "0"});
            this.cmbValdateOffset.Location = new System.Drawing.Point(82, 24);
            this.cmbValdateOffset.Name = "cmbValdateOffset";
            this.cmbValdateOffset.Size = new System.Drawing.Size(70, 21);
            this.cmbValdateOffset.TabIndex = 4;
            // 
            // frmOnDemand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 737);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkNonSensitivityConfiguration);
            this.Controls.Add(this.btnUncheckAll);
            this.Controls.Add(this.cmbEnvironment);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkSensitivityConfiguration);
            this.Name = "frmOnDemand";
            this.Text = "OneMDLC - OnDemand Program";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox chkSensitivityConfiguration;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRunDefinition;
        private System.Windows.Forms.ComboBox cmbProcessTimeToMonthOffset;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cmbEnvironment;
        private System.Windows.Forms.Button btnUncheckAll;
        private System.Windows.Forms.CheckedListBox chkNonSensitivityConfiguration;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbValdateOffset;

    }
}

