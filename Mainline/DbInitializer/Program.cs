﻿using DataAccess;
using Domain;
using System;
using System.Data.Entity;
using System.Linq;

namespace DbInitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<ModelManagerDbContext, DataAccess.Migrations.Configuration>());

            //using(var context = new ModelManagerDbContext())
            //{
            //    var modelStatus = context.ModelStatus.Add(new ModelStatus { Status = "Test" });
            //    context.SaveChanges();

            //    context.ModelStatus.Remove(modelStatus);
            //    context.SaveChanges();
            //}

            Console.WriteLine("DB created. Press any key...");
            Console.ReadKey();
        }
    }
}
