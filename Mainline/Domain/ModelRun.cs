﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelRun
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Run { get; set; }

        public int ModelID { get; set; }
        public Model Model { get; set; }

        public int ModelRunStatusID { get; set; }
        public virtual ModelRunStatus ModelRunStatus { get; set; }
    }
}
