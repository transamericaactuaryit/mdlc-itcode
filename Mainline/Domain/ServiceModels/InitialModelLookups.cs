﻿namespace Domain.ServiceModels
{
    public class InitialModelLookups
    {
        public ModelStatus ModelStatus { get; set; }
        public ModelApplication ModelApplication { get; set; }
        public ModelType ModelType { get; set; }
    }
}
