﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ServiceModels
{
    public class OnDemandRun
    {
        public string Environment { get; set; }
        public string Configuration { get; set; }
        public string RunDefinition { get; set; }
        public string ProcessTimeToMonthOffset { get; set; }
        public string ProcessTimeToMonthOffsetForValDate { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime SubmittedDate { get; set; }
    }
}
