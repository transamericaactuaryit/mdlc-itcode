﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelStatus
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Status { get; set; }
        
    }
}
