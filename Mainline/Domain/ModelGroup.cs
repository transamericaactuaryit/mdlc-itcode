﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelGroup
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }
    }
}
