﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelRequest
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? CompletedTime { get; set; }
        public string Tags { get; set; }

        public int ModelRequestStatusID { get; set; }

        public virtual ModelRequestStatus ModelRequestStatus { get; set; }

        public ICollection<Model> Models { get; set; }

    }
}
