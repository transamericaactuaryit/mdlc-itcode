﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelApplication
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
