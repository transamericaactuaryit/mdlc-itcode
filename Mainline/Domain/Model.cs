﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Model
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? CompletedTime { get; set; }
        public int Priority { get; set; }
        public string OnDemandConfigurationFile { get; set; }
        public bool? PostOnlyRun { get; set; }

        public int ModelRequestID { get; set; }
        public ModelRequest ModelRequest { get; set; }

        public int ModelApplicationID { get; set; }
        public virtual ModelApplication ModelApplication { get; set; }

        public int ModelStatusID { get; set; }
        public virtual ModelStatus ModelStatus { get; set; }

        public int ModelTypeID { get; set; }
        public virtual ModelType ModelType { get; set; }

        public ICollection<ModelRun> ModelRuns { get; set; }
    }
}
