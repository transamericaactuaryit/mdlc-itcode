﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelRequestStatus
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Status { get; set; }
    }
}
