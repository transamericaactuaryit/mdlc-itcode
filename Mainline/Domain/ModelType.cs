﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ModelType
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Score { get; set; }

        [Required]
        public string ConfigurationFile { get; set; }

        public string Tags { get; set; }

        public int ModelGroupID { get; set; }
        public virtual ModelGroup ModelGroup { get; set; }
    }
}
