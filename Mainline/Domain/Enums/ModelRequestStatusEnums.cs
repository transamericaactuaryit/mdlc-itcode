﻿using System.ComponentModel;

namespace Domain
{
    //the descriptions are what we use in the DB for UI purposes.
    public enum ModelRequestStatusEnums
    {
        [Description("Not Started")]
        NotStarted = 1,
        [Description("In Queue")]
        InQueue = 2,
        Executing = 3,
        Complete = 4,
        Error = 5,
        [Description("On Hold")]
        OnHold = 7,
    }
}
