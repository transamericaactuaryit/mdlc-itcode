﻿using System.ComponentModel;

namespace Domain
{
    //the descriptions are what we use in the DB for UI purposes.
    public enum ModelGroupEnums
    {
        [Description("Fair Value")]
        FairValue = 1,
        DAC = 2
    }
}
