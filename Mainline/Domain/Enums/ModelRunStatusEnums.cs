﻿using System.ComponentModel;

namespace Domain
{
    //the descriptions are what we use in the DB for UI purposes.
    public enum ModelRunStatusEnums
    {
        [Description("Not Started")]
        NotStarted = 1,
        Executing = 3,
        Complete = 4,
        Error = 5
    }
}
