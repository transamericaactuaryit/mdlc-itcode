﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Configuration;
using Microsoft.Practices.Unity;
using System.IO;
using EnablingSystems.ProcessInterfaces;
using EnablingSystems.OneMDLC.History;
using EnablingSystems.Mocks;
using EnablingSystems.MDLC.Processes;
using Domain;

namespace EnablingSystems.Processes
{
    public abstract class ProcessAggregator : IAutomationAggregator, IDisposable
    {
        private bool disposed = false;
        protected string _environment = "DEV";
        protected string _metaConfigFile = string.Empty;
        protected Model _model;
        protected NameValueCollection _processParams;

        protected UnityContainer _unityContainer;

        public string Environment
        {
            set
            {
                _environment = value;
            }
        }

        public virtual string MetaConfigFile
        {
            set
            {
                _metaConfigFile = value;
            }
        }

        public virtual Model Model
        {
            set
            {
                _model = value;
            }
        }

        public bool PostModelOnly { get; set; } = false;

        public void SetupAutomations()
        {
            if (string.IsNullOrEmpty(_metaConfigFile))
            {
                _metaConfigFile = System.IO.Directory.GetCurrentDirectory() + @"\MetaProcesses.xml";
            }

            _unityContainer = new UnityContainer();
            _unityContainer.RegisterType<IJobHistory, JobHistoryFile>("JobHistoryFile");
            _unityContainer.RegisterType <IJobHistory, JobHistorySaveToDB>("JobHistoryDatabase");
            _unityContainer.RegisterType<IDashboard, DashboardMock>("DashboardMock");
            _unityContainer.RegisterType<IDashboard, DashboardSaveToDB>("DashboardDatabase");            
        }

        public abstract void RunAutomations();

        private void ProcessJobType(UnityContainer container, XElement childElement, string processName)
        {
            DateTime processStartTime = DateTime.Now;
            DateTime processFinishTime;
            var thisProcess = container.Resolve<IProcess>();

            NameValueCollection processParams = new NameValueCollection();

            foreach (var keyValue in childElement.Elements())
            {
                processParams.Add(keyValue.Attribute("key").Value,
                    keyValue.Attribute("value").Value);
            }

            LoadSingleParams(thisProcess, processParams);
            thisProcess.ProcessName = processName;

            bool useDbJobHistory = bool.Parse(processParams["UseDBJobHistory"] ?? "false");

            IJobHistory jobHistory;
            IDashboard dashboard;

            if (useDbJobHistory)
            {
                jobHistory = _unityContainer.Resolve<IJobHistory>("JobHistoryDatabase", 
                    new ResolverOverride[] { new ParameterOverride("model", _model) });
                //dashboard = _unityContainer.Resolve<IDashboard>("DashboardDatabase");
                dashboard = _unityContainer.Resolve<IDashboard>("DashboardMock");
            }
            else
            {
                jobHistory = _unityContainer.Resolve<IJobHistory>("JobHistoryFile");
                dashboard = _unityContainer.Resolve<IDashboard >("DashboardMock");

            }
            dashboard.ConnectionInformation =
                processParams["DashboardConnection"] ?? thisProcess.ProcessParams["JobHistoryConnection"];
            dashboard.JobInstanceID = int.Parse(thisProcess.ProcessParams["JobInstanceID"] ?? "-1");
            dashboard.DashboardID = thisProcess.ProcessParams["DashboardID"];
            dashboard.ProcessName = thisProcess.ProcessParams["DashboardProcessName"];

            jobHistory.ConnectionInformation = thisProcess.ProcessParams["JobHistoryConnection"];
            thisProcess.JobHistory = jobHistory;

            try
            {
                thisProcess.Execute();
            }
            catch (Exception eX)
            {
                processFinishTime = DateTime.Now;
                dashboard.ProcessMessage = "Error: " + eX.Message;
                dashboard.StartTime = processStartTime;
                dashboard.EndTime = processFinishTime;
                dashboard.Success = false;
                dashboard.Save();
                eX.Data.Add("DashboardID", dashboard.DashboardID);
                eX.Data.Add("Dashboard Process Name", dashboard.ProcessName);
                
                throw;
            }
            processFinishTime = DateTime.Now;
            dashboard.StartTime = processStartTime;
            dashboard.EndTime = processFinishTime;
            dashboard.Success = true;
            dashboard.ProcessMessage = "Completed successfully";
            dashboard.Save();
            thisProcess.SaveHistory(dashboard.JobHistoryID);
        }

        protected void ProcessInOrder()
        {

            XElement xelement = XElement.Load(_metaConfigFile);
            IEnumerable<XElement> configs;
           
            if (PostModelOnly)
            {
                configs = from configSettings in xelement.Elements()
                          where (string)configSettings.Elements().First().Attribute("ProcessWorkflow") == "PostModelRun"
                          select configSettings;
            }
            else
            {
                configs = from configSettings in xelement.Elements()
                          select configSettings;
            }

            foreach (var thisElement in configs)
            {
                string parentElementName = thisElement.Name.ToString();
                string parentElementNameAttribute = thisElement.Attribute("name").Value;

                foreach (var childElement in thisElement.Elements())
                {
                    string childElementName = childElement.Name.ToString();
                    var container = new UnityContainer();

                    switch (childElementName.ToLower())
                    {
                        case "cleardirectory":
                            container.RegisterType<IProcess, ClearDirectory>();
                            break;
                        case "copyfileprocess":
                            container.RegisterType<IProcess, CopyFileProcess>();
                            break;
                        case "dynamicdirectorycopyfileprocess":
                            container.RegisterType<IProcess, DynamicDirectoryCopyFileProcess>();
                            break;
                        case "excelcopypasteonetomanyprocess":
                            container.RegisterType<IProcess, ExcelCopyPasteOneToManyProcess>();
                            break;
                        case "excelcopypasterangeprocess":
                            container.RegisterType<IProcess, ExcelCopyPasteRangeProcess>();
                            break;
                        case "deletefile":
                            container.RegisterType<IProcess, DeleteFile>();
                            break;
                        case "emailprocess":
                            container.RegisterType<IProcess, EmailProcess>();
                            break;
                        case "filecheckandcopy":
                            container.RegisterType<IProcess, FileCheckAndCopy>();
                            break;
                        case "fileexistencechecker":            
                            container.RegisterType<IProcess, FileExistenceChecker>();
                            break;
                        case "createdynamicdirectoryandcopyfiles":
                            container.RegisterType<IProcess, CreateDynamicDirectoryAndCopyFiles>();
                            break;
                        case "createdynamicdirectoryprocess":
                            container.RegisterType<IProcess, CreateDynamicDirectoryProcess>();
                            break;
                        case "externalprocess":
                            container.RegisterType<IProcess, ExternalProcess>();
                            break;
                        case "filecopyoverridedefault":
                            container.RegisterType<IProcess, FileCopyOverrideDefault>();
                            break;
                        case "setalfaprocesstrigger":
                            container.RegisterType<IProcess, SetAlfaProcessTrigger>();
                            break;
                        case "svnupdate":
                            container.RegisterType<IProcess, SVNUpdate>();
                            break;
                        case "svncheckout":
                            container.RegisterType<IProcess, SVNCheckout>();
                            break;
                        case "svncommitconditional":
                            container.RegisterType<IProcess, SVNCommitConditional>();
                            break;
                        case "svncommit":
                            container.RegisterType<IProcess, SVNCommit>();
                            break;
                        case "systempause":
                            container.RegisterType<IProcess, SystemPause>();
                            break;
                        case "textreportconsolidation":
                            container.RegisterType<IProcess, TextReportConsolidation>();
                            break;
                        case "timedfilecheckprocess":
                            container.RegisterType<IProcess, TimedFileCheckProcess>();
                            break;

                        default:
                            ModelSpecificExtensions(childElementName.ToLower(), container);
                            break;
                    }
                    try
                    {
                        if(RunThisProcess(childElement))
                        {
                            ProcessJobType(container, childElement, parentElementNameAttribute);
                        }
                    }
                    catch(Exception eX)
                    {
                        eX.Data.Add("ParentElement", parentElementName);
                        eX.Data.Add("ParentElementNameAttribute", parentElementNameAttribute);
                        eX.Data.Add("Config File", _metaConfigFile);

                        throw;
                    }

                }
            }
        }

        private bool RunThisProcess(XElement childElement)
        {

            bool tempResult = true;

            if ((childElement.Attribute("PeriodSpecificQuarterList") !=null) &&
                    (childElement.Attribute("PeriodSpecificProcessTimeToMonthOffset") != null))
            {
                tempResult = DetermineWhetherToRun(childElement);
            }
            else
            {
                tempResult = true;
            }

            return tempResult;
        }

        private bool DetermineWhetherToRun(XElement childElement)
        {
            bool tempResult = false;
            int processTimeToMonthOffset = int.Parse(childElement.Attribute("PeriodSpecificProcessTimeToMonthOffset").Value.ToString());
            string periodSpecificQuarters = childElement.Attribute("PeriodSpecificQuarterList").Value.ToString();

            NameValueCollection processParams = new NameValueCollection();

            foreach (var keyValue in childElement.Elements())
            {
                processParams.Add(keyValue.Attribute("key").Value,
                    keyValue.Attribute("value").Value);
            }

            string Q1MonthMap = "1,2,3";
            string Q2MonthMap = "4,5,6";
            string Q3MonthMap = "7,8,9";
            string Q4MonthMap = "10,11,12";

            if (processParams["Q1MonthMap"] != null)
            {
                Q1MonthMap = processParams["Q1MonthMap"];
                Q2MonthMap = processParams["Q2MonthMap"];
                Q3MonthMap = processParams["Q3MonthMap"];
                Q4MonthMap = processParams["Q4MonthMap"];
            }

            DateTime monthToEvaluate = DateTime.Now;
            monthToEvaluate = monthToEvaluate.AddMonths(processTimeToMonthOffset);

            var ValidQuartersForProcess = periodSpecificQuarters.Split(',');

            foreach(var quarter in ValidQuartersForProcess)
            {
                int thisQuarter = int.Parse(quarter);

                switch (thisQuarter)
                {
                    case 1:
                        tempResult = EvaluateQuarterToMonthMap(Q1MonthMap, monthToEvaluate.Month);
                        break;

                    case 2:
                        tempResult = EvaluateQuarterToMonthMap(Q2MonthMap, monthToEvaluate.Month);
                        break;

                    case 3:
                        tempResult = EvaluateQuarterToMonthMap(Q3MonthMap, monthToEvaluate.Month);
                        break;

                    case 4:
                        tempResult = EvaluateQuarterToMonthMap(Q4MonthMap, monthToEvaluate.Month);
                        break;
                    default:
                        throw new Exception("PeriodSpecificQuarterList contains in invalid quarter");
                }

                if (tempResult)
                    break;
            }

            return tempResult;
        }

        private bool EvaluateQuarterToMonthMap(string monthMap, int monthToMatch)
        {
            var monthsString = monthMap.Split(',');
            bool tempResult = false;

            foreach (string thisMonthStr in monthsString)
            {
                int monthInt = int.Parse(thisMonthStr);

                if (monthToMatch == monthInt)
                {
                    tempResult = true;
                    break;
                }
            }
            return tempResult;
        }

        protected abstract void ModelSpecificExtensions(string processIdentifier, UnityContainer container);

        protected void LoadSingleParams(IProcess thisProcess, NameValueCollection processParams)
        {
            NameValueCollection refinedParams = new NameValueCollection();

            foreach (string key in processParams)
            {
                string settingValue = processParams[key];
                string keyUpper = key.ToUpper();
                //check if it's a configuration depenedent on environment
                if ((keyUpper.Contains("DEV_")) ||
                    (keyUpper.Contains("INT_")) ||
                    (keyUpper.Contains("PROD_")) ||
                    (keyUpper.Contains("TEST_")))
                {
                    string settingEnv = keyUpper.Split('_')[0];

                    if (settingEnv == _environment)
                    {
                        //strip out the environment declaration of the config key,
                        //as the process should not have any knowledge of environment
                        string newKey = key.Replace(settingEnv + "_", "");
                        AddToProcessParams(refinedParams, newKey, settingValue);
                    }
                }
                //must be a configuration item that is not dependent on 
                //environment, so add it
                else
                {
                    AddToProcessParams(refinedParams, key, settingValue);
                }
            }

            thisProcess.ProcessParams = refinedParams;
        }

        private static void AddToProcessParams(NameValueCollection theseParams, string key, string settingValue)
        {
            if (settingValue != null)
            {
                theseParams.Add(key, settingValue);
            }
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                _unityContainer.Dispose();
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }
    }

    public class AlfaProcessAggregator : ProcessAggregator
    {
        private readonly string _metaConfigSwitchLocation = string.Empty;
        private readonly string _metaConfigSwitchArchiveLocation = string.Empty;

        public AlfaProcessAggregator(Model model) : this(model, string.Empty) { }

        public AlfaProcessAggregator(Model model, string metaConfigSwitchLocation)
        {
            base.Model = model;
            if (!string.IsNullOrEmpty(metaConfigSwitchLocation))
            {
                _metaConfigSwitchLocation = metaConfigSwitchLocation.EndsWith(@"\") ? metaConfigSwitchLocation : metaConfigSwitchLocation + @"\";
                _metaConfigSwitchArchiveLocation += _metaConfigSwitchLocation + @"archive\";
            }
        }

        public void CheckForSwtich()
        {
            if(!string.IsNullOrEmpty(_metaConfigSwitchLocation))
            {
                if (!Directory.Exists(_metaConfigSwitchLocation))
                {
                    Directory.CreateDirectory(_metaConfigSwitchLocation);
                }

                if (!Directory.Exists(_metaConfigSwitchArchiveLocation))
                {
                    Directory.CreateDirectory(_metaConfigSwitchArchiveLocation);
                }

                string justFileName = Path.GetFileNameWithoutExtension(_metaConfigFile).Replace(".xml", ".txt");

                string justFileNameWithExtension = Path.GetFileName(_metaConfigFile).Replace(".xml", ".txt");

                string archiveFileName = string.Concat(
                     justFileName,
                     DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                     ".txt"
                    );

                string thisSwitchFile = _metaConfigSwitchLocation + justFileNameWithExtension;

                string thisSwitchFileArchive = _metaConfigSwitchArchiveLocation + archiveFileName;

                if (File.Exists(thisSwitchFile))
                {
                    File.Move(thisSwitchFile, thisSwitchFileArchive);
                    string line;
                    StreamReader file = new StreamReader(thisSwitchFileArchive);
                    while ((line = file.ReadLine()) != null)
                    {
                        _metaConfigFile = line;
                        break;
                    }
                    file.Close();
                }
            }
        }

        public override void RunAutomations()
        {
            if(string.IsNullOrEmpty(_metaConfigFile))
            {
                _metaConfigFile = Directory.GetCurrentDirectory() + @"\MetaProcess.xml";
            }

            ProcessInOrder();
        }

        protected override void ModelSpecificExtensions(string processIdentifier, UnityContainer container)
        {
            switch (processIdentifier)
            {
                case "mgalfaexternalprocess":
                    container.RegisterType<IProcess, MGAlfaExternalProcess>();
                    break;
                case "mgalfabasemodeldynamicdirectorycopyfileprocess":
                    container.RegisterType<IProcess, MGAlfaBaseModelDynamicDirectoryCopyFileProcess>();
                    break;
                default:
                    throw new Exception(string.Format("Process {0} is not defined", processIdentifier));                    
            }

        }

    }
}
