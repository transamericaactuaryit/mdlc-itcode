﻿using EnablingSystems.ProcessInterfaces;
using System;
using System.Collections.Specialized;
using System.Configuration;


namespace EnablingSystems.Processes
{
    public class ProcessLauncher 
    {
        private DateTime _processStartTime;
        private DateTime _processFinishTime;
        private IDashboard _dashboard;
        private IProcess _process;
        private bool _success;
        private string _environment = "DEV";
        //TODO: pass in job instance id
        //int _jobInstanceID = 1;

        public IDashboard Dashboard
        {
            set
            {
                _dashboard = value;
            }
        }

        public IProcess Process
        {
            set
            {
                _process = value;
            }
        }

        public string Environment
        {
            set
            {
                _environment = value;
            }
        }

        public void RunProcess()
        {
            try
            {
                _dashboard.ConnectionInformation = ConfigurationManager.AppSettings["DashboardConnection"];
                _dashboard.JobInstanceID =
                    int.Parse(ConfigurationManager.AppSettings["JobInstanceID"]);

                _processStartTime = DateTime.Now;
                _process.Finish += ProcessFinished;
                _process.JobHistory.ConnectionInformation = 
                    ConfigurationManager.AppSettings["JobHistoryConnection"];
                LoadParams(_process);
                _process.Execute();
            }
            catch (Exception ex)
            {

                //exceptionManager.HandleException(ex, "MainExceptionPolicy");
                _processFinishTime = DateTime.Now;
                _success = false;

                _dashboard.ProcessMessage = "Error: " + ex.Message;
                _dashboard.StartTime = _processStartTime;
                _dashboard.EndTime = _processFinishTime;
                _dashboard.Success = _success;
                //TODO: test if there is a database exception when saving to dashboard
                _dashboard.Save();
                _process.SaveHistory(_dashboard.JobHistoryID);
                //TODO: Should attempt to save params here, even if error?
                throw ex;
            }
        }
        /// <summary>
        /// If a configuration key starts with DEV_ INT_ or PROD_
        /// check if that value is appropriate to add based on the environment 
        /// (which is set in ProcessLauncherHost)
        /// Otherwise, if the configuration key does not start with DEV_ INT_ or PROD_,
        /// it is a param that is not dependent on the environment, so add it in
        /// </summary>
        /// <param name="process"></param>
        public virtual void LoadParams(IProcess process)
        {

            NameValueCollection theseParams = new NameValueCollection();

            foreach (string key in ConfigurationManager.AppSettings)
            {
                if (!key.Contains("Dashboard"))
                {
                    string settingValue = ConfigurationManager.AppSettings[key];

                    string keyUpper = key.ToUpper();
                    //check if it's a configuration depenedent 
                    //on environment
                    if ((keyUpper.Contains("DEV_")) ||
                        (keyUpper.Contains("INT_")) ||
                        (keyUpper.Contains("PROD_")))
                    {
                        string settingEnv = keyUpper.Split('_')[0];

                        if (settingEnv == _environment)
                        {
                            //strip out the environment declaration of the config key,
                            //as the process should not have any knowledge of environment
                            string newKey = key.Replace(settingEnv + "_", "");
                            AddToProcessParams(theseParams, newKey, settingValue);
                        }
                    }
                    //must be a configuration item that is not dependent on 
                    //environment, so add it
                    else
                    {
                        AddToProcessParams(theseParams, key, settingValue);
                    }
                    
                }
            }

            process.ProcessParams = theseParams;
        }

        private static void AddToProcessParams(NameValueCollection theseParams, string key, string settingValue)
        {
            if (settingValue != null)
            {
                theseParams.Add(key, settingValue);
            }
        }

        public virtual void ProcessFinished(object sender, EventArgs e)
        {
            _processFinishTime = DateTime.Now;
            _success = true;

            _dashboard.ProcessMessage = "Completed successfully";
            _dashboard.StartTime = _processStartTime;
            _dashboard.EndTime = _processFinishTime;
            _dashboard.Success = _success;
            _dashboard.Save();
            _process.SaveHistory(_dashboard.JobHistoryID);

        }
    }

    public class JobParams
    {
        public string ParamName { get; set; }
        public string ParamValue { get; set; }

        public int JobInstanceID { get; set; }

        //public string ConcreteProcess { get; set; }

    }
}
