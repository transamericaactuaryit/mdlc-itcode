﻿using DataAccess;
using Domain;
using EnablingSystems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace IntegrationTest
{
    [TestClass]
    public class ModelManagerHttpClientTest
    {
        private Uri baseWebAddress;

        [TestInitialize]
        public void Init()
        {
            //To run this test you need to deploy the ModelManagerWeb project locally
            //or point to the dev server if it has the same version you are testing. SJO
            baseWebAddress = new Uri("http://localhost:8020");
        }

        [TestMethod]
        public void CanGetInitialModelLookups()
        {
            //this test returns data that was seeded on DB creation.    
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var initialModelLookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "Base", "Fair Value").Result;

            Assert.AreEqual("ALFA", initialModelLookups.ModelApplication.Name);
            Assert.AreEqual("Base", initialModelLookups.ModelType.Name);
            Assert.AreEqual("Not Started", initialModelLookups.ModelStatus.Status);

        }

        [TestMethod]
        public void CanGetModelRequest()
        {
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var model1Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "Base", "Fair Value").Result;
            var model2Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "BOPParallel", "Fair Value").Result;

            var createdDate = DateTime.Now;
            var modelRequest = new ModelRequest
            {
                Name = "Test",
                CreatedBy = "Simon Ovens",
                CreatedDate = createdDate,
                ModelRequestStatusID = 1,
                Models = new List<Model>
                {
                    new Model
                    {
                        Name = "RateAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model1Lookups.ModelApplication.ID,
                        ModelStatusID = model1Lookups.ModelStatus.ID,
                        ModelTypeID = model1Lookups.ModelType.ID
                    },
                    new Model
                    {
                        Name = "CrossAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model2Lookups.ModelApplication.ID,
                        ModelStatusID = model2Lookups.ModelStatus.ID,
                        ModelTypeID = model2Lookups.ModelType.ID
                    }
                }
            };

            modelRequest = modelManagerHttpClient.AddModelRequest(modelRequest).Result;

            var newModelRequest = modelManagerHttpClient.GetModelRequest(modelRequest.ID).Result;

            Assert.IsTrue(newModelRequest.ID > 0);
            Assert.AreEqual("Test", newModelRequest.Name);
            Assert.AreEqual("Simon Ovens", newModelRequest.CreatedBy);
            Assert.AreEqual(createdDate.ToString(), newModelRequest.CreatedDate.ToString());
            Assert.AreEqual(1, newModelRequest.ModelRequestStatusID);
            Assert.AreEqual(2, newModelRequest.Models.Count);

            TestHelper.RemoveTestModelRequest(modelRequest.ID);
        }

        [TestMethod]
        public void CanGetModel()
        {
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var model1Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "Base", "Fair Value").Result;
            var model2Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "BOPParallel", "Fair Value").Result;

            var createdDate = DateTime.Now;
            var modelRequest = new ModelRequest
            {
                Name = "Test",
                CreatedBy = "Simon Ovens",
                CreatedDate = createdDate,
                ModelRequestStatusID = 1,
                Models = new List<Model>
                {
                    new Model
                    {
                        Name = "RateAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model1Lookups.ModelApplication.ID,
                        ModelStatusID = model1Lookups.ModelStatus.ID,
                        ModelTypeID = model1Lookups.ModelType.ID
                    },
                    new Model
                    {
                        Name = "CrossAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model2Lookups.ModelApplication.ID,
                        ModelStatusID = model2Lookups.ModelStatus.ID,
                        ModelTypeID = model2Lookups.ModelType.ID
                    }
                }
            };

            modelRequest = modelManagerHttpClient.AddModelRequest(modelRequest).Result;

            var testModel = modelRequest.Models.First();

            var results = modelManagerHttpClient.GetModel(testModel.ID).Result;
            
            Assert.AreEqual("RateAttribution", results.Name);

            TestHelper.RemoveTestModelRequest(modelRequest.ID);
        }

        [TestMethod]
        public void CanAddModelRequest()
        {
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var model1Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "Base", "Fair Value").Result;
            var model2Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "BOPParallel", "Fair Value").Result;

            var createdDate = DateTime.Now;
            var modelRequest = new ModelRequest
            {
                Name = "Test",
                CreatedBy = "Simon Ovens",
                CreatedDate = createdDate,
                ModelRequestStatusID = 1,
                Models = new List<Model>
                {
                    new Model
                    {
                        Name = "RateAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model1Lookups.ModelApplication.ID,
                        ModelStatusID = model1Lookups.ModelStatus.ID,
                        ModelTypeID = model1Lookups.ModelType.ID
                    },
                    new Model
                    {
                        Name = "CrossAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model2Lookups.ModelApplication.ID,
                        ModelStatusID = model2Lookups.ModelStatus.ID,
                        ModelTypeID = model2Lookups.ModelType.ID
                    }
                }
            };

            modelRequest = modelManagerHttpClient.AddModelRequest(modelRequest).Result;

            Assert.IsTrue(modelRequest.ID > 0);
            Assert.AreEqual("Test", modelRequest.Name);
            Assert.AreEqual("Simon Ovens", modelRequest.CreatedBy);
            Assert.AreEqual(createdDate, modelRequest.CreatedDate);
            Assert.AreEqual(1, modelRequest.ModelRequestStatusID);
            Assert.AreEqual(2, modelRequest.Models.Count);

            TestHelper.RemoveTestModelRequest(modelRequest.ID);
        }

        [TestMethod]
        public void CanGetModelsByModelRequestID()
        {
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var model1Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "Base", "Fair Value").Result;
            var model2Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "BOPParallel", "Fair Value").Result;

            var createdDate = DateTime.Now;
            var modelRequest = new ModelRequest
            {
                Name = "Test",
                CreatedBy = "Simon Ovens",
                CreatedDate = createdDate,
                ModelRequestStatusID = 1,
                Models = new List<Model>
                {
                    new Model
                    {
                        Name = "RateAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model1Lookups.ModelApplication.ID,
                        ModelStatusID = model1Lookups.ModelStatus.ID,
                        ModelTypeID = model1Lookups.ModelType.ID
                    },
                    new Model
                    {
                        Name = "CrossAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model2Lookups.ModelApplication.ID,
                        ModelStatusID = model2Lookups.ModelStatus.ID,
                        ModelTypeID = model2Lookups.ModelType.ID
                    }
                }
            };

            modelRequest = modelManagerHttpClient.AddModelRequest(modelRequest).Result;

            //Act
            var models = modelManagerHttpClient.GetModelsByModelRequestId(modelRequest.ID).Result;

            Assert.AreEqual(2, models.Count());
            Assert.AreEqual("RateAttribution", models.First().Name);

            TestHelper.RemoveTestModelRequest(modelRequest.ID);
        }

        [TestMethod]
        public void CanUpdateModelStatus()
        {
            //Setup test data.
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var model1Lookups = modelManagerHttpClient.GetInitialModelLookups("ALFA", "Base", "Fair Value").Result;

            var createdDate = DateTime.Now;
            var modelStatusID = model1Lookups.ModelStatus.ID;
            var modelRequest = new ModelRequest
            {
                Name = "Test",
                CreatedBy = "Simon Ovens",
                CreatedDate = createdDate,
                ModelRequestStatusID = 1,
                Models = new List<Model>
                {
                    new Model
                    {
                        Name = "RateAttribution",
                        CreatedDate = DateTime.Now,
                        Priority = 10,
                        ModelApplicationID = model1Lookups.ModelApplication.ID,
                        ModelStatusID = modelStatusID,
                        ModelTypeID = model1Lookups.ModelType.ID
                    }
                }
            };

            modelRequest = modelManagerHttpClient.AddModelRequest(modelRequest).Result;

            //Act
            var model = modelRequest.Models.First();
            var completedDate = DateTime.Now;
            model.CompletedTime = completedDate;
            model.ModelStatusID = (int)ModelStatusEnums.Complete;
            var results = modelManagerHttpClient.UpdateModel(model).Result;

            Assert.AreEqual(completedDate, results.CompletedTime);
            Assert.AreEqual((int)ModelStatusEnums.Complete, results.ModelStatusID);

            TestHelper.RemoveTestModelRequest(modelRequest.ID);
        }

        [TestMethod]
        public void CanUpdateModelRequestsStatusByStatus()
        {
            //Setup test data.
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);

            var modelRequests = new List<ModelRequest> {
                new ModelRequest
                {
                    Name = "Test",
                    CreatedBy = "Simon Ovens",
                    CreatedDate = DateTime.Now,
                    ModelRequestStatusID = (int)ModelRequestStatusEnums.NotStarted
                },
                new ModelRequest
                {
                    Name = "Test2",
                    CreatedBy = "Simon Ovens",
                    CreatedDate = DateTime.Now,
                    ModelRequestStatusID = (int)ModelRequestStatusEnums.NotStarted
                },
                new ModelRequest
                {
                    Name = "Test3",
                    CreatedBy = "Simon Ovens",
                    CreatedDate = DateTime.Now,
                    ModelRequestStatusID = (int)ModelRequestStatusEnums.NotStarted
                },
            };

            var updatedModelRequests = new List<ModelRequest>();
            foreach (var modelRequest in modelRequests)
            {
                updatedModelRequests.Add(modelManagerHttpClient.AddModelRequest(modelRequest).Result);
            }

            //Act
            var results = modelManagerHttpClient.UpdateModelRequestsStatusByStatus(ModelRequestStatusEnums.NotStarted, ModelRequestStatusEnums.InQueue).Result;

            Assert.IsTrue(results);

            foreach(var updatedModelRequest in updatedModelRequests)
            {
                var modelRequest = modelManagerHttpClient.GetModelRequest(updatedModelRequest.ID).Result;
                Assert.AreEqual((int)ModelRequestStatusEnums.InQueue, modelRequest.ModelRequestStatusID);
            }

            foreach (var updatedModelRequest in updatedModelRequests)
            {
                TestHelper.RemoveTestModelRequest(updatedModelRequest.ID);
            }
        }
    }
}

