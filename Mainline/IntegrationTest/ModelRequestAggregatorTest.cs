﻿using DataAccess;
using Domain;
using EnablingSystems;
using EnablingSystems.Processes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IntegrationTest
{
    [TestClass]
    public class ModelRequestAggregatorTest
    {
        private Uri baseWebAddress;
        private ModelRequest modelRequest;

        [TestInitialize]
        public void Init()
        {
            //To run this test you need to deploy the ModelManagerWeb project locally
            //or point to the dev server if it has the same version you are testing. SJO
            baseWebAddress = new Uri("http://localhost:8020");
        }

        [TestMethod]
        public void CanLoadDatabaseWithModelRun()
        {
            var modelRequestLocation = "..\\..\\TestFixtures\\Configurations\\ModelRequest.xml";
            //below is used for test assertions as config could be changed.
            XElement xelement = XElement.Load(modelRequestLocation);
            var configs = from configSettings in xelement.Elements()
                          select configSettings;

            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            modelRequest = new ModelRequestAggregator(modelRequestLocation, modelManagerHttpClient).Process();

            Assert.AreEqual(xelement.Elements("model").Count(), modelRequest.Models.Count, "check the modelRequestLocation config file above.");
            Assert.AreEqual(999, modelRequest.Models.First().Priority);
            Assert.AreEqual(xelement.Elements("model").First().Value, modelRequest.Models.First().Name);
            //Class archives file so ensure the original is no longer there. The cleanup will revert this change for next test.
            Assert.IsFalse(File.Exists(modelRequestLocation));
        }

        [TestCleanup]
        public void CleanUp()
        {
            //Change back or test fixture file
            RevertBackConfigFilenames();
            //Remove test object after Assertions.
            //Using service as we do not want to expose delete via web api
            if (modelRequest != null)
            {
                var uow = new UnitOfWork(new ModelManagerDbContext());
                var modelRequestService = new ModelRequestService(uow).Delete(modelRequest.ID);
            }
        }


        private static void RevertBackConfigFilenames()
        {
            //Change back or test fixture file
            var files = Directory.GetFiles("..\\..\\TestFixtures\\Configurations\\");
            foreach (var file in files)
            {
                if (file.Contains("ModelRequest"))
                    File.Move(file, "..\\..\\TestFixtures\\Configurations\\ModelRequest.xml");
                else if (file.Contains("Model2Request"))
                    File.Move(file, "..\\..\\TestFixtures\\Configurations\\Model2Request.xml");
            }
        }
    }
}
