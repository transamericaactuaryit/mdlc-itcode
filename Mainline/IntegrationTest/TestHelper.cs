﻿using DataAccess;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationTest
{
    public static class TestHelper
    {
        public static void RemoveTestModelRequest(int id)
        {
            //Remove test object after Assertions.
            //Using service as we do not want to expose delete via web api
            var uow = new UnitOfWork(new ModelManagerDbContext());
            new ModelRequestService(uow).Delete(id);
        }
    }
}
