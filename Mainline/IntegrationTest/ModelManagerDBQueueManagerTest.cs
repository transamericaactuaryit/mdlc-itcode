﻿using EnablingSystems;
using EnablingSystems.Processes;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace IntegrationTest
{
    [TestClass]
    public class ModelManagerDBQueueManagerTest
    {

        private Uri baseWebAddress;
        private LogSource _logSource;
        private ExceptionManager _exceptionManager;
        private string _mainExceptionPolicy = "MainExceptionPolicy";

        [TestInitialize]
        public void Init()
        {
            //To run this test you need to deploy the ModelManagerWeb project locally
            //or point to the dev server if it has the same version you are testing. SJO
            baseWebAddress = new Uri("http://localhost:8020");

            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);
            LogSource logSource;
            Logger.Writer.TraceSources.TryGetValue("Exceptions", out logSource);

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            _exceptionManager = exceptionFactory.CreateManager();
        }

        [TestMethod]
        public void WillProcessMutlipleRequests()
        {
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var modelRequestLocation = "..\\..\\TestFixtures\\Configurations\\ModelRequest.xml";
            new ModelRequestAggregator(modelRequestLocation, modelManagerHttpClient).Process();
            var modelRequestLocation2 = "..\\..\\TestFixtures\\Configurations\\Model2Request.xml";
            new ModelRequestAggregator(modelRequestLocation2, modelManagerHttpClient).Process();

            var queueManager = new ModelManagerDBQueueManager(modelManagerHttpClient,
                                        _exceptionManager,
                                        _logSource,
                                        _mainExceptionPolicy,
                                        ConfigurationManager.AppSettings["Environment"],
                                        int.Parse(ConfigurationManager.AppSettings["MaxConcurrent"]),
                                        ConfigurationManager.AppSettings["OnDemandWorkArea"]);
            queueManager.Process()
                        .Wait(240000); //call wait and timeout as the class is designed to run continuously

            RemoveTriggerFiles();
            RevertBackConfigFilenames();
        }

        [TestMethod]
        public void WillProcessNewItemsInQueue()
        {
            var modelManagerHttpClient = new ModelManagerHttpClient(new HttpClient(), baseWebAddress);
            var modelRequestLocation = "..\\..\\TestFixtures\\Configurations\\ModelRequest.xml";
            new ModelRequestAggregator(modelRequestLocation, modelManagerHttpClient).Process();

            //create new items in DB after the queue is running the first batch
            Task.Run(() => {
                Task.Delay(60000);
                var modelRequestLocation2 = "..\\..\\TestFixtures\\Configurations\\Model2Request.xml";
                new ModelRequestAggregator(modelRequestLocation2, modelManagerHttpClient).Process();
            });

            var queueManager = new ModelManagerDBQueueManager(modelManagerHttpClient,
                                        _exceptionManager,
                                        _logSource,
                                        _mainExceptionPolicy,
                                        ConfigurationManager.AppSettings["Environment"],
                                        int.Parse(ConfigurationManager.AppSettings["MaxConcurrent"]),
                                        ConfigurationManager.AppSettings["OnDemandWorkArea"]);

            queueManager.Process()
                        .Wait(240000); //call wait and timeout as the class is designed to run continuously

            RemoveTriggerFiles();
            RevertBackConfigFilenames();
        }

        private void RemoveTriggerFiles()
        {
            //remove test trigger files
            var triggerFilesLocation = "..\\..\\TestFixtures\\MGALFA\\AlfaRunTriggerFiles";
            var triggerFiles = Directory.GetFiles(triggerFilesLocation);
            foreach (var file in triggerFiles)
            {
                File.Delete(file);
            }
        }

        private static void RevertBackConfigFilenames()
        {
            //Change back or test fixture file
            var files = Directory.GetFiles("..\\..\\TestFixtures\\Configurations\\");
            foreach (var file in files)
            {
                if (file.Contains("ModelRequest"))
                    File.Move(file, "..\\..\\TestFixtures\\Configurations\\ModelRequest.xml");
                else if(file.Contains("Model2Request"))
                    File.Move(file, "..\\..\\TestFixtures\\Configurations\\Model2Request.xml");
            }
        }
    }
}
