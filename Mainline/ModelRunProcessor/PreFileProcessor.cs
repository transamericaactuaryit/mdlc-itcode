﻿using EnablingSystems.ProcessInterfaces;

namespace EnablingSystems.Processes
{
    public class PreFileProcessor : IPreFileProcessor
    {
        private readonly IModelRequestAggregator _modelRunAggregator;
        private readonly IAutomationAggregator _automationAggregator;

        public PreFileProcessor(IModelRunAggregator modelRunAggregator, IAutomationAggregator automationAggregator)
        {
            _modelRunAggregator = modelRunAggregator;
            _automationAggregator = automationAggregator;
        }

        public void Process()
        {
            var modelRun = _modelRunAggregator.Get();

            foreach (var modelRuns in modelRun.ModelRuns)
            {
                _automationAggregator.MetaConfigFile = modelRuns.PreConfigLocation;
                _automationAggregator.Environment = modelRun.Environment;
                _automationAggregator.SetupAutomations();
                _automationAggregator.RunAutomations();
            }
        }
    }
}
