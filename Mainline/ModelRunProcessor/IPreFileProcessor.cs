﻿namespace EnablingSystems.Processes
{
    public interface IPreFileProcessor
    {
        void Process();
    }
}