﻿using System;
using EnablingSystems.Processes;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace AggregatorHost
{
    class Program
    {
        static void Main(string[] args)
        {            
            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);

            ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
            ExceptionManager exceptionManager = exceptionFactory.CreateManager();

            try
            {
                string environment = DetermineEnvironment(args);
                string metaConfig = DetermineConfig(args);
                string switchLocation = ConfigurationManager.AppSettings["MetaConfigSwitchLocation"] ?? @"C:\MgAlfa\OnDemandSwitchFiles";
                AlfaProcessAggregator alfaProcessAggregator = new AlfaProcessAggregator(switchLocation);

                alfaProcessAggregator.MetaConfigFile = metaConfig;
                alfaProcessAggregator.CheckForSwtich();
                alfaProcessAggregator.Environment = environment;
                alfaProcessAggregator.SetupAutomations();
                alfaProcessAggregator.RunAutomations();
            }
            catch (Exception ex)
            {
                exceptionManager.HandleException(ex, "MainExceptionPolicy");

                //Anything other than 0 will tell scheduler that something went wrong
                Environment.Exit(-1);
            }

            Environment.Exit(0);
        }

        private static string DetermineEnvironment(string[] args)
        {
            string env = "DEV";
            if (args.Length > 0)
            {
                env = args[0].ToUpperInvariant().Trim();

                if ((env == "PROD") ||
                    (env == "INT") ||
                    (env == "DEV") ||
                    (env == "TEST"))
                {
                    return env;
                }
            }
            return env;
        }

        private static string DetermineConfig(string[] args)
        {
            string config = ConfigurationManager.AppSettings["MetaConfigFile"] ?? "";
            //must have an argument for environment
            //and another for config
            if(string.IsNullOrEmpty(config))
            {
                if(args.Length < 2)
                {
                    throw new MissingFieldException("MetaConfigFile is not set in configuration nor arguments");                    
                }
                config = args[1].Trim();                
            }

            return config;
        }
    }
}
