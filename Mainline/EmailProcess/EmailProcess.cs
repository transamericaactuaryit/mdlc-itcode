﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnablingSystems.Utilities;
using EnablingSystems.ProcessInterfaces;
using Microsoft.Practices.EnterpriseLibrary.Logging;
//using EnablingSystems.MDLC.Processes;

namespace EnablingSystems.MDLC.Processes
{
    //Though this may be a fairly common process, 
    //Separated into it's own assembly because of the dependency
    //on TAEmailer
    public class EmailProcess : ProcessBaseWithHistory
    {
        public EmailProcess(LogWriter logWriter) : base(logWriter)
        {
        }

        public override void Execute()
        {
            string toAddress = _processParams["ToAddress"];
            string fromAddress = _processParams["FromAddress"];
            string smtpAddress = _processParams["SmtpServer"];
            string body = _processParams["Body"];
            string ccAddress = _processParams["CcAddress"] ?? "";
            string subject = _processParams["Subject"] ?? "";
            bool isHtml = bool.Parse(_processParams["IsHtml"] ?? "false");

            TAEmailer emailer = new TAEmailer(toAddress, fromAddress, ccAddress, 
                subject, body, isHtml, smtpAddress);

            emailer.ToAddress = toAddress;
            emailer.FromAddress = fromAddress;
            emailer.SmtpServer = smtpAddress;
            emailer.Body = body;
            emailer.CcAddress = ccAddress;
            emailer.Subject = subject;
            emailer.IsHtml = isHtml;

            var attachments = _processParams.AllKeys
                .Where(key => key.ToLower().StartsWith("attachment"))
                .Select(key => _processParams[key]);

            foreach (string attachment in attachments)
            {
                emailer.Attachments.Add(attachment);
            }

            emailer.SendEmail();

        }
    }
}
