namespace ModelDashboardAndHistory
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JobHistoryValues
    {
        public int JobHistoryID { get; set; }

        public int JobHistoryValuesID { get; set; }

        [Required]
        [StringLength(255)]
        public string PropertyName { get; set; }

        [Required]
        [StringLength(255)]
        public string PropertyValue { get; set; }

        public virtual JobHistory JobHistory { get; set; }
    }
}
