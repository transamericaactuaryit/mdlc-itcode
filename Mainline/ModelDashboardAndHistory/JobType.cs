namespace ModelDashboardAndHistory
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobType")]
    public partial class JobType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobType()
        {
            JobInstance = new HashSet<JobInstance>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobTypeId { get; set; }

        [StringLength(255)]
        public string SystemType { get; set; }

        [Required]
        [StringLength(50)]
        public string JobTypeName { get; set; }

        [Required]
        [StringLength(50)]
        public string JobTypeDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobInstance> JobInstance { get; set; }
    }
}
