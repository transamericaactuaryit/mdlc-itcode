namespace ModelDashboardAndHistory
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MDLC_Context : DbContext
    {
        public MDLC_Context()
            : base("name=MDLC_Context")
        {
        }

        public virtual DbSet<JobHistory> JobHistory { get; set; }
        public virtual DbSet<JobHistoryValues> JobHistoryValues { get; set; }
        public virtual DbSet<JobInstance> JobInstance { get; set; }
        public virtual DbSet<JobType> JobType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JobHistory>()
                .Property(e => e.WhoSubmitted)
                .IsUnicode(false);

            modelBuilder.Entity<JobHistory>()
                .HasMany(e => e.JobHistoryValues)
                .WithRequired(e => e.JobHistory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobHistoryValues>()
                .Property(e => e.PropertyName)
                .IsUnicode(false);

            modelBuilder.Entity<JobHistoryValues>()
                .Property(e => e.PropertyValue)
                .IsUnicode(false);

            modelBuilder.Entity<JobInstance>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobInstance>()
                .HasMany(e => e.JobHistory)
                .WithRequired(e => e.JobInstance)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobType>()
                .Property(e => e.SystemType)
                .IsUnicode(false);

            modelBuilder.Entity<JobType>()
                .Property(e => e.JobTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<JobType>()
                .Property(e => e.JobTypeDescription)
                .IsUnicode(false);

            modelBuilder.Entity<JobType>()
                .HasMany(e => e.JobInstance)
                .WithRequired(e => e.JobType)
                .WillCascadeOnDelete(false);
        }
    }
}
