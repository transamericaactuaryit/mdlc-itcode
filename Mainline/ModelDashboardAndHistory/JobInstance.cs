namespace ModelDashboardAndHistory
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobInstance")]
    public partial class JobInstance
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobInstance()
        {
            JobHistory = new HashSet<JobHistory>();
        }

        public int JobInstanceID { get; set; }

        public int JobTypeID { get; set; }

        public int ModelBusinessUnit { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobHistory> JobHistory { get; set; }

        public virtual JobType JobType { get; set; }
    }
}
