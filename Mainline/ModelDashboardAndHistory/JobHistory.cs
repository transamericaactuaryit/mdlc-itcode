namespace ModelDashboardAndHistory
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobHistory")]
    public partial class JobHistory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobHistory()
        {
            JobHistoryValues = new HashSet<JobHistoryValues>();
        }

        public int JobHistoryID { get; set; }

        public int JobInstanceID { get; set; }

        [Required]
        [StringLength(255)]
        public string WhoSubmitted { get; set; }

        public DateTime DateRan { get; set; }

        public virtual JobInstance JobInstance { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobHistoryValues> JobHistoryValues { get; set; }
    }
}
