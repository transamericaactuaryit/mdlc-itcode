﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Specialized;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using EnablingSystems.ProcessInterfaces;

namespace EnablingSystems.MDLC.Processes
{
    public class ExternalProcess : ProcessBaseWithHistory
    {
        private NameValueCollection _processParams;

        public ExternalProcess(LogWriter logWriter) : base(logWriter)
        {
        }

        public ExternalProcess() : base()
        {

        }

        //public NameValueCollection ProcessParams
        //{
        //    set
        //    {
        //        _processParams = value;
        //    }
        //}

        public event EventHandler Finish;

        public override void Execute()
        {
            bool useShellExecute = false;
            string processArgs = "";
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;

            string processLocation = _processParams["ProcessLocation"];
            string useShellExecuteParam = _processParams["UseShellExecute"];
            processArgs = _processParams["Arguments"];

            if (processLocation == null)
            {
                throw new Exception("ProcessLocation did not exist in parameter collection");
            }

            if (useShellExecuteParam != null)
            {
                useShellExecute = bool.Parse(useShellExecuteParam);
            }

            startInfo.FileName = processLocation;
            startInfo.UseShellExecute = useShellExecute;
            startInfo.Arguments = processArgs;

            string standardOutput = "";
            string errorOutput = "";

            using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
            {
                //exeProcess.Start();
                standardOutput = exeProcess.StandardOutput.ReadToEnd();
                errorOutput = exeProcess.StandardError.ReadToEnd();
                exeProcess.WaitForExit();

            }

            if (errorOutput.Length > 0)
            {
                throw new Exception(errorOutput);
            }

            Finish?.Invoke(this, EventArgs.Empty);
        }

    }
}
