﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelRequestStatusRepository : IModelRequestStatusRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelRequestStatusRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelRequestStatus Get(int id)
        {
            try
            {
                return _context.ModelRequestStatus.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Request Status With ID {id}");
            }
        }
        public IEnumerable<ModelRequestStatus> Get()
        {
            return _context.ModelRequestStatus;
        }

        public ModelRequestStatus Add(ModelRequestStatus ModelRequestStatus)
        {
            if (ModelRequestStatus == null || string.IsNullOrEmpty(ModelRequestStatus.Status))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelRequestStatus.Add(ModelRequestStatus);
        }

        public ModelRequestStatus Delete(ModelRequestStatus ModelRequestStatus)
        {
            if (ModelRequestStatus == null || ModelRequestStatus.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelRequestStatus.Remove(ModelRequestStatus);
        }
    }
}
