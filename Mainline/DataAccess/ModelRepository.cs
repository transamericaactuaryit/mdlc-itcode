﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DataAccess
{
    public class ModelRepository : IModelRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public Model Get(int id)
        {
            try
            {
                return _context.Model.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model With ID {id}");
            }
        }

        public Model GetLatestByModelTypeId(int modelTypeId)
        {
            return _context.Model.OrderByDescending(m => m.CreatedDate)
                                        .FirstOrDefault(m => m.ModelTypeID == modelTypeId);
        }

        public IEnumerable<Model> GetAllByModelRequestId(int id)
        {
            if(!_context.Model.Any(m => m.ModelRequestID == id))
            {
                throw new Exception($"No Models Found With Model Request ID {id}");
            }

             return _context.Model.Where(m => m.ModelRequestID == id);
        }

        public Model Add(Model model)
        {
            if (model == null || string.IsNullOrEmpty(model.Name))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.Model.Add(model);
        }

        public Model Delete(Model model)
        {
            if (model == null || model.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.Model.Remove(model);
        }

        public Model Update(Model model)
        {
            _context.Entry(model).State = System.Data.Entity.EntityState.Modified;

            return model;
        }

        public Model UpdateStatus(int id, ModelStatusEnums status, DateTime? timeStamp)
        {
            var model = _context.Model.Single(m => m.ID == id);
            model.ModelStatusID = (int)status;
            if(timeStamp != null)
            {
                if (status == ModelStatusEnums.Executing)
                    model.StartTime = timeStamp;
                else if (status == ModelStatusEnums.Complete || status == ModelStatusEnums.Error)
                    model.CompletedTime = timeStamp;
            }

            return model;
        }

        public IEnumerable<Model> GetUnprocessed()
        {
            _context.ModelRequest.Load();
            _context.ModelType.Load(); 
            _context.ModelGroup.Load();
            return _context.Model.Where(m => m.ModelRequest.ModelRequestStatusID == (int)ModelRequestStatusEnums.InQueue && 
                                              m.ModelStatusID == (int)ModelStatusEnums.NotStarted)
                                    .OrderBy(m => m.Priority)
                                    .ThenBy(m => m.ModelType.Score)
                                    .ThenBy(m => m.CreatedDate);
        }

    }
}
