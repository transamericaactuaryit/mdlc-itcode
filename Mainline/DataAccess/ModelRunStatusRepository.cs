﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelRunStatusRepository : IModelRunStatusRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelRunStatusRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelRunStatus Get(int id)
        {
            try
            {
                return _context.ModelRunStatus.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Run Status With ID {id}");
            }
        }
        public IEnumerable<ModelRunStatus> Get()
        {
            return _context.ModelRunStatus;
        }

        public ModelRunStatus Add(ModelRunStatus modelRunStatus)
        {
            if (modelRunStatus == null || string.IsNullOrEmpty(modelRunStatus.Status))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelRunStatus.Add(modelRunStatus);
        }

        public ModelRunStatus Delete(ModelRunStatus modelRunStatus)
        {
            if (modelRunStatus == null || modelRunStatus.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelRunStatus.Remove(modelRunStatus);
        }
    }
}
