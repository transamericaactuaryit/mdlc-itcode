﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelGroupRepository : IModelGroupRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelGroupRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelGroup Get(int id)
        {
            try
            {
                return _context.ModelGroup.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Group With ID {id}");
            }
        }

        public IEnumerable<ModelGroup> Get()
        {
            return _context.ModelGroup;
        }

        public ModelGroup Add(ModelGroup modelGroup)
        {
            if (modelGroup == null || string.IsNullOrEmpty(modelGroup.Name))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelGroup.Add(modelGroup);
        }

        public ModelGroup Delete(ModelGroup modelGroup)
        {
            if (modelGroup == null || modelGroup.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelGroup.Remove(modelGroup);
        }
    }
}
