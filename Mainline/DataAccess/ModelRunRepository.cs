﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelRunRepository : IModelRunRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelRunRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelRun Get(int id)
        {
            try
            {
                return _context.ModelRun.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Run Status With ID {id}");
            }
        }
        public IEnumerable<ModelRun> Get()
        {
            return _context.ModelRun;
        }

        public ModelRun Add(ModelRun ModelRun)
        {
            if (ModelRun == null || string.IsNullOrEmpty(ModelRun.Run))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelRun.Add(ModelRun);
        }

        public ModelRun Delete(ModelRun ModelRun)
        {
            if (ModelRun == null || ModelRun.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelRun.Remove(ModelRun);
        }
    }
}
