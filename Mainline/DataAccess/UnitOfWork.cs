﻿using DataAccess.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ModelManagerDbContext _context;

        public IModelRequestRepository ModelRequestRepository =>
        new ModelRequestRepository(_context);

        public IModelRequestStatusRepository ModelRequestStatusRepository =>
            new ModelRequestStatusRepository(_context);

        public IModelRepository ModelRepository =>
            new ModelRepository(_context);

        public IModelStatusRepository ModelStatusRepository =>
            new ModelStatusRepository(_context);

        public IModelApplicationRepository ModelApplicationRepository =>
            new ModelApplicationRepository(_context);

        public IModelGroupRepository ModelGroupRepository =>
            new ModelGroupRepository(_context);

        public IModelTypeRepository ModelTypeRepository =>
            new ModelTypeRepository(_context);

        public IModelRunRepository ModelRunRepository =>
            new ModelRunRepository(_context);

        public IModelRunStatusRepository ModelRunStatusRepository =>
            new ModelRunStatusRepository(_context);

        public UnitOfWork(ModelManagerDbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
        public void RejectChanges()
        {
            foreach (var entry in _context.ChangeTracker.Entries()
                  .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
