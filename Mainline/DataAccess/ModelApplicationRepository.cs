﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelApplicationRepository : IModelApplicationRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelApplicationRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelApplication Get(int id)
        {
            try
            {
                return _context.ModelApplication.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Application With ID {id}");
            }
        }

        public ModelApplication Get(string name)
        {
            try
            {
                return _context.ModelApplication.Single(m => m.Name == name);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Application With Name {name}");
            }
        }

        public IEnumerable<ModelApplication> Get()
        {
            return _context.ModelApplication;
        }

        public ModelApplication Add(ModelApplication modelApplication)
        {
            if (modelApplication == null || string.IsNullOrEmpty(modelApplication.Name))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelApplication.Add(modelApplication);
        }

        public ModelApplication Delete(ModelApplication modelApplication)
        {
            if (modelApplication == null || modelApplication.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelApplication.Remove(modelApplication);
        }
    }
}
