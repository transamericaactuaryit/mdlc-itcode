﻿using DataAccess.Migrations;
using Domain;
using System.Data.Entity;

namespace DataAccess
{
    public class ModelManagerDbContext : DbContext
    {
        public ModelManagerDbContext() : base("name=ModelManagerDBConnectionString")
        {
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ModelManagerDbContext, Configuration>());
        }

        //Marked virtual so that Moq can override them.

        public virtual DbSet<ModelRequest> ModelRequest { get; set; }
        public virtual DbSet<ModelRequestStatus> ModelRequestStatus { get; set; }
        public virtual DbSet<Model> Model { get; set; }
        public virtual DbSet<ModelApplication> ModelApplication { get; set; }
        public virtual DbSet<ModelStatus> ModelStatus { get; set; }
        public virtual DbSet<ModelGroup> ModelGroup { get; set; }
        public virtual DbSet<ModelType> ModelType { get; set; }
        public virtual DbSet<ModelRun> ModelRun { get; set; }
        public virtual DbSet<ModelRunStatus> ModelRunStatus { get; set; }
    }
}
