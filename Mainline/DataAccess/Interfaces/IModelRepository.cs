﻿using System;
using System.Collections.Generic;
using Domain;

namespace DataAccess.Interfaces
{
    public interface IModelRepository
    {
        Model Get(int id);
        Model GetLatestByModelTypeId(int modelTypeId);
        IEnumerable<Model> GetAllByModelRequestId(int id);
        Model Add(Model model);
        Model Delete(Model model);
        Model Update(Model model);
        Model UpdateStatus(int id, ModelStatusEnums status, DateTime? timeStamp);
        IEnumerable<Model> GetUnprocessed();
    }
}