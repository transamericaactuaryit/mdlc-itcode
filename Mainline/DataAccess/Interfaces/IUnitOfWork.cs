﻿using System.Threading.Tasks;

namespace DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        IModelRequestRepository ModelRequestRepository { get; }
        IModelRequestStatusRepository ModelRequestStatusRepository { get; }
        IModelRepository ModelRepository { get; }
        IModelStatusRepository ModelStatusRepository { get; }
        IModelApplicationRepository ModelApplicationRepository { get; }
        IModelGroupRepository ModelGroupRepository { get; }
        IModelTypeRepository ModelTypeRepository { get; }
        IModelRunRepository ModelRunRepository { get; }
        IModelRunStatusRepository ModelRunStatusRepository { get; }

        void Dispose();
        void RejectChanges();
        void SaveChanges();
        Task<int> SaveChangesAsync();
    }
}