﻿using Domain;
using System.Collections.Generic;

namespace DataAccess.Interfaces
{
    public interface IModelStatusRepository
    {
        ModelStatus Add(ModelStatus modelStatus);
        ModelStatus Delete(ModelStatus modelStatus);
        ModelStatus Get(int id);
        IEnumerable<ModelStatus> Get();
    }
}