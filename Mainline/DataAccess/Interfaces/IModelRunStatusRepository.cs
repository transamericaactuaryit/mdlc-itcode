﻿using Domain;
using System.Collections.Generic;

namespace DataAccess.Interfaces
{
    public interface IModelRunStatusRepository
    {
        ModelRunStatus Add(ModelRunStatus modelRunStatus);
        ModelRunStatus Delete(ModelRunStatus modelRunStatus);
        ModelRunStatus Get(int id);
        IEnumerable<ModelRunStatus> Get();
    }
}