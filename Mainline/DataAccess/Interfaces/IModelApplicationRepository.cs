﻿using Domain;
using System.Collections.Generic;

namespace DataAccess.Interfaces
{
    public interface IModelApplicationRepository
    {
        ModelApplication Add(ModelApplication modelApplication);
        ModelApplication Delete(ModelApplication modelApplication);
        ModelApplication Get(int id);
        ModelApplication Get(string name);
        IEnumerable<ModelApplication> Get();
    }
}