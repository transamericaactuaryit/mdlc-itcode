﻿using System.Collections.Generic;
using Domain;

namespace DataAccess.Interfaces
{
    public interface IModelTypeRepository
    {
        ModelType Add(ModelType modelType);
        ModelType Delete(ModelType modelType);
        IEnumerable<ModelType> Get();
        ModelType Get(int id);
        ModelType Get(string name, string modelGroupName);
        IEnumerable<ModelType> GetByModelGroupId(int modelGroupId);
    }
}