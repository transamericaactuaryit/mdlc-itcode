﻿using Domain;
using System.Collections.Generic;

namespace DataAccess.Interfaces
{
    public interface IModelRunRepository
    {
        ModelRun Add(ModelRun modelRun);
        ModelRun Delete(ModelRun modelRun);
        ModelRun Get(int id);
        IEnumerable<ModelRun> Get();
    }
}