﻿using System.Collections.Generic;
using Domain;

namespace DataAccess.Interfaces
{
    public interface IModelRequestRepository
    {
        ModelRequest Add(ModelRequest modelRequest);
        ModelRequest Delete(ModelRequest modelRequest);
        ModelRequest Get(int id, bool loadAllChildEntities = true);
        IEnumerable<ModelRequest> Get();
        void UpdateStatusByStatus(ModelRequestStatusEnums statusToChange, ModelRequestStatusEnums newStatus);
    }
}