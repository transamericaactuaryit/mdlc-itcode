﻿using Domain;
using System.Collections.Generic;

namespace DataAccess.Interfaces
{
    public interface IModelRequestStatusRepository
    {
        ModelRequestStatus Add(ModelRequestStatus modelRequestStatus);
        ModelRequestStatus Delete(ModelRequestStatus modelRequestStatus);
        ModelRequestStatus Get(int id);
        IEnumerable<ModelRequestStatus> Get();
    }
}