﻿using System.Collections.Generic;
using Domain;

namespace DataAccess.Interfaces
{
    public interface IModelGroupRepository
    {
        ModelGroup Add(ModelGroup modelGroup);
        ModelGroup Delete(ModelGroup modelGroup);
        IEnumerable<ModelGroup> Get();
        ModelGroup Get(int id);
    }
}