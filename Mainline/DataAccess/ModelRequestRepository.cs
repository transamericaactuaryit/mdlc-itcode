﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DataAccess
{
    public class ModelRequestRepository : IModelRequestRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelRequestRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelRequest Get(int id, bool loadAllChildEntities = true)
        {
            try
            {
                //pass in false via unit test as unable to mock the load. SJO
                if (loadAllChildEntities)
                {
                    _context.Model.Load();
                    _context.ModelStatus.Load();
                    _context.ModelApplication.Load();
                    _context.ModelType.Load();
                }

                return _context.ModelRequest.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Request With ID {id}");
            }
        }

        public IEnumerable<ModelRequest> Get()
        {
            if (!_context.ModelRequest.Any())
            {
                throw new Exception("Could Not Find Any Model Requests");
            }

            return _context.ModelRequest;
        }

        public ModelRequest Add(ModelRequest modelRequest)
        {
            if (modelRequest == null || string.IsNullOrEmpty(modelRequest.Name))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelRequest.Add(modelRequest);
        }

        public void UpdateStatusByStatus(ModelRequestStatusEnums statusToChange, ModelRequestStatusEnums newStatus)
        {
            foreach(var modelRequest in _context.ModelRequest.Where(m => m.ModelRequestStatusID == (int)statusToChange))
            {
                modelRequest.ModelRequestStatusID = (int)newStatus;
            }
        }

        public ModelRequest Delete(ModelRequest modelRequest)
        {
            if (modelRequest == null || modelRequest.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelRequest.Remove(modelRequest);
        }
    }
}

