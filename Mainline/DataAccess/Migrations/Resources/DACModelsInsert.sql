USE [ModelManager]
GO

declare @dacConfigLocation varchar(max);
set @dacConfigLocation = '\\crasmand01\MGALFA\JobConfigurations\DAC\';

declare @dacSensitivityConfigLocation varchar(max);
set @dacSensitivityConfigLocation = '\\crasmand01\MGALFA\JobConfigurations\DAC\Sensitivities\';


INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('BOPParallel'
           ,2
           ,@dacConfigLocation + 'BOPParallel.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('PassageOfTimeAttribution'
           ,3
           ,@dacConfigLocation + 'PassageOfTimeAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('EqAttribution'
           ,3
           ,@dacConfigLocation + 'EqAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('EqVCFDynVolAttribution'
           ,3
           ,@dacConfigLocation + 'EqVCFDynVolAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('IntAttribution'
           ,3
           ,@dacConfigLocation + 'IntAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('IntActualReturnAttribution'
           ,3
           ,@dacConfigLocation + 'IntActualReturnAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('CrossAttribution'
           ,3
           ,@dacConfigLocation + 'CrossAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('BondMMAttribution'
           ,3
           ,@dacConfigLocation + 'BondMMAttribution.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Base'
           ,3
           ,@dacConfigLocation + 'Base.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('NB'
           ,4
           ,@dacConfigLocation + 'NB.xml'
           ,'NonSensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('NBUE'
           ,4
           ,@dacConfigLocation + 'NBUE.xml'
           ,'NonSensitivity'
           ,2)

/* Sensitivities Start */

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Greeks_EqUp_5'
           ,4
           ,@dacSensitivityConfigLocation + 'Greeks_EqUp_5.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Greeks_EqDn_5'
           ,4
           ,@dacSensitivityConfigLocation + 'Greeks_EqDn_5.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Greeks_Int_Up_10'
           ,4
           ,@dacSensitivityConfigLocation + 'Greeks_Int_Up_10.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Greeks_Int_Dn_10'
           ,4
           ,@dacSensitivityConfigLocation + 'Greeks_Int_Dn_10.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type3UW_Equity7pct'
           ,4
           ,@dacSensitivityConfigLocation + 'Type3UW_Equity7pct.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_1ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_1ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_2ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_2ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_3ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_3ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_4ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_4ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Finalye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Finalye.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_1qtrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_1ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_1ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_2ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_2ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_3ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_3ye.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_4ye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_4ye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Finalye'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Finalye.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqUp10_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqUp10_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqUp25_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqUp25_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqDn10_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqDn10_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqDn25_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqDn25_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqDn40_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqDn40_1qtrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp50_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntUp50_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp100_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntUp100_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp200_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntUp200_1qtrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn50_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntDn50_1qtrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn100_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntDn100_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn200_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntDn200_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqUp10_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqUp10_1yrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqUp25_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqUp25_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqDn10_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqDn10_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqDn25_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqDn25_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EqDn40_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_EqDn40_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp50_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntUp50_1yrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp100_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntUp100_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp200_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_IntUp200_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_IntDn50_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_IntDn50_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_IntDn100_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_IntDn100_1yrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_IntDn200_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_IntDn200_1yrout.xml'
           ,'Sensitivity'
           ,2)			   

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_LapseUp20_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_LapseUp20_1qtrout.xml'
           ,'Sensitivity'
           ,2)	

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_LapseDn20_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_LapseDn20_1qtrout.xml'
           ,'Sensitivity'
           ,2)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_MortUp10_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_MortUp10_1qtrout.xml'
           ,'Sensitivity'
           ,2)		   
		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_MortDn10_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_MortDn10_1qtrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_LapseUp20_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_LapseUp20_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_LapseDn20_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_LapseDn20_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_MortUp1pct0_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_MortUp1pct0_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Perm_MortDn10_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_Perm_MortDn10_1yrout.xml'
           ,'Sensitivity'
           ,2)			   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_LapseUp20_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_LapseUp20_1qtrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_LapseDn20_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_LapseDn20_1qtrout.xml'
           ,'Sensitivity'
           ,2)			   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_MortUp1pct0_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_MortUp1pct0_1qtrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_MortDn10_1qtrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_MortDn10_1qtrout.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_LapseUp20_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_LapseUp20_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_LapseDn20_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_LapseDn20_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_MortUp1pct0_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_MortUp1pct0_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NB_Perm_MortDn10_1yrout'
           ,5
           ,@dacSensitivityConfigLocation + 'FP_NB_Perm_MortDn10_1yrout.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type3UW_MortUp10'
           ,6
           ,@dacSensitivityConfigLocation + 'Type3UW_MortUp10.xml'
           ,'Sensitivity'
           ,2)		   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type3UW_MortDn10'
           ,6
           ,@dacSensitivityConfigLocation + 'Type3UW_MortDn10.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type3UW_Lapse_Up20'
           ,6
           ,@dacSensitivityConfigLocation + 'Type3UW_Lapse_Up20.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type3UW_Lapse_Dn20'
           ,6
           ,@dacSensitivityConfigLocation + 'Type3UW_Lapse_Dn20.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type3UW_ExpUp10'
           ,6
           ,@dacSensitivityConfigLocation + 'Type3UW_ExpUp10.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Mtp_1qtrout'
           ,7
           ,@dacSensitivityConfigLocation + 'Mtp_1qtrout.xml'
           ,'Sensitivity'
           ,2)		
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_1ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_1ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_1yrout'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_2ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_2ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_3ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_3ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_4ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_4ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_Finalye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_Finalye.xml'
           ,'Sensitivity'
           ,2)			   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_1qtrout'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_1qtrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_1ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_1ye.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_1yrout'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_1yrout.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_2ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_2ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_3ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_3ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_4ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_4ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_Finalye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_Finalye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_FinAdv_2ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_FinAdv_2ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_FinAdv_4ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_FinAdv_4ye.xml'
           ,'Sensitivity'
           ,2)			   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_FinAdv_2ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_FinAdv_2ye.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NB_FinAdv_4ye'
           ,7
           ,@dacSensitivityConfigLocation + 'MTP_NB_FinAdv_4ye.xml'
           ,'Sensitivity'
           ,2)			   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_Base'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_Base.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_MTP'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_MTP.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_MTP'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_MTP.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_MTP_FinAdv'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_MTP_FinAdv.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_MTP_FinAdv'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_MTP_FinAdv.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_IR_LiqUp150Grade300'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_IR_LiqUp150Grade300.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('NAR'
           ,5
           ,@dacSensitivityConfigLocation + 'NAR.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Liquidity'
           ,5
           ,@dacSensitivityConfigLocation + 'Liquidity.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Liquidity_NB'
           ,5
           ,@dacSensitivityConfigLocation + 'Liquidity_NB.xml'
           ,'Sensitivity'
           ,2)		
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqUp10'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqUp10.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqUp25'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqUp25.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn10'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn10.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn25'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn25.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn40'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn40.xml'
           ,'Sensitivity'
           ,2)			   
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn40'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn40.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn40_IntDn50'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn40_IntDn50.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn40_IntUp50'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn40_IntUp50.xml'
           ,'Sensitivity'
           ,2)		
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn40_IntDn100'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn40_IntDn100.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_EqDn40_IntUp100'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_EqDn40_IntUp100.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_IntUp50'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_IntUp50.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_IntUp100'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_IntUp100.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_IntUp200'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_IntUp200.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_IntDn50'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_IntDn50.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_IntDn100'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_IntDn100.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_Perm_LapseDn20'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_Perm_LapseDn20.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_Perm_LapseUp20'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_Perm_LapseUp20.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_Perm_MortUp10'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_Perm_MortUp10.xml'
           ,'Sensitivity'
           ,2)
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_Perm_MortDn10'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_Perm_MortDn10.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_MortUp100'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_MortUp100.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_Perm_LapseDn20'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_Perm_LapseDn20.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_Perm_LapseUp20'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_Perm_LapseUp20.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_Perm_MortUp10'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_Perm_MortUp10.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_Perm_MortDn10'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_Perm_MortDn10.xml'
           ,'Sensitivity'
           ,2)	
		   		   
INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Det_NB_MortUp100'
           ,6
           ,@dacSensitivityConfigLocation + 'Det_NB_MortUp100.xml'
           ,'Sensitivity'
           ,2)			   
		   
GO

