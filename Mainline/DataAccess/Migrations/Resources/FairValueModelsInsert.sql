USE [ModelManager]
GO

declare @fairValueConfigLocation varchar(max);
set @fairValueConfigLocation = '\\crasmand01\MGALFA\JobConfigurations\FairValue\';

declare @fairValueSensitivityConfigLocation varchar(max);
set @fairValueSensitivityConfigLocation = '\\crasmand01\MGALFA\JobConfigurations\FairValue\Sensitivities\';


INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('BOPParallel'
           ,2
           ,@fairValueConfigLocation + 'BOPParallel.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('PassageOfTimeAttribution'
           ,3
           ,@fairValueConfigLocation + 'PassageOfTimeAttribution.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('EquityAttribution'
           ,3
           ,@fairValueConfigLocation + 'EquityAttribution.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('RateAttribution'
           ,3
           ,@fairValueConfigLocation + 'RateAttribution.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('CrossAttribution'
           ,3
           ,@fairValueConfigLocation + 'CrossAttribution.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('BondMMAttribution'
           ,3
           ,@fairValueConfigLocation + 'BondMMAttribution.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('VolatilityAttribution'
           ,3
           ,@fairValueConfigLocation + 'VolatilityAttribution.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Base'
           ,3
           ,@fairValueConfigLocation + 'Base.xml'
           ,'NonSensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('NB6pt'
           ,3
           ,@fairValueConfigLocation + 'NB6pt.xml'
           ,'NonSensitivity'
           ,1)

/* Sensitivities Start */

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Equity_Up_5'
           ,4
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Equity_Up_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Equity_Down_5'
           ,4
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Equity_Down_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Int_Up_10'
           ,4
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Int_Up_10.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Int_Down_10'
           ,4
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Int_Down_10.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_1YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_1YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_2YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_2YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_3YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_3YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_4YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_4YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_5YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_5YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_6YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_6YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_1YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_1YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_2YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_2YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_3YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_3YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_4YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_4YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_5YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_5YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_NewBusiness_6YE'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_NewBusiness_6YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn10_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn20_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn20_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn40_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn40_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQUp10_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQUp10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQUp20_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQUp20_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn10_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn10_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn20_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn20_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn40_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn40_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQUp10_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQUp10_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQUp20_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQUp20_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn25_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn25_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQUp25_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQUp25_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQDn25_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQDn25_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_EQSens_EQUp25_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_EQSens_EQUp25_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn200_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntDn200_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn100_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntDn100_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn50_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntDn50_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp50_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntUp50_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp100_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntUp100_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp200_1QTROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntUp200_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn200_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntDn200_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn100_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntDn100_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntDn50_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntDn50_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp50_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntUp50_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp100_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntUp100_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_IntUp200_1YROUT'
           ,5
           ,@fairValueSensitivityConfigLocation + 'FP_IntUp200_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_2_Sens_LTVolUp5'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_2_Sens_LTVolUp5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_2_Sens_LTVolDn5'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_2_Sens_LTVolDn5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_III_UW_LapseUp20'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_III_UW_LapseUp20.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_III_UW_LapseDn20'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_III_UW_LapseDn20.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_III_UW_MortUp10'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_III_UW_MortUp10.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_III_UW_MortDn10'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_III_UW_MortDn10.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_III_UW_MortUp5'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_III_UW_MortUp5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('Type_III_UW_MortDn5'
           ,6
           ,@fairValueSensitivityConfigLocation + 'Type_III_UW_MortDn5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_LapseUp20_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_LapseUp20_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_LapseDn20_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_LapseDn20_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortUp10_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortUp10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortDn10_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortDn10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortUp5_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortUp5_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortDn5_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_LapseUp20_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_LapseUp20_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_LapseDn20_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_LapseDn20_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortUp10_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortUp10_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortDn10_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortDn10_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortUp5_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortUp5_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_MortDn5_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_MortDn5_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_LapseUp20_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_LapseUp20_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_LapseDn20_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_LapseDn20_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_MortUp10_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_MortUp10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_MortDn10_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_MortDn10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_LapseUp20_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_LapseUp20_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_LapseDn20_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_LapseDn20_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_MortDn10_1QTROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_MortDn10_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_MortDn10_1YROUT'
           ,6
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_MortDn10_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Vol_Up_5'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Vol_Up_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Vol_Down_5'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Vol_Down_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_HY_Up_5'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_HY_Up_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_HY_Down_5'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_HY_Down_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Agg_Up_5'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Agg_Up_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FV_Greeks_Agg_Down_5'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FV_Greeks_Agg_Down_5.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_Mort1pct_1QTROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_Mort1pct_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_Mort100pct1YR_1QTROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_Mort100pct1YR_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_Mort1pct_1YROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_Mort1pct_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_Mort100pct1YR_1YROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_Mort100pct1YR_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_Mort1pct_1QTROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_Mort1pct_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_Mort100pct1YR_1QTROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_Mort100pct1YR_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_Mort1pct_1YROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_Mort1pct_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('FP_Type_I_UW_Sensitivities_NewBusiness_Mort100pct1YR_1YROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'FP_Type_I_UW_Sensitivities_NewBusiness_Mort100pct1YR_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_1QTROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_1YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_1YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_1YROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_2YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_2YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_3YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_3YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_4YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_4YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_5YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_5YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_6YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_6YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_1QTROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_1QTROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_1YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_1YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_1YROUT'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_1YROUT.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_2YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_2YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_3YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_3YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_4YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_4YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_5YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_5YE.xml'
           ,'Sensitivity'
           ,1)

INSERT INTO [dbo].[ModelTypes]
           ([Name]
           ,[Score]
           ,[ConfigurationFile]
           ,[Tags]
           ,[ModelGroupID])
     VALUES
           ('MTP_NewBusiness_6YE'
           ,7
           ,@fairValueSensitivityConfigLocation + 'MTP_NewBusiness_6YE.xml'
           ,'Sensitivity'
           ,1)

GO
