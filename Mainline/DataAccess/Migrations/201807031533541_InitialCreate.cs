namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Models",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        StartTime = c.DateTime(),
                        CompletedTime = c.DateTime(),
                        Priority = c.Int(nullable: false),
                        OnDemandConfigurationFile = c.String(),
                        PostOnlyRun = c.Boolean(),
                        ModelRequestID = c.Int(nullable: false),
                        ModelApplicationID = c.Int(nullable: false),
                        ModelStatusID = c.Int(nullable: false),
                        ModelTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ModelApplications", t => t.ModelApplicationID, cascadeDelete: true)
                .ForeignKey("dbo.ModelRequests", t => t.ModelRequestID, cascadeDelete: true)
                .ForeignKey("dbo.ModelStatus", t => t.ModelStatusID, cascadeDelete: true)
                .ForeignKey("dbo.ModelTypes", t => t.ModelTypeID, cascadeDelete: true)
                .Index(t => t.ModelRequestID)
                .Index(t => t.ModelApplicationID)
                .Index(t => t.ModelStatusID)
                .Index(t => t.ModelTypeID);
            
            CreateTable(
                "dbo.ModelApplications",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ModelRequests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        StartTime = c.DateTime(),
                        CompletedTime = c.DateTime(),
                        Tags = c.String(),
                        ModelRequestStatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ModelRequestStatus", t => t.ModelRequestStatusID, cascadeDelete: true)
                .Index(t => t.ModelRequestStatusID);


            
            CreateTable(
                "dbo.ModelRequestStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Status = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ModelRuns",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Run = c.String(nullable: false),
                        ModelID = c.Int(nullable: false),
                        ModelRunStatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Models", t => t.ModelID, cascadeDelete: true)
                .ForeignKey("dbo.ModelRunStatus", t => t.ModelRunStatusID, cascadeDelete: true)
                .Index(t => t.ModelID)
                .Index(t => t.ModelRunStatusID);
            
            CreateTable(
                "dbo.ModelRunStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Status = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ModelStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Status = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ModelTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Score = c.Int(nullable: false),
                        ConfigurationFile = c.String(nullable: false),
                        Tags = c.String(),
                        ModelGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ModelGroups", t => t.ModelGroupID, cascadeDelete: true)
                .Index(t => t.ModelGroupID);
            
            CreateTable(
                "dbo.ModelGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Models", "ModelTypeID", "dbo.ModelTypes");
            DropForeignKey("dbo.ModelTypes", "ModelGroupID", "dbo.ModelGroups");
            DropForeignKey("dbo.Models", "ModelStatusID", "dbo.ModelStatus");
            DropForeignKey("dbo.ModelRuns", "ModelRunStatusID", "dbo.ModelRunStatus");
            DropForeignKey("dbo.ModelRuns", "ModelID", "dbo.Models");
            DropForeignKey("dbo.Models", "ModelRequestID", "dbo.ModelRequests");
            DropForeignKey("dbo.ModelRequests", "ModelRequestStatusID", "dbo.ModelRequestStatus");
            DropForeignKey("dbo.Models", "ModelApplicationID", "dbo.ModelApplications");
            DropIndex("dbo.ModelTypes", new[] { "ModelGroupID" });
            DropIndex("dbo.ModelRuns", new[] { "ModelRunStatusID" });
            DropIndex("dbo.ModelRuns", new[] { "ModelID" });
            DropIndex("dbo.ModelRequests", new[] { "ModelRequestStatusID" });
            DropIndex("dbo.Models", new[] { "ModelTypeID" });
            DropIndex("dbo.Models", new[] { "ModelStatusID" });
            DropIndex("dbo.Models", new[] { "ModelApplicationID" });
            DropIndex("dbo.Models", new[] { "ModelRequestID" });
            DropTable("dbo.ModelGroups");
            DropTable("dbo.ModelTypes");
            DropTable("dbo.ModelStatus");
            DropTable("dbo.ModelRunStatus");
            DropTable("dbo.ModelRuns");
            DropTable("dbo.ModelRequestStatus");
            DropTable("dbo.ModelRequests");
            DropTable("dbo.ModelApplications");
            DropTable("dbo.Models");
        }
    }
}
