namespace DataAccess.Migrations
{
    using Domain;
    using Extensions;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class Configuration : DbMigrationsConfiguration<DataAccess.ModelManagerDbContext>
    {
        public Configuration()
        {
            ContextKey = "DataAccess.ModelManagerDbContext";
        }

        protected override void Seed(DataAccess.ModelManagerDbContext context)
        {
            //Seed look up tables. 

            //Check if exists instead of using AddOrUpdate as Model Groups may be updated directly in DB or via APP if DB already exists.
            if (!context.ModelGroup.Any(m => m.ID == (int)ModelGroupEnums.FairValue))
            {
                context.ModelGroup.Add(new ModelGroup
                {
                    ID = (int)ModelGroupEnums.FairValue,
                    Name = Enums.GetEnumDescription(ModelGroupEnums.FairValue),
                    Email = "simon.ovens@transamerica.com"
                });
            }
            if (!context.ModelGroup.Any(m => m.ID == (int)ModelGroupEnums.DAC))
            {
                context.ModelGroup.Add(new ModelGroup
                {
                    ID = (int)ModelGroupEnums.DAC,
                    Name = Enums.GetEnumDescription(ModelGroupEnums.DAC),
                    Email = "simon.ovens@transamerica.com"
                });
            }

            IList<ModelApplication> modelApplications = new List<ModelApplication>
            {
                new ModelApplication { ID = (int)ModelApplicationEnums.ALFA, Name = Enums.GetEnumDescription(ModelApplicationEnums.ALFA) },
                new ModelApplication { ID = (int)ModelApplicationEnums.AXIS, Name = Enums.GetEnumDescription(ModelApplicationEnums.AXIS) }
            };
            context.ModelApplication.AddOrUpdate(modelApplications.ToArray());

            IList<ModelRequestStatus> modelRequest = new List<ModelRequestStatus>
            {
                new ModelRequestStatus { ID = (int)ModelRequestStatusEnums.NotStarted, Status = Enums.GetEnumDescription(ModelRequestStatusEnums.NotStarted) },
                new ModelRequestStatus { ID = (int)ModelRequestStatusEnums.InQueue, Status = Enums.GetEnumDescription(ModelRequestStatusEnums.InQueue) },
                new ModelRequestStatus { ID = (int)ModelRequestStatusEnums.Executing, Status = Enums.GetEnumDescription(ModelRequestStatusEnums.Executing) },
                new ModelRequestStatus { ID = (int)ModelRequestStatusEnums.Complete, Status = Enums.GetEnumDescription(ModelRequestStatusEnums.Complete) },
                new ModelRequestStatus { ID = (int)ModelRequestStatusEnums.Error, Status = Enums.GetEnumDescription(ModelRequestStatusEnums.Error) },
                new ModelRequestStatus { ID = (int)ModelRequestStatusEnums.OnHold, Status = Enums.GetEnumDescription(ModelRequestStatusEnums.OnHold) }
            };
            context.ModelRequestStatus.AddOrUpdate(modelRequest.ToArray());

            IList<ModelStatus> modelStatus = new List<ModelStatus>
            {
                new ModelStatus { ID = (int)ModelStatusEnums.NotStarted, Status = Enums.GetEnumDescription(ModelStatusEnums.NotStarted) },
                new ModelStatus { ID = (int)ModelStatusEnums.InQueue, Status = Enums.GetEnumDescription(ModelStatusEnums.InQueue) },
                new ModelStatus { ID = (int)ModelStatusEnums.Executing, Status = Enums.GetEnumDescription(ModelStatusEnums.Executing) },
                new ModelStatus { ID = (int)ModelStatusEnums.Complete, Status = Enums.GetEnumDescription(ModelStatusEnums.Complete) },
                new ModelStatus { ID = (int)ModelStatusEnums.Error, Status = Enums.GetEnumDescription(ModelStatusEnums.Error) },
                new ModelStatus { ID = (int)ModelStatusEnums.WaitingOnDependency, Status = Enums.GetEnumDescription(ModelStatusEnums.WaitingOnDependency) },
                new ModelStatus { ID = (int)ModelStatusEnums.OnHold, Status = Enums.GetEnumDescription(ModelStatusEnums.OnHold) },

            };
            context.ModelStatus.AddOrUpdate(modelStatus.ToArray());

            IList<ModelRunStatus> modelRunStatus = new List<ModelRunStatus>
            {
                new ModelRunStatus { ID = (int)ModelRunStatusEnums.NotStarted, Status = Enums.GetEnumDescription(ModelRunStatusEnums.NotStarted) },
                new ModelRunStatus { ID = (int)ModelRunStatusEnums.Executing, Status = Enums.GetEnumDescription(ModelRunStatusEnums.Executing) },
                new ModelRunStatus { ID = (int)ModelRunStatusEnums.Error, Status = Enums.GetEnumDescription(ModelRunStatusEnums.Error) },
                new ModelRunStatus { ID = (int)ModelRunStatusEnums.Complete, Status = Enums.GetEnumDescription(ModelRunStatusEnums.Complete) },
            };
            context.ModelRunStatus.AddOrUpdate(modelRunStatus.ToArray());

            base.Seed(context);
        }
    }
}
