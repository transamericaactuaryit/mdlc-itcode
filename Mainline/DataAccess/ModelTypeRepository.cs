﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelTypeRepository : IModelTypeRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelTypeRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelType Get(int id)
        {
            try
            {
                return _context.ModelType.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Type With ID {id}");
            }
        }

        public ModelType Get(string name, string modelGroupName)
        {
            try
            {
                var modelGroup = _context.ModelGroup.Single(m => m.Name == modelGroupName);
                return _context.ModelType.Single(m => m.Name == name && m.ModelGroupID == modelGroup.ID);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Type With Name {name}");
            }
        }

        public IEnumerable<ModelType> Get()
        {
            return _context.ModelType;
        }

        public IEnumerable<ModelType> GetByModelGroupId(int modelGroupId)
        {
            return _context.ModelType.Where(m => m.ModelGroupID == modelGroupId);
        }

        public ModelType Add(ModelType modelType)
        {
            if (modelType == null || string.IsNullOrEmpty(modelType.Name))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelType.Add(modelType);
        }

        public ModelType Delete(ModelType modelType)
        {
            if (modelType == null || modelType.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelType.Remove(modelType);
        }
    }
}
