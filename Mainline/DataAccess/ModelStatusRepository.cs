﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class ModelStatusRepository : IModelStatusRepository
    {
        private readonly ModelManagerDbContext _context;

        public ModelStatusRepository(ModelManagerDbContext context)
        {
            _context = context;
        }

        public ModelStatus Get(int id)
        {
            try
            {
                return _context.ModelStatus.Single(m => m.ID == id);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException($"Could Not Find Model Status With ID {id}");
            }
        }

        public IEnumerable<ModelStatus> Get()
        {
            return _context.ModelStatus;
        }

        public ModelStatus Add(ModelStatus modelStatus)
        {
            if (modelStatus == null || string.IsNullOrEmpty(modelStatus.Status))
            {
                throw new ArgumentException("Unable to Add Invalid Entity.");
            }

            return _context.ModelStatus.Add(modelStatus);
        }

        public ModelStatus Delete(ModelStatus modelStatus)
        {
            if (modelStatus == null || modelStatus.ID <= 0)
            {
                throw new ArgumentException("Unable to Delete Invalid Entity.");
            }

            return _context.ModelStatus.Remove(modelStatus);
        }
    }
}
