﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace EnablingSystems.ProcessInterfaces
{
    public interface IProcessLauncher
    {
        void LoadParams(IProcess process);
        void ProcessFinished(object sender, System.EventArgs e);
        void RunProcess();
        IProcess Process { set; }
        IDashboard Dashboard { set; }
        string Environment { set; }

    }
    public interface IProcess
    {
        string ProcessName { get; set; }
        NameValueCollection ProcessParams { set; get; }
        void Execute();
        void SaveHistory(int historyID);
        event System.EventHandler Finish;
        IJobHistory JobHistory{ set; get; }
    }

    public interface IDashboard
    {
        string ConnectionInformation { get; set; }
        int JobInstanceID { get; set; }
        string DashboardID { get; set; }
        string ProcessName { get; set; }
        string ProcessMessage { get; set; }

        System.DateTime StartTime { get; set; }
        System.DateTime EndTime { get; set; }
        bool Success { get; set; }
        int JobHistoryID { get; }
        void Save();
    }

    public interface IJobHistory
    {
        string ConnectionInformation { set; }
        void SaveHistory(int jobHistoryID, NameValueCollection processHistoryItems);
    }

    public interface IJobHistorySerialzie
    {
        int JobHistoryID { get; set; }

        NameValueCollection ProcessHistoryItems { get; set; }
    }

    public interface IAutomationAggregator
    {
        string MetaConfigFile { set; }

        string Environment { set; }

        void RunAutomations();

        void SetupAutomations();
    }

    public interface IModelExecutionQueue
    {

        string ExecutionMachine { get; set; }

        int MaxConcurrentModels { get; set; }

        int SlotsUsed { get; }

        bool SlotAvailable();

        int TakeSlot();

        int ReleaseSlot();
    }

    public interface IModelState
    {

        string ModelIdentifier { get; set; }

        string MetaConfigFile { get; set; }

        int ModelPriority { get; set; }

        string ModelStatus { get; }

        void SetModelStatus(string modelStatus);

        string ModelType { get; set; }

    }

    public interface IModelAggregator
    {
        List<IModelState> ModelStates { get; set; }

        string Envrionment { get; set; }

        string ErrorEmailAddress { get; set; }

        IModelExecutionQueue Queue { get; set; }
    }

    public interface IModelAggregatorQuery
    {
        List<IModelAggregator> ModelsToProcess { get; }

        void FindUnProcessedModels();

        string ConnectionInformation { set; }
    }


}
