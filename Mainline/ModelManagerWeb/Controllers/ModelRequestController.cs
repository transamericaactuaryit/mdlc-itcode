﻿using Domain;
using Service.Interfaces;
using System.Collections.Generic;
using System.Web.Http;

namespace ModelManagerWeb.Controllers
{
    public class ModelRequestController : ApiController
    {
        private readonly IModelRequestService _modelRequestService;

        public ModelRequestController(IModelRequestService modelRequestService)
        {
            _modelRequestService = modelRequestService;
        }

        [Route("api/ModelRequest/{id}")]
        public ModelRequest Get(int id)
        {
            return _modelRequestService.Get(id);
        }

        [Route("api/ModelRequests")]
        public IEnumerable<ModelRequest> Get()
        {
            return _modelRequestService.Get();
        }

        [HttpPost]
        public ModelRequest Add(ModelRequest ModelRequest)
        {
            return _modelRequestService.Add(ModelRequest);
        }

        [Route("api/modelrequests/UpdateStatusByStatus/{statusToChange}")]
        [HttpPost]
        public void UpdateStatusByStatus(ModelRequestStatusEnums statusToChange, [FromBody] ModelRequestStatusEnums newStatus)
        {
            _modelRequestService.UpdateStatusByStatus(statusToChange, newStatus);
        }

    }
}


