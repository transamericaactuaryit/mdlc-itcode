﻿using Domain;
using Domain.ServiceModels;
using Service;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace ModelManagerWeb.Controllers
{
    public class ModelController : ApiController
    {
        private readonly IModelService _modelService;

        public ModelController(IModelService modelService)
        {
            _modelService = modelService;
        }

        [Route("api/model/initialmodellookups/modelapplication/{modelApplicationName}/modeltype/{modeltypeName}/modelgroup/{modelgroupname}")]
        public InitialModelLookups GetInitialModelLookups(string modelApplicationName, string modelTypeName, string modelGroupName)
        {
            modelGroupName = Uri.UnescapeDataString(modelGroupName);
            var initialModelLookups = new InitialModelLookups();
            initialModelLookups.ModelApplication = _modelService.GetModelApplication(modelApplicationName);
            initialModelLookups.ModelStatus = _modelService.GetStatus((int)ModelStatusEnums.NotStarted);
            initialModelLookups.ModelType = _modelService.GetModelType(modelTypeName, modelGroupName);

            return initialModelLookups;
        }

        [HttpPost]
        [Route("api/model/Update")]
        public Model UpdateModel(Model model)
        {
            return _modelService.UpdateModel(model);
        }

        [HttpPost]
        [Route("api/model/{id}/UpdateStatus/{newStatus}")]
        public Model UpdateModel(int id, ModelStatusEnums newStatus, [FromBody] DateTime? timeStamp)
        {
            return _modelService.UpdateModelStatus(id, newStatus, timeStamp);
        }

        [Route("api/model/{id}")]
        public Model Get(int id)
        {
            return _modelService.Get(id);
        }

        [Route("api/model/modelrequest/{id}")]
        public IEnumerable<Model> GetAllByModelRequestId(int id)
        {
            return _modelService.GetAllByModelRequestId(id);
        }

        [Route("api/model/unprocessed")]
        public IEnumerable<Model> GetUnprocessed()
        {
            return _modelService.GetUnprocessed();
        }

        [Route("api/model/modeltype/{modelTypeId}")]
        public Model GetLatestByModelTypeId(int modelTypeId)
        {
            return _modelService.GetLatestByModelTypeId(modelTypeId);
        }

        [Route("api/model/ondemand/create")]
        [HttpPost]
        public string CreateDynamicOnDemandConfiguration(OnDemandRun onDemandRun)
        {
            return _modelService.CreateDynamicOnDemandConfiguration(onDemandRun);
        }
    }
}
