﻿using Domain;
using Service.Interfaces;
using System.Collections.Generic;
using System.Web.Http;

namespace ModelManagerWeb.Controllers
{
    public class ModelGroupController : ApiController
    {
        private readonly IModelGroupService _modelGroupService;

        public ModelGroupController(IModelGroupService modelGroupService)
        {
            _modelGroupService = modelGroupService;
        }

        [Route("api/modelgroups")]
        public IEnumerable<ModelGroup> Get()
        {
            return _modelGroupService.Get();
        }

        [Route("api/modelgroup/{id}/modeltypes")]
        public IEnumerable<ModelType> GetModelTypesByModelGroupId(int id)
        {
            return _modelGroupService.GetModelTypesByModelGroupId(id);
        }
    }
}
